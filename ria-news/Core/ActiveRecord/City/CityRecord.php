<?php
declare(strict_types=1);

namespace Ria\News\Core\ActiveRecord\City;

use Yii;
use yii\db\ActiveQuery;
use Ria\Core\Queries\NotPersistentActiveRecord;

/**
 * Class StoryRecord
 * @package Ria\News\Core\ActiveRecord\City
 * @property int $id
 * @property int $region_id
 * @property bool $name
 *
 * @property TranslationRecord $translation
 */
class CityRecord extends NotPersistentActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

    /**
     * @param null $code
     * @return ActiveQuery
     */
    public function getTranslation($code = null)
    {
        return $this->hasOne(TranslationRecord::class, ['city_id' => 'id'])->andOnCondition(['language' => $code ?: Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TranslationRecord::class, ['city_id' => 'id']);
    }
}