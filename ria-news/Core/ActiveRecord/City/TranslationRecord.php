<?php
declare(strict_types=1);

namespace Ria\News\Core\ActiveRecord\City;

use Ria\Core\Queries\NotPersistentActiveRecord;

/**
 * Class Translation
 * @package Ria\News\Core\ActiveRecord\City
 * @property int $id
 * @property int $city_id
 * @property string $language
 * @property string $title
 */
class TranslationRecord extends NotPersistentActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%cities_lang}}';
    }
}