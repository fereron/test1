<?php
declare(strict_types=1);

namespace Ria\News\Core\ActiveRecord\Region;

use Ria\Core\Queries\NotPersistentActiveRecord;

/**
 * Class Translation
 * @package Ria\Persons\Core\ActiveRecord
 * @property int $id
 * @property string $language
 * @property string $title
 */
class TranslationRecord extends NotPersistentActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%regions_lang}}';
    }
}