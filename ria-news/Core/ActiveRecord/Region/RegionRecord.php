<?php
declare(strict_types=1);

namespace Ria\News\Core\ActiveRecord\Region;

use Yii;
use yii\db\ActiveQuery;
use Ria\Core\Queries\NotPersistentActiveRecord;

/**
 * Class StoryRecord
 * @package Ria\News\Core\ActiveRecord\Region
 * @property int $id
 * @property int $order
 *
 * @property TranslationRecord $translation
 */
class RegionRecord extends NotPersistentActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%regions}}';
    }

    /**
     * @param null $code
     * @return ActiveQuery
     */
    public function getTranslation($code = null)
    {
        return $this->hasOne(TranslationRecord::class, ['region_id' => 'id'])->andOnCondition(['language' => $code ?: Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TranslationRecord::class, ['region_id' => 'id']);
    }
}