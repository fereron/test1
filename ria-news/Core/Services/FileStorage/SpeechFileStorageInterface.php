<?php
declare(strict_types=1);

namespace Ria\News\Core\Services\FileStorage;

use yii\web\UploadedFile;

/**
 * Interface SpeechFileStorageInterface
 * @package Ria\Photos\Services\FileStorage
 */
interface SpeechFileStorageInterface
{
    /**
     * @param UploadedFile $file
     * @param string $filename
     */
    public function upload(UploadedFile $file, string $filename);

    /**
     * @param string $filename
     */
    public function delete(string $filename);
}
