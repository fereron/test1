<?php

namespace Ria\News\Core\Services\FileStorage;

use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Yii;
use yii\base\Component;
use yii\web\UploadedFile;

/**
 * Class SpeechFileStorage
 * @package Ria\News\Core\Services\FileStorage
 */
class SpeechFileStorage extends Component implements SpeechFileStorageInterface
{
    /**
     * @var FilesystemInterface
     */
    public $adapter;

    /**
     * @param UploadedFile $file
     * @param string $filename
     */
    public function upload(UploadedFile $file, string $filename)
    {
        $file->saveAs($this->getFile($filename, true));
    }

    /**
     * @param string $filename
     * @throws FileNotFoundException
     */
    public function delete(string $filename)
    {
        $file = Yii::getAlias('@storage/speech/' . $filename);
        if (is_file($file)) {
            unlink($file);
        }
//        $this->adapter->delete($filename);
    }

    /**
     * @param string $filename
     * @param bool $absolute
     * @return string
     */
    public function getFile(string $filename, bool $absolute = false): string
    {
        return $absolute
            ? $this->adapter->getAdapter()->getPathPrefix() . $filename
            : Yii::getAlias('@static/speech/' . $filename);
    }
}
