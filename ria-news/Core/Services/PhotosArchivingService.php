<?php
declare(strict_types=1);

namespace Ria\News\Core\Services;

use Ria\News\Core\Models\Post\Post;
use Ria\Photos\Core\Models\Photo\Photo;
use Yii;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;

/**
 * Class PhotosArchivingService
 * @package Ria\News\Core\Services
 */
class PhotosArchivingService
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * PhotosArchivingService constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @throws PhotoArchiveException
     * @throws \ZipStream\Exception\FileNotFoundException
     * @throws \ZipStream\Exception\FileNotReadableException
     * @throws \ZipStream\Exception\OverflowException
     */
    public function createArchive()
    {
        $photos = $this->post->getPhotos();
        if ($this->post->getPhotos()->isEmpty()) {
            throw new PhotoArchiveException('No photo');
        }

        // enable output of HTTP headers
        $options = new Archive();
        $options->setSendHttpHeaders(true);

        // create a new zipstream object
        $zip = new ZipStream($this->post->getSlug() . '.zip', $options);

        /** @var Photo $photo */
        foreach ($photos as $photo) {
            // add a file named 'some_image.jpg' from a local file 'path/to/image.jpg'
            $zip->addFileFromPath($photo->getFilename(), Yii::$app->photo->getFile($photo->getFilename(), true));
        }

        // finish the zip stream
        $zip->finish();
    }
}