<?php

namespace Ria\News\Core\Commands\Story;

use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class DeleteStoryCommand
 * @package Ria\News\Core\Commands\Story
 * @property int $id
 */
class DeleteStoryCommand implements CommandInterface
{
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
}