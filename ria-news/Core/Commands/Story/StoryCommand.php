<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Story;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Forms\Story\StoryForm;

/**
 * Class StoryCommand
 * @package Ria\News\Core\Commands\Story
 *
 * @property int $id
 * @property bool $status
 * @property bool $showOnSite
 * @property string|null $cover
 * @property array $translations
 */
class StoryCommand implements CommandInterface
{

    /**
     * StoryCommand constructor.
     * @param StoryForm $form
     */
    public function __construct(StoryForm $form)
    {
        $this->id           = $form->id;
        $this->status       = (bool)$form->status;
        $this->showOnSite   = (bool)$form->showOnSite;
        $this->cover        = $form->cover;
        $this->translations = array_map(function ($translation) {
            return [
                'language'    => $translation->language,
                'title'       => $translation->title,
                'slug'        => $translation->slug,
                'description' => $translation->description,
                'meta'        => $translation->meta->toArray(),
            ];
        }, $form->translations);
    }
}