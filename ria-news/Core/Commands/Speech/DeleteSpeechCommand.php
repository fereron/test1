<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Speech;

use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class DeleteSpeechCommand
 * @package Ria\News\Core\Commands\Speech
 *
 * @property int $id
 */
class DeleteSpeechCommand implements CommandInterface
{

    /**
     * DeleteSpeechCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}