<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Speech;

use Faker\Provider\Uuid;
use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Forms\Speech\SpeechForm;
use yii\web\UploadedFile;

/**
 * Class SpeechCommand
 * @package Ria\News\Core\Commands\Speech
 *
 * @property int|null $id
 * @property int $postId
 * @property string $filename
 * @property string $originalFilename
 * @property UploadedFile $file
 */
class SpeechCommand implements CommandInterface
{

    public function __construct(SpeechForm $form)
    {
        $this->id               = $form->getId();
        $this->postId           = $form->postId;
        $this->filename         = $form->filename;
        $this->originalFilename = $form->file->getBaseName();
        $this->file             = $form->file;
    }

}