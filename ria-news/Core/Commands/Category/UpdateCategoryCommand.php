<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Category;

/**
 * Class UpdateCategoryCommand
 * @package Ria\News\Core\Commands\Category
 */
class UpdateCategoryCommand extends CategoryCommand
{
}