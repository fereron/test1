<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Category;

/**
 * Class CreateCategoryCommand
 * @package Ria\News\Core\Commands\Category
 */
class CreateCategoryCommand extends CategoryCommand
{
}