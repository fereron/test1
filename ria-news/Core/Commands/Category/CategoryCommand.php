<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Category;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Forms\Category\CategoryForm;
use Ria\News\Core\Models\Category\Template;

/**
 * Class CategoryCommand
 * @package Ria\News\Core\Commands\Category
 */
class CategoryCommand implements CommandInterface
{
    /**
     * CategoryCommand constructor.
     * @param CategoryForm $form
     */
    public function __construct(CategoryForm $form)
    {
        $this->id       = $form->id;
        $this->parent_id = !empty($form->parent_id) ? $form->parent_id : null;
        $this->template = new Template($form->template);
        $this->status   = (int)$form->status;
        $this->translations = array_map(function ($translation) {
            return [
                'language' => $translation->language,
                'title'    => $translation->title,
                'slug'     => $translation->slug,
                'meta'     => $translation->meta->toArray(),
            ];
        }, $form->translations);
    }

}