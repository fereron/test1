<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Category;

use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class MoveDownCategoryCommand
 * @package Ria\News\Core\Commands\Category
 *
 * @property int $id
 */
class MoveDownCategoryCommand implements CommandInterface
{

    /**
     * MoveDownCategoryCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}