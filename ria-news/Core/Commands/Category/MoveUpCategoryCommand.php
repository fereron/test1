<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Category;
use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class MoveUpCategoryCommand
 * @package Ria\News\Core\Commands\Category
 *
 * @property int $id
 */
class MoveUpCategoryCommand implements CommandInterface
{

    /**
     * MoveUpCategoryCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}