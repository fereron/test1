<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Widget;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Models\Widget\Type;

/**
 * Class CreateWidgetCommand
 * @package Ria\News\Core\Commands\Widget
 *
 * @property int $id
 * @property string $content
 * @property Type $type
 */
class CreateWidgetCommand implements CommandInterface
{

    /**
     * CreateWidgetCommand constructor.
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = utf8_encode($content);
        $this->type    = Type::createFromContent($content);
    }

}