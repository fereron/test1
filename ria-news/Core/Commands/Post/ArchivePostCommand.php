<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Post;

/**
 * Class ArchivePostCommand
 * @package Ria\News\Core\Commands\Post
 * @property int $id
 * @property string|null $url
 */
class ArchivePostCommand
{
    /**
     * ArchivePostCommand constructor.
     * @param int $id
     * @param string $url
     */
    public function __construct(int $id, ?string $url)
    {
        $this->id  = $id;
        $this->url = $url;
    }
}