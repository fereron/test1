<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Post;

use DateTime;
use Exception;
use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\Core\Models\Meta;
use Ria\News\Core\Forms\Post\PostForm;
use Ria\News\Core\Models\Post\Content;
use Ria\News\Core\Models\Post\Icon;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;

/**
 * Class PostCommand
 * @package Ria\News\Core\Commands\Post
 *
 * @property Status $status
 * @property int $id
 * @property int $groupId
 * @property int $cityId
 * @property Icon $icon
 * @property Type $type
 * @property string $optionType
 * @property int $categoryId
 * @property int $authorId
 * @property int $expertId
 * @property int $translatorId
 * @property int $createdBy
 * @property int $storyId
 * @property array $tags
 * @property array $persons
 * @property array $marked_words
 * @property array $related
 * @property array $exports
 * @property array $photos
 * @property string $youtubeId
 * @property string $title
 * @property string $description
 * @property Content $content
 * @property string $source
 * @property string $note
 * @property string $language
 * @property DateTime $publishedAt
 * @property string $slug
 * @property string $image
 * @property bool $isMain
 * @property bool $isExclusive
 * @property bool $isActual
 * @property bool $isBreaking
 * @property bool $isImportant
 * @property bool $linksNoindex
 * @property Meta $meta
 */
class PostCommand implements CommandInterface
{
    /**
     * PostCommand constructor.
     * @param PostForm $form
     * @throws Exception
     */
    public function __construct(PostForm $form)
    {
        $this->id           = $form->id;
        $this->groupId      = $form->group_id;
        $this->icon         = new Icon($form->icon);
        $this->type         = new Type($form->type);
        $this->optionType   = !empty($form->optionType) ? $form->optionType : null;
        $this->categoryId   = (int)$form->categoryId;
        $this->authorId     = (int)$form->authorId;
        $this->expertId     = (int)$form->expertId;
        $this->translatorId = !empty($form->translatorId) ? (int)$form->translatorId : null;
        $this->cityId       = (int)$form->cityId;
        $this->createdBy    = $form->createdBy;
        $this->storyId      = !empty($form->storyId) ? (int)$form->storyId : null;
        $this->tags         = $form->tags ?: [];
        $this->persons      = $form->persons ?: [];
        $this->marked_words = $form->marked_words ?: [];
        $this->related      = $form->related ?: [];
        $this->exports      = $form->exports ?: [];
        $this->photos       = $form->photos ?: [];
        $this->youtubeId    = $form->youtubeId;
        $this->title        = $form->title;
        $this->description  = $form->description;
        $this->content      = new Content($form->content);
        $this->source       = $form->source;
        $this->note         = $form->note;
        $this->language     = $form->language;
        $this->status       = new Status($form->status);
        $this->publishedAt  = new DateTime($form->publishedAt);
        $this->slug         = $form->slug;
        $this->image        = $form->image;
        $this->isMain       = (bool)$form->isMain;
        $this->isExclusive  = (bool)$form->isExclusive;
        $this->isActual     = (bool)$form->isActual;
        $this->isBreaking   = (bool)$form->isBreaking;
        $this->isImportant  = (bool)$form->isImportant;
        $this->linksNoindex = (bool)$form->linksNoindex;
        $this->meta         = Meta::fromArray($form->meta->toArray());
    }
}