<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Post;

use DateTime;
use Exception;
use Ria\News\Core\Forms\Post\MoveDateForm;

/**
 * Class MoveDateCommand
 * @property string cause
 * @property DateTime date
 * @property int id
 * @package Ria\News\Core\Commands\Post
 *
 */
class MoveDateCommand
{

    /**
     * MoveDateCommand constructor.
     * @param int $id
     * @param MoveDateForm $form
     * @throws Exception
     */
    public function __construct(int $id, MoveDateForm $form)
    {
        $this->id    = $id;
        $this->date  = new DateTime($form->date);
        $this->cause = $form->cause;
    }
}