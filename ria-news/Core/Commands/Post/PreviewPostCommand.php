<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Post;

use Cocur\Slugify\Slugify;
use DateTime;
use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\Core\Models\Meta;
use Ria\News\Core\Forms\Post\PostForm;
use Ria\News\Core\Models\Post\Content;
use Ria\News\Core\Models\Post\Icon;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;
use Yii;

/**
 * Class PreviewPostCommand
 * @package Ria\News\Core\Commands\Post
 * @property string $key
 * @property Status $status
 * @property int $id
 * @property int $groupId
 * @property int $cityId
 * @property Icon $icon
 * @property Type $type
 * @property string $optionType
 * @property int $categoryId
 * @property int $authorId
 * @property int $createdBy
 * @property int $storyId
 * @property array $tags
 * @property array $persons
 * @property array $marked_words
 * @property array $related
 * @property array $exports
 * @property array $photos
 * @property string $youtubeId
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $source
 * @property string $note
 * @property string $language
 * @property DateTime $publishedAt
 * @property string $slug
 * @property string $image
 * @property bool $isMain
 * @property bool $isExclusive
 * @property bool $isActual
 * @property bool $isBreaking
 * @property bool $linksNoindex
 * @property Meta $meta
 */
class PreviewPostCommand implements CommandInterface
{
    /**
     * PreviewPostCommand constructor.
     * @throws \yii\base\Exception
     */
    public function __construct()
    {
        $this->key = Yii::$app->security->generateRandomString();
    }

    /**
     * @param PostForm $form
     * @return PreviewPostCommand
     * @throws \Exception
     */
    public static function new(PostForm $form): self
    {
        $tags = [];
        if (!empty($form->tags)) {
            foreach ($form->tags as $tag) {
                $tags[] = [
                    'name' => $tag,
                    'slug' => Slugify::create()->slugify($tag)
                ];
            }
        }

        $command               = new self();
        $command->id           = $form->id;
        $command->groupId      = $form->group_id;
        $command->icon         = new Icon($form->icon);
        $command->type         = !empty($form->type) ? new Type($form->type) : null;
        $command->optionType   = !empty($form->optionType) ? $form->optionType : null;
        $command->categoryId   = (int)$form->categoryId;
        $command->authorId     = (int)$form->authorId;
        $command->cityId       = (int)$form->cityId;
        $command->createdBy    = $form->createdBy;
        $command->storyId      = !empty($form->storyId) ? (int)$form->storyId : null;
        $command->tags         = $tags;
        $command->persons      = $form->persons ?: [];
        $command->marked_words = $form->marked_words ?: [];
        $command->related      = $form->related ?: [];
        $command->exports      = $form->exports ?: [];
        $command->photos       = $form->photos ?: [];
        $command->youtubeId    = $form->youtubeId;
        $command->title        = $form->title;
        $command->description  = $form->description;
        $command->content      = new Content($form->content);
        $command->source       = $form->source;
        $command->note         = $form->note;
        $command->language     = $form->language;
        $command->status       = !empty($form->status) ? new Status($form->status) : null;
        $command->publishedAt  = !empty($form->publishedAt) ? new DateTime($form->publishedAt) : null;
        $command->slug         = $form->slug;
        $command->image        = $form->image;
        $command->isMain       = (bool)$form->isMain;
        $command->isExclusive  = (bool)$form->isExclusive;
        $command->isActual     = (bool)$form->isActual;
        $command->isBreaking   = (bool)$form->isBreaking;
        $command->linksNoindex = (bool)$form->linksNoindex;
        $command->meta         = Meta::fromArray($form->meta->toArray());

        return $command;
    }

    /**
     * @param int $id
     * @return static
     */
    public static function existing(int $id): self
    {
        $command     = new self();
        $command->id = $id;
        return $command;
    }
}