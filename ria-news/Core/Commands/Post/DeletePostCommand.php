<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Post;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\Users\Core\Models\User;

/**
 * Class DeletePostCommand
 * @package Ria\News\Core\Commands\Post
 * @property int $id
 * @property string $cause
 * @property string|bool $status
 * @property User $user
 */
class DeletePostCommand implements CommandInterface
{
    /**
     * DeletePostCommand constructor.
     * @param int $id
     * @param string $cause
     * @param $user
     * @param string|null $status
     */
    public function __construct(int $id, string $cause, $user, ?string $status = null)
    {
        $this->id     = $id;
        $this->cause  = $cause;
        $this->status = $status;
        $this->user   = $user;
    }
}