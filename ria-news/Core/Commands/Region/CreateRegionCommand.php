<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Region;

class CreateRegionCommand extends RegionCommand {}