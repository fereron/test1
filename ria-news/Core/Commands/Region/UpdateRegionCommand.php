<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Region;

use Ria\News\Core\Forms\Region\RegionUpdateForm;

class UpdateRegionCommand extends RegionCommand {

    public function __construct($id, RegionUpdateForm $form)
    {
        $this->id = $id;

        parent::__construct($form);
    }

}