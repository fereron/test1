<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Region;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Forms\Region\RegionForm;

class RegionCommand implements CommandInterface
{
    public function __construct(RegionForm $form)
    {
        $this->order = $form->order;
        $this->translations = array_map(function ($translation) {
            return [
                'language'    => $translation->language,
                'slug'        => $translation->slug,
                'title'       => $translation->title,
            ];
        }, $form->translations);
    }
}