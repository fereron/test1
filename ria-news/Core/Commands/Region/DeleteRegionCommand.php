<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Region;

use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class DeleteRegionCommand
 * @package Ria\News\Core\Commands\Region
 * @property int $id
 */
class DeleteRegionCommand implements CommandInterface
{
    public function __construct($id)
    {
        $this->id = $id;
    }
}