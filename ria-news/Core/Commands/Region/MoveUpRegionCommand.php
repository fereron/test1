<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\Region;
use Ria\Core\Components\CommandBus\CommandInterface;
/**
 * Class MoveUpRegionCommand
 * @package Ria\News\Core\Commands\Region
 *
 * @property int $id
 */
class MoveUpRegionCommand implements CommandInterface
{
    /**
     * MoveUpCategoryCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}