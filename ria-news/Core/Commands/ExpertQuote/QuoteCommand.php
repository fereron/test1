<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\ExpertQuote;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Forms\ExpertQuote\QuoteForm;

/**
 * Class QuoteCommand
 * @package Ria\News\Core\Commands\ExpertQuote
 *
 * @property int|null $id
 * @property int $expertId
 * @property int|null $postId
 * @property string $text
 */
class QuoteCommand implements CommandInterface
{

    /**
     * QuoteCommand constructor.
     * @param QuoteForm $form
     */
    public function __construct(QuoteForm $form)
    {
        $this->id       = $form->id;
        $this->expertId = $form->expertId;
        $this->postId   = $form->postId;
        $this->text     = $form->text;
    }

}