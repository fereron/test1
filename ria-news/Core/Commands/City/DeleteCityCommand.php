<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\City;

use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class DeleteCityCommand
 * @package Ria\News\Core\Commands\City
 * @property int $id
 */
class DeleteCityCommand implements CommandInterface
{
    public function __construct($id)
    {
        $this->id = $id;
    }
}