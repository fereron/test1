<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\City;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\News\Core\Forms\City\CityForm;

class CityCommand implements CommandInterface
{
    public function __construct(CityForm $form)
    {
        $this->region_id = $form->region_id;
        $this->translations = array_map(function ($translation) {
            return [
                'language'    => $translation->language,
                'slug'       => $translation->slug,
                'title'       => $translation->title,
            ];
        }, $form->translations);
    }
}