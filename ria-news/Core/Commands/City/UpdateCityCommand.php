<?php
declare(strict_types=1);

namespace Ria\News\Core\Commands\City;

use Ria\News\Core\Forms\City\CityUpdateForm;

class UpdateCityCommand extends CityCommand
{
    public function __construct($id, CityUpdateForm $form)
    {
        $this->id = $id;
        $this->region_id = $form->region_id;

        parent::__construct($form);
    }
}