<?php
declare(strict_types=1);

namespace Ria\News\Core\Helpers;


/**
 * Class TokensHelper
 * @package Ria\News\Core\Helpers
 */
final class TokensHelper
{

    /**
     * @param string $text
     * @return string
     */
    public static function clearAll(string $text): string
    {
        return self::clearExpertQuoteTokens(
            self::clearMediaWidgetTokens(
                self::clearPhotoTokens($text)
            )
        );
    }

    /**
     * @param string $content
     * @return string
     */
    public static function clearExpertQuoteTokens(string $content): string
    {
        return preg_replace('/({{expert-quote-[\d]+}})/i', '', $content);
    }

    /**
     * @param string $content
     * @return string
     */
    public static function clearMediaWidgetTokens(string $content): string
    {
        return preg_replace('/({{widget-content-[\d]+}})/i', '', $content);
    }

    /**
     * @param string $content
     * @return string
     */
    public static function clearArrowKeys(string $content): string
    {
        return str_replace(['**up**', '**down**'], '', $content);
    }

    /**
     * @param string $content
     * @return string
     */
    public static function clearPhotoTokens(string $content): string
    {
        return preg_replace('/({{photo-(small-left|small-right|big)-[\d]+}})/i', '', $content);
    }

}