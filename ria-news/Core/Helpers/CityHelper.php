<?php
declare(strict_types=1);

namespace Ria\News\Core\Helpers;

use Doctrine\ORM\Query\Expr\Join;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Models\Region\Region;
use Ria\News\Core\Helpers\RegionHelper;
use Yii;

class CityHelper
{
    public static function getRegionCities($region, $language) {
        $query = Yii::$app->doctrine->getEntityManager()
            ->getRepository(City::class)
            ->createQueryBuilder('c');

        $cities = $query
            ->select('c.id', 'identity(c.region) as region_id', 'ct.title')
            ->join('c.translations', 'ct', Join::WITH, 'ct.language = :language')
            ->where('identity(c.region) = :region_id')
            ->setParameters([
                'language' => $language,
                'region_id' => $region['id']
            ])
            ->getQuery()
            ->execute();

        return $cities;
    }

    /**
     * @param string|null $language
     * @return array
     */
    public static function sortedList(string $language = null): array
    {
        // get all regions to data
        $data = RegionHelper::getRegions($language);
        $regionsAndCities = [];

        foreach ($data as $region) {
            $regionCities = self::getRegionCities($region, $language);
            foreach ($regionCities as $regionCity) {
                if(!empty($regionCity['id'])) {
                    $regionsAndCities[$region['title']][$regionCity['id']] = $regionCity['title'];
                }
            }
        }

        return $regionsAndCities;
    }

}