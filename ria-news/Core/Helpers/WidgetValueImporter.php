<?php
declare(strict_types=1);

namespace Ria\News\Core\Helpers;

/**
 * Class WidgetValueImporter
 * @package Ria\News\Core\Helpers
 */
final class WidgetValueImporter
{

    /**
     * @param string $htmlCode
     * @return string|null
     */
    public static function getYoutubeHash(string $htmlCode): ?string
    {
        $matches = [];
        if (!preg_match('/\/embed\/([a-z0-9\_\-]+)/i', $htmlCode, $matches)) {
            return null;
        }

        list($matchString, $youtubeHash) = $matches;
        return $youtubeHash;
    }

    /**
     * @param string $htmlCode
     * @return string|null
     */
    public static function getFacebookPostLink(string $htmlCode): ?string
    {
        $matches = [];
        if (!preg_match('/src="https:\/\/www.facebook.com\/plugins\/(post|video|comment_embed).php\?href=([^"]+)"/ui', $htmlCode, $matches)
        ) {
            return null;
        }

        list($link, $type, $facebookPostLink) = $matches;
        return str_replace('&width=500', '', $facebookPostLink);
    }

    /**
     * @param string $htmlCode
     * @return int|null
     */
    public static function getTwitterId(string $htmlCode): ?int
    {
        $matches = [];
        if (!preg_match('/https:\/\/twitter.com\/[a-z0-9\-\_]+\/status\/([0-9]+)/ui', $htmlCode, $matches)) {
            return null;
        }

        list($link, $twitterId) = $matches;
        return is_null($twitterId) ? null : intval($twitterId);
    }

    /**
     * @param string $htmlCode
     * @return string|null
     */
    public static function getInstagramHash(string $htmlCode): ?string
    {
        $matches = [];
        if (!preg_match('/https:\/\/www.instagram.com\/(p|tv)\/([a-z0-9\-\_]+)\//ui', $htmlCode, $matches)) {
            return null;
        }

        list($link, $type, $instagramHash) = $matches;
        return $instagramHash;
    }

    /**
     * @param string $htmlCode
     * @return array|null
     */
    public static function getVkParams(string $htmlCode): ?array
    {
        $matches = [];
        if (!preg_match('/VK\.Widgets\.Post\("vk_post_([0-9\-]+)_([0-9]+)", [0-9\-]+, [0-9]+, \'([a-z0-9]+)\'\)/i', $htmlCode, $matches)) {
            return null;
        }

        list($scriptHtml, $ownerId, $postId, $hash) = $matches;
        return [
            'ownerId' => $ownerId,
            'postId'  => $postId,
            'hash'    => $hash
        ];
    }

}