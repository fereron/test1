<?php
declare(strict_types=1);

namespace Ria\News\Core\Helpers;

/**
 * Class CategoryHelper
 * @package Ria\News\Core\Helpers
 */
class CategoryHelper
{
    /**
     * @param int $parentId
     * @param array $child
     * @return int[]
     */
    public static function pluckChildren(int $parentId, $child)
    {
        $ids = [$parentId];
        foreach ($child as $item) {
            $ids[] = $item->id;
        }
        return $ids;
    }

    /**
     * @param array $elements
     * @param null $parentId
     * @return array
     */
    protected static function buildTree(array $elements, $parentId = null): array
    {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}