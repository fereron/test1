<?php
declare(strict_types=1);

namespace Ria\News\Core\Helpers;

use Doctrine\ORM\Query\Expr\Join;
use Ria\News\Core\Models\Region\Region;
use Yii;

/**
 * Class RegionHelper
 * @package Ria\News\Core\Helpers
 */
class RegionHelper
{
    /**
     * @param string $language
     * @return array
     */
    public static function getRegions(string $language): array
    {
        $query = Yii::$app->doctrine->getEntityManager()
            ->getRepository(Region::class)
            ->createQueryBuilder('r');

        return $query
            ->select('r.id', 'rt.title', 'rt.slug')
            ->join('r.translations', 'rt', Join::WITH, 'rt.language = :language')
            ->setParameters([
                'language' => $language,
            ])
            ->getQuery()
            ->execute();
    }

    /**
     * @param string|null $language
     * @return array
     */
    public static function sortedList(string $language = null): array
    {
        return self::getRegions($language);
    }

}