<?php
declare(strict_types=1);

namespace Ria\News\Core\Helpers;

use Ria\News\Core\Models\Speech\Speech;

/**
 * Class SpeechHelper
 * @package Ria\News\Core\Helpers
 */
class SpeechHelper
{

    /**
     * @param int $postId
     * @return Speech|object|null
     */
    public static function exists(int $postId): ?Speech
    {
        return \Yii::$app->doctrine
            ->getEntityManager()
            ->getRepository(Speech::class)
            ->findOneBy(['post' => $postId]);
    }

}