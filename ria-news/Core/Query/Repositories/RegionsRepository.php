<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Ria\Core\Query\EntitySpecificationRepositoryTrait;
use Ria\News\Core\Query\Hydrator\RegionHydrator;
use Ria\News\Core\Query\ViewModel\RegionViewModel;

/**
 * Class RegionsRepository
 * @package Ria\News\Core\Repositories
 */
class RegionsRepository extends EntityRepository
{
    use EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    function getSpecsNamespace(): string
    {
        return "Ria\\News\\Core\\Query\\Specifications\\Region";
    }

    /**
     * @param string $slug
     * @param string $language
     * @return RegionViewModel|null
     * @throws NonUniqueResultException
     */
    public function getBySlug(string $slug, string $language): ?RegionViewModel
    {
        $queryBuilder = $this->createQueryBuilder('r');

        return $queryBuilder
            ->select(['r.id', 'rt.slug', 'rt.title'])
            ->innerJoin('r.translations', 'rt', 'WITH', 'rt.language = :lang')
            ->where('rt.slug = :slug')
            ->setParameters(['lang' => $language, 'slug' => $slug])
            ->getQuery()
            ->getOneOrNullResult(RegionHydrator::HYDRATION_MODE);
    }

    public function get(string $language): array
    {
        $queryBuilder = $this->createQueryBuilder('r');

        return $queryBuilder
            ->select(['r.id', 'rt.title', 'rt.slug'])
            ->innerJoin('r.translations', 'rt', 'WITH', 'rt.language = :lang')
            ->setParameters(['lang' => $language])
            ->getQuery()
            ->getResult(RegionHydrator::HYDRATION_MODE);
    }

    public function getApiList(string $language)
    {
        $queryBuilder = $this->createQueryBuilder('r');

        return $queryBuilder
            ->select(['rt.slug as index', 'rt.title as name'])
            ->innerJoin('r.translations', 'rt', 'WITH', 'rt.language = :language')
            ->setParameters(['language' => $language])
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

}