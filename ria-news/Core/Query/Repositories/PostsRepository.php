<?php

namespace Ria\News\Core\Query\Repositories;

use Core\Helpers\StatusHelper;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Elasticsearch\Client;
use ReflectionException;
use Ria\Core\Query\EntitySpecificationRepositoryTrait;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;
use Ria\News\Core\Models\Speech\Speech;
use Ria\News\Core\Models\Tag\Tag;
use Ria\News\Core\Models\Tag\Translation;
use Ria\News\Core\Query\Hydrator\PostsHydrator;
use Ria\News\Core\Query\ViewModel\PostViewModel;
use Ria\Persons\Core\Models\Translation as PersonTranslation;
use Ria\Persons\Core\Query\ViewModel\PersonViewModel;
use Ria\Users\Core\Models\Translation as UserTranslation;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use Ria\Photos\Core\Models\Photo\Photo;

/**
 * Class PostsRepository
 * @package Ria\News\Core\Query\Repositories
 */
class PostsRepository extends EntityRepository
{
    use EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    public function getSpecsNamespace(): string
    {
        return "Ria\\News\\Core\\Query\\Specifications\\Post";
    }

    /**
     * @return string
     */
    public function getHydrationMode()
    {
        return PostsHydrator::HYDRATION_MODE;
    }

    /**
     * @param string $language
     * @param array|int $categories
     * @param int $limit
     * @param array $filters
     * @return array
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getByCategories(string $language, $categories, int $limit, array $filters = []): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $this->applySpecifications($queryBuilder, array_merge([
            'select',
            'lang'     => $language,
            'category' => $categories,
            'latest',
            'withCategory',
            'limit'    => $limit,
            'published'
        ], $filters));

        $query = $queryBuilder->addSelect(['p.type', 'p.description']);

        return $query->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param array $citiesIds
     * @param string $language
     * @param int $limit
     * @param string|null $timestamp
     * @return array|null
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getByCities(array $citiesIds, string $language, int $limit, string $timestamp = null): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $this->applySpecifications($queryBuilder, [
            'select',
            'lang'       => $language,
            'latest',
            'limit'      => $limit,
            'withCities' => $citiesIds,
            'withCategory',
            'published'
        ]);

        if ($timestamp) {
            $queryBuilder
                ->andWhere('p.published_at < :date')
                ->setParameter('date', $timestamp);
        }

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $categoryId
     * @param DateTime $publishedAt
     * @param string $language
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function getPrevious(int $categoryId, DateTime $publishedAt, string $language)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $query = $queryBuilder
            ->select([
                'p.id', 'p.group_id', 'p.language', 'p.status', 'p.title', 'p.content.content as content', 'p.description', 'p.slug',
                'p.image', 'p.published_at', 'p.is_published', 'p.created_at', 'p.updated_at', 'p.views', 'p.marked_words',
                'p.youtube_id', 'p.type', 'p.group_id', 'p.meta',
                'IDENTITY(p.category) as category_id', 'ct.title as category_title', 'ct.slug as category_slug',
                'CONCAT(ut.first_name, \' \', ut.last_name) as author_name', 'ut.slug as author_slug', 'u.thumb as author_thumb',
                'speech.filename as speech_filename'
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->innerJoin('p.author', 'u')
            ->innerJoin('u.translations', 'ut', 'WITH', 'ut.language = :lang')
            ->leftJoin(Speech::class, 'speech', 'WITH', 'speech.post = p.id')
            ->where('p.category = :category_id and p.language = :lang and p.is_published = 1')
            ->andWhere($queryBuilder->expr()->in('p.status', StatusHelper::activeStatuses()))
            ->setParameters(['category_id' => $categoryId, 'lang' => $language])
            ->setMaxResults(1)
            ->addOrderBy('p.published_at', 'DESC');

        if ($publishedAt->getTimestamp() > (time() - 86400 * 21)) {
            $query->andWhere('p.published_at < :published_at')->setParameter('published_at', $publishedAt);
        }

        return $query->getQuery()->getOneOrNullResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $language
     * @param int $limit
     * @param mixed $city
     * @param null $timestamp
     * @return array|null
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getByCity(string $language, int $limit, $city = null, $timestamp = null): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $this->applySpecifications($queryBuilder, [
            'select',
            'lang'     => $language,
            'latest',
            'limit'    => $limit,
            'withCity' => ['id' => $city],
            'withCategory',
            'published'
        ]);

        if ($timestamp) {
            $queryBuilder
                ->andWhere('p.published_at < :date')
                ->setParameter('date', $timestamp);
        }

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $language
     * @param int $limit
     * @param int $authorId
     * @param string|null $timestamp
     * @return array|null
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getByAuthor(int $authorId, string $language, int $limit, ?string $timestamp = null): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $specifications = [
            'select',
            'published',
            'latest',
            'lang'       => $language,
            'limit'      => $limit,
            'withCategory',
            'withAuthor' => ['id' => $authorId]
        ];

        if ($timestamp) {
            $specifications['dateLess'] = $timestamp;
        }

        $this->applySpecifications($queryBuilder, $specifications);

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $language
     * @param int $limit
     * @param int $expertId
     * @param string|null $timestamp
     * @return array|null
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getByExpert(int $expertId, string $language, int $limit, ?string $timestamp = null): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $specifications = [
            'select',
            'published',
            'latest',
            'lang'       => $language,
            'limit'      => $limit,
            'withCategory',
            'withExpert' => ['id' => $expertId]
        ];

        if ($timestamp) {
            $specifications['dateLess'] = $timestamp;
        }

        $this->applySpecifications($queryBuilder, $specifications);

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $id
     * @param string $language
     * @param int $limit
     * @return PersonViewModel[]
     */
    public function getByPerson(int $id, string $language, int $limit): array
    {
        return $this->createQueryBuilder('p')
            ->select([
                'p.id', 'p.title', 'p.image', 'p.published_at',
                'p.content.content as content', 'p.slug as slug', 'p.marked_words',
                'ct.slug as category_slug'
            ])
            ->innerJoin('p.post_person', 'pp', 'WITH', 'pp.person = :person')
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('p.is_published = 1 AND p.language = :lang')
            ->orderBy('p.published_at', 'DESC')
            ->setParameters(['person' => $id, 'lang' => $language])
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $slug
     * @param string $language
     * @return PostViewModel|null
     * @throws NonUniqueResultException
     */
    public function getBySlug(string $slug, string $language): ?PostViewModel
    {
        $queryBuilder = $this->createQueryBuilder('p');

        /** @var PostViewModel|null $post */
        $post = $queryBuilder
            ->select([
                'p.id', 'p.group_id', 'p.language', 'p.status', 'p.title', 'p.content.content as content', 'p.description', 'p.slug',
                'p.image', 'p.published_at', 'p.is_published', 'p.created_at', 'p.updated_at', 'p.views', 'p.marked_words',
                'p.youtube_id', 'p.type', 'p.group_id', 'p.meta', 'p.links_noindex',
                'IDENTITY(p.category) as category_id', 'ct.title as category_title', 'ct.slug as category_slug',
                'CONCAT(ut.first_name, \' \', ut.last_name) as author_name', 'ut.slug as author_slug', 'u.thumb as author_thumb',
                'speech.filename as speech_filename'
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->innerJoin('p.author', 'u')
            ->innerJoin('u.translations', 'ut', 'WITH', 'ut.language = :lang')
            ->leftJoin(Speech::class, 'speech', 'WITH', 'speech.post = p.id')
            ->where('p.slug = :slug and p.language = :lang')
            ->andWhere(
                $queryBuilder->expr()->in(
                    'p.status',
                    array_merge(StatusHelper::activeStatuses(), [Status::PRIVATE])
                )
            )
            ->setParameters(['slug' => $slug, 'lang' => $language])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(PostsHydrator::HYDRATION_MODE);

        if (!empty($post)) {
            $post->tags = $this->getTagsByPost($post->id, $language);
        }

        return $post;
    }

    public function getByIdForRoute($id, $language)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        /** @var PostViewModel|null $post */
        return $queryBuilder
            ->select(['p.id', 'p.slug', 'ct.slug as category_slug'])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('p.id = :id')
            ->setParameters(['id' => $id, 'lang' => $language])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $postId
     * @param string $language
     * @return array
     */
    public function getTagsByPost(int $postId, string $language): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select(['t.id', 'tt.name', 't.slug', 'tt.language'])
            ->from(Tag::class, 't')
            ->innerJoin(Translation::class, 'tt', 'WITH', 'tt.tag = t.id AND tt.language = :lang')
            ->innerJoin('t.posts', 'pt', 'WITH', 'pt.id = :post')
            ->where('tt.count > 2')
            ->setParameters([':post' => $postId, 'lang' => $language])
            ->getQuery()
            ->execute();
    }

    /**
     * @param int $groupId
     * @param string $excludeLanguage
     * @return array
     */
    public function getOtherTranslates(int $groupId, string $excludeLanguage): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $query = $queryBuilder
            ->select([
                'p.id', 'p.title', 'p.slug', 'p.description', 'p.image', 'p.published_at',
                'p.language', 'p.type',
                'ct.title as category_title', 'ct.slug as category_slug',
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = p.language')
            ->where('p.group_id = :group and p.is_published = 1', 'p.language <> :excludeLang')
            ->setParameters(['group' => $groupId, 'excludeLang' => $excludeLanguage])
            ->orderBy('p.published_at', 'DESC')
            ->setMaxResults(2);

        return $query->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $start
     * @param int $limit
     * @param string $language
     * @param string|int $hydrationMode
     * @return PostViewModel[]
     */
    public function getByRange(int $start, int $limit, string $language, $hydrationMode = PostsHydrator::HYDRATION_MODE): ?array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $query = $queryBuilder
            ->select([
                'p.id', 'p.updated_at', 'p.language as lang', 'p.slug', 'p.image', 'p.group_id as group',
                'p.published_at as created', 'p.updated_at as updated', 'p.title', 'p.description',
                'ct.title as category_name', 'ct.slug as category_slug'
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :language')
            ->where('p.language = :language')
            ->andWhere('p.is_published = :is_published')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->orderBy('p.published_at', 'DESC')
            ->setParameter('language', $language)
            ->setParameter('is_published', true);

        return $query->getQuery()->getResult($hydrationMode);
    }

    /**
     * @param string $language
     * @return int|mixed|string
     * @throws NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getCount(string $language)
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->where('p.language = :language')
            ->andWhere('p.is_published = :is_published')
            ->setParameter('language', $language)
            ->setParameter('is_published', true)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int|array $categories
     * @param string $language
     * @param int $page
     * @param int $limit
     * @return array|null
     */
    public function getApiPostsByCategory($categories, string $language, int $page, int $limit): ?array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $query = $queryBuilder
            ->select([
                'p.id', 'p.language as lang', 'p.slug as url', 'p.image', 'p.group_id as group',
                'p.published_at as created', 'p.title', 'p.description',
                'c.id as category_id', 'ct.title as category_name', 'ct.slug as category_url',
                'IDENTITY(c.parent) as category_parent_id',
                'a.id as user_id', 'a.email.email as user_email', 'at.first_name as user_first_name', 'at.last_name as user_last_name',
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :language')
            ->innerJoin('p.author', 'a')
            ->innerJoin('a.translations', 'at', 'WITH', 'at.language = :language')
            ->where('p.language = :language')
            ->andWhere($queryBuilder->expr()->in('p.category', ':categories'))
            ->andWhere('p.is_published = :is_published')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit)
            ->orderBy('p.published_at', 'DESC')
            ->setParameter('language', $language)
            ->setParameter('categories', array_wrap($categories))
            ->setParameter('is_published', true);

        return $query->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param int $id
     * @param $language
     * @return array|null
     * @throws NonUniqueResultException
     */
    public function getApiPostById(int $id, $language): ?array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $query = $queryBuilder
            ->select([
                'p.id', 'p.image', 'p.group_id as group', 'p.published_at as created', 'p.updated_at as updated',
                'p.language as lang', 'p.slug as url',
                'p.title', 'p.description', 'p.content.content as content',
//            'is_important false',
                'c.id as category_id', 'ct.title as category_name', 'ct.slug as category_url',
                'IDENTITY(c.parent) as category_parent_id',
                'a.id as user_id', 'a.email.email as user_email', 'at.first_name as user_first_name', 'at.last_name as user_last_name',
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :language')
            ->innerJoin('p.author', 'a')
            ->innerJoin('a.translations', 'at', 'WITH', 'at.language = :language')
            ->where('p.id = :id')
            ->andWhere('p.is_published = :is_published')
            ->setMaxResults(1)
            ->setParameter('id', $id)
            ->setParameter('language', $language)
            ->setParameter('is_published', true);

        return $query->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY) ?? [];
    }

    /**
     * @param string $keyword
     * @param string $language
     * @param int $page
     * @param int $limit
     * @return array
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getApiPostsByKeyword(string $keyword, string $language, int $page, int $limit): array
    {
        $client = Yii::$container->get(Client::class);

        $response = $client->search([
            'index' => 'reportaz',
            'type'  => 'posts',
            'body'  => [
                'from'  => $page * $limit,
                'size'  => $limit,
                'sort'  => ['published_at' => ['order' => 'desc']],
                'query' => [
                    'bool' => [
                        'must'   => [
                            [
                                'multi_match' => [
                                    'query'       => $keyword,
                                    'type'        => 'phrase_prefix',
                                    'fields'      => [
                                        'title',
                                        'content',
                                    ],
                                    'tie_breaker' => 0.3,
                                ],
                            ]
                        ],
                        'filter' => [
                            'term' => ['lang' => $language],
                        ],
                    ],
                ],
            ],
        ]);

        $posts = [];
        foreach ($response['hits']['hits'] as $hit) {
            $posts[] = [
                'id'            => $hit['_source']['id'],
                'title'         => $hit['_source']['title'],
                'description'   => $hit['_source']['description'],
                'content'       => $hit['_source']['content'],
                'created'       => $hit['_source']['published_at'],
                'url'           => $hit['_source']['slug'],
                'lang'          => $hit['_source']['lang'],
                'group'         => $hit['_source']['group_id'],
                'image'         => $hit['_source']['image'],
                'category_name' => $hit['_source']['category_title'],
                'category_url'  => $hit['_source']['category_slug'],
            ];
        }

        return $posts;
    }

    /**
     * @param bool $is_main
     * @param string $language
     * @param int $page
     * @param int $limit
     * @return int|mixed|string
     */
    public function getApiPostsByType(bool $is_main, string $language, int $page, int $limit)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        if (empty($page)) {
            $page = 1;
        }

        $query = $queryBuilder
            ->select([
                'p.id', 'p.language as lang', 'p.slug as url', 'p.image', 'p.group_id as group',
                'p.published_at as created', 'p.updated_at as updated', 'p.title', 'p.description',
                'c.id as category_id', 'ct.title as category_name', 'ct.slug as category_url',
                'IDENTITY(c.parent) as category_parent_id',
                'a.id as user_id', 'a.email.email as user_email', 'at.first_name as user_first_name', 'at.last_name as user_last_name',
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :language')
            ->innerJoin('p.author', 'a')
            ->innerJoin('a.translations', 'at', 'WITH', 'at.language = :language')
            ->where('p.language = :language')
            ->andWhere('p.is_published = :is_published')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit)
            ->orderBy('p.published_at', 'DESC')
            ->setParameter('language', $language)
            ->setParameter('is_published', true);

        if ($is_main) {
            $query->andWhere('p.is_main = :is_main')->setParameter('is_main', true);
        }

        return $query->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param int $author_id
     * @param string $language
     * @param int $limit
     * @return int|mixed|string
     */
    public function getApiPostsByAuthor(int $author_id, string $language, int $limit)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $query = $queryBuilder
            ->select([
                'p.id', 'p.language as lang', 'p.slug as url', 'p.image', 'p.group_id as group',
                'p.published_at as created', 'p.updated_at as updated', 'p.title', 'p.description',
                'c.id as category_id', 'ct.title as category_name', 'ct.slug as category_url',
                'IDENTITY(c.parent) as category_parent_id',
                'a.id as user_id',
            ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :language')
            ->innerJoin('p.author', 'a')
            ->innerJoin('a.translations', 'at', 'WITH', 'at.language = :language')
            ->where('p.language = :language')
            ->andWhere('IDENTITY(p.author) = :author_id')
            ->andWhere('p.is_published = :is_published')
            ->setMaxResults($limit)
            ->orderBy('p.published_at', 'DESC')
            ->setParameter('language', $language)
            ->setParameter('author_id', $author_id)
            ->setParameter('is_published', true);

        return $query->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param string $language
     * @return PostViewModel[]
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ReflectionException
     */
    public function getRssTurboPosts(string $language): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $this->applySpecifications($queryBuilder, [
            'select'   => [
                'id', 'type', 'title', 'language', 'description', 'slug', 'published_at', 'image',
                'content.content', 'language', 'links_noindex'
            ],
            'latest',
            'published',
            'lang'     => $language,
            'dateLess' => date('Y-m-d H:i:00'),
            'limit'    => 50,
            'withCategory', 'withAuthor'
        ]);

        $queryBuilder
            ->addSelect(['photo_t.information as image_information', 'photo_t.author as image_author'])
            ->innerJoin(Photo::class, 'photo')
            ->andWhere('photo.filename = p.image')
            ->innerJoin('photo.translations', 'photo_t', 'WITH', 'photo_t.language = :lang')
            ->setParameter('lang', $language);

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $language
     * @return PostViewModel[]
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ReflectionException
     */
    public function getRssTurboDeletedPosts(string $language): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $this->applySpecifications($queryBuilder, [
            'select'   => [
                'id', 'type', 'title', 'language', 'description', 'slug', 'published_at', 'image',
                'content.content', 'language', 'links_noindex'
            ],
            'latest',
            'deactivated',
            'lang'     => $language,
            'dateLess' => date('Y-m-d H:i:00'),
            'limit'    => 100,
            'withCategory', 'withAuthor'
        ]);

        $queryBuilder
            ->addSelect(['photo_t.information as image_information', 'photo_t.author as image_author'])
            ->innerJoin(Photo::class, 'photo')
            ->andWhere('photo.filename = p.image')
            ->innerJoin('photo.translations', 'photo_t', 'WITH', 'photo_t.language = :lang')
            ->setParameter('lang', $language);

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $language
     * @return PostViewModel[]
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ReflectionException
     */
    public function getRssGooglePosts(string $language): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $this->applySpecifications($queryBuilder, [
            'select'       => [
                'id', 'type', 'title', 'language', 'description', 'slug', 'published_at', 'image',
                'content.content', 'language',
            ],
            'latest',
            'published',
            'lang'         => $language,
            'limit'        => 100,
            'withCategory' => $language,
            'withAuthor',
            'whichHasPhoto',
        ]);

        $queryBuilder
            ->addSelect(['photo_t.information as image_information', 'photo_t.author as image_author'])
            ->innerJoin(Photo::class, 'photo')
            ->andWhere('photo.filename = p.image')
            ->innerJoin('photo.translations', 'photo_t', 'WITH', 'photo_t.language = :lang')
            ->setParameter('lang', $language);

        return $queryBuilder->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $tagId
     * @param string $language
     * @param int $limit
     * @param string|null $timestamp
     * @return int|mixed|string
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ReflectionException
     */
    public function getByTag(int $tagId, string $language, int $limit, ?string $timestamp = null)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $this->applySpecifications($queryBuilder, [
            'select',
            'lang'  => $language,
            'latest',
            'withCategory',
            'limit' => $limit,
            'published'
        ]);

        $query = $queryBuilder
            ->addSelect(['p.type', 'p.description'])
            ->innerJoin('p.tags', 'pt', 'WITH', 'pt.id = :tag_id')
            ->setParameter('tag_id', $tagId);

        if ($timestamp) {
            $queryBuilder
                ->andWhere('p.published_at < :date')
                ->setParameter('date', $timestamp);
        }

        return $query->getQuery()->getResult(PostsHydrator::HYDRATION_MODE);
    }


    /**
     * @param int $groupId
     * @param string $excludeLanguage
     * @return array
     */
    public function getOtherTranslations(int $groupId, string $excludeLanguage): array
    {
        $queryBuilder = $this->createQueryBuilder('p');

        return $queryBuilder
            ->where('p.group_id = :group and p.is_published = 1', 'p.language <> :excludeLang')
            ->setParameters(['group' => $groupId, 'excludeLang' => $excludeLanguage])
            ->orderBy('p.published_at', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $params
     * @return array
     */
    public function getArticlesAndOpinions($params)
    {
        $query = $this->createQueryBuilder('p');

        return $query->select([
            'p.id', 'IDENTITY(p.category)', 'p.title', 'p.type', 'p.slug', 'p.published_at', 'p.image', 'p.description',
            'e.photo as expert_thumb',
            'et.first_name as expert_first_name', 'et.last_name as expert_last_name', 'et.position as expert_position', 'et.slug as expert_slug',
            'a.thumb as author_thumb',
            'c.id as category_id', 'ct.title as category_name', 'ct.slug as category_url',
            'at.first_name as author_first_name', 'at.last_name as author_last_name', 'at.position as author_position', 'at.slug as author_slug',
        ])
            ->innerJoin('p.category', 'c')
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->leftJoin('p.author', 'a')
            ->leftJoin(UserTranslation::class, 'at', Join::WITH, 'IDENTITY(at.user) = a.id and at.language = :lang')
            ->leftJoin('p.expert', 'e')
            ->leftJoin(PersonTranslation::class, 'et', Join::WITH, 'IDENTITY(et.person) = e.id and et.language = :lang')
            ->where('p.is_published = 1')
            ->andWhere('p.language = :lang')
            ->andWhere('p.type in (:types)')
            ->orderBy('p.published_at', 'DESC')
            ->setMaxResults($params['limit'])
            ->setParameters([':lang' => $params['lang'], ':types' => [Type::ARTICLE, Type::OPINION]])
            ->getQuery()
            ->setHydrationMode(PostsHydrator::HYDRATION_MODE)
            ->execute();
    }

}