<?php
declare(strict_types=1);

namespace Ria\News\Core\Query\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Ria\Core\Query\EntitySpecificationRepositoryTrait;
use Ria\News\Core\Query\Hydrator\CityHydrator;
use Ria\News\Core\Query\ViewModel\CityViewModel;
use Ria\News\Core\Query\ViewModel\RegionViewModel;

/**
 * Class CitiesRepository
 * @package Ria\News\Core\Repositories
 */
class CitiesRepository extends EntityRepository
{
    use EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    function getSpecsNamespace(): string
    {
        return "Ria\\News\\Core\\Query\\Specifications\\City";
    }

    /**
     * @param string $city
     * @param string $language
     * @return CityViewModel|null
     * @throws NonUniqueResultException
     */
    public function getBySlug($city, string $language): ?CityViewModel
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'ct.slug', 'ct.title'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('ct.slug = :city')
            ->setParameters(['city' => $city, 'lang' => $language])
            ->getQuery()
            ->getOneOrNullResult(CityHydrator::HYDRATION_MODE);
    }

    /**
     * @param RegionViewModel $region
     * @param string $language
     * @param int $limit
     * @return array
     */
    public function getRegionCities(RegionViewModel $region, string $language, int $limit): array
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'ct.slug', 'ct.title'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('identity(c.region) = :region')
            ->setParameters(['region' => $region->id, 'lang' => $language])
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult(CityHydrator::HYDRATION_MODE);
    }

    /**
     * @param RegionViewModel $region
     * @param string $language
     * @return CityViewModel
     */
    public function getRegionCity(RegionViewModel $region, string $language): CityViewModel
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'ct.slug', 'ct.title'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('identity(c.region) = :region')
            ->setParameters(['region' => $region->id, 'lang' => $language])
            ->getQuery()
            ->getResult(CityHydrator::HYDRATION_MODE);
    }


}