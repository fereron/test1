<?php
declare(strict_types=1);

namespace Ria\News\Core\Query\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * Class ExpertQuotesRepository
 * @package Ria\News\Core\Query\Repositories
 */
class ExpertQuotesRepository extends EntityRepository
{

    /**
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function latestOne()
    {
        return $this->createQueryBuilder('eq')
            ->orderBy('eq.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}