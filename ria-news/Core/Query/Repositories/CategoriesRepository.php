<?php
declare(strict_types=1);

namespace Ria\News\Core\Query\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Happyr\DoctrineSpecification\Spec;
use Ria\Core\Query\EntitySpecificationRepositoryTrait;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Query\Hydrator\CategoryHydrator;
use Ria\News\Core\Query\ViewModel\CategoryViewModel;

/**
 * Class CategoriesRepository
 * @package Ria\News\Core\Repositories
 */
class CategoriesRepository extends EntityRepository
{
    use EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    function getSpecsNamespace(): string
    {
        return "Ria\\News\\Core\\Query\\Specifications\\Category";
    }

    /**
     * @param string $language
     * @param int $limit
     * @return array
     */
    public function menuItems(string $language, int $limit): array
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'ct.title', 'ct.slug'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where($queryBuilder->expr()->isNull('c.parent'))
            ->andWhere('c.status = :status')
            ->setParameters(['lang' => $language, 'status' => true])
            ->orderBy('c.sort')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult(CategoryHydrator::HYDRATION_MODE);
    }

    /**
     * @param string $language
     * @return array
     */
    public function get(string $language): array
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'ct.title', 'ct.slug'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('c.status = :status')
            ->setParameters(['lang' => $language, 'status' => true])
            ->orderBy('c.sort')
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $slug
     * @param string $language
     * @return CategoryViewModel|null
     * @throws NonUniqueResultException
     */
    public function getBySlug(string $slug, string $language): ?CategoryViewModel
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'IDENTITY(c.parent) as parent_id', 'ct.title', 'ct.slug', 'ct.meta',
                'cpt.title as parent_title', 'cpt.slug as parent_slug'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->leftJoin('c.parent', 'cp')
            ->leftJoin('cp.translations', 'cpt', 'WITH', 'cpt.language = :lang')
            ->where('ct.slug = :slug AND c.status = :status')
            ->setParameters(['lang' => $language, 'status' => true, 'slug' => $slug])
            ->getQuery()
            ->getOneOrNullResult(CategoryHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $id
     * @param string $language
     * @return CategoryViewModel|null
     * @throws NonUniqueResultException
     */
    public function getById(int $id, string $language): ?CategoryViewModel
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'IDENTITY(c.parent) as parent_id', 'ct.title', 'ct.slug', 'ct.meta',
                'cpt.title as parent_title', 'cpt.slug as parent_slug'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->leftJoin('c.parent', 'cp')
            ->leftJoin('cp.translations', 'cpt', 'WITH', 'cpt.language = :lang')
            ->where('c.id = :id AND c.status = :status')
            ->setParameters(['lang' => $language, 'status' => true, 'id' => $id])
            ->getQuery()
            ->getOneOrNullResult(CategoryHydrator::HYDRATION_MODE);
    }

    /**
     * @param int $parentId
     * @param string $language
     * @return array
     */
    public function getSubCategories(int $parentId, string $language)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id', 'IDENTITY(c.parent) as parent_id', 'ct.title', 'ct.slug'])
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('c.parent = :parent AND c.status = :status')
            ->setParameters(['lang' => $language, 'status' => true, 'parent' => $parentId])
            ->getQuery()
            ->getResult(CategoryHydrator::HYDRATION_MODE);
    }

    /**
     * @param int|array $parentIds
     * @return int|mixed|string
     */
    public function getChildren($parentIds)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        return $queryBuilder
            ->select(['c.id'])
            ->where($queryBuilder->expr()->in('c.parent', ':parent_ids'))
            ->andWhere('c.status = :status')
            ->setParameters([
                'parent_ids' => array_wrap($parentIds),
                'status' => true
            ])
            ->getQuery()
            ->execute();
    }

    public function getAllWithChildren($language, $limit = null, $parent_id = null)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        $query = $queryBuilder
            ->innerJoin('c.translations', 'ct', 'WITH', 'ct.language = :lang')
            ->where('c.status = :status')
            ->setParameter('lang', $language)
            ->setParameter('status', true)
            ->orderBy('c.sort')
            ->setMaxResults($limit);

        if ($parent_id) {
            $query->andWhere('IDENTITY(c.parent) = :parent_id')
                ->setParameter('parent_id', $parent_id);
        } else {
            $query->andWhere($queryBuilder->expr()->isNull('c.parent'));
        }

        $categories = $query->getQuery()->getResult();

        $categories = collect($categories)
            ->transform(function (Category $category) use ($language) {
                return [
                  'id' => (string) $category->getId(),
                  'parent_id' => $category->getParent() ? (string) $category->getParent()->getId() : "0",
                  'name' => $category->getTranslation($language)->getTitle(),
                  'slug' => $category->getTranslation($language)->getSlug(),
                  'url' => $category->getTranslation($language)->getSlug(),
                  'parent_in_url' => "0",
                  'category_parent_id' => $category->getParent() ? (string) $category->getParent()->getId() : null,
                  'category_parent_group' => $category->getParent() ? (string) $category->getParent()->getId() : null,
                  'category_parent_name' => $category->getParent() ? $category->getParent()->getTranslation($language)->getTitle() : null,
                  'category_parent_slug' => $category->getParent() ? $category->getParent()->getTranslation($language)->getSlug() : null,
                  'category_parent_url' => $category->getParent() ? $category->getParent()->getTranslation($language)->getSlug() : null,
                  'category_parent_lang' => $category->getParent() ? $language : null,
                ];
            })->toArray();

        if (!$parent_id) {
            foreach ($categories as &$category) {
                $category['sub'] = $this->getAllWithChildren($language, $limit, $category['id']);
            }
        }

        return $categories;
    }

    public function getForPosts($ids, string $language)
    {
        $specs = Spec::andX(
            Spec::in('id', array_wrap($ids)),
            Spec::innerJoin('translations', 'ct'),
            Spec::eq('language', $language, 'ct'),
            Spec::select(
                Spec::selectAs(Spec::field('id'), 'category_id'),
                Spec::selectAs(Spec::field('title', 'ct'), 'category_title'),
                Spec::selectAs(Spec::field('slug', 'ct'), 'category_slug'),
            ),
        );

        return $this->match($specs);
    }

}