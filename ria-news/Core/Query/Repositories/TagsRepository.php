<?php
declare(strict_types=1);

namespace Ria\News\Core\Query\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Ria\Core\Query\EntitySpecificationRepositoryTrait;
use Ria\News\Core\Query\Hydrator\TagHydrator;
use Ria\News\Core\Query\ViewModel\TagViewModel;

/**
 * Class TagsRepository
 * @package Ria\News\Core\Query\Repositories
 */
class TagsRepository extends EntityRepository
{
    use EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    function getSpecsNamespace(): string
    {
        return "Ria\\News\\Core\\Query\\Specifications\\Tag";
    }

    /**
     * @param string $slug
     * @param string $language
     * @return TagViewModel|null
     * @throws NonUniqueResultException
     */
    public function getBySlug(string $slug, string $language): ?TagViewModel
    {
        $queryBuilder = $this->createQueryBuilder('t');

        return $queryBuilder
            ->select(['t.id', 'tt.name', 't.slug'])
            ->innerJoin('t.translations', 'tt', 'WITH', 'tt.language = :lang')
            ->where('t.slug = :slug')
            ->andWhere('tt.count > 2')
            ->setParameters(['lang' => $language, 'slug' => $slug])
            ->getQuery()
            ->getOneOrNullResult(TagHydrator::HYDRATION_MODE);
    }

}