<?php

namespace Ria\News\Core\Query\Specifications\Category;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

class Latest extends BaseSpecification
{

    public function getSpec()
    {
        return Spec::orderBy('id', 'DESC');
    }

}