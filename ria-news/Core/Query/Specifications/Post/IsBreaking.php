<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

class IsBreaking extends BaseSpecification
{

    public function getSpec()
    {
        return Spec::eq('is_breaking', true);
    }

}