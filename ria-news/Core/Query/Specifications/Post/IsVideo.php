<?php
declare(strict_types=1);

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

/**
 * Class IsVideo
 * @package Ria\News\Core\Query\Specifications\Post
 */
class IsVideo extends BaseSpecification
{

    public function getSpec()
    {
        return Spec::eq('type', 'video');
    }
}