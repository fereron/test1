<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

/**
 * Class Latest
 * @package Ria\News\Core\Query\Specifications\Post
 */
class Latest extends BaseSpecification
{

    /**
     * @return \Happyr\DoctrineSpecification\Filter\Filter
     * |\Happyr\DoctrineSpecification\Query\OrderBy
     * |\Happyr\DoctrineSpecification\Query\QueryModifier
     * |null
     */
    public function getSpec()
    {
        return Spec::orderBy('published_at', 'DESC');
    }

}