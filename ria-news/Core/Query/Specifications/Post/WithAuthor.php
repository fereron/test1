<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;
use Yii;

/**
 * Class WithAuthor
 * @package Ria\News\Core\Query\Specifications\Post
 */
class WithAuthor extends BaseSpecification
{
    /**
     * @var string|null
     */
    private $language;

    /**
     * @var int
     */
    private $id;

    /**
     * WithAuthor constructor.
     * @param array|string $params
     */
    public function __construct($params)
    {
        parent::__construct(($params['dqlAlias']) ?? null);

        $this->language = $params['language'] ?? Yii::$app->language;
        $this->id       = $params['id'] ?? null;
    }

    /**
     * @return \Happyr\DoctrineSpecification\Filter\Filter
     * |\Happyr\DoctrineSpecification\Logic\AndX
     * |\Happyr\DoctrineSpecification\Query\QueryModifier
     * |null
     */
    public function getSpec()
    {
        return Spec::andX(
            Spec::innerJoin('author', 'a'),
            Spec::innerJoin('translations', 'at', 'a'),
            $this->id == null ?: Spec::eq('id', $this->id, 'a'),
            Spec::eq('language', $this->language, 'at'),
            Spec::addSelect(
                Spec::selectAs(Spec::field('first_name', 'at'), 'author_first_name'),
                Spec::selectAs(Spec::field('last_name', 'at'), 'author_last_name'),
                Spec::selectAs(Spec::field('position', 'at'), 'author_position'),
                Spec::selectAs(Spec::field('slug', 'at'), 'author_slug'),
                Spec::selectAs(Spec::field('thumb', 'a'), 'author_thumb'),
            )
        );
    }

}