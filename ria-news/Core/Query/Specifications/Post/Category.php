<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

/**
 * Class Category
 * @package Ria\News\Core\Query\Specifications\Post
 */
class Category extends BaseSpecification
{
    /**
     * @var string|array
     */
    private $categoryId;

    /**
     * Limit constructor.
     * @param string|array $categoryId
     * @param null $dqlAlias
     */
    public function __construct($categoryId, $dqlAlias = null)
    {
        $this->categoryId = $categoryId;
        parent::__construct($dqlAlias);
    }

    /**
     * @return \Happyr\DoctrineSpecification\Filter\Comparison|\Happyr\DoctrineSpecification\Filter\Filter|\Happyr\DoctrineSpecification\Filter\In|\Happyr\DoctrineSpecification\Query\QueryModifier|null
     */
    public function getSpec()
    {
        return Spec::in('category', array_wrap($this->categoryId));
    }

}