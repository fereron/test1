<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

class IsActual extends BaseSpecification
{

    public function getSpec()
    {
        return Spec::eq('is_actual', true);
    }

}