<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Query\Select as QuerySelect;
use Happyr\DoctrineSpecification\Query\Selection\SelectAs;
use Ria\Core\Query\IdentitySelect;

class Select extends BaseSpecification
{

    /**
     * @var array
     */
    private $fields;

    /**
     * Select constructor.
     * @param array $fields
     * @param null $dqlAlias
     */
    public function __construct(array $fields = [], $dqlAlias = null)
    {
        parent::__construct($dqlAlias);
        $this->fields = $fields;
    }

    public function getSpec()
    {
        return new QuerySelect($this->fields ?: $this->getDefaultFields());
    }

    /**
     * @return array
     */
    private function getDefaultFields(): array
    {
        return [
            'id', 'title', 'slug', 'published_at', 'image', 'type', 'description', 'language'
        ];
    }

}