<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;
use Ria\Core\Query\IdentitySelect;

/**
 * Class WithCategory
 * @package Ria\News\Core\Query\Specifications\Post
 */
class WithCategory extends BaseSpecification
{
    /**
     * @var string|null
     */
    private $language;

    /**
     * WithCategory constructor.
     * @param string|null $language
     * @param null $dqlAlias
     */
    public function __construct(?string $language = null, $dqlAlias = null)
    {
        parent::__construct($dqlAlias);
        $this->language = $language ?: \Yii::$app->language;
    }

    /**
     * @return \Happyr\DoctrineSpecification\Logic\AndX
     */
    public function getSpec()
    {
        return Spec::andX(
            Spec::addSelect(
                Spec::selectAs(new IdentitySelect('category'), 'category_id'),
            ),
        );
    }

}