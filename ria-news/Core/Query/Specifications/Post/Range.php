<?php

namespace Ria\News\Core\Query\Specifications\Post;

use Happyr\DoctrineSpecification\BaseSpecification;
use Happyr\DoctrineSpecification\Spec;

/**
 * Class Range
 * @package Ria\News\Core\Query\Specifications\Post
 */
class Range extends BaseSpecification
{

    /**
     * @var array
     */
    public $rangeAsArray = [];

    /**
     * Limit constructor.
     * @param array $rangeAsArray
     * @param null $dqlAlias
     */
    public function __construct(array $rangeAsArray, $dqlAlias = null)
    {
        $this->rangeAsArray = $rangeAsArray;
        parent::__construct($dqlAlias);
    }

    /**
     * @return \Happyr\DoctrineSpecification\Filter\Filter|
     * \Happyr\DoctrineSpecification\Logic\AndX|
     * \Happyr\DoctrineSpecification\Query\QueryModifier|null
     */
    public function getSpec()
    {
        return Spec::andX(
            Spec::gte('published_at', $this->rangeAsArray[0]),
            Spec::lte('published_at', $this->rangeAsArray[1]),
        );
    }

}