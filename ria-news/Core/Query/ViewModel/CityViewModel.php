<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\ViewModel;

use Ria\Core\Query\ViewModel;

/**
 * Class CityViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property RegionViewModel|null $region
 * @property PostViewModel[] $posts
 */

class CityViewModel extends ViewModel
{

}