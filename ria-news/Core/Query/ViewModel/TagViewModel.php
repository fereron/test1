<?php

namespace Ria\News\Core\Query\ViewModel;

use Ria\Core\Query\ViewModel;

/**
 * Class TagViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 */
class TagViewModel extends ViewModel
{

}