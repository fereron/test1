<?php

namespace Ria\News\Core\Query\ViewModel;

use Ria\Core\Helpers\StringHelper;
use Ria\Core\Query\ViewModel;

/**
 * Class StoryViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property-read int $id
 * @property-read string $title
 * @property-read string $description
 * @property-read string $slug
 * @property-read string|null $cover
 * @property PostViewModel[] $posts
 */
class StoryViewModel extends ViewModel
{

    /**
     * @return string
     */
    public function getTitleWithoutQuotes(): string
    {
        return StringHelper::clearDoubleQuotes($this->title);
    }

    /**
     * @param array $posts
     */
    public function setPosts(array $posts)
    {
        $this->posts = $posts;
    }

}