<?php
declare(strict_types=1);

namespace Ria\News\Core\Query\ViewModel;

use DateTime;
use Ria\Core\Helpers\HtmlHelper;
use Ria\Core\Helpers\StringHelper;
use Ria\Core\Models\Meta;
use Ria\Core\Query\ViewModel;
use Ria\News\Core\Components\ExpertQuoteApplicator;
use Ria\News\Core\Components\MediaWidgetsApplicator\AmpApplicator;
use Ria\News\Core\Components\MediaWidgetsApplicator\InTextApplicator;
use Ria\News\Core\Components\PostPhotosApplicator;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\helpers\Html;

/**
 * Class PostViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property int $id
 * @property int $group_id
 * @property int $category_id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property array $marked_words
 * @property string $type
 * @property string $status
 * @property string $icon
 * @property string $content
 * @property string $language
 * @property bool $is_published
 * @property string $category_title
 * @property string $category_slug
 * @property DateTime $published_at
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property string|null $image
 * @property string|null $youtube_id
 * @property bool $is_main
 * @property bool $is_exclusive
 * @property bool $is_actual
 * @property bool $is_breaking
 * @property bool $has_audio
 * @property bool $is_important
 * @property bool $links_noindex
 * @property integer $views
 * @property string $author_name
 * @property string $author_first_name
 * @property string $author_last_name
 * @property string $author_slug
 * @property string|null $author_thumb
 * @property string|null $author_position
 * @property string $expert_first_name
 * @property string $expert_last_name
 * @property string $expert_slug
 * @property string|null $expert_thumb
 * @property string|null $expert_position
 * @property string|null $image_author
 * @property string|null $image_information
 * @property array $tags
 * @property Meta $meta
 */
class PostViewModel extends ViewModel
{

    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $category_slug;

    /**
     * @var string
     */
    public $marked_words;

    /**
     * @return bool
     */
    public function isPhoto(): bool
    {
        return $this->type == Type::PHOTO;
    }

    /**
     * @return string
     */
    public function getPreparedTitle(): string
    {
        $title = $this->getTitleWithType() . $this->getMarkedWords();

        if ($this->hasAudio()) {
            $title .= '<span class="icon"><i class="icon-volume-on"></i></span>';
        }

        return $title;
    }

    /**
     * @return string
     */
    public function getTitleWithType(): string
    {
        return $this->getTypes() . $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        if (empty($this->description)) {
            $this->description = StringHelper::clearDoubleQuotes(StringHelper::getFirstParagraph($this->content) ?? '');
        }
        return $this->description;
    }

    /**
     * @return string
     */
    public function getTypes(): string
    {
        $tags = [];

        if ($this->type == Type::PHOTO) {
            $tags[] = Html::tag('span', Yii::t('common', Type::PHOTO), ['class' => 'type']);
        }

        if ($this->type == Type::VIDEO || $this->hasYoutubeVideo()) {
            $tags[] = Html::tag('span', Yii::t('common', Type::VIDEO), ['class' => 'type']);
        }

        return implode('', $tags);
    }

    /**
     * @return string
     */
    public function getMarkedWords(): string
    {
        $tags = [];
        if (!empty($this->marked_words)) {
            foreach ($this->marked_words as $word) {
                $tags[] = Html::tag('span', $word, ['class' => 'official']);
            }
        }
        return implode('', $tags);
    }

    /**
     * @return string
     */
    public function getTitleWithoutQuotes(): string
    {
        return StringHelper::clearDoubleQuotes($this->title);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getPreparedContent(): string
    {
        /** @var InTextApplicator $widgetsApplicator */
        $widgetsApplicator = Yii::$container->get(InTextApplicator::class);
        $widgetsApplicator->setContent($this->content);
        $widgetsApplicator->apply();
        $content = $widgetsApplicator->getContent();

        /** @var PostPhotosApplicator $photosApplicator */
        $photosApplicator = Yii::$container->get(PostPhotosApplicator::class);
        $content          = $photosApplicator->apply($content);

        /** @var ExpertQuoteApplicator $quotesApplicator */
        $quotesApplicator = Yii::$container->get(ExpertQuoteApplicator::class);
        $content          = $quotesApplicator->apply($content);

        if ($this->links_noindex) {
            $content = HtmlHelper::addNoindexToExternalLinks($content, Yii::$app->params['frontendHostInfo']);
        }

        $content = HtmlHelper::clearEmptyParagraphs($content);

        return $content;
    }

    /**
     * @return string
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getPreparedAmpContent(): string
    {
        /** @var PostPhotosApplicator $photosApplicator */
        $photosApplicator = Yii::$container->get(PostPhotosApplicator::class);
        $content          = $photosApplicator->apply($this->content);

        /** @var AmpApplicator $widgetsApplicator */
        $widgetsApplicator = Yii::$container->get(AmpApplicator::class);
        $widgetsApplicator->setContent($content);
        $widgetsApplicator->apply();
        $content = $widgetsApplicator->getContent();

        /** @var ExpertQuoteApplicator $quotesApplicator */
        $quotesApplicator = Yii::$container->get(ExpertQuoteApplicator::class);
        $content          = $quotesApplicator->apply($content);

        $content = HtmlHelper::replaceImgToAmpImg($content, ['width' => 610, 'height' => 486, 'layout' => 'responsive']);
        $content = HtmlHelper::removeIframesWithHttpSource($content);
        $content = HtmlHelper::replaceIframeToAmpIframe($content, [
            'width'       => 610,
            'height'      => 486,
            'layout'      => 'responsive',
            'frameborder' => 0,
            'sandbox'     => 'allow-scripts allow-same-origin'
        ]);
        $content = HtmlHelper::removeStyleAttributes($content);

        $content = HtmlHelper::clearEmptyParagraphs($content);

        return $content;
    }

    /**
     * @return bool
     */
    public function hasImage(): bool
    {
        return !empty($this->image);
    }

    /**
     * @return bool
     */
    public function hasYoutubeVideo(): bool
    {
        return !empty($this->youtube_id);
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->is_published;
    }

    /**
     * @return bool
     */
    public function isOpinion(): bool
    {
        return $this->type === Type::OPINION;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->status == Status::PRIVATE;
    }

    /**
     * @return bool
     */
    public function isFuture(): bool
    {
        return $this->published_at->getTimestamp() > time();
    }

    /**
     * @return string
     */
    public function getAuthorName(): string
    {
        return $this->author_first_name . ' ' . $this->author_last_name;
    }

    public function getExpertName(): string
    {
        return $this->expert_first_name . ' ' . $this->expert_last_name;
    }

    /**
     * @return bool
     */
    public function hasAudio(): bool
    {
        return isset($this->has_audio) ? (bool) $this->has_audio : false;
    }

}