<?php

namespace Ria\News\Core\Query\ViewModel;

use Ria\Core\Query\ViewModel;
use Ria\News\Core\Models\Widget\Type;

/**
 * Class WidgetViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property-read int $id
 * @property-read string $type
 * @property-read string $content
 */
class WidgetViewModel extends ViewModel
{

    /**
     * @return bool
     */
    public function isYoutube(): bool
    {
        return $this->type == Type::YOUTUBE;
    }

    /**
     * @return bool
     */
    public function isInstagram(): bool
    {
        return $this->type == Type::INSTAGRAM;
    }

}