<?php

namespace Ria\News\Core\Query\ViewModel;

use Ria\Core\Models\Meta;
use Ria\Core\Query\ViewModel;

/**
 * Class CategoryViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string $slug
 * @property Meta $meta
 * @property CategoryViewModel|null $parent
 * @property PostViewModel[] $posts
 */
class CategoryViewModel extends ViewModel
{
    /**
     * @param array $posts
     */
    public function setPosts(array $posts)
    {
        $this->posts = $posts;
    }

}