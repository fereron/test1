<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\ViewModel;

use Ria\Core\Query\ViewModel;

/**
 * Class RegionViewModel
 * @package Ria\News\Core\Query\ViewModel
 *
 * @property int $id
 * @property string $title
 */

class RegionViewModel extends ViewModel
{

}