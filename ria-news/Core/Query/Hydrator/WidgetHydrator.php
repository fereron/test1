<?php

namespace Ria\News\Core\Query\Hydrator;

use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Query\ViewModel\StoryViewModel;
use Ria\News\Core\Query\ViewModel\WidgetViewModel;

/**
 * Class WidgetHydrator
 * @package Ria\News\Core\Query\Hydrator
 */
class WidgetHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'WidgetHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return WidgetViewModel::class;
    }
}