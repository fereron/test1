<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\Hydrator;

use Doctrine\ORM\Internal\Hydration\HydrationException;
use PDO;
use Ria\Core\Models\Meta;
use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Query\ViewModel\CategoryViewModel;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;

class CategoryHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'CategoryHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return CategoryViewModel::class;
    }

    /**
     * @return array
     * @throws HydrationException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    protected function hydrateAllData()
    {
        $result = [];

        while ($row = $this->_stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrateRowData($row, $result);

            $className  = $this->getViewModelClassName();
            $lastResult = end($result);
            if (array_key_exists('meta', $lastResult)) {
                $metaAsArray = json_decode($lastResult['meta'], true);
                $lastResult['meta'] = new Meta(
                    $metaAsArray['title'],
                    $metaAsArray['description'],
                    $metaAsArray['keywords']
                );
            }

            $parentColumns = [];
            foreach ($lastResult as $column => $value) {
                if (strpos($column, 'parent_') !== false && !is_null($value)) {
                    $parentColumns[str_replace('parent_', '', $column)] = $value;
                    unset($lastResult[$column]);
                }
            }

            if (!empty($parentColumns)) {
                $lastResult['parent'] = Yii::$container->get($className, [], $parentColumns);
            }

            $result[array_key_last($result)] = new $className($lastResult);
        }

        return $result;
    }
}
