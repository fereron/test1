<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\Hydrator;

use PDO;
use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Query\ViewModel\TagViewModel;

class TagHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'TagHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return TagViewModel::class;
    }

}
