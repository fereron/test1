<?php

namespace Ria\News\Core\Query\Hydrator;

use Doctrine\ORM\Internal\Hydration\HydrationException;
use PDO;
use Ria\Core\Models\Meta;
use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Query\ViewModel\PostViewModel;

class PostsHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'PostHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return PostViewModel::class;
    }

    /**
     * @return array
     * @throws HydrationException
     */
    protected function hydrateAllData()
    {
        $result = [];

        while ($row = $this->_stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrateRowData($row, $result);

            $className = $this->getViewModelClassName();
            $lastResult = end($result);

            if (isset($lastResult['content.content'])) {
                $lastResult['content'] = $lastResult['content.content'];
                unset($lastResult['content.content']);
            }

            if (array_key_exists('meta', $lastResult)) {
                $metaAsArray = json_decode($lastResult['meta'], true);
                $lastResult['meta'] = new Meta(
                    $metaAsArray['title'],
                    $metaAsArray['description'],
                    $metaAsArray['keywords']
                );
            }

            $result[array_key_last($result)] = new $className($lastResult);
        }

        if (!(isset($result[0]) && isset($result[0]->category_id))) {
            return $result;
        }

        $language = isset($result[0]->language) ? $result[0]->language : \Yii::$app->language;
        $categoryIds = collect($result)->pluck('category_id')->toArray();
        $categories = collect(
            $this->_em->getRepository(Category::class)->getForPosts($categoryIds, $language)
        );

        foreach ($result as &$post) {
            $postCategory = $categories->where('category_id', $post->category_id)->first();
            $post->category_slug = $postCategory['category_slug'];
            $post->category_title = $postCategory['category_title'];
        }

        return $result;
    }
}