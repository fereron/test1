<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\Hydrator;

use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Query\ViewModel\CityViewModel;

class CityHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'CityHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return CityViewModel::class;
    }

}