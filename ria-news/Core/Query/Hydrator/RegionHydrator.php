<?php

declare(strict_types=1);

namespace Ria\News\Core\Query\Hydrator;

use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Query\ViewModel\RegionViewModel;

class RegionHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'RegionHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return RegionViewModel::class;
    }

}