<?php

namespace Ria\News\Core\Query\Hydrator;

use Ria\Core\Query\ViewModelHydrator;
use Ria\News\Core\Query\ViewModel\StoryViewModel;

class StoryHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'StoryHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return StoryViewModel::class;
    }
}