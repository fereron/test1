<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Story;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Models\Meta;
use Ria\News\Core\Commands\Story\CreateStoryCommand;
use Ria\News\Core\Commands\Story\UpdateStoryCommand;
use Ria\News\Core\Models\Story\Translation;
use Ria\News\Core\Models\Story\Story;

class StoryHandler extends EntityHandler
{
    /**
     * @param CreateStoryCommand $command
     */
    public function create(CreateStoryCommand $command)
    {
        $this->persistModel($command);
    }

    /**
     * @param UpdateStoryCommand $command
     */
    public function update(UpdateStoryCommand $command)
    {
        $this->persistModel($command);
    }

    /**
     * @param $command
     */
    public function persistModel($command)
    {
        $story = $command->id
            ? $this->find(Story::class, $command->id)
            : new Story;

        $story->setStatus($command->status);
        $story->setShowOnSite($command->showOnSite);
        $story->setCover($command->cover);

        foreach ($command->translations as $translationParams) {
            $translation = $story->getTranslation($translationParams['language']) ?: new Translation();

            $translation
                ->setTitle($translationParams['title'])
                ->setSlug($translationParams['slug'])
                ->setDescription($translationParams['description'])
                ->setLanguage($translationParams['language'])
                ->setMeta(Meta::fromArray($translationParams['meta']));

            $story->addTranslation($translation);
        }

        $this->persist($story);
    }

}