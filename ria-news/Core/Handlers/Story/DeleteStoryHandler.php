<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Story;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Story\DeleteStoryCommand;
use Ria\News\Core\Models\Story\Story;

class DeleteStoryHandler extends EntityHandler
{
    public function handle(DeleteStoryCommand $command): void
    {
        $this->remove($this->find(Story::class, $command->id));
    }
}