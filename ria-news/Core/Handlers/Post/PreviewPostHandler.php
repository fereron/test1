<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Post;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Ria\Core\Components\CommandBus\Handlers\HandlerException;
use Ria\News\Core\Commands\Post\PreviewPostCommand;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Tag\Tag;
use Ria\News\Core\Query\ViewModel\PostViewModel;
use Ria\Users\Core\Models\User;
use yii\caching\CacheInterface;

/**
 * Class PreviewPostHandler
 * @package Ria\News\Core\Handlers\Post
 */
class PreviewPostHandler
{
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PreviewPostHandler constructor.
     * @param CacheInterface $cache
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(CacheInterface $cache, EntityManagerInterface $entityManager)
    {
        $this->cache         = $cache;
        $this->entityManager = $entityManager;
    }

    /**
     * @param PreviewPostCommand $command
     */
    public function handle(PreviewPostCommand $command)
    {
        if ($this->cache->exists(['preview', $command->key])) {
            return;
        }

        if ($command->id) {
            /** @var Post $post */
            $post = $this->entityManager->find(Post::class, $command->id);
            $data = $this->setFromPost($post);
        } else {
            $data = $this->setFromCommand($command);
        }

        $set = $this->cache->set(['preview', $command->key], $data, 3600);

        if (!$set) {
            throw new HandlerException('Set preview cache error for command with id: ' . $command->key);
        }
    }

    /**
     * @param Post $post
     * @return PostViewModel
     */
    public function setFromPost(Post $post)
    {
        $tags = [];
        /** @var Tag[] $postTags */
        $postTags = $post->getTags();
        foreach ($postTags as $tag) {
            if ($tag->getTranslation($post->getLanguage())->getCount() > 2) {
                $tags[] = [
                    'name' => $tag->getTranslation($post->getLanguage())->getName(),
                    'slug' => $tag->getSlug()
                ];
            }
        }

        return new PostViewModel([
            'id'                => $post->getId(),
            'group_id'          => $post->getGroupId(),
            'category_id'       => $post->getCategory()->getId(),
            'title'             => $post->getTitle(),
            'description'       => $post->getDescription(),
            'slug'              => $post->getSlug(),
            'marked_words'      => $post->getMarkedWords(),
            'type'              => $post->getType(),
            'status'            => $post->getStatus(),
            'icon'              => $post->getIcon(),
            'content'           => (string)$post->getContent(),
            'language'          => $post->getLanguage(),
            'is_published'      => $post->isPublished(),
            'category_title'    => $post->getCategory()->getTranslation($post->getLanguage())->getTitle(),
            'category_slug'     => $post->getCategory()->getTranslation($post->getLanguage())->getSlug(),
            'published_at'      => $post->getPublishedAt(),
            'created_at'        => $post->getCreatedAt(),
            'updated_at'        => $post->getUpdatedAt(),
            'image'             => $post->getImage(),
            'youtube_id'        => $post->getYoutubeId(),
            'is_main'           => $post->getIsMain(),
            'is_exclusive'      => $post->getIsExclusive(),
            'is_actual'         => $post->getIsActual(),
            'is_breaking'       => $post->getIsBreaking(),
            'links_noindex'     => $post->getLinksNoindex(),
            'views'             => $post->getViews(),
            'author_name'       => $post->getAuthor()->getTranslation($post->getLanguage())->getFullName(),
            'author_first_name' => $post->getAuthor()->getTranslation($post->getLanguage())->getFirstName(),
            'author_last_name'  => $post->getAuthor()->getTranslation($post->getLanguage())->getLastName(),
            'author_slug'       => $post->getAuthor()->getTranslation($post->getLanguage())->getSlug(),
            'author_thumb'      => $post->getAuthor()->getThumb(),
            'tags'              => $tags,
            'meta'              => $post->getMeta(),
            'speech_filename'   => null
        ]);
    }

    /**
     * @param PreviewPostCommand $command
     * @return PostViewModel
     */
    public function setFromCommand(PreviewPostCommand $command)
    {
        $category = $command->categoryId ? $this->entityManager->find(Category::class, $command->categoryId) : null;
        $author   = $command->authorId ? $this->entityManager->find(User::class, $command->authorId) : null;

        return new PostViewModel([
            'id'                => (int)$command->id,
            'group_id'          => $command->groupId,
            'category_id'       => $command->categoryId,
            'title'             => $command->title,
            'description'       => $command->description,
            'slug'              => $command->slug,
            'marked_words'      => $command->marked_words,
            'type'              => $command->type,
            'status'            => $command->status,
            'icon'              => $command->icon,
            'content'           => (string)$command->content,
            'language'          => $command->language,
            'category_title'    => $category ? $category->getTranslation($command->language)->getTitle() : null,
            'category_slug'     => $category ? $category->getTranslation($command->language)->getSlug() : null,
            'is_published'      => true,
            'published_at'      => $command->publishedAt,
            'created_at'        => new DateTime(),
            'updated_at'        => new DateTime(),
            'image'             => $command->image,
            'youtube_id'        => $command->youtubeId,
            'is_main'           => $command->isMain,
            'is_exclusive'      => $command->isExclusive,
            'is_actual'         => $command->isActual,
            'is_breaking'       => $command->isBreaking,
            'links_noindex'     => $command->linksNoindex,
            'views'             => 0,
            'author_name'       => $author ? $author->getTranslation($command->language)->getFullName() : null,
            'author_first_name' => $author ? $author->getTranslation($command->language)->getFirstName() : null,
            'author_last_name'  => $author ? $author->getTranslation($command->language)->getLastName() : null,
            'author_slug'       => $author ? $author->getTranslation($command->language)->getSlug() : null,
            'author_thumb'      => $author ? $author->getThumb() : null,
            'tags'              => $command->tags,
            'meta'              => $command->meta,
            'speech_filename'   => null
        ]);
    }
}