<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Post;

use Doctrine\ORM\EntityManagerInterface;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Components\CommandBus\Handlers\HandlerException;
use Ria\News\Core\Commands\Post\MoveDateCommand;
use Ria\News\Core\Models\Post\Post;
use Yii;
use yii\mail\MailerInterface;

/**
 * Class MoveDateHandler
 * @package Ria\News\Core\Handlers\Post
 */
class MoveDateHandler extends EntityHandler
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * MoveDateHandler constructor.
     * @param MailerInterface $mailer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(MailerInterface $mailer, EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->mailer = $mailer;
    }

    /**
     * @param MoveDateCommand $command
     */
    public function handle(MoveDateCommand $command)
    {
        /** @var Post $post */
        $post = $this->find(Post::class, $command->id);
        $post->setPublishedAt($command->date);
        $post->setIsPublished($command->date->getTimestamp() <= time());
        $this->persist($post);

        $mailer = $this->mailer->compose(
            ['html' => 'post/date-moved-html', 'text' => 'post/date-moved-text'],
            ['post' => $post, 'cause' => $command->cause]
        )
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Post publish date moved | [ ' . Yii::$app->name . ' ]');

        if (!$mailer->send()) {
            throw new HandlerException('Post with id: ' . $command->id . ' - move date mail send error');
        }
    }
}