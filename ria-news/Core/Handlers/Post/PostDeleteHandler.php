<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Post;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Components\Dispatcher\EventDispatcher;
use Ria\News\Core\Commands\Post\DeletePostCommand;
use Ria\News\Core\Models\Post\Events\PostDeleted;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\Status;
use Yii;
use yii\db\Exception;

/**
 * Class PostDeleteHandler
 * @package Ria\News\Core\Handlers\Post
 */
class PostDeleteHandler extends EntityHandler
{
    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * PostDeleteHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcher $dispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcher $dispatcher)
    {
        parent::__construct($entityManager);
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param DeletePostCommand $command
     * @throws Exception
     */
    public function handle(DeletePostCommand $command)
    {
        /** @var Post $post */
        $post = $this->entityManager->getRepository(Post::class)->find($command->id);

        if (!empty($command->status)) {
            $post->setStatus(new Status($command->status));
            $post->setIsPublished(false);
            $this->persist($post);
        }

        $this->flushCache($post);

        $this->dispatcher->dispatch(new PostDeleted($post, $command->user, $command->cause));
    }

    /**
     * @param Post $post
     * @throws Exception
     */
    private function flushCache(Post $post)
    {
        $this->flushPostPageCache($post);

        if ($post->getIsMain()) {
            $this->flushMainPageCache($post->getLanguage());
        }

        $this->flushFeedPage($post->getLanguage());
        $this->flushLastNewsCache($post->getLanguage());
    }

    /**
     * @param string $language
     */
    private function flushMainPageCache(string $language)
    {
        foreach (['default', 'mobile'] as $theme) {
            Yii::$app->redis->expire('cache_' . md5(sprintf('app/.%s.%s', $theme, $language)), 1);
        }
    }

    /**
     * @param string $language
     */
    private function flushFeedPage(string $language)
    {
        $feedPageUrls = [
            'az' => '/son-xeberler/',
            'ru' => '/ru/последние-новости/',
            'en' => '/en/latest-news/'
        ];

        Yii::$app->redis->expire('cache_' . md5(sprintf('app%s.default.%s', $feedPageUrls[$language], $language)), 1);
    }

    /**
     * @param string $language
     */
    private function flushLastNewsCache(string $language)
    {
        foreach (['default', 'mobile'] as $theme) {
            foreach (['index', 'post', 'amp'] as $type) {
                $langPrefix = $language == Yii::$app->params['defaultLanguage'] ? '' : '/' . $language;

                Yii::$app->redis->expire(
                    'cache_' . md5(sprintf('app%s/getLastNews/%s/.%s.%s', $langPrefix, $type, $theme, $language)),
                    90
                );
            }
        }
    }

    /**
     * @param Post $post
     * @throws Exception
     */
    private function flushPostPageCache(Post $post)
    {
        $langPrefix   = ($post->getLanguage() != Yii::$app->params['defaultLanguage']) ? ($post->getLanguage() . '/') : '';
        $postSlug     = $post->getSlug();
        $category     = $post->getCategory();
        $categorySlug = $category->getTranslation($post->getLanguage())->getSlug();

        foreach (['default', 'mobile'] as $theme) {
            // Здесь по каким-то причинам с ttl 200 не сбрасывал кеш, поэтому поставили 2 секунды :))
            // Сбрасывания кеша должно быть тут, не удалять :)
            Yii::$app->redis->executeCommand('EXPIRE', ['cache_' . md5("app/{$langPrefix}{$categorySlug}/" . $postSlug . "/.{$theme}.{$post->getLanguage()}"), 2]);
            Yii::$app->redis->executeCommand('EXPIRE', ['cache_' . md5("app/{$langPrefix}amp/{$categorySlug}/" . $postSlug . "/.{$theme}.{$post->getLanguage()}"), 2]);

            Yii::$app->redis->executeCommand('EXPIRE', ['cache_' . md5('app/' . $categorySlug . '/.' . $theme . '.' . $post->getLanguage()), 60]);
        }
    }
}