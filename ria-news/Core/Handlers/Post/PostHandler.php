<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Post;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Components\Dispatcher\EventDispatcher;
use Ria\News\Core\Commands\Post\CreatePostCommand;
use Ria\News\Core\Commands\Post\PostCommand;
use Ria\News\Core\Commands\Post\UpdatePostCommand;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Models\Post\Events\PostCreated;
use Ria\News\Core\Models\Post\Events\PostSaved;
use Ria\News\Core\Models\Post\Events\PostUpdated;
use Ria\News\Core\Models\Post\Export;
use Ria\News\Core\Models\Post\Note;
use Ria\News\Core\Models\Post\Plain;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Story\Story;
use Ria\News\Core\Models\Tag\Tag;
use Ria\News\Core\Models\Tag\Translation;
use Ria\Persons\Core\Models\Person;
use Ria\Users\Core\Helpers\Identity;
use Ria\Users\Core\Models\User;
use Ria\Photos\Core\Models\Photo\Photo;
use Yii;

/**
 * Class PostCreateHandler
 * @package Ria\News\Core\Handlers\Post
 */
class PostHandler extends EntityHandler
{

    /**
     * @var EventDispatcher
     */
    private $dispatcher;
    /**
     * @var User
     */
    private $user;

    /**
     * PostHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcher $dispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcher $dispatcher)
    {
        parent::__construct($entityManager);
        $this->dispatcher = $dispatcher;
    }

    /**
     * @var PostCommand
     */
    private $command;

    /**
     * @param CreatePostCommand $command
     * @throws ORMException
     */
    public function create(CreatePostCommand $command)
    {
        $this->command = $command;
        $post          = $this->persistModel();
        $this->dispatcher->dispatch(new PostCreated($post, Yii::$app->user->identity->getUser()));
    }

    /**
     * @param UpdatePostCommand $command
     * @throws ORMException
     */
    public function update(UpdatePostCommand $command)
    {
        $this->command = $command;

        /** @var Post $post */
        $post = $this->entityManager->getRepository(Post::class)->find($this->command->id);
        $old  = clone $post;
        $new  = $this->persistModel($post);

        /** @noinspection PhpNonStrictObjectEqualityInspection */
        $this->dispatcher->dispatch(new PostUpdated($new, Yii::$app->user->identity->getUser(), Plain::fromEntity($old), $new == $old));
    }

    /**
     * @param Post|null $post
     * @return Post
     * @throws ORMException
     */
    private function persistModel(?Post $post = null)
    {
        $post = $post ?: new Post;

        $post = $post
            ->setLanguage($this->command->language)
            ->setType($this->command->type)
            ->setOptionType($this->command->optionType)
            ->setIcon($this->command->icon)
            ->setCategory(
                $this->entityManager->getReference(Category::class, $this->command->categoryId)
            )
            ->setAuthor(
                $this->entityManager->getReference(User::class, $this->command->authorId)
            )
            ->setExpert(
                $this->command->expertId
                    ? $this->entityManager->getReference(Person::class, $this->command->expertId)
                    : null
            )
            ->setTranslator(
                $this->command->translatorId
                    ? $this->entityManager->getReference(User::class, $this->command->translatorId)
                    : null
            )
            ->setCity(
                $this->command->cityId
                    ? $this->entityManager->getReference(City::class, $this->command->cityId)
                    : null
            )
            ->setGroupId($this->command->groupId)
            ->setStory(
                $this->command->storyId
                    ? $this->entityManager->getReference(Story::class, $this->command->storyId)
                    : null
            )
            ->setTitle($this->command->title)
            ->setDescription($this->command->description)
            ->setContent($this->command->content)
            ->setYoutubeId($this->command->youtubeId)
            ->setSlug($this->command->slug)
            ->setSource($this->command->source)
            ->setMeta($this->command->meta)
            ->setStatus($this->command->status)
            ->setImage($this->command->image)
            ->setCreatedBy($this->command->createdBy)
            ->setIsMain($this->command->isMain)
            ->setIsExclusive($this->command->isExclusive)
            ->setIsActual($this->command->isActual)
            ->setIsBreaking($this->command->isBreaking)
            ->setIsImportant($this->command->isImportant)
            ->setLinksNoindex($this->command->linksNoindex)
            ->setIsPublished($this->command->status->isActive() && $this->command->publishedAt->getTimestamp() <= time())
            ->setPublishedAt($this->command->publishedAt)
            ->setMarkedWords($this->command->marked_words);

        if (!$post->photosAreEqual($this->command->photos)) {
            $post->setPhotoRelations($this->preparePhotos($post));
        }

        $post->sync('exports', $this->prepareExports($post));
        $post->sync('related', $this->prepareRelationCollection(Post::class, $this->command->related));
        $post->syncTags($this->prepareTags());
        $post->syncPersons($this->preparePersons());

        $this->persist($post);
        $this->addNote($post);
        $this->dispatcher->dispatch(new PostSaved($post, Yii::$app->user->identity->getUser()));

        return $post;
    }

    /**
     * @return ArrayCollection
     */
    private function prepareTags(): ArrayCollection
    {
        $tags = new ArrayCollection();
        foreach ($this->command->tags as $tagName) {
            $slug = (new Slugify())->slugify($tagName, ['separator' => '-']);
            $tag  = $this->entityManager->getRepository(Tag::class)->findOneBy(['slug' => $slug]);

            if (!$tag) {
                $tag = new Tag();
                $tag->setSlug($slug);
            }

            $translation = $tag->getTranslations()
                ->filter(function ($translation) {
                    return $translation->language == $this->command->language;
                })
                ->first();

            if (empty($translation)) {
                $translation = new Translation();
                $translation
                    ->setName($tagName)
                    ->setLanguage($this->command->language)
                    ->setTag($tag);

                $tag->addTranslation($translation);
            }

            $tags->add($tag);
        }

        return $tags;
    }

    /**
     * @return ArrayCollection
     * @throws ORMException
     */
    private function preparePersons(): ArrayCollection
    {
        $persons = new ArrayCollection();

        foreach ($this->command->persons as $personId) {
            $person = $this->entityManager->getReference(Person::class, $personId);
            $persons->add($person);
        }

        return $persons;
    }

    /**
     * @param Post $post
     * @return ArrayCollection
     */
    private function preparePhotos(Post $post): ArrayCollection
    {
        $photoRelations = new ArrayCollection();
        foreach ($this->command->photos as $i => $photoId) {
            $photoRelation = new \Ria\News\Core\Models\Post\Photo();
            $photoRelation
                ->setPhoto($this->entityManager->getRepository(Photo::class)->find($photoId))
                ->setPost($post)
                ->setSort($i + 1);

            $photoRelations->add($photoRelation);
        }

        return $photoRelations;
    }

    /**
     * @param Post $post
     * @return ArrayCollection
     */
    private function prepareExports(Post $post): ArrayCollection
    {
        $exports = new ArrayCollection();
        foreach ($this->command->exports as $exportType) {
            $export = $post->getId()
                ? $this->entityManager->getRepository(Export::class)->findOneBy(['post' => $post->getId(), 'type' => $exportType])
                : null;

            if (!$export) {
                $export = new Export();
                $export->setType($exportType);
                $post->addExport($export);
            }

            $exports->add($export);
        }

        return $exports;
    }


    /**
     * @param Post $post
     */
    private function addNote(Post $post): void
    {
        /** @var Note $note */
        $note = $this->entityManager->getRepository(Note::class)
            ->findOneBy(['post_group_id' => $post->getGroupId()]);

        if (empty($this->command->note) && empty($note)) {
            return;
        }

        if (!$note) {
            $note = new Note();
            $note->setPostGroupId($post->getGroupId());
        } elseif (empty($this->command->note)) {
            $this->remove($note);
            return;
        }

        $note->setBody($this->command->note);
        $this->persist($note);
    }
}
