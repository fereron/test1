<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Post;

use Doctrine\ORM\EntityManagerInterface;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Components\Dispatcher\EventDispatcher;
use Ria\News\Core\Commands\Post\ArchivePostCommand;
use Ria\News\Core\Models\Post\Events\PostArchived;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\Status;

/**
 * Class ArchivePostHandler
 * @package Ria\News\Core\Handlers\Post
 */
class ArchivePostHandler extends EntityHandler
{
    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * ArchivePostHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcher $dispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcher $dispatcher)
    {
        parent::__construct($entityManager);
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ArchivePostCommand $command
     */
    public function handle(ArchivePostCommand $command)
    {
        /** @var Post $post */
        $post       = $this->find(Post::class, $command->id);
        $prevStatus = $post->getStatus();

        $post->setStatus(new Status(Status::ARCHIVED));
        $post->setIsPublished(false);
        $this->persist($post);

        if ($prevStatus->isActive()) {
            $this->dispatcher->dispatch(new PostArchived($post, $command->url));
        }
    }
}