<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Widget;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Widget\CreateWidgetCommand;
use Ria\News\Core\Models\Widget\Widget;

/**
 * Class CreateWidgetHandler
 * @package Ria\News\Core\Handlers\Widget
 */
class CreateWidgetHandler extends EntityHandler
{

    /**
     * @param CreateWidgetCommand $command
     */
    public function handle(CreateWidgetCommand $command)
    {
        $widget = new Widget();
        $widget
            ->setContent($command->content)
            ->setType($command->type);

        $this->persist($widget);

        $command->id = $widget->getId();
    }

}