<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\ExpertQuote;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\ExpertQuote\CreateQuoteCommand;
use Ria\News\Core\Commands\ExpertQuote\UpdateQuoteCommand;
use Ria\News\Core\Models\ExpertQuote\ExpertQuote;
use Ria\News\Core\Models\Post\Post;
use Ria\Persons\Core\Models\Person;

/**
 * Class UpdateQuoteHandler
 * @package Ria\News\Core\Handlers\ExpertQuote
 */
class UpdateQuoteHandler extends EntityHandler
{

    /**
     * @param UpdateQuoteCommand $command
     * @throws \Doctrine\ORM\ORMException
     */
    public function handle(UpdateQuoteCommand $command)
    {
        /** @var Person $person */
        $person = $this->entityManager->getReference(Person::class, $command->expertId);
        /** @var Post|null $post */
        $post = empty($command->postId) ? null : $this->entityManager->getReference(Post::class, $command->postId);

        $quote = $this->find(ExpertQuote::class, $command->id);
        $quote
            ->setExpert($person)
            ->setPost($post)
            ->setText($command->text);

        $this->persist($quote);
    }

}