<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\ExpertQuote;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\ExpertQuote\CreateQuoteCommand;
use Ria\News\Core\Models\ExpertQuote\ExpertQuote;
use Ria\News\Core\Models\Post\Post;
use Ria\Persons\Core\Models\Person;

/**
 * Class CreateQuoteHandler
 * @package Ria\News\Core\Handlers\ExpertQuote
 */
class CreateQuoteHandler extends EntityHandler
{

    /**
     * @param CreateQuoteCommand $command
     * @throws \Doctrine\ORM\ORMException
     */
    public function handle(CreateQuoteCommand $command)
    {
        /** @var Person $person */
        $person = $this->entityManager->getReference(Person::class, $command->expertId);
        /** @var Post|null $post */
        $post = empty($command->postId) ? null : $this->entityManager->getReference(Post::class, $command->postId);

        $quote = new ExpertQuote();
        $quote
            ->setExpert($person)
            ->setPost($post)
            ->setText($command->text);

        $this->persist($quote);
    }

}