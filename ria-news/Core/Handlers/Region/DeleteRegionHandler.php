<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Region;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Region\DeleteRegionCommand;
use Ria\News\Core\Models\Region\Region;

class DeleteRegionHandler extends EntityHandler
{
    public function handle(DeleteRegionCommand $command): void
    {
        $this->remove($this->find(Region::class, $command->id));
    }
}