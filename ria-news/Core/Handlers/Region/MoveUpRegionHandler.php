<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Region;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Query\Expr;
use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Region\MoveUpRegionCommand;
use Ria\News\Core\Models\Region\Region;

class MoveUpRegionHandler extends EntityHandler
{
    public function handle(MoveUpRegionCommand $command): void
    {
        /** @var Region $region */
        $region = $this->entityManager->getRepository(Region::class)->find($command->getId());

        $newPosition = ($region->getOrder()) > 1 ? $region->getOrder() - 1 : 1;

        $this->reorderOtherItems($region->getOrder(), $newPosition);

        $region->setOrder($newPosition);

        try {
            $this->entityManager->persist($region);
            $this->entityManager->flush();
        } catch (DBALException $e) {
            throw new CommandHandlerException($region, $e->getMessage());
        }
    }

    /**
     * @param int $oldPosition
     * @param int $newPosition
     */
    protected function reorderOtherItems(int $oldPosition, int $newPosition)
    {
        $qb = $this->entityManager->createQueryBuilder();

        // new position smaller than or equal to old position,
        // so all positions from new position up to and including old position - 1 should increment
        $qb
            ->update(Region::class, 'c')
            ->set('c.order', 'c.order + 1')
            ->where('c.order BETWEEN :new AND :old')
            ->setParameters(array_merge([
                ':old' => $oldPosition + 1, ':new' => $newPosition]
            ))
            ->getQuery()
            ->execute();
    }

}