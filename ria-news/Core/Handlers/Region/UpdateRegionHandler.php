<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Region;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Region\UpdateRegionCommand;
use Ria\News\Core\Models\Region\Region;
use Ria\News\Core\Models\Region\Translation;

/**
 * Class UpdateRegionHandler
 * @package Ria\News\Core\Handlers\Region
 */
class UpdateRegionHandler extends EntityHandler
{
    /**
     * @param UpdateRegionCommand $command
     */
    public function handle(UpdateRegionCommand $command)
    {
        $region  = $this->find(Region::class, $command->id);
        $region->set(
            (int)$command->order
        );
        foreach ($command->translations as $translationParams) {
            $translation = $region->getTranslation($translationParams['language']) ?: new Translation();
            $translation->set(
                $translationParams['title'],
                $translationParams['slug'],
                $translationParams['language'],
            );

            $region->addTranslation($translation);
        }

        $this->persist($region);
    }
}