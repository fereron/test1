<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Region;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;

use Ria\News\Core\Commands\Region\CreateRegionCommand;
use Ria\News\Core\Models\Region\Translation;
use Ria\News\Core\Models\Region\Region;

class CreateRegionHandler extends EntityHandler
{
    public function handle(CreateRegionCommand $command)
    {
        $region = new Region();
        $region->set(
            (int)$command->order
        );

        foreach ($command->translations as $translationParams) {
            $translation = (new Translation())->set(
                $translationParams['title'],
                $translationParams['slug'],
                $translationParams['language'],
            );
            $region->addTranslation($translation);
        }

        $this->persist($region);
    }

}