<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\City;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\City\UpdateCityCommand;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Models\City\Translation;
use Ria\News\Core\Models\Region\Region;

class UpdateCityHandler extends EntityHandler
{
    public function handle(UpdateCityCommand $command)
    {
        $region  = $this->find(Region::class, $command->region_id);
        $city    = $this->find(City::class, $command->id);
        $city->set($region);

        foreach ($command->translations as $translationParams) {
            $translation = $city->getTranslation($translationParams['language']) ?: new Translation();
            $translation->set(
                $translationParams['title'],
                $translationParams['slug'],
                $translationParams['language'],
            );
            $city->addTranslation($translation);
        }

        $this->persist($city);
    }
}