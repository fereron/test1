<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\City;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\City\CreateCityCommand;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Models\City\Translation;
use Ria\News\Core\Models\Region\Region;

class CreateCityHandler extends EntityHandler
{
    public function handle(CreateCityCommand $command)
    {
//        $region = $this->entityManager->getRepository(Region::class)->find($command->region_id);
        $region  = $this->find(Region::class, $command->region_id);


        $city = new City();
        $city->set(
            $region
        );

        foreach ($command->translations as $translationParams) {
            $translation = (new Translation())->set(
                $translationParams['title'],
                $translationParams['slug'],
                $translationParams['language'],
            );
            $city->addTranslation($translation);
        }

        $this->persist($city);
    }

}