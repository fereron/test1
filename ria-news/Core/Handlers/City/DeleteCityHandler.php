<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\City;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\City\DeleteCityCommand;
use Ria\News\Core\Models\City\City;

class DeleteCityHandler extends EntityHandler
{
    public function handle(DeleteCityCommand $command): void
    {
        $this->remove($this->find(City::class, $command->id));
    }
}