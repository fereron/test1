<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Category;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Category\DeleteCategoryCommand;
use Ria\News\Core\Models\Category\Category;

class DeleteCategoryHandler extends EntityHandler
{
    public function handle(DeleteCategoryCommand $command): void
    {
        $this->remove($this->find(Category::class, $command->id));
    }
}