<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Category;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Category\MoveDownCategoryCommand;
use Ria\News\Core\Models\Category\Category;

class MoveDownCategoryHandler extends EntityHandler
{
    public function handle(MoveDownCategoryCommand $command): void
    {
        /** @var Category $category */
        $category = $this->entityManager->getRepository(Category::class)->find($command->getId());
        $category->setSort($category->getSort() + 1);

        $this->persist($category);
    }

}