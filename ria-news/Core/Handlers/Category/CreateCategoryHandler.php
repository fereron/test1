<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Category;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Models\Meta;
use Ria\News\Core\Commands\Category\CreateCategoryCommand;
use Ria\News\Core\Models\Category\Translation;
use Ria\News\Core\Models\Category\Category;

class CreateCategoryHandler extends EntityHandler
{
    public function handle(CreateCategoryCommand $command)
    {
        $category = (new Category)
            ->setStatus($command->status)
            ->setTemplate($command->template)
            ->setSort($this->getLastOrder() + 1);

        if ($command->parent_id) {
            $category->setParent($this->entityManager->getReference(Category::class, $command->parent_id));
        }

        foreach ($command->translations as $translationParams) {
            $translation = (new Translation)
                ->setTitle($translationParams['title'])
                ->setSlug($translationParams['slug'])
                ->setLanguage($translationParams['language'])
                ->setMeta(Meta::fromArray($translationParams['meta']));

            $category->addTranslation($translation);
        }

        $this->persist($category);
    }

    /**
     * @return int
     */
    private function getLastOrder(): int
    {
        $result = $this->entityManager->getRepository(Category::class)
            ->createQueryBuilder('c')
            ->select('c.sort')
            ->orderBy('c.sort', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $result ? $result['sort'] : 0;
    }

}