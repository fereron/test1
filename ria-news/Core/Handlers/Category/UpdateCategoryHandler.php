<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Category;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Core\Models\Meta;
use Ria\News\Core\Commands\Category\UpdateCategoryCommand;
use Ria\News\Core\Models\Category\Translation;
use Ria\News\Core\Models\Category\Category;

class UpdateCategoryHandler extends EntityHandler
{
    public function handle(UpdateCategoryCommand $command)
    {
        /** @var Category $category */
        $category  = $this->find(Category::class, $command->id);

        $category
            ->setStatus($command->status)
            ->setTemplate($command->template)
            ->setParent(
                $command->parent_id
                ? $this->entityManager->getReference(Category::class, $command->parent_id)
                : null
            );

        foreach ($command->translations as $translationParams) {
            $translation = $category->getTranslation($translationParams['language']) ?: new Translation();

            $translation->setTitle($translationParams['title'])
                ->setSlug($translationParams['slug'])
                ->setLanguage($translationParams['language'])
                ->setMeta(Meta::fromArray($translationParams['meta']));

            $category->addTranslation($translation);
        }

        $this->persist($category);
    }
}