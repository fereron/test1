<?php
declare(strict_types=1);

namespace Ria\News\Core\Handlers\Speech;

use Doctrine\ORM\EntityManagerInterface;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\News\Core\Commands\Speech\DeleteSpeechCommand;
use Ria\News\Core\Commands\Speech\SpeechCommand;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Speech\Speech;
use Ria\News\Core\Services\FileStorage\SpeechFileStorageInterface;

/**
 * Class SpeechHandler
 * @package Ria\News\Core\Handlers\Speech
 */
class SpeechHandler extends EntityHandler
{

    /**
     * @var SpeechFileStorageInterface
     */
    private $speechFileStorage;

    /**
     * SpeechHandler constructor.
     * @param SpeechFileStorageInterface $fileStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SpeechFileStorageInterface $fileStorage, EntityManagerInterface $entityManager)
    {
        $this->speechFileStorage = $fileStorage;

        parent::__construct($entityManager);
    }

    /**
     * @param SpeechCommand $command
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(SpeechCommand $command)
    {
        $this->speechFileStorage->upload($command->file, $command->filename);

        /** @var Post $post */
        $post = $this->entityManager->getReference(Post::class, $command->postId);

        $entity = new Speech();
        $entity
            ->setFilename($command->filename)
            ->setOriginalFilename($command->originalFilename)
            ->setPost($post);

        $this->persist($entity);
    }

    /**
     * @param SpeechCommand $command
     */
    public function update(SpeechCommand $command)
    {
        /** @var Speech $entity */
        $entity = $this->entityManager->find(Speech::class, $command->id);

        $entity->setOriginalFilename($command->originalFilename);

        $this->persist($entity);

        $this->speechFileStorage->upload($command->file, $command->filename);
    }

    /**
     * @param DeleteSpeechCommand $command
     */
    public function delete(DeleteSpeechCommand $command)
    {
        /** @var Speech $speech */
        $speech = $this->find(Speech::class, $command->getId());
        $this->speechFileStorage->delete($speech->getFilename());
        $this->remove($speech);
    }

}