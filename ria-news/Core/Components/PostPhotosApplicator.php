<?php
declare(strict_types=1);

namespace Ria\News\Core\Components;

use Doctrine\ORM\EntityManagerInterface;
use Ria\Photos\Core\Models\Photo\Photo;
use Ria\Photos\Core\Query\Repositories\PhotosRepository;
use Yii;
use yii\helpers\Html;

/**
 * Class PostPhotosApplicator
 * @package Ria\News\Core\Components
 */
final class PostPhotosApplicator
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PostPhotosApplicator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $content
     * @return string
     */
    public function apply(string $content): string
    {
        preg_match_all('/{{photo-(big|small-left|small-right)-([a-z0-9\-]+)}}/i', $content, $matches);

        foreach ($matches[2] as $index => $photoId) {
            $photo = $this->getPhoto($photoId);

            if (empty($photo)) {
                $content = str_replace($matches[0][$index], '', $content);
                continue;
            }

            $photoLink = Yii::$app->photo->getFile($photo->getFilename());

            $photoInformation = $photo->getTranslation(Yii::$app->language)->getInformation();
            $photoAuthor      = $photo->getTranslation(Yii::$app->language)->getAuthor();

            if (!empty($photoInformation) && !empty($photoAuthor))
                $photoInformation = "$photoInformation / $photoAuthor";
            elseif (!empty($photoAuthor) && empty($photoInformation))
                $photoInformation = $photoAuthor;

            $photoWrapper = Html::tag('div',
                Html::tag('div',
                    Html::img($photoLink, ['alt' => $photoInformation]),
                    ['class' => 'image']
                )
                . Html::tag('span', $photoInformation, ['class' => 'description']),
                ['class' => 'news-image']
            );

            $content = str_replace($matches[0][$index], $photoWrapper, $content);
        }

        return $content;
    }

    /**
     * @param $idOrHash
     * @return Photo|object|null
     */
    private function getPhoto($idOrHash): ?Photo
    {
        /** @var PhotosRepository $repository */
        $repository = $this->entityManager->getRepository(Photo::class);

        return is_numeric($idOrHash)
            ? $repository->find($idOrHash)
            : $repository->findOneBy(['filename' => $idOrHash . '.jpg']);
    }

}