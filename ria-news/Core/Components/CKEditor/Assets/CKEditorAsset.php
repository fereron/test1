<?php
declare(strict_types=1);

namespace Ria\News\Core\Components\CKEditor\Assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class CKEditorAsset
 * @package Ria\News\Core\Components\CKEditor\Assets
 */
class CKEditorAsset extends AssetBundle
{

    /**
     * @var string[]
     */
    public $js = [
        'ckeditor.js',
    ];

    /**
     * @var string[]
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    /**
     * @var array
     */
    public $jsOptions = [
        'position' => View::POS_END
    ];

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/public';

        parent::init();
    }

}