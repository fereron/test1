<?php
declare(strict_types=1);

namespace Ria\News\Core\Components\CKEditor;


use Ria\News\Core\Components\CKEditor\Assets\CKEditorAsset;
use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

/**
 * Class CKEditor
 * @package Ria\News\Core\Components\CKEditor
 */
class CKEditor extends \sadovojav\ckeditor\CKEditor
{

    /**
     * @var bool
     */
    private $_inline = false;

    /**
     * @return string|void
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        CKEditorAsset::register($this->getView());

        $this->addExtraPlugins();

        echo Html::beginTag('div', $this->containerOptions);

        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }

        echo Html::endTag('div');


        if ($this->_inline) {
            $editorJs = $this->getCKeditor(self::TYPE_INLINE);

            $this->getView()->registerCss('#' . $this->containerOptions['id'] . ', #' . $this->containerOptions['id']
                . ' .cke_textarea_inline{height: ' . $this->editorOptions['height'] . 'px;}');

            $this->getView()->registerJs($editorJs, View::POS_END);
        } elseif ($this->initOnEvent) {
            $editorJs = $this->getCKeditor(self::TYPE_STANDARD);

            $js = 'jQuery("#' . $this->options['id'] . '").one("' . $this->initOnEvent . '", function () {' . $editorJs . '});';

            $this->getView()->registerJs($js, View::POS_END);
        } else {
            $editorJs = $this->getCKeditor(self::TYPE_STANDARD);

            $this->getView()->registerJs($editorJs, View::POS_END);
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private function addExtraPlugins()
    {
        if (!is_array($this->extraPlugins) || !count($this->extraPlugins)) {
            return false;
        }

        foreach ($this->extraPlugins as $value) {
            list($name, $path, $file) = $value;
            list(, $assetPath) = Yii::$app->assetManager->publish($path);

            $pluginJs = 'CKEDITOR.plugins.addExternal( "' . $name . '", "' . $assetPath . '/", "' . $file . '");';

            $this->getView()->registerJs($pluginJs);
        }
    }

    /**
     * @param $type
     * @return string|null
     */
    private function getCKeditor($type)
    {
        $editorJs = null;

        switch ($type) {
            case self::TYPE_STANDARD :
                $editorJs = $this->typeStandard();
                break;
            case self::TYPE_INLINE :
                $editorJs = $this->typeInline();
                break;
        }

        return $editorJs;
    }

    /**
     * @return string
     */
    private function typeInline()
    {
        $js = "CKEDITOR.inline(";
        $js .= Json::encode($this->options['id']);
        $js .= empty($this->editorOptions) ? '' : ', ' . Json::encode($this->editorOptions);
        $js .= ");";

        return $js;
    }

    /**
     * @return string
     */
    private function typeStandard()
    {
        $js = "CKEDITOR.replace(";
        $js .= Json::encode($this->options['id']);
        $js .= empty($this->editorOptions) ? '' : ', ' . Json::encode($this->editorOptions);
        $js .= ");";

        return $js;
    }

}