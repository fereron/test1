<?php
declare(strict_types=1);

namespace Ria\News\Core\Components\MediaWidgetsApplicator;

use Doctrine\ORM\EntityManagerInterface;
use Ria\News\Core\Helpers\WidgetValueImporter;
use Ria\News\Core\Models\Widget\Widget;
use Ria\News\Core\Query\Hydrator\WidgetHydrator;
use Ria\News\Core\Query\ViewModel\WidgetViewModel;
use yii\helpers\Html;

/**
 * Class Applicator
 * @package Ria\News\Core\Components\MediaWidgetsApplicator
 */
abstract class Applicator
{
    /**
     * @var string
     */
    protected $content;
    /**
     * @var array
     */
    private $widgetsInContent = [];
    /**
     * @var WidgetValueImporter
     */
    private $valueImporter;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Applicator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        $this->widgetsInContent = $this->extractWidgetIds();
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return array
     */
    protected function extractWidgetIds(): array
    {
        preg_match_all('/{{widget-content-([0-9]+)}}/', $this->content, $match);

        $widgetIds = [];
        if (isset($match[1])) {
            foreach ($match[1] as $index => $widgetId) {
                $widgetIds[$widgetId] = $match[0][$index];
            }
        }

        return $widgetIds;
    }

    /**
     * @return WidgetValueImporter
     */
    protected function getValueImporter(): WidgetValueImporter
    {
        if (empty($this->valueImporter)) {
            $this->valueImporter = new WidgetValueImporter();
        }
        return $this->valueImporter;
    }

    /**
     * @return array
     */
    protected function getWidgetsInContent(): array
    {
        return $this->widgetsInContent;
    }

    /**
     * @param string $hash
     * @param string $width
     * @return string
     */
    protected static function getYouTubeIframe(string $hash, string $width = '100%'): string
    {
        return Html::tag('iframe', '', [
            'type'            => 'text/html',
            'width'           => $width,
            'height'          => '468',
            'src'             => 'https://www.youtube.com/embed/' . $hash . '?rel=0&color=white&hl=' . \Yii::$app->language,
            'frameborder'     => 0,
            'allowfullscreen' => 'allowfullscreen'
        ]);
    }

    /**
     * Generate and get <amp-youtube> tag
     *
     * @param string $youtubeId
     * @return string
     */
    protected function getAmpYouTube($youtubeId)
    {
        return Html::tag('amp-youtube', '', [
            'data-videoid' => $youtubeId,
            'layout'       => 'responsive',
            'width'        => 480,
            'height'       => 270
        ]);
    }

    /**
     * @param string $facebookLink
     * @return string
     */
    protected function getAmpFacebook($facebookLink)
    {
        return Html::tag('amp-facebook', '', [
            'data-href' => $facebookLink,
            'layout'    => 'responsive',
            'width'     => 552,
            'height'    => 310
        ]);
    }

    /**
     * @param integer $twitterId
     * @return string
     */
    protected function getAmpTwitter($twitterId)
    {
        return Html::tag('amp-twitter', '', [
            'data-tweetid' => $twitterId,
            'layout'       => 'responsive',
            'width'        => 375,
            'height'       => 472
        ]);
    }

    /**
     * @param string $instagramCode
     * @return string
     */
    protected function getAmpInstagram($instagramCode)
    {
        return Html::tag('amp-instagram', '', [
            'data-shortcode' => $instagramCode,
            'data-captioned' => 'data-captioned',
            'layout'         => 'responsive',
            'width'          => 400,
            'height'         => 400
        ]);
    }

    /**
     * @param array $params
     * @return string
     */
    protected function getAmpVk(array $params)
    {
        return Html::tag('amp-vk', '', [
            'data-embedtype' => 'post',
            'data-owner-id'  => $params['ownerId'],
            'data-post-id'   => $params['postId'],
            'data-hash'      => $params['hash'],
            'layout'         => 'responsive',
            'width'          => 500,
            'height'         => 300
        ]);
    }

    /**
     * @param int $id
     * @return WidgetViewModel|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getWidget(int $id): ?WidgetViewModel
    {
        $qb = $this->entityManager->createQueryBuilder();
        return $qb->select('w')
            ->from(Widget::class, 'w')
            ->where('w.id = :id')
            ->setParameters(['id' => $id])
            ->getQuery()
            ->getOneOrNullResult(WidgetHydrator::HYDRATION_MODE);
    }

    abstract public function apply();

}