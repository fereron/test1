<?php
declare(strict_types=1);

namespace Ria\News\Core\Components\MediaWidgetsApplicator;

use Yii;
use Ria\News\Core\Models\Widget\Type;
use yii\helpers\Html;

/**
 * Class InTextApplicator
 * @package Ria\News\Core\Components\MediaWidgetsApplicator
 */
class InTextApplicator extends Applicator
{
    /**
     * Apply widgets to content
     */
    public function apply()
    {
        $replacementParams = [];
        foreach ($this->getWidgetsInContent() as $widgetId => $replacementToken) {

            try {
                $widget = $this->getWidget($widgetId);
            } catch (\Exception $exception) {
                $replacementParams[$replacementToken] = '';
                continue;
            }

            if ($widget->isYoutube()) {
                $youtubeHash                          = $this->getValueImporter()->getYoutubeHash($widget->content);
                $replacementParams[$replacementToken] = empty($youtubeHash) ? '' : self::getYouTube($youtubeHash);
            } elseif ($widget->isInstagram() && (!empty(Yii::$app->params['theme']) && \Yii::$app->params['theme'] == 'mobile')) {
                $replacementParams[$replacementToken] = self::getAmpInstagram($this->getValueImporter()->getInstagramHash($widget->content));
            } else {
                $replacementParams[$replacementToken] = $widget->content;
            }
        }

        $this->content = str_replace(array_keys($replacementParams), array_values($replacementParams), $this->content);
    }

    /**
     * Apply widget to content
     * @param string|null $youtubeHash
     */
    public function applyOnlyYouTube(?string $youtubeHash = null)
    {
        $replacementParams = [];
        foreach ($this->getWidgetsInContent() as $widgetId => $replacementToken) {
            try {
                $widget = $this->getWidget($widgetId);
            } catch (\Exception $exception) {
                $replacementParams[$replacementToken] = '';
                continue;
            }

            if ($widget->isYouTube()) {
                $replacementParams[$replacementToken] = self::getYouTubeIframe($this->getValueImporter()->getYoutubeHash($widget->content), '560');
            }
        }

        if (!empty($youtubeHash)) {
            $this->content .= self::getYouTubeIframe($youtubeHash);
        }

        $this->content = str_replace(array_keys($replacementParams), array_values($replacementParams), $this->content);
    }

    /**
     * @param string|null $youtubeHash
     */
    public function applyAmpWidgets(?string $youtubeHash = null)
    {
        $replacementParams = [];
        foreach ($this->getWidgetsInContent() as $widgetId => $replacementToken) {
            try {
                $widget = $this->getWidget($widgetId);
            } catch (\Exception $exception) {
                $replacementParams[$replacementToken] = '';
                continue;
            }

            switch ($widget->type) {
                case Type::YOUTUBE:
                    $replacementParams[$replacementToken] = self::getAmpYouTube($this->getValueImporter()->getYoutubeHash($widget->content));
                    break;
                case Type::FACEBOOK:
                    $replacementParams[$replacementToken] = self::getAmpFacebook($this->getValueImporter()->getFacebookPostLink($widget->content));
                    break;
                case Type::TWITTER:
                    $replacementParams[$replacementToken] = self::getAmpTwitter($this->getValueImporter()->getTwitterId($widget->content));
                    break;
                case Type::VKONTAKTE:
                    $vkParams                             = $this->getValueImporter()->getVkParams($widget->content);
                    $replacementParams[$replacementToken] = empty($vkParams) ? '' : self::getAmpVk($this->getValueImporter()->getVkParams($widget->content));
                    break;
                case Type::INSTAGRAM:
                    $replacementParams[$replacementToken] = self::getAmpInstagram($this->getValueImporter()->getInstagramHash($widget->content));
                    break;
                case Type::PLAYBUZZ:
                    $replacementParams[$replacementToken] = '';
                    break;
                default:
                    # apply widget content as is
                    if (mb_strpos($widget->content, '<script') !== false) {
                        $replacementParams[$replacementToken] = '';
                    } else {
                        $replacementParams[$replacementToken] = $widget->content;
                    }
            }

            $this->content = str_replace(array_keys($replacementParams), array_values($replacementParams), $this->content);
        }

        if (!empty($youtubeHash)) {
            $this->content .= self::getAmpYouTube($youtubeHash);
        }
    }

    /**
     * @param string $hash
     * @return string
     */
    public static function getYouTube(string $hash): string
    {
        return Html::tag('div', self::getYouTubeIframe($hash), ['class' => 'video-container']);
    }

}