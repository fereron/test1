<?php
declare(strict_types=1);

namespace Ria\News\Core\Components\MediaWidgetsApplicator;

use Ria\News\Core\Models\Widget\Type;

/**
 * Class AmpApplicator
 * @package Ria\News\Core\Components\MediaWidgetsApplicator
 */
class AmpApplicator extends Applicator
{

    /**
     * Apply widgets for amp page
     */
    public function apply()
    {
        $replacementParams = [];
        foreach ($this->getWidgetsInContent() as $widgetId => $replacementToken) {

            try {
                $widget = $this->getWidget($widgetId);
            } catch (\Exception $exception) {
                $replacementParams[$replacementToken] = '';
                continue;
            }

            switch ($widget->type) {
                case Type::YOUTUBE:
                    $replacementParams[$replacementToken] = self::getAmpYouTube($this->getValueImporter()->getYoutubeHash($widget->content));
                    break;
                case Type::FACEBOOK:
                    $replacementParams[$replacementToken] = self::getAmpFacebook($this->getValueImporter()->getFacebookPostLink($widget->content));
                    break;
                case Type::TWITTER:
                    $replacementParams[$replacementToken] = self::getAmpTwitter($this->getValueImporter()->getTwitterId($widget->content));
                    break;
                case Type::VKONTAKTE:
                    $vkParams = $this->getValueImporter()->getVkParams($widget->content);
                    $replacementParams[$replacementToken] = empty($vkParams) ? '' : self::getAmpVk($this->getValueImporter()->getVkParams($widget->content));
                    break;
                case Type::INSTAGRAM:
                    $replacementParams[$replacementToken] = self::getAmpInstagram($this->getValueImporter()->getInstagramHash($widget->content));
                    break;
                case Type::PLAYBUZZ:
                    $replacementParams[$replacementToken] = '';
                    break;
                default:
                    # apply widget content as is
                    if (mb_strpos($widget->content, '<script') !== false) {
                        $replacementParams[$replacementToken] = '';
                    } else {
                        $replacementParams[$replacementToken] = $widget->content;
                    }
            }
        }

        $this->content = str_replace(array_keys($replacementParams), array_values($replacementParams), $this->content);
    }

}