<?php
declare(strict_types=1);

namespace Ria\News\Core\Components;

use Doctrine\ORM\EntityManagerInterface;
use Ria\News\Core\Models\ExpertQuote\ExpertQuote;
use Yii;
use yii\helpers\Html;

/**
 * Class ExpertQuoteApplicator
 * @package Ria\News\Core\Components
 */
class ExpertQuoteApplicator
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PostPhotosApplicator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $content
     * @return string
     */
    public function apply(string $content): string
    {
        preg_match_all('/{{expert-quote-(\d+)}}/i', $content, $matches);

        foreach ($matches[1] as $i => $quoteId) {
            $quote = $this->getQuote((int)$quoteId);

            if (empty($quote)) {
                $content = str_replace($matches[0][$i], '', $content);
                continue;
            }

            $content = str_replace($matches[0][$i], $this->getQuoteHtmlContent($quote), $content);
        }

        return $content;
    }

    /**
     * @param int $id
     * @return ExpertQuote|object|null
     */
    protected function getQuote(int $id): ?ExpertQuote
    {
        return $this->entityManager->find(ExpertQuote::class, $id);
    }

    /**
     * @param ExpertQuote $quote
     * @return string
     */
    protected function getQuoteHtmlContent(ExpertQuote $quote): string
    {
        $expertTranslation = $quote->getExpert()->getTranslation(Yii::$app->language);

        $expertInfoHtml = Html::tag('div',
            Html::tag('div', Html::img(
                Yii::$app->photo->getThumb(50, $quote->getExpert()->getPhoto())),
                ['class' => 'thumb']
            ) .
            Html::tag('div',
                Html::tag('span', $expertTranslation->getFirstName() . ' ' . $expertTranslation->getLastName(), ['class' => 'name']) .
                Html::tag('p', $expertTranslation->getPosition(), ['class' => 'he-is']), ['class' => 'info']
            )
            , [
            'class' => 'person flex'
        ]);

        return Html::tag('blockquote', Html::tag('p', $quote->getText()) . $expertInfoHtml, [
            'class' => 'person-quote'
        ]);
    }

}