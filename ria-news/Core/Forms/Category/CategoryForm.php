<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Category;

use Ria\Core\Forms\CompositeForm;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Models\Category\Template;
use Ria\News\Module;
use Yii;

/**
 * Class CategoryForm
 * @package Ria\News\Core\Forms\Category
 * @property TranslationForm[] $translations
 */
class CategoryForm extends CompositeForm
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $parent_id;
    /**
     * @var string
     */
    public $template;
    /**
     * @var int
     */
    public $status;

    /**
     * @param Category|null $category
     * @return CategoryForm
     */
    public static function create(Category $category = null): self
    {
        $form         = new self();
        $translations = [];

        if ($category) {
            $form->id        = $category->getId();
            $form->parent_id = $category->getParent() ? $category->getParent()->getId() : null;
            $form->template  = $category->getTemplate();
            $form->status    = $category->getStatus();
        }

        foreach (Yii::$app->params['languages'] as $languageCode => $label) {
            $translations[] = $category
                ? TranslationForm::create($category->getTranslation($languageCode))
                : TranslationForm::create(null, ['language' => $languageCode]);
        }

        $form->translations = $translations;

        return $form;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['template', 'status'], 'required'],
            ['template', 'in', 'range' => Template::all()],
            ['parent_id', 'integer'],
            ['status', 'boolean']
        ];
    }

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    protected function internalForms(): array
    {
        return ['translations'];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function attributeLabels(): array
    {
        return [
            'parent_id' => Module::t('categories', 'Parent id'),
            'template'  => Module::t('categories', 'Template'),
            'status'    => Module::t('categories', 'Status'),
        ];
    }

    /**
     * @return array
     */
    public function getParentsList(): array
    {
        $query = Yii::$app->doctrine->getEntityManager()
            ->getRepository(Category::class)
            ->createQueryBuilder('c');

        $categories = $query->andWhere($query->expr()->isNull('c.parent'))->getQuery()->execute();

        $data = [];
        foreach ($categories as $parent) {
            $data[$parent->getId()] = $parent->getTranslation(Yii::$app->language)->getTitle();
        }

        return $data;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getTemplatesList(): array
    {
        $data = [];
        foreach (Template::all() as $item) {
            $data[$item] = Module::t('news', $item);
        }
        return $data;
    }

}