<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Category;

use Doctrine\ORM\Query\Expr\Join;
use Ria\News\Core\Query\Repositories\CategoriesRepository;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class CategorySearchForm
 * @package Ria\News\Core\Forms\Category
 */
class CategorySearch extends Model
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var int
     */
    public $status;
    /**
     * @var CategoriesRepository
     */
    private $repository;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'id'], 'integer'],
            ['title', 'string'],
        ];
    }

    /**
     * CategorySearch constructor.
     * @param CategoriesRepository $repository
     * @param array $config
     */
    public function __construct(CategoriesRepository $repository, $config = [])
    {
        parent::__construct($config);
        $this->repository = $repository;
    }

    /**
     * @param $params
     * @return ArrayDataProvider
     * @throws NotFoundHttpException
     */
    public function search($params): ArrayDataProvider
    {
        $query = $this->repository
            ->createQueryBuilder('c')
            ->join('c.translations', 'ct', Join::WITH, 'ct.language = :language')
            ->orderBy('c.sort', 'ASC')
            ->setParameter('language', \Yii::$app->language);

        $query->andWhere($query->expr()->isNull('c.parent'));
        $this->load($params);

        if (!$this->validate()) {
            throw new NotFoundHttpException();
        }

        if ($this->id) {
            $query->andWhere($query->expr()->eq('c.id', $this->id));
        }

        if ($this->status) {
            $query->andWhere($query->expr()->eq('c.status', $this->status));
        }

        if ($this->title) {
            $query->andWhere($query->expr()->like('ct.title', $query->expr()->literal("%$this->title%")));
        }

        return new ArrayDataProvider([
            'allModels'  => $this->getCategoriesTree($query->getQuery()->execute()),
            'pagination' => false
        ]);
    }

    /**
     * @param array $categories
     * @return array
     */
    private function getCategoriesTree(array $categories): array
    {
        $tree = [];
        foreach ($categories as $category) {
            $tree[] = $category;

            foreach ($category->getChildren() as $child) {
                $tree[] = $child;
            }
        }

        return $tree;
    }

}