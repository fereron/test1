<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Category;

use Doctrine\ORM\QueryBuilder;
use Ria\Core\Validators\UniqueValidator;
use Ria\News\Core\Models\Category\Translation;
use Ria\Core\Forms\CompositeForm;
use Ria\Core\Forms\MetaForm;
use Ria\News\Core\Validators\SlugValidator;
use Ria\News\Module;
use yii\base\InvalidConfigException;

/**
 * Class TranslationForm
 * @package Ria\News\Core\Forms\Category
 * @property MetaForm $meta
 */
class TranslationForm extends CompositeForm
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $language;
    /**
     * @var Translation
     */
    public $translation;

    /**
     * @param Translation|null $translation
     * @param array $config
     * @return TranslationForm
     */
    public static function create(Translation $translation = null, $config = [])
    {
        if ($translation) {
            $properties = [
                'language'    => $translation->getLanguage(),
                'title'       => $translation->getTitle(),
                'slug'        => $translation->getSlug(),
                'translation' => $translation,
                'meta'        => MetaForm::create($translation->language, $translation->getMeta()),
            ];
        } else {
            $properties = [
                'meta' => MetaForm::create($config['language'])
            ];
        }

        return new self(array_replace($config, $properties));
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function formName(): string
    {
        return parent::formName() . '_' . $this->language;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['title', 'slug'], 'required'],
            [['title', 'slug'], 'string', 'max' => 255],
            ['slug', SlugValidator::class],
            [
                'slug',
                UniqueValidator::class,
                'targetClass' => Translation::class,
                'message' => 'Этот slug уже существует.',
                'filter' => function (QueryBuilder $query) {
                    if (!$this->translation) {
                        return $query;
                    }

                    $alias = $alias = $query->getRootAliases()[0];

                    return $query->andWhere($query->expr()->neq($alias. '.slug', '?1'))
                        ->setParameter('1', $this->translation->getSlug());
                }
            ],
        ];
    }

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    protected function internalForms(): array
    {
        return ['meta'];
    }

    public function attributeLabels()
    {
        return [
            'title' => Module::t('categories', 'Title'),
            'slug'  => Module::t('categories', 'Slug'),
        ];
    }
}