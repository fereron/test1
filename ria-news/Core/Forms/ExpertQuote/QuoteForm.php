<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\ExpertQuote;

use Ria\Core\Validators\ExistValidator;
use Ria\News\Core\Models\ExpertQuote\ExpertQuote;
use Ria\News\Core\Models\Post\Post;
use Ria\Persons\Core\Models\Person;
use yii\base\Model;

/**
 * Class QuoteForm
 * @package Ria\News\Core\Forms\ExpertQuote
 */
class QuoteForm extends Model
{
    /**
     * @var int|null
     */
    public $id;
    /**
     * @var int
     */
    public $expertId;
    /**
     * @var int|null
     */
    public $postId;
    /**
     * @var string
     */
    public $text;
    /**
     * @var string
     */
    public $language;

    /**
     * QuoteForm constructor.
     * @param ExpertQuote|null $quote
     * @param array $config
     */
    public function __construct(ExpertQuote $quote = null, $config = [])
    {
        parent::__construct($config);

        if ($quote) {
            $this->id       = $quote->getId();
            $this->expertId = $quote->getExpert()->getId();
            $this->postId   = $quote->getPost()->getId();
            $this->text     = $quote->getText();
        }
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['expertId', 'text'], 'required'],
            ['expertId', ExistValidator::class, 'targetClass' => Person::class, 'targetAttribute' => 'id'],
            ['postId', ExistValidator::class, 'targetClass' => Post::class, 'targetAttribute' => 'id'],
            ['language', 'in', 'range' => array_keys(\Yii::$app->params['languages'])],
            ['text', 'string']
        ];
    }

}