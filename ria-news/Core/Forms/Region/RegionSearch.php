<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Region;

use Yii;
use yii\base\Model;
use yii\data\DataProviderInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Ria\Core\Data\DoctrineDataProvider;

/**
 * Class UserSearch
 * @package Ria\News\Core\Forms\Region
 */
class RegionSearch extends Model
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $order;
    /**
     * @var string
     */
    public $title;

    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * RegionSearch constructor.
     * @param EntityRepository $repository
     * @param array $config
     */
    public function __construct(EntityRepository $repository, $config = [])
    {
        parent::__construct($config);
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'order'], 'integer'],
            [['title',], 'string'],

        ];
    }

    /**
     * @param $params
     * @return DataProviderInterface
     */
    public function search($params): DataProviderInterface
    {
        $query = $this->repository->createQueryBuilder('r');

        $dataProvider = new DoctrineDataProvider([
            'query'      => $query,
            'modelClass' => $this->repository->getClassName(),
            'sort'       => [
                'defaultOrder' => ['order' => SORT_ASC]
            ],
            'pagination' => [
                'pageSize' => 12,
                'pageSizeParam' => false
            ]
        ]);

        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->innerJoin('r.translations', 't', Join::WITH, $query->expr()->eq('t.language', ':locale'))
            ->setParameters([':locale' => Yii::$app->language]);

        if ($this->id) {
            $query->andWhere($query->expr()->eq('r.id', $this->id));
        }

        if ($this->order) {
            $query->andWhere($query->expr()->eq('r.order', $this->order));
        }

        if ($this->title){
            $query->andWhere($query->expr()->like('t.title', $query->expr()->literal("%$this->title%")));
        }

        return $dataProvider;
    }
}
