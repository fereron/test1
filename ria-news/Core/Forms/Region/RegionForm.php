<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Region;

use Ria\Core\Forms\CompositeForm;
use Ria\News\Module;

/**
 * Class CityForm
 * @package Ria\News\Core\Forms\City
 * @property TranslationForm[] $translations
 */
class RegionForm extends CompositeForm
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $order;

    public function rules()
    {
        return [];
    }

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    protected function internalForms()
    {
        return ['translations'];
    }

    public function attributeLabels()
    {
        return [
            'order' => Module::t('regions', 'Order'),
            'region_id' => 'Region Id'
        ];
    }

}