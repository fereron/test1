<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Region;

use Doctrine\ORM\QueryBuilder;
use Ria\Core\Forms\CompositeForm;
use Ria\Core\Validators\UniqueValidator;
use yii\base\InvalidConfigException;
use Ria\News\Core\Models\Region\Translation;
use Ria\News\Core\Validators\SlugValidator;
use Ria\News\Module;

/**
 * Class TranslationForm
 * @package Ria\News\Core\Forms\Region
 */
class TranslationForm extends CompositeForm
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $language;

    /**
     * @var Translation
     */
    public $translation;

    public static function new(string $language, ?Translation $translation = null)
    {
        $form = new self();
        if ($translation) {
            $form->translation = $translation;
            $form->title = $translation->getTitle();
            $form->slug = $translation->getSlug();
            $form->language = $translation->getLanguage();
        } else {
            $form->language = $language;
        }
        return $form;
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function formName(): string
    {
        return parent::formName() . '_' . $this->language;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['title', 'slug'], 'required'],
            [['title', 'slug'], 'string', 'max' => 255],
            ['slug', SlugValidator::class],
            [
                'slug',
                UniqueValidator::class,
                'targetClass' => Translation::class,
                'message' => 'Этот slug уже существует.',
                'filter' => function (QueryBuilder $query) {
                    if (!$this->translation) {
                        return $query;
                    }

                    $alias = $alias = $query->getRootAliases()[0];

                    return $query->andWhere($query->expr()->neq($alias. '.slug', '?1'))
                        ->setParameter('1', $this->translation->getSlug());
                }
            ],
        ];
    }

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    protected function internalForms(): array
    {
        return [];
    }

    public function attributeLabels()
    {
        return [
            'title' => Module::t('regions', 'Title'),
            'slug'  => Module::t('regions', 'Slug'),
        ];
    }
}