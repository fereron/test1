<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Region;

use Ria\Core\Validators\UniqueValidator;
use Ria\News\Core\Models\Region\Region;
use Yii;

/**
 * Class RegionCreateForm
 * @package Ria\News\Core\Forms\Region
 * @property TranslationForm[] $translations
 */
class RegionCreateForm extends RegionForm
{
    /**
     * @var string
     */
    public $order;

    /**
     * RegionForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $translations = [];
        foreach (Yii::$app->params['languages'] as $languageCode => $label) {
            $translations[] = TranslationForm::new($languageCode);
        }
        $this->translations = $translations;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [
                'order',
                UniqueValidator::class,
                'targetClass' => Region::class,
            ],
            [['order'], 'required'],
            ['order', 'integer', 'min' => 1],
        ]);
    }
}