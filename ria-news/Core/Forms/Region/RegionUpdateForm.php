<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Region;

use Doctrine\ORM\QueryBuilder;
use Ria\Core\Validators\UniqueValidator;
use Ria\News\Core\Models\Region\Region;
use Yii;

/**
 * Class RegionCreateForm
 * @package Ria\News\Core\Forms\Region
 * @property TranslationForm[] $translations
 */
class RegionUpdateForm extends RegionForm
{
    /**
     * @var Region
     */
    private $region;

    /**
     * @param Region $region
     * @return RegionForm
     */
    public static function createFromModel(Region $region)
    {
        $translations           = [];
        $form                   = new self();
        $form->order            = $region->getOrder();

        foreach (Yii::$app->params['languages'] as $key => $label) {
            $translations[] = TranslationForm::new($key, $region->getTranslation($key));
        }

        $form->region         = $region;
        $form->translations = $translations;

        return $form;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [
                'order',
                UniqueValidator::class,
                'targetClass' => Region::class,
                'filter'      => function (QueryBuilder $query, $alias) {
                    return $query->andWhere($query->expr()->neq($alias . '.id', $this->region->getId()));
                }
            ],
            [['order'], 'required'],
            ['order', 'integer', 'min' => 1],
        ]);
    }
}