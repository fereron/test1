<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Speech;

use Faker\Provider\Uuid;
use Ria\Core\Validators\ExistValidator;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Speech\Speech;
use Ria\News\Module;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class SpeechForm
 * @package Ria\News\Core\Forms\Speech
 */
class SpeechForm extends Model
{

    /**
     * In mb
     */
    public const AUDIO_MAX_FILE_SIZE = 15 * 1024 * 1024;

    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @var int
     */
    public $postId;
    /**
     * @var string
     */
    public $filename;
    /**
     * @var int|null
     */
    private $id;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['postId', 'filename'], 'required'],
            ['filename', 'string'],
            ['postId', ExistValidator::class, 'targetClass' => Post::class, 'targetAttribute' => 'id'],
            ['file', 'file', 'extensions' => ['mp3'], 'maxSize' => self::AUDIO_MAX_FILE_SIZE]
        ];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function attributeLabels(): array
    {
        return [
            'file' => Module::t('news', 'File'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->filename) && $this->file instanceof UploadedFile) {
            $this->filename = Uuid::uuid() . '.' . $this->file->getExtension();
        }

        return parent::beforeValidate();
    }

    /**
     * @param Speech $speech
     * @return static
     */
    public static function createFrom(Speech $speech): self
    {
        $model           = new self();
        $model->id       = $speech->getId();
        $model->postId   = $speech->getPost()->getId();
        $model->filename = $speech->getFilename();

        return $model;
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

}