<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\City;

use Yii;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Ria\Core\Data\DoctrineDataProvider;
use yii\base\Model;
use yii\data\DataProviderInterface;

/**
 * Class CitySearch
 * @package Ria\News\Core\Forms\City
 */
class CitySearch extends Model
{

    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $region_id;
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * CitySearch constructor.
     * @param EntityRepository $repository
     * @param array $config
     */
    public function __construct(EntityRepository $repository, $config = [])
    {
        parent::__construct($config);

        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'region_id'], 'integer'],
            [['title',], 'string'],
        ];
    }

    /**
     * @param $params
     * @return DoctrineDataProvider
     */
    public function search($params): DoctrineDataProvider
    {
        $query = $this->repository->createQueryBuilder('c');

        $dataProvider = new DoctrineDataProvider([
            'query'      => $query,
            'modelClass' => $this->repository->getClassName(),
            'sort'       => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => 12,
                'pageSizeParam' => false
            ]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->innerJoin('c.translations', 't', Join::WITH, $query->expr()->eq('t.language', ':locale'))
            ->setParameters([':locale' => Yii::$app->language]);

        if ($this->id) {
           $query->andWhere( $query->expr()->eq('c.id', $this->id));
        }

        if ($this->title){
            $query->andWhere($query->expr()->like('t.title', $query->expr()->literal("%$this->title%")));
        }

        if ($this->region_id) {
            $query->andWhere($query->expr()->eq('c.region', $this->region_id));
        }

        return $dataProvider;
    }

}