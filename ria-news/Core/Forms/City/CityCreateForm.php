<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\City;

use Yii;

/**
 * Class RegionCreateForm
 * @package Ria\News\Core\Forms\City
 * @property TranslationForm[] $translations
 */
class CityCreateForm extends CityForm
{
    /**
     * @var string
     */
    public $region_id;

    /**
     * RegionForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $translations = [];
        foreach (Yii::$app->params['languages'] as $languageCode => $label) {
            $translations[] = TranslationForm::new($languageCode);
        }
        $this->translations = $translations;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['region_id'], 'required'],
            ['region_id', 'integer', 'min' => 1],
        ]);
    }
}