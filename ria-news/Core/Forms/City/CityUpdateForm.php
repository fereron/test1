<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\City;

use Ria\News\Core\Models\Region\Region;
use Yii;
use Ria\News\Core\Models\City\City;

class CityUpdateForm extends CityForm
{
    /**
     * @var Region
     */
    public $region_id;

    /**
     * @param City $city
     * @return CityForm
     */
    public static function createFromModel(City $city)
    {
        $translations   = [];
        $form           = new self();
        $form->id       = $city->getId();
        $form->region_id = $city->getRegionId();

        foreach (Yii::$app->params['languages'] as $key => $label) {
            $translations[] = TranslationForm::new($key, $city->getTranslation($key));
        }

        $form->translations = $translations;

        return $form;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['region_id'], 'required'],
            ['region_id', 'integer', 'min' => 1],
        ]);
    }
}