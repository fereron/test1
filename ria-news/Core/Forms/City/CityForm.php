<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\City;

use Yii;
use Ria\News\Module;
use Ria\Core\Forms\CompositeForm;
use Ria\News\Core\ActiveRecord\City\CityRecord;

/**
 * Class CityForm
 * @package Ria\News\Core\Forms\City
 * @property TranslationForm[] $translations
 */
class CityForm extends CompositeForm
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $region_id;

    public function rules()
    {
        return [
            ['region_id', 'integer'],
        ];
    }

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    protected function internalForms()
    {
        return ['translations'];
    }

    public function attributeLabels()
    {
        return [
            'region_id' => Module::t('cities', 'Region'),
        ];
    }

}