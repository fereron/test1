<?php

namespace Ria\News\Core\Forms\Post;

use Ria\News\Core\Models\Post\Post;
use Ria\News\Module;
use yii\base\Model;

class MoveDateForm extends Model
{
    public $date;

    public $cause;

    public function __construct(Post $post, $config = [])
    {
        $this->date = $post->getPublishedAt()->format('Y-m-d H:i:s');
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['date', 'cause'], 'required'],
            ['date', 'date', 'format' => 'php:Y-m-d H:i:s'],
            ['cause', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date' => Module::t('news', 'Datetime'),
            'cause' => Module::t('news', 'Cause'),
        ];
    }
}