<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Post;

use Doctrine\ORM\Query\Expr\Join;
use Ria\Core\Data\DoctrineDataProvider;
use Ria\Core\Validators\ExistValidator;
use Ria\News\Core\Helpers\CategoryHelper;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Models\Post\Note;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Query\Repositories\CategoriesRepository;
use Ria\News\Core\Query\Repositories\PostsRepository;
use Ria\News\Module;
use Ria\Users\Core\Models\User;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class PostSearch
 * @package Ria\News\Core\Forms\Post
 */
class PostSearch extends Model
{
    /**
     * @var
     */
    public $id;
    /**
     * @var int
     */
    public $author_id;
    /**
     * @var int
     */
    public $translator_id;
    /**
     * @var
     */
    public $title;
    /**
     * @var
     */
    public $status;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string
     */
    public $dateTo;
    /**
     * @var
     */
    public $createdBy;
    /**
     * @var
     */
    public $category;
    /**
     * @var
     */
    public $language;
    /**
     * @var PostsRepository
     */
    private $repository;
    /**
     * @var CategoriesRepository
     */
    private $categoryRepository;

    /**
     * PostSearchForm constructor.
     * @param PostsRepository $repository
     * @param CategoriesRepository $categoriesRepository
     * @param array $config
     */
    public function __construct(PostsRepository $repository, CategoriesRepository $categoriesRepository, $config = [])
    {
        parent::__construct($config);

        $this->repository         = $repository;
        $this->categoryRepository = $categoriesRepository;
        $this->language           = Yii::$app->params['defaultLanguage'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'translator_id'], 'integer'],
            [['title'], 'string'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:Y-m-d'],
            ['status', 'in', 'range' => Status::all()],
            ['language', 'in', 'range' => array_keys(Yii::$app->params['languages'])],
            ['createdBy', ExistValidator::class, 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['category', ExistValidator::class, 'targetClass' => Category::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @param array $params
     * @return DoctrineDataProvider
     */
    public function search(array $params)
    {
        $query = $this->repository->createQueryBuilder('p');

        $dataProvider = new DoctrineDataProvider([
            'query'      => $query,
            'modelClass' => $this->repository->getClassName(),
            'pagination' => [
                'pageSize'      => 15,
                'pageSizeParam' => false
            ],
            'sort'       => [
                'defaultOrder' => ['published_at' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->language) {
            $this->language = Yii::$app->params['defaultLanguage'];
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->where($query->expr()->andX(
            $query->expr()->eq('p.language', ':language')
        ))
            ->setParameter('language', $this->language ?? Yii::$app->params['defaultLanguage']);

        if ($this->id) {
            $query->andWhere($query->expr()->eq('p.id', $this->id));
        }

        if ($this->author_id) {
            $query->andWhere($query->expr()->eq('p.author', $this->author_id));
        }

        if ($this->translator_id) {
            $query->andWhere($query->expr()->eq('p.translator', $this->translator_id));
        }

        if ($this->status) {
            $query->andWhere('p.status = :status')
                ->setParameter('status', $this->status);
        } else {
            $query->andWhere($query->expr()->andX(
                $query->expr()->notIn('p.status', ':statuses')
            ))->setParameter('statuses', [Status::DELETED]);
        }

        if ($this->createdBy) {
            $query->andWhere($query->expr()->eq('p.created_by', $this->createdBy));
        }

        if ($this->category) {
            $this->category = (int)$this->category;

            $subCategories = $this->categoryRepository->getSubCategories($this->category, $this->language);

            if (!empty($subCategories)) {
                $query->andWhere(
                    $query->expr()->in('IDENTITY(p.category)',
                        CategoryHelper::pluckChildren($this->category, $subCategories)
                    ));
            } else {
                $query->andWhere($query->expr()->eq('IDENTITY(p.category)', $this->category));
            }
        }

        if (!empty($this->dateFrom)) {
            $dateFrom = $this->dateFrom . ' 00:00:00';
            $dateTo   = $this->dateTo . ' 23:59:59';

            $query->andWhere($query->expr()->between(
                'p.published_at',
                $query->expr()->literal($dateFrom),
                $query->expr()->literal($dateTo)
            ));
        }

        if ($this->title) {
            $query->andWhere($query->expr()->like('p.title', $query->expr()->literal('%' . $this->title . '%')));
        }

        return $dataProvider;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getStatusesList(): array
    {
        $data = [];
        foreach (Status::all() as $status) {
            $data[$status] = Module::t('news', $status);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getTranslatorsList(): array
    {
        $translators = Yii::$app->doctrine->getEntityManager()
            ->getRepository(User::class)
            ->getByRoles(['Translator', 'LeadTranslator']);

        return ArrayHelper::map($translators, 'id', function (User $user) {
            return $user->getTranslation($this->language)->getFullName();
        });
    }

    /**
     * @return array
     */
    public function getAuthorsList(): array
    {
        $authors = Yii::$app->doctrine->getEntityManager()
            ->getRepository(User::class)
            ->getByPermission('canBeAuthor');

        return ArrayHelper::map($authors, 'id', function (User $user) {
            return $user->getTranslation($this->language)->getFullName();
        });
    }

    /**
     * @return array
     */
    public function getCategoriesList()
    {
        $query = Yii::$app->doctrine
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from(Category::class, 'c')
            ->join('c.translations', 'ct', Join::WITH, 'ct.language = :language')
            ->orderBy('c.sort', 'ASC')
            ->setParameter('language', Yii::$app->language);

        $query->andWhere($query->expr()->isNull('c.parent'));
        $categories = $query->getQuery()->execute();

        $tree = [];
        foreach ($categories as $category) {
            /** @var Category $category */
            $tree[$category->getId()] = $category->getTranslation($this->language)->getTitle();

            foreach ($category->getChildren() as $child) {
                $tree[$child->getId()] = ' - ' . $child->getTranslation($this->language)->getTitle();
            }
        }

        return $tree;
    }

    public function getNoteForPost(Post $post)
    {
        /** @var Note $note */
        $note = Yii::$app->doctrine
            ->getEntityManager()
            ->getRepository(Note::class)
            ->findOneBy(['post_group_id' => $post->getGroupId()]);

        return $note ? $note->getBody() : '-';
    }

}