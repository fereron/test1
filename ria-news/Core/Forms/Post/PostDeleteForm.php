<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Post;

use Ria\News\Core\Models\Post\Status;
use yii\base\Model;

/**
 * Class PostDeleteForm
 * @package Ria\News\Core\Forms\Post
 */
class PostDeleteForm extends Model
{
    /**
     * @var string
     */
    public $cause;
    /**
     * @var string|null
     */
    public $status;

    /**
     * @return string
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            ['cause', 'string'],
            ['status', 'in', 'range' => Status::all()]
        ];
    }
}