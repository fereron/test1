<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Post;

use Cocur\Slugify\Slugify;
use Core\Helpers\StatusHelper;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Imagine\Exception\InvalidArgumentException;
use Ria\Core\Forms\CompositeForm;
use Ria\Core\Forms\MetaForm;
use Ria\Core\Helpers\StringHelper;
use Ria\Core\Validators\ExistValidator;
use Ria\Core\Validators\UniqueValidator;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Models\Post\Note;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Story\Story;
use Ria\News\Core\Validators\SlugValidator;
use Ria\News\Core\Models\Post\Export;
use Ria\News\Core\Models\Post\Icon;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;
use Ria\News\Module;
use Ria\Persons\Core\Models\Person;
use Ria\Users\Core\Models\User;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class PostForm
 * @package Ria\News\Core\Forms\Post
 * @property MetaForm $meta
 */
class PostForm extends CompositeForm
{
    public $id;
    /**
     * @var int
     */
    public $group_id;
    /**
     * @var string
     */
    public $icon;
    /**
     * @var string
     */
    public $type;
    /**
     * @var int
     */
    public $categoryId;
    /**
     * @var int
     */
    public $createdBy;
    /**
     * @var int
     */
    public $authorId;
    /**
     * @var int
     */
    public $expertId;
    /**
     * @var int
     */
    public $cityId;
    /**
     * @var int
     */
    public $translatorId;
    /**
     * @var array
     */
    public $tags = [];
    /**
     * @var array
     */
    public $persons = [];
    /**
     * @var array
     */
    public $marked_words = [];
    /**
     * @var string
     */
    public $youtubeId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $description;
    /**
     * @var string
     */
    public $content;
    /**
     * @var string
     */
    public $source;
    /**
     * @var string
     */
    public $note;
    /**
     * @var string
     */
    public $publishedAt;
    /**
     * @var bool
     */
    public $setCurrentDate;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $language;
    /**
     * @var string
     */
    public $status;
    /**
     * @var array
     */
    public $exports = [];
    /**
     * @var array
     */
    public $related = [];
    /**
     * @var array
     */
    public $photos = [];
    /**
     * @var int
     */
    public $storyId;
    /**
     * @var string
     */
    public $optionType;
    /**
     * @var string|null
     */
    public $image;
    /**
     * @var bool
     */
    public $isMain;
    /**
     * @var bool
     */
    public $isActual;
    /**
     * @var bool
     */
    public $isBreaking;
    /**
     * @var bool
     */
    public $isExclusive;
    /**
     * @var bool
     */
    public $isImportant;
    /**
     * @var bool
     */
    public $linksNoindex = true;
    /**
     * @var string
     */
    public $btnSave;
    /**
     * @var string
     */
    public $btnCancel;
    /**
     * @var Post
     */
    private $post;
    /**
     * @var bool
     */
    private $isTranslateCreation = false;
    /**
     * @var string
     */
    private $_oldSlug = '';
    /**
     * @var string
     */
    private $_oldPublishedAt = '';

    /**
     * @param Post|null $post
     * @param array $config
     * @return PostForm
     */
    public static function create(?Post $post = null, array $config = []): self
    {
        if (!$post) {
            $postForm              = new self($config);
            $postForm->createdBy   = Yii::$app->user->id;
            $postForm->authorId    = $postForm->createdBy;
            $postForm->publishedAt = date('Y-m-d H:i:s');
            $postForm->meta        = MetaForm::create($config['language']);

            return $postForm;
        }

        $postForm = new self(array_merge($config, [
            'id'           => $post->getId(),
            'language'     => $post->getLanguage(),
            'icon'         => $post->getIcon(),
            'type'         => $post->getType(),
            'optionType'   => $post->getOptionType(),
            'categoryId'   => $post->getCategory()->getId(),
            'createdBy'    => $post->getCreatedBy(),
            'authorId'     => $post->getAuthor()->getId(),
            'expertId'     => $post->getExpert() ? $post->getExpert()->getId() : null,
            'translatorId' => $post->getTranslator() ? $post->getTranslator()->getId() : null,
            'cityId'       => $post->getCity() ? $post->getCity()->getId() : null,
            'storyId'      => $post->getStory() ? $post->getStory()->getId() : null,
            'youtubeId'    => $post->getYoutubeId(),
            'title'        => $post->getTitle(),
            'description'  => $post->getDescription(),
            'content'      => $post->getContent(),
            'source'       => $post->getSource(),
            'publishedAt'  => $post->getPublishedAt()->format('Y-m-d H:i:s'),
            'slug'         => $post->getSlug(),
            'status'       => (string)$post->getStatus(),
            'marked_words' => $post->getMarkedWords(),
            'image'        => $post->getImage(),
            'isMain'       => $post->getIsMain(),
            'isExclusive'  => $post->getIsExclusive(),
            'isActual'     => $post->getIsActual(),
            'isBreaking'   => $post->getIsBreaking(),
            'isImportant'  => $post->getIsImportant(),
            'linksNoindex' => $post->getLinksNoindex(),
            'meta'         => MetaForm::create($post->getLanguage(), $post->getMeta()),
        ]));

        $postForm->setPost($post);
        $postForm->note    = $postForm->getPostNote($post);
        $postForm->related = $postForm->getRelatedList(true);

        foreach ($post->getTags() as $tag) {
            $postForm->tags[] = $tag->getTranslation($post->getLanguage())->getName();
        }

        $postForm->persons = $postForm->getPersonsList(true);

        foreach ($post->getExports() as $export) {
            $postForm->exports[] = $export->value();
        }

        $postForm->_oldSlug        = $post->getSlug();
        $postForm->_oldPublishedAt = $post->getPublishedAt()->getTimestamp();

        return $postForm;
    }

    /**
     * @param Post $post
     * @param array $config
     * @return static
     */
    public static function createFromOriginal(Post $post, array $config = []): self
    {
        $postForm                      = new self(array_replace($config, [
            'language'     => $config['language'],
            'icon'         => $post->getIcon(),
            'type'         => $post->getType(),
            'categoryId'   => $post->getCategory()->getId(),
            'createdBy'    => $post->getCreatedBy(),
            'authorId'     => $post->getAuthor()->getId(),
            'expertId'     => $post->getExpert() ? $post->getExpert()->getId() : null,
            'cityId'       => $post->getCity() ? $post->getCity()->getId() : null,
            'storyId'      => $post->getStory() ? $post->getStory()->getId() : null,
            'image'        => $post->getImage(),
            'youtubeId'    => $post->getYoutubeId(),
            'publishedAt'  => date('Y-m-d H:i:s'),
            'meta'         => MetaForm::create($config['language']),
            'isMain'       => $post->getIsMain(),
            'isExclusive'  => $post->getIsExclusive(),
            'isActual'     => $post->getIsActual(),
            'isBreaking'   => $post->getIsBreaking(),
            'isImportant'  => $post->getIsImportant(),
            'linksNoindex' => $post->getLinksNoindex()
        ]));
        $postForm->isTranslateCreation = true;
        $postForm->note                = $postForm->getPostNote($post);

        return $postForm;
    }

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    protected function internalForms(): array
    {
        return ['meta'];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['categoryId', 'type', 'title', 'slug', 'publishedAt', 'status'], 'required'],
            ['authorId', 'required', 'when' => function () {
                return $this->type != Type::OPINION;
            }],
            ['expertId', 'required', 'when' => function () {
                return $this->type == Type::OPINION;
            }],
            [['setCurrentDate', 'isMain', 'isExclusive', 'isActual', 'isBreaking', 'isImportant', 'linksNoindex'], 'boolean'],
            ['translatorId', 'required', 'when' => function () {
                return $this->isTranslateCreation();
            }],
//            ['youtubeId', 'required', 'when' => function () {
//                return $this->type == Type::VIDEO;
//            }],
            ['categoryId', ExistValidator::class, 'targetClass' => Category::class, 'targetAttribute' => 'id'],
            ['authorId', ExistValidator::class, 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['expertId', ExistValidator::class, 'targetClass' => Person::class, 'targetAttribute' => 'id'],
            ['cityId', ExistValidator::class, 'targetClass' => City::class, 'targetAttribute' => 'id'],
            ['translatorId', ExistValidator::class, 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['storyId', ExistValidator::class, 'targetClass' => Story::class, 'targetAttribute' => 'id'],
            ['type', 'in', 'range' => Type::all()],
            ['icon', 'in', 'range' => Icon::all()],
            ['photos', 'each', 'rule' => ['string']],
            ['title', 'string', 'max' => 150],
            ['slug', 'string', 'max' => 150],
            ['image', 'string', 'max' => 50],
            ['youtubeId', 'string', 'max' => 11],
            [['source', 'note'], 'string', 'max' => 255],
            ['tags', 'each', 'rule' => ['string', 'max' => 255]],
            ['persons', 'each', 'rule' => ['string', 'max' => 255]],
            ['marked_words', 'each', 'rule' => ['string', 'max' => 255]],
            ['exports', 'each', 'rule' => ['in', 'range' => Export::all()]],
            ['status', 'in', 'range' => Status::all()],
            ['slug', SlugValidator::class],
            ['slug', 'checkSlugReadOnly'],
            [
                'slug',
                UniqueValidator::class,
                'targetClass' => Post::class,
                'filter'      => function (QueryBuilder $query) {
                    $alias = $query->getRootAliases()[0];

                    $query = $query
                        ->andWhere($alias . '.language = :language')
                        ->setParameter('language', $this->language);

                    if ($this->post) {
                        $query->andWhere($query->expr()->not($query->expr()->eq($alias . '.id', ':post_id')))
                            ->setParameter('post_id', $this->post->getId());
                    }

                    return $query;
                }
            ],
//            ['content', LinksValidator::class],
            [['description', 'content', 'btnSave', 'btnCancel'], 'string'],
            ['optionType', 'in', 'range' => ['ads', 'closed']],
            ['related', 'each', 'rule' => [ExistValidator::class, 'targetClass' => Post::class, 'targetAttribute' => 'id']],
            ['exports', 'each', 'rule' => ['in', 'range' => Export::all()]],

            ['publishedAt', 'date', 'format' => 'php:Y-m-d H:i:s'],
            ['publishedAt', 'checkPublishedAt'],
            ['image', 'validatePhotoResolution', 'when' => function () {
                // дата с которой активируем проверку, т.к. для старых новостей уже не актуально
                return time() > strtotime('2020-08-24 00:00:00');
            }],
        ];
    }

    /**
     * @param string $attribute
     * @param array|null $params
     * @throws InvalidConfigException
     */
    public function checkSlugReadOnly(string $attribute, ?array $params)
    {
        if (!empty($this->_oldSlug)) {
            $value = $this->{$attribute};
            if (StatusHelper::isActive((string)$this->getPost()->getStatus()) && strcmp($value, $this->_oldSlug) !== 0) {
                $this->addError($attribute, Module::t('news', 'You cannot change post url when post is active.'));
            }
        }
    }

    /**
     * @param string $attribute
     * @param array|null $params
     * @throws InvalidConfigException
     */
    public function checkPublishedAt(string $attribute, ?array $params)
    {
        $timestamp = strtotime($this->publishedAt);
        if (!$this->isNewRecord() && $timestamp < time() && $timestamp < strtotime($this->_oldPublishedAt)) {
            $this->publishedAt = $this->_oldPublishedAt;
            $this->addError($attribute, Module::t('news', 'You cannot set date to before.'));
        }
    }

    /**
     * Validate photo resolution if post chosen as main
     * @param string $attribute
     */
    public function validatePhotoResolution(string $attribute)
    {
        if (!empty($this->{$attribute})) {
            $minSize    = Yii::$app->params['minImageSize'];
            try {
                $box = Yii::$app->photo->getSize($this->{$attribute});
            } catch (InvalidArgumentException $exception) {
                return;
            }

            [$width, $height] = [$box->getWidth(), $box->getHeight()];

            if (
                (($width === $height || $width > $height) && $width < $minSize)
                || ($width < $height && $width < $minSize)
            ) {
                $this->addError($attribute, Yii::t('news', 'Main image is too small {size}', [
                    'size' => $minSize
                ]));
            }
        }
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true)
    {
        $this->_oldSlug = $this->slug;
        if (!$this->isNewRecord()) {
            $this->_oldPublishedAt = $this->post->getPublishedAt()->format('Y-m-d H:i:s');
        }

        parent::setAttributes($values, $safeOnly);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function attributeLabels(): array
    {
        return [
            'title'          => Module::t('news', 'Title'),
            'slug'           => Module::t('news', 'Slug'),
            'tags'           => Module::t('news', 'Tags'),
            'persons'        => Module::t('news', 'Persons'),
            'marked_words'   => Module::t('news', 'Marked Words'),
            'description'    => Module::t('news', 'Description'),
            'type'           => Module::t('news', 'Type'),
            'optionType'     => Module::t('news', 'Option'),
            'icon'           => Module::t('news', 'Icon'),
            'youtubeId'      => Module::t('news', 'YouTube Video'),
            'categoryId'     => Module::t('news', 'Category'),
            'authorId'       => Module::t('news', 'Author'),
            'expertId'       => Module::t('news', 'Expert'),
            'cityId'         => Module::t('news', 'City'),
            'translatorId'   => Module::t('news', 'Translator'),
            'status'         => Module::t('news', 'Status'),
            'content'        => Module::t('news', 'Content'),
            'setCurrentDate' => Module::t('news', 'Set current date'),
            'source'         => Module::t('news', 'Source'),
            'note'           => Module::t('news', 'Note'),
            'related'        => Module::t('news', 'Related'),
            'storyId'        => Module::t('news', 'Story'),
            'isMain'         => Module::t('news', 'Main'),
            'isExclusive'    => Module::t('news', 'Exclusive'),
            'isActual'       => Module::t('news', 'Actual'),
            'isBreaking'     => Module::t('news', 'Breaking'),
            'isImportant'    => Module::t('news', 'Important'),
            'linksNoindex'   => Module::t('news', 'Links noindex'),
            'publishedAt'    => Module::t('news', 'Datetime'),
            'photos'         => Module::t('news', 'Photos')
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->isNewRecord() && (bool)$this->setCurrentDate) {
            $this->publishedAt = date('Y-m-d H:i:s');
        }

        $this->slug = Slugify::create([])->slugify($this->slug);

        if (empty($this->description)) {
            $this->description = trim(strip_tags(StringHelper::getFirstParagraph($this->content) ?? ''));
        }

        return parent::beforeValidate();
    }

    /**
     * {@inheritDoc}
     */
    public function afterValidate()
    {
        if ($this->allowToSetDatetimeAutomatically()) {
            $this->publishedAt = date('Y-m-d H:i:s');
        }
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @return int|null
     */
    public function getPostId(): ?int
    {
        return $this->isNewRecord() ? null : $this->getPost()->getId();
    }

    /**
     * @return bool
     */
    public function isNewRecord(): bool
    {
        return empty($this->post);
    }

    /**
     * @return bool
     */
    public function isTranslateCreation(): bool
    {
        return $this->isTranslateCreation;
    }

    /**
     * @param bool $onlyId
     * @return array
     */
    public function getRelatedList($onlyId = false)
    {
        $result = [];

        if (!$this->post) {
            return $result;
        }

        foreach ($this->post->getRelated() as $related) {
            if ($onlyId) {
                $result[] = $related->getId();
            } else {
                $result[$related->getId()] = $related->getTitle();
            }
        }

        return $result;
    }

    /**
     * @param bool $onlyId
     * @return array
     */
    public function getPersonsList(bool $onlyId = false): array
    {
        $result = [];

        if (!$this->post) {
            return $result;
        }

        foreach ($this->post->getPersons() as $person) {
            if ($onlyId) {
                $result[] = $person->getId();
            } else {
                $result[$person->getId()] = $person->getTranslation($this->post->getLanguage())->getName();
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getStory()
    {
        if (!$this->post || !$this->post->getStory()) {
            return [];
        }

        return [$this->post->getStory()->getId() => $this->post->getStory()->getTranslation($this->language)->getTitle()];
    }

    /**
     * @return bool
     */
    public function chosenReturnToForm(): bool
    {
        return $this->btnSave === 'stay';
    }

    /**
     * @return bool
     */
    public function chosenCancel(): bool
    {
        return !empty($this->btnCancel);
    }

    /**
     * @return string
     */
    public function getYoutubeEmbedUrl(): string
    {
        if (empty($this->youtubeId)) {
            return '';
        }
        return 'https://www.youtube.com/embed/' . $this->youtubeId;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getExports(): array
    {
        $exports = [];

        foreach (Export::all() as $export) {
            $exports[$export] = Module::t('exports', $export);
        }

        return $exports;
    }

    /**
     * @return array
     */
    public function getAuthorsList(): array
    {
        $authors = Yii::$app->doctrine->getEntityManager()
            ->getRepository(User::class)
            ->getByPermission('canBeAuthor');

        return ArrayHelper::map($authors, 'id', function (User $user) {
            return $user->getTranslation($this->language)->getFullName();
        });
    }

    /**
     * @return array
     */
    public function getExpertsList(): array
    {
        $experts = Yii::$app->doctrine->getEntityManager()
            ->getRepository(Person::class)
            ->createQueryBuilder('p')
            ->select(['p.id', 'pt.first_name', 'pt.last_name'])
            ->innerJoin('p.translations', 'pt', 'WITH', 'pt.language = :lang')
            ->where('p.status.status = 1')
            ->orderBy('pt.last_name')
            ->setParameter('lang', $this->language)
            ->getQuery()
            ->execute();

        return ArrayHelper::map($experts, 'id', function ($item) {
            return $item['last_name'] . ' ' . $item['first_name'];
        });
    }

    /**
     * @return array
     */
    public function getTranslatorsList(): array
    {
        $translators = Yii::$app->doctrine->getEntityManager()
            ->getRepository(User::class)
            ->getByRoles(['Translator', 'LeadTranslator']);

        return ArrayHelper::map($translators, 'id', function (User $user) {
            return $user->getTranslation($this->language)->getFullName();
        });
    }

    /**
     * @return array
     */
    public function getCitiesList(): array
    {
        $query = Yii::$app->doctrine->getEntityManager()
            ->getRepository(City::class)
            ->createQueryBuilder('c');

        $cities = $query
            ->join('c.translations', 'ct', Join::WITH, 'ct.language = :language')
            ->setParameters([
                'language' => $this->language,
            ])
            ->getQuery()
            ->execute();

        return ArrayHelper::map($cities, 'id', function (City $city) {
            return $city->getTranslation($this->language)->getTitle();
        });
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getIconsList(): array
    {
        $data = [];
        foreach (Icon::all() as $icon) {
            $data[$icon] = Module::t('news', $icon);
        }

        return $data;
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getTypesList(): array
    {
        $data = [];
        foreach (Type::all() as $type) {
            $data[$type] = Module::t('news', 'type_' . $type);
        }

        return $data;
    }


    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getOptionTypesList(): array
    {
        return [
            'ads'    => Module::t('news', 'option_type_ads'),
            'closed' => Module::t('news', 'option_type_closed'),
        ];
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getStatusesList(): array
    {
        $data = [];
        foreach (Status::list() as $status) {
            $data[$status] = Module::t('news', $status);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getCategoriesList()
    {
        $query = Yii::$app->doctrine
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from(Category::class, 'c')
            ->join('c.translations', 'ct', Join::WITH, 'ct.language = :language')
            ->orderBy('c.sort', 'ASC')
            ->setParameter('language', Yii::$app->language);

        $query->andWhere($query->expr()->isNull('c.parent'));
        $categories = $query->getQuery()->execute();

        $tree = [];
        foreach ($categories as $category) {
            /** @var Category $category */
            $label = $category->getTranslation($this->language)->getTitle();
            /** @var PersistentCollection $children */
            $children = $category->getChildren();
            if ($children->isEmpty()) {
                $tree[$category->getId()] = $label;
            } else {
                foreach ($children as $child) {
                    $tree[$label][$child->getId()] = $child->getTranslation($this->language)->getTitle();
                }
            }
        }

        return $tree;
    }

    /**
     * @return array
     */
    public function getStoriesList()
    {
        $stories = Yii::$app->doctrine
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from(Story::class, 's')
            ->join('s.translations', 'st', Join::WITH, 'st.language = :language')
            ->where('s.status = :status')
            ->setParameter('language', Yii::$app->language)
            ->setParameter('status', true)
            ->orderBy('s.id', 'desc')
            ->getQuery()
            ->execute();

        return ArrayHelper::map($stories, 'id', function (Story $story) {
            return $story->getTranslation($this->language)->getTitle();
        });
    }

    /**
     * @return bool
     */
    protected function allowToSetDatetimeAutomatically(): bool
    {
        return $this->setCurrentDate && (!$this->isNewRecord() && !StatusHelper::isActive((string)$this->post->getStatus()))
            && StatusHelper::isActive($this->status)
            && strtotime($this->publishedAt) <= time();
    }

    /**
     * @param Post $post
     * @return string|null
     */
    private function getPostNote(Post $post): ?string
    {
        $note = Yii::$app->doctrine
            ->getEntityManager()
            ->getRepository(Note::class)
            ->findOneBy(['post_group_id' => $post->getGroupId()]);

        return $note ? $note->getBody() : null;
    }

}