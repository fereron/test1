<?php

namespace Ria\News\Core\Forms\Story;

use Doctrine\ORM\QueryBuilder;
use Ria\Core\Validators\UniqueValidator;
use Ria\News\Core\Models\Story\Translation;
use Ria\Core\Forms\CompositeForm;
use Ria\Core\Forms\MetaForm;
use Ria\News\Core\Validators\SlugValidator;
use Ria\News\Module;

/**
 * Class TranslationForm
 * @package Ria\News\Core\Forms
 * @property MetaForm $meta
 */
class TranslationForm extends CompositeForm
{
    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $language;

    /**
     * @var Translation|null
     */
    public $translation;

    /**
     * @param Translation|null $translation
     * @param array $config
     * @return TranslationForm
     */
    public static function create(Translation $translation = null, $config = [])
    {
        if ($translation) {
            $properties = [
                'language'    => $translation->getLanguage(),
                'title'       => $translation->getTitle(),
                'slug'        => $translation->getSlug(),
                'description' => $translation->getDescription(),
                'translation' => $translation,
                'meta'        => MetaForm::create($translation->language, $translation->getMeta()),
            ];
        } else {
            $properties = [
                'meta' => MetaForm::create($config['language'])
            ];
        }

        return new self(array_replace($config, $properties));
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . '_' . $this->language;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @inheritDoc
     */
    protected function internalForms()
    {
        return ['meta'];
    }

    public function rules()
    {
        return [
            [['slug', 'title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string'],
            ['slug', SlugValidator::class],
            [
                'slug',
                UniqueValidator::class,
                'targetClass' => Translation::class,
                'message' => 'Этот slug уже существует.',
                'filter' => function (QueryBuilder $query) {
                    if (!$this->translation) {
                        return $query;
                    }

                    $alias = $alias = $query->getRootAliases()[0];
                    return $query->andWhere($query->expr()->neq($alias. '.slug', '?1'))
                        ->setParameter('1', $this->translation->getSlug());
                }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'       => Module::t('stories', 'Title'),
            'slug'        => Module::t('stories', 'Slug'),
            'description' => Module::t('stories', 'Description'),
        ];
    }


}