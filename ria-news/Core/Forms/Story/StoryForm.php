<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Story;

use Ria\Core\Forms\CompositeForm;
use Ria\News\Core\Models\Story\Story;
use yii\web\UploadedFile;
use Ria\News\Module;
use Yii;

/**
 * Class StoryForm
 * @package Ria\News\Core\Forms\Story
 * @property TranslationForm[] $translations
 */
class StoryForm extends CompositeForm
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var bool
     */
    public $status;
    /**
     * @var bool
     */
    public $showOnSite;
    /**
     * @var UploadedFile|null
     */
    public $cover;

    /**
     * @param Story|null $story
     * @return StoryForm
     */
    public static function create(Story $story = null)
    {
        $form         = new self();
        $translations = [];

        if ($story) {
            $form->id         = $story->getId();
            $form->status     = $story->getStatus();
            $form->showOnSite = $story->getShowOnSite();
            $form->cover      = $story->getCover();
        }

        foreach (Yii::$app->params['languages'] as $languageCode => $label) {
            $translations[$languageCode] = $story
                ? TranslationForm::create($story->getTranslation($languageCode))
                : TranslationForm::create(null, ['language' => $languageCode]);
        }

        $form->translations = $translations;

        return $form;
    }

    /**
     * @return string[]
     */
    protected function internalForms(): array
    {
        return ['translations'];
    }

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            ['status', 'required'],
            [['status', 'showOnSite'], 'boolean'],
            ['cover', 'string'],
        ];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function attributeLabels(): array
    {
        return [
            'status'     => Module::t('stories', 'Status'),
            'showOnSite' => Module::t('stories', 'Show on site'),
            'cover'      => Module::t('stories', 'Cover'),
        ];
    }

}