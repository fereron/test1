<?php
declare(strict_types=1);

namespace Ria\News\Core\Forms\Redirect;

use Ria\News\Module;
use Yii;
use yii\base\Model;

/**
 * Class RedirectForm
 * @package Ria\News\Core\Forms\Redirect
 */
class RedirectForm extends Model
{
    /**
     * @var string
     */
    public $new;

    /**
     * @return string
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function rules(): array
    {
        return [
            ['new', 'required'],
            [
                'new',
                'match',
                'pattern' => '/^' . str_replace('/', '\/', Yii::$app->params['frontendHostInfo'])
                    . '\/((ru|en)\/)?([\w\-]+\/)?([\w\-]+)\/([\w\d\-]+)\//i',
                'message' => Module::t('news', 'Incorrect post url')
            ]
        ];
    }
}