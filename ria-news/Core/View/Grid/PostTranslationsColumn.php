<?php
declare(strict_types=1);
namespace Ria\News\Core\View\Grid;

use Ria\Core\Helpers\FlagIconHelper;
use Ria\News\Core\Models\Post\Post;
use Yii;
use yii\grid\DataColumn;
use yii\helpers\Html;

/**
 * Class PostTranslationsColumn
 * @package Ria\News\Core\View\Grid
 */
class PostTranslationsColumn extends DataColumn
{

    /**
     * @param mixed $post
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($post, $key, $index)
    {
        /** @var Post $post */

        $languages = Yii::$app->params['languages'];
        unset($languages[$post->getLanguage()]);

        $links = [];
        foreach ($languages as $languageCode => $label) {
            $translation = $post->getTranslation($languageCode);
            $translationExists = !empty($translation);
            $existClass = $translationExists ? ' ' : 'empty-translate ';

            $links[] = Html::a(
                '',
                $translationExists
                    ? ['update', 'id' => $translation->getId()]
                    : ['create-translation', 'group_id' => $post->getGroupId(), 'lang' => $languageCode, 'originalLang' => $post->getLanguage()] ,
                ['class' => $existClass .'flag-icon flag-icon-' . FlagIconHelper::getIcon($languageCode)]);
        }

        return implode(' ', $links);
    }

}