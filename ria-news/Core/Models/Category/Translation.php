<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Category;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Ria\Core\Models\Meta;
use Ria\News\Core\Models\Category\Traits\TranslationLifecycleCallbacks;

/**
 * @ORM\Table(name="categories_lang", uniqueConstraints={@UniqueConstraint(name="slug", columns={"slug", "language"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Translation
{
    use TranslationLifecycleCallbacks;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    public $language;

    /**
     * @ORM\Column(type="text")
     */
    private $meta;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="translations")
     * @JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Meta
     */
    public function getMeta(): Meta
    {
        return $this->meta;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $slug
     * @return Translation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param string $title
     * @return Translation
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $language
     * @return Translation
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @param Meta $meta
     * @return Translation
     */
    public function setMeta(Meta $meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @param mixed $category
     * @return Translation
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
}