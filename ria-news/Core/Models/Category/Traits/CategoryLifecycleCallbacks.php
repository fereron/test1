<?php

namespace Ria\News\Core\Models\Category\Traits;

use Ria\News\Core\Models\Category\Template;

/**
 * Trait CategoryLifecycleCallbacks
 * @package Ria\News\Core\Models\Category\Traits
 * @property Template $template
 */
trait CategoryLifecycleCallbacks
{
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
//        $this->template = $this->getTemplate()->value();
    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {
        $this->template = new Template($this->template);
    }
}