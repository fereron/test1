<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Category;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Ria\Core\Models\Entity;
use Ria\Core\Helpers\Models\RelationsTrait;
use Ria\Core\Models\TagCache;
use Ria\News\Core\Models\Category\Traits\CategoryLifecycleCallbacks;

/**
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="Ria\News\Core\Query\Repositories\CategoriesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Category extends Entity implements TagCache
{
    use CategoryLifecycleCallbacks;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $template = 'default';

    /**
     * @ORM\Column(name="sort", type="integer")
     */
    private $sort;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\OneToMany(
     *     targetEntity="\Ria\News\Core\Models\Category\Translation",
     *     mappedBy="category",
     *     cascade={"persist", "remove"},
     *     indexBy="language"
     *     )
     */
    private $translations;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Ria\News\Core\Models\Post\Post",
     *     mappedBy="category",
     *     cascade={"persist", "remove"}
     *     )
     */
    private $posts;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->posts        = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $language
     * @return Translation|false
     */
    public function getTranslation(string $language)
    {
        return $this->translations[$language];
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param int $status
     * @return Category
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param Translation $translation
     * @return $this
     */
    public function addTranslation(Translation $translation): self
    {
        if (!$this->getTranslations()->contains($translation)) {
            $this->translations[$translation->getLanguage()] = $translation;
        }
        $translation->setCategory($this);

        return $this;
    }

    /**
     * @param mixed $template
     * @return Category
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return Category
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return Category
     */
    public function incrementSort(): self
    {
        $this->sort++;
        return $this;
    }

    /**
     * @param int $sort
     * @return Category
     */
    public function decrementSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return bool
     */
    public function isParent(): bool
    {
        return $this->parent === null;
    }

    /**
     * @param $parent
     * @return Category
     */
    public function setParent($parent): Category
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return Category|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getCacheTags(): array
    {
        return ['menu'];
    }
}