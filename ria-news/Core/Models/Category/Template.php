<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Category;

use InvalidArgumentException;
use Ria\Core\Models\ObjectValue;

/**
 * Class Template
 * @package Ria\News\Core\Models\Category
 */
class Template extends ObjectValue
{
    const DEFAULT      = 'default';
    const ECONOMICS    = 'economics';
    const SPORTS       = 'sports';
    const FOOTBALL     = 'football';
    const TECHNOLOGIES = 'technologies';
    /**
     * @var string
     */
    private $template;

    /**
     * template constructor.
     * @param string $template
     */
    public function __construct(string $template)
    {
        $this->set($template);
    }

    /**
     * @param string $template
     */
    public function set(string $template): void
    {
        if (!in_array($template, self::all())) {
            throw new InvalidArgumentException('Invalid category template: ' . $template);
        }

        $this->template = $template;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        return [
            self::DEFAULT,
            self::ECONOMICS,
            self::SPORTS,
            self::FOOTBALL,
            self::TECHNOLOGIES
        ];
    }

    public function __toString()
    {
        return $this->template;
    }
}