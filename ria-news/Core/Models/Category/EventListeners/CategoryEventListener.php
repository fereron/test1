<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Category\EventListeners;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Ria\News\Core\Models\Category\Category;

/**
 * Class CategoryEventListener
 * @package Ria\News\Core\Models\Category\EventListeners
 */
class CategoryEventListener
{

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {

        /** @var Category $category */
        $category = $args->getEntity();
        $em   = $args->getEntityManager();

        if ($category instanceof Category && $args->hasChangedField('sort')) {
            $qb = $em->createQueryBuilder();

            // new position smaller than or equal to old position,
            // so all positions from new position up to and including old position - 1 should increment
            $query = $qb
                ->update(Category::class, 'c')
                ->andWhere($qb->expr()->neq('c.id', ':current_id'))
                ->setParameter('current_id', $category->getId());

            if ($category->getParent()) {
                $query->andWhere('c.parent', ':parent')
                    ->setParameter('parent', $category->getParent()->getId());
            } else {
                $query->andWhere($qb->expr()->isNull('c.parent'));
            }


            if ($args->getNewValue('sort') < $args->getOldValue('sort')) {
                $query->set('c.sort', 'c.sort + 1')
                    ->andWhere($qb->expr()->between('c.sort', ':new', ':old'))
                    ->setParameter('new', $args->getNewValue('sort') - 1)
                    ->setParameter('old', $args->getOldValue('sort'));
            } else {
                $query->set('c.sort', 'c.sort - 1')
                    ->andWhere($qb->expr()->between('c.sort', ':old', ':new'))
                    ->setParameter('old', $args->getOldValue('sort'))
                    ->setParameter('new', $args->getNewValue('sort') + 1);
            }

            $query->getQuery()->execute();
        }
    }

}