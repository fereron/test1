<?php
declare(strict_types=1);

namespace Ria\Users\Models\Keyword;

/**
 * Class Keyword
 * @package Ria\Users\Models\Keyword
 * News, which include keywords are notified to specified users
 */
class Keyword
{
    /**
     * @var string
     * Слово, которое ищем в тексте
     */
    private $word;

    /**
     * @var bool
     * Чувствительность к регистру, если выбрано, учитываем регистр тоже
     */
    private $caseSensitive;

    /**
     * @var bool
     */
    private $status;
}