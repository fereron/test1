<?php

namespace Ria\News\Core\Models\Speech\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ria\Core\Models\Meta;
use Ria\News\Core\Models\Post\Icon;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;

/**
 * Trait LifecycleCallbacks
 * @package Ria\News\Core\Models\Speech\Traits\LifecycleCallbacks
 */
trait LifecycleCallbacks
{
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
        $this->updated_at = new DateTime();
    }

    /**
     * @ORM\PostUpdate()
     */
    public function postUpdate()
    {

    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {

    }
}