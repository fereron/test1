<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Speech;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Speech\Traits\LifecycleCallbacks;

/**
 * @ORM\Entity
 * @ORM\Table(name="post_speech")
 * @ORM\HasLifecycleCallbacks()
 */
class Speech
{

    use LifecycleCallbacks;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $filename;

    /**
     * @ORM\Column(type="string")
     */
    private $original_filename;

    /**
     * @ORM\OneToOne(targetEntity="Ria\News\Core\Models\Post\Post")
     * @JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return Speech
     */
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOriginalFilename(): ?string
    {
        return $this->original_filename;
    }

    /**
     * @param string|null $original_filename
     * @return Speech
     */
    public function setOriginalFilename(?string $original_filename): self
    {
        $this->original_filename = $original_filename;
        return $this;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post|null $post
     * @return Speech
     */
    public function setPost(?Post $post): self
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updated_at;
    }

}