<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Person;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="post_person")
 */

class PersonPost
{

    /**
     * @ORM\ManyToOne(targetEntity="Ria\News\Core\Models\Post\Post", inversedBy="post_person")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     * @ORM\Id()
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity="Ria\Persons\Core\Models\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * @ORM\Id()
     */
    private $person;


}