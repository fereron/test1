<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\ShortLink;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Ria\News\Core\Models\Post\Post;

/**
 * @ORM\Entity
 * @ORM\Table(name="post_short_links")
 */
class ShortLink
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $hash;

    /**
     * @ORM\Column(type="string")
     */
    private $redirect_to;

    /**
     * @ORM\Column(type="integer")
     */
    private $transits = 0;

    /**
     * @ORM\OneToOne(targetEntity="Ria\News\Core\Models\Post\Post")
     * @JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return ShortLink
     */
    public function setHash(string $hash): self
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectTo(): string
    {
        return $this->redirect_to;
    }

    /**
     * @param string $redirectTO
     * @return ShortLink
     */
    public function setRedirectTo(string $redirectTO): self
    {
        $this->redirect_to = $redirectTO;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransits()
    {
        return $this->transits;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return ShortLink
     */
    public function setPost(Post $post): self
    {
        $this->post = $post;
        return $this;
    }

    /**
     * Increment transits count +1
     */
    public function incrementTransits()
    {
        $this->transits++;
    }

    /**
     * @param int $transits
     * @return ShortLink
     */
    public function setTransits(int $transits): self
    {
        $this->transits = $transits;
        return $this;
    }

}