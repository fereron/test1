<?php

namespace Ria\News\Core\Models\Redirect;

use Doctrine\ORM\Mapping as ORM;
use Ria\Core\Models\Entity;

/**
 * Class Redirect
 * @package Ria\News\Core\Models\Redirect
 * @ORM\Table(name="redirects")
 * @ORM\Entity()
 */
class Redirect extends Entity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $old;

    /**
     * @ORM\Column(type="string")
     */
    private $new;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOld()
    {
        return $this->old;
    }

    /**
     * @return mixed
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * @param mixed $old
     * @return Redirect
     */
    public function setOld($old)
    {
        $this->old = $old;
        return $this;
    }

    /**
     * @param mixed $new
     * @return Redirect
     */
    public function setNew($new)
    {
        $this->new = $new;
        return $this;
    }
}