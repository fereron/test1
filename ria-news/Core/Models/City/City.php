<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\City;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;
use Ria\Core\Models\Entity;
use Ria\News\Core\Models\Region\Region;

/**
 * @ORM\Table(name="cities")
 * @ORM\Entity(repositoryClass="Ria\News\Core\Query\Repositories\CitiesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class City extends Entity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Translation",
     *     mappedBy="city",
     *     cascade={"persist", "remove"},
     *     indexBy="language"
     *     )
     */
    private $translations;

//    /**
//     * @ORM\OneToOne(targetEntity="Ria\News\Core\Models\Region\Region")
//     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
//     */

    /**
     * @ORM\ManyToOne(targetEntity="Ria\News\Core\Models\Region\Region")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;

    /**
     * Story constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @param mixed $region
     * @return $this
     */
    public function set($region)
    {
        $this->region     = $region;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    public function getRegionId()
    {
        return $this->region->id;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param string $language
     * @return Translation|false
     */
    public function getTranslation(string $language)
    {
        return $this->translations[$language];
    }

    /**
     * @param Translation $translation
     * @return $this
     */
    public function addTranslation(Translation $translation): self
    {
        if (!$this->getTranslations()->contains($translation)) {
            $this->translations[$translation->getLanguage()] = $translation;
            $translation->setCity($this);
        }

        return $this;
    }

}