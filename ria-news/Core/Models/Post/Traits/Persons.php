<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ria\News\Core\Models\Post\Post;
use Ria\Persons\Core\Models\Person;

/**
 * Class Persons
 * @package Ria\News\Core\Models\Post\Traits
 */
trait Persons
{
    /**
     * @ORM\ManyToMany(targetEntity="Ria\Persons\Core\Models\Person", inversedBy="posts", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="post_person")
     */
    private $persons;

    /**
     * @return array
     */
    public function getPersons(): array
    {
        return $this->persons->toArray();
    }

    /**
     * @param Person $person
     * @return Post
     */
    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons->add($person);
            /** @var Post $this */
            $person->addPost($this);
        }

        return $this;
    }


    /**
     * @param Person $person
     * @return Post
     */
    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            /** @var Post $this */
            $person->removePost($this);
//            $person->getTranslation($this->language)->decrementCount();
        }
        return $this;
    }

    /**
     * @param ArrayCollection $items
     * @return $this
     */
    public function syncPersons(ArrayCollection $items): self
    {
        // Remove unused relation
        foreach ($this->getForRemoval('persons', $items) as $item) {
            $this->removePerson($item);
        }

        // Add new relations
        foreach ($items as $item) {
            $this->addPerson($item);
        }

        return $this;
    }

}