<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Traits;

use Ria\News\Core\Models\Post\Notification;
use Ria\News\Core\Models\Post\Post;

/**
 * Class Notifications
 * @package Ria\News\Core\Models\Post\Traits
 */
trait Notifications
{
    /**
     * @ORM\OneToOne(
     *     targetEntity="\Ria\News\Core\Models\Post\Notification",
     *     mappedBy="post",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     *     )
     */
    private $notifications;

    /**
     * @return array
     */
    public function getNotifications(): array
    {
        return $this->notifications->toArray();
    }

    /**
     * @param mixed $notification
     * @return Post
     */
//    public function addNotification(Notification $notification): self
//    {
//        if (!empty($this->notifications) && !$this->notifications->contains($notification)) {
//            $this->notifications->add($notification);
//            $notification->setPost($this);
//        }
//
//        return $this;
//    }
}