<?php

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Ria\News\Core\Models\Post\Post;

trait Related
{
    /**
     * @ManyToMany(targetEntity="Post")
     * @JoinTable(name="post_related",
     *     joinColumns={@JoinColumn(name="post_id", referencedColumnName="id")},
     *     inverseJoinColumns={@JoinColumn(name="related_id", referencedColumnName="id")}
     * )
     */
    private $related;

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related->toArray();
    }

    /**
     * @param mixed $related
     * @return Post
     */
    public function addRelated(self $related)
    {
        if (!$this->related->contains($related)) {
            $this->related->add($related);
        }

        /** @var Post $this */
        return $this;
    }

    /**
     * @param mixed $related
     * @return Post
     */
    public function removeRelated(self $related)
    {
        if ($this->related->contains($related)) {
            $this->related->removeElement($related);
        }

        /** @var Post $this */
        return $this;
    }
}