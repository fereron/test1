<?php

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;

trait Translations
{
    /**
     * @ManyToMany(targetEntity="Post", indexBy="language")
     * @JoinTable(name="posts",
     *     joinColumns={@JoinColumn(name="id", referencedColumnName="group_id")},
     *     inverseJoinColumns={@JoinColumn(name="id", referencedColumnName="group_id")})
     */
    private $translations;

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->translations->toArray();
    }

    public function getTranslation($language)
    {
        return $this->translations[$language];
    }
}