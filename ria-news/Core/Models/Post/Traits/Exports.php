<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\ORM\Mapping as ORM;
use Ria\News\Core\Models\Post\Export;
use Ria\News\Core\Models\Post\Post;

/**
 * Trait Exports
 * @package Ria\News\Core\Models\Post\Traits
 */
trait Exports
{
    /**
     * @ORM\OneToMany(
     *     targetEntity="Ria\News\Core\Models\Post\Export",
     *     mappedBy="post",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     *     )
     */
    private $exports;

    /**
     * @return array
     */
    public function getExports(): array
    {
        return $this->exports->toArray();
    }

    /**
     * @param mixed $export
     * @return Post
     */
    public function addExport(Export $export): self
    {
        if (!$this->exports->contains($export)) {
            $this->exports->add($export);
            $export->setPost($this);
        }

        return $this;
    }
}