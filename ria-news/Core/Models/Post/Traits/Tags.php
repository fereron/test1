<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Tag\Tag;

/**
 * Trait Tags
 * @package Ria\News\Core\Models\Post\Traits
 */
trait Tags
{
    /**
     * @ORM\ManyToMany(targetEntity="Ria\News\Core\Models\Tag\Tag", inversedBy="posts", cascade={"persist", "remove"})
     * @JoinTable(name="post_tag")
     */
    private $tags;

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags->toArray();
    }

    /**
     * @param Tag $tag
     * @return Post
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            /** @var Post $this */
            $tag->addPost($this);

            $tag->getTranslations()
                ->filter(function ($translation) {
                    return $translation->language == $this->language;
                })
                ->first()
                ->incrementCount();
        }

        return $this;
    }

    /**
     * @param Tag $tag
     * @return Post
     */
    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            /** @var Post $this */
            $tag->removePost($this);
            $tag->getTranslation($this->language)->decrementCount();
        }

        return $this;
    }

    /**
     * @param ArrayCollection $items
     * @return $this
     */
    public function syncTags(ArrayCollection $items): self
    {
        // Remove unused relation
        foreach ($this->getForRemoval('tags', $items) as $item) {
            $this->removeTag($item);
        }

        // Add new relations
        foreach ($items as $item) {
            $this->addTag($item);
        }

        return $this;
    }
}