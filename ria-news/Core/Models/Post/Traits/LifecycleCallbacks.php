<?php

namespace Ria\News\Core\Models\Post\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ria\Core\Models\Meta;
use Ria\News\Core\Models\Post\Icon;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;

/**
 * Trait Post
 * @package Ria\News\Core\Models\Post\Traits\LifecycleCallbacks
 * @property Type $type
 * @property Icon $icon
 * @property Status $status
 * @property Meta $meta
 */
trait LifecycleCallbacks
{
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
        if (empty($this->created_at)) {
            $this->created_at = new DateTime();
            $this->updated_at = new DateTime();
        }
    }

    /**
     * @ORM\PostUpdate()
     */
    public function postUpdate()
    {

    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {
        $this->type   = new Type($this->type);
        $this->icon   = new Icon($this->icon);
        $this->status = new Status($this->status);
        $this->meta   = Meta::fromJson($this->meta);
    }
}