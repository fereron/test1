<?php

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Logs
{
    /**
     * @ORM\OneToMany(targetEntity="Ria\News\Core\Models\Post\Log\Log", mappedBy="post")
     * @ORM\JoinColumn(name="id", referencedColumnName="post_id")
     */
    private $logs;

    /**
     * @return mixed
     */
    public function getLogs()
    {
        return $this->logs;
    }
}