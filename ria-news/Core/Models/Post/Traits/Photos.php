<?php

namespace Ria\News\Core\Models\Post\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Ria\News\Core\Models\Post\Photo;

/**
 * Trait Photos
 * @package Ria\News\Core\Models\Post\Traits
 * @property ArrayCollection $photo_relations
 */
trait Photos
{
    /**
     * @ORM\OneToMany(targetEntity="Ria\News\Core\Models\Post\Photo", mappedBy="post", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $photo_relations;

    /**
     * @return ArrayCollection
     */
    public function getPhotos()
    {
        return $this->photo_relations->map(function (Photo $postPhoto) {
            return $postPhoto->getPhoto();
        });
    }
    /**
     * @return ArrayCollection
     */
    public function getPhotoRelations()
    {
        return $this->photo_relations;
    }

    /**
     * @param array $ids
     * @return bool
     */
    public function photosAreEqual(array $ids)
    {
        if (count($ids) != $this->photo_relations->count()) return false;

        foreach ($ids as $i => $id) {
            if ($id != $this->photo_relations[$i]->getPhoto()->getId()) return false;
        }

        return true;
    }

    /**
     * @param ArrayCollection $photoRelations
     * @return $this
     */
    public function syncPhotoRelations(ArrayCollection $photoRelations)
    {
        if ($photoRelations->isEmpty()) {
            $this->photo_relations->clear();
            return $this;
        }

        foreach ($this->getPhotoRelationsForRemoval($photoRelations) as $item) {
            $this->photo_relations->removeElement($item);
        }

        foreach ($photoRelations as $relation) {
            if (!$this->photo_relations->offsetExists($relation->getPhoto()->getId())) {
                $this->photo_relations->add($relation);
            }
        }

        return $this;
    }

    /**
     * @param ArrayCollection $photoRelations
     * @return array|ArrayCollection
     */
    protected function getPhotoRelationsForRemoval(ArrayCollection $photoRelations)
    {
        $selfIds = $this->photo_relations->map(function ($relation) {
            /** @var Photo $relation */
            return $relation->getPhoto()->getId();
        });

        if ($selfIds->isEmpty()) {
            return [];
        }

        $alienIds  = $photoRelations->map(function ($relation) {
            /** @var Photo $relation */
            return $relation->getPhoto()->getId();
        });

        $unusedIds = array_diff($selfIds->toArray(), $alienIds->toArray());

        return $this->photo_relations->filter(function ($relation) use ($unusedIds) {
            return in_array($relation->getPhoto()->getId(), $unusedIds);
        });
    }

    /**
     * @param ArrayCollection $photo_relations
     * @return Photos
     */
    public function setPhotoRelations(ArrayCollection $photo_relations)
    {
        $this->photo_relations = $photo_relations;
        return $this;
    }
}