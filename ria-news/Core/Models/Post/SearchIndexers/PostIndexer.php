<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\SearchIndexers;

use Doctrine\ORM\EntityManagerInterface;
use Elasticsearch\Client;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Query\ViewModel\PostViewModel;
use Yii;

/**
 * Class PostIndexer
 * @package Ria\News\Core\Models\Post\SearchIndexers
 */
class PostIndexer
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PostIndexer constructor.
     * @param Client $client
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Client $client, EntityManagerInterface $entityManager)
    {
        $this->client        = $client;
        $this->entityManager = $entityManager;
    }

    /**
     * Delete all records
     */
    public function clear(): void
    {
        $this->client->deleteByQuery([
            'index' => Yii::$app->params['elasticSearch']['index'],
            'type'  => Yii::$app->params['elasticSearch']['postsType'],
            'body'  => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);
    }

    /**
     * @param Post $post
     */
    public function index(Post $post)
    {
        $post         = $this->entityManager->find(Post::class, $post->getId());
        if (empty($post)) {
            return;
        }

        $postCategory = $post->getCategory()->getTranslation($post->getLanguage());

        $this->client->index([
            'index' => Yii::$app->params['elasticSearch']['index'],
            'type'  => Yii::$app->params['elasticSearch']['postsType'],
            'id'    => $post->getId(),
            'body'  => [
                'id'             => $post->getId(),
                'group_id'       => $post->getGroupId(),
                'title'          => $post->getTitle(),
                'description'    => $post->getDescription(),
                'slug'           => $post->getSlug(),
                'content'        => $post->getContent()->withoutTokens(),
                'category_title' => $postCategory->getTitle(),
                'category_slug'  => $postCategory->getSlug(),
                'lang'           => $post->getLanguage(),
                'image'          => $post->getImage(),
                'type'           => (string)$post->getType(),
                'status'         => (string)$post->getStatus(),
                'icon'           => (string)$post->getIcon(),
                'published_at'   => $post->getPublishedAt()->format('Y-m-d H:i:s'),
                'is_main'        => $post->getIsMain(),
                'is_exclusive'   => $post->getIsExclusive(),
                'is_actual'      => $post->getIsActual(),
                'is_breaking'    => $post->getIsBreaking(),
                'views'          => $post->getViews(),
            ]
        ]);
    }

    /**
     * @param PostViewModel|Post $post
     */
    public function remove($post): void
    {
        try {
            $this->client->delete([
                'index' => Yii::$app->params['elasticSearch']['index'],
                'type'  => Yii::$app->params['elasticSearch']['postsType'],
                'id'    => $post instanceof Post ? $post->getId() : $post->id,
            ]);
        } catch (\Exception $e) {
        }
    }
}