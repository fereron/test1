<?php

namespace Ria\News\Core\Models\Post\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ria\News\Core\Models\Post\Events\PostCreated;
use Ria\News\Core\Models\Post\Post;

/**
 * Class PostCreatedListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class PostCreatedListener
{

    /**
     * @param LifecycleEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        /** @var Post $post */
        $post = $args->getEntity();
        $em   = $args->getEntityManager();

        if ($post instanceof Post && is_null($post->getGroupId())) {
            $post->setGroupId($post->getId());

            $em->persist($post);
            $em->flush();
        }
    }

}