<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Core\Helpers\StatusHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Ria\News\Core\Models\Post\Post;
use Yii;
use yii\db\Exception;

/**
 * Class PostUpdatedListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class PostUpdatedListener
{

    /**
     * @param LifecycleEventArgs $args
     * @throws Exception
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        if (!$args->getEntity() instanceof Post) {
            return;
        }

        /** @var Post $post */
        $post = $args->getEntity();

        $this->flushTranslatesCache($post);

        if ($post->getIsMain()) {
            $this->flushMainPageCache($post->getLanguage());
        }

        $this->flushFeedPage($post->getLanguage());
        $this->flushLastNewsCache($post->getLanguage());
    }

    /**
     * @param PreUpdateEventArgs $args
     * @throws Exception
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        if (!$args->getEntity() instanceof Post) {
            return;
        }

        /** @var Post $post */
        $post = $args->getEntity();

        // set post deactivated true/false depending on the established status
        if ($args->hasChangedField('status')) {
            if (
                in_array($args->getOldValue('status'), StatusHelper::activeStatuses())
                && !in_array($args->getNewValue('status'), StatusHelper::activeStatuses())
            ) {
                $post->setIsDeactivated(true);
            }

            if (
                !in_array($args->getOldValue('status'), StatusHelper::activeStatuses())
                && in_array($args->getNewValue('status'), StatusHelper::activeStatuses())
            ) {
                $post->setIsDeactivated(false);
            }

            $args->getEntityManager()->persist($post);
        }
        // end of post deactivated

        if ($post->getStatus()->isDeleted()) {
            return;
        }

        $this->flushTranslatesCache($post, $args);

        $allowToFlushMainPage = $args->hasChangedField('is_main') || $post->getIsMain();
        if ($allowToFlushMainPage) {
            $this->flushMainPageCache($post->getLanguage());
        }

        $this->flushFeedPage($post->getLanguage());
        $this->flushLastNewsCache($post->getLanguage());
    }

    /**
     * @param string $language
     */
    private function flushMainPageCache(string $language)
    {
        foreach (['default', 'mobile'] as $theme) {
            Yii::$app->redis->expire('cache_' . md5(sprintf('app/.%s.%s', $theme, $language)), 90);
        }
    }

    /**
     * @param string $language
     */
    private function flushFeedPage(string $language)
    {
        $feedPageUrls = [
            'az' => '/son-xeberler/',
            'ru' => '/ru/posledniye-novosti/',
            'en' => '/en/latest-news/'
        ];

        Yii::$app->redis->expire('cache_' . md5(sprintf('app%s.default.%s', $feedPageUrls[$language], $language)), 90);
    }

    /**
     * @param string $language
     */
    private function flushLastNewsCache(string $language)
    {
        foreach (['default', 'mobile'] as $theme) {
            foreach (['index', 'post', 'amp'] as $type) {
                $langPrefix = $language == Yii::$app->params['defaultLanguage'] ? '' : '/' . $language;

                Yii::$app->redis->expire(
                    'cache_' . md5(sprintf('app%s/getLastNews/%s/.%s.%s', $langPrefix, $type, $theme, $language)),
                    90
                );
            }
        }
    }

    /**
     * @param Post $post
     * @param PreUpdateEventArgs|null $args
     * @throws Exception
     */
    private function flushTranslatesCache(Post $post, ?PreUpdateEventArgs $args = null)
    {
        $otherTranslates = Yii::$app->doctrine->getEntityManager()
            ->getRepository(Post::class)
            ->getOtherTranslations($post->getGroupId() ?? 0, $post->getLanguage());

        $translates = array_merge([$post], $otherTranslates);

        foreach ($translates as $i => $translate) {
            $langPrefix = ($post->getLanguage() != Yii::$app->params['defaultLanguage']) ? ($post->getLanguage() . '/') : '';

            if (!empty($args) && !$i) {
                $postSlug = $args->hasChangedField('slug') ? $args->getOldValue('slug') : $post->getSlug();
                $category = $args->hasChangedField('category') ? $args->getOldValue('category') : $post->getCategory();
            } else {
                $postSlug = $translate->getSlug();
                $category = $translate->getCategory();
            }

            $categorySlug = $category->getTranslation($translate->getLanguage())->getSlug();

            foreach (['default', 'mobile'] as $theme) {
                Yii::$app->redis->executeCommand('EXPIRE', ['cache_' . md5("app/{$langPrefix}{$categorySlug}/" . $postSlug . "/.{$theme}.{$translate->getLanguage()}"), 200]);
                Yii::$app->redis->executeCommand('EXPIRE', ['cache_' . md5("app/{$langPrefix}amp/{$categorySlug}/" . $postSlug . "/.{$theme}.{$translate->getLanguage()}"), 200]);

                if ($post->isPublished()) {
                    Yii::$app->redis->executeCommand('EXPIRE', ['cache_' . md5('app/' . $categorySlug . '/.' . $theme . '.' . $translate->getLanguage()), 200]);
                }
            }
        }
    }

}