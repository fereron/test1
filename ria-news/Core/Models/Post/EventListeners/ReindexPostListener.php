<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Ria\News\Core\Models\Post\Events\PostSaved;
use Ria\News\Core\Models\Post\SearchIndexers\PostIndexer;

/**
 * Class ReindexPostListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class ReindexPostListener
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ReindexPostListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostSaved $event
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function handle(PostSaved $event)
    {
        $post = $event->getPost();

        if (empty($post)) {
            return;
        }

        /** @var PostIndexer $postIndexer */
        $postIndexer = \Yii::$container->get(PostIndexer::class);

        if ($post->isPublished()) {
            $postIndexer->index($post);
        } else {
            $postIndexer->remove($post);
        }
    }

}