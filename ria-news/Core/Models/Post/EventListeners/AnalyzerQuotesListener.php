<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Ria\News\Core\Models\ExpertQuote\ExpertQuote;
use Ria\News\Core\Models\Post\Events\PostCreated;

/**
 * Class AnalyzerQuotesListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class AnalyzerQuotesListener
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PostCreatedListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostCreated $event
     */
    public function handle(PostCreated $event)
    {
        $post = $event->getPost();

        preg_match_all('/{{expert-quote-(\d+)}}/i', (string)$post->getContent(), $matches);

        foreach ($matches[1] as $i => $quoteId) {
            $quote = $this->getQuote((int)$quoteId);
            $quote->setPost($post);

            $this->entityManager->persist($quote);
            $this->entityManager->flush();
        }
    }

    /**
     * @param int $id
     * @return ExpertQuote|object|null
     */
    private function getQuote(int $id): ?ExpertQuote
    {
        return $this->entityManager->find(ExpertQuote::class, $id);
    }

}