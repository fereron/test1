<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Ria\News\Core\Models\Post\Events\HasPostAndUser;
use Ria\News\Core\Models\Post\Events\PostCreated;
use Ria\News\Core\Models\Post\Events\PostUpdated;
use RuntimeException;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class LongTitleListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class LongTitleListener
{
    /**
     * @param HasPostAndUser $event
     */
    public function handle(HasPostAndUser $event)
    {
        $post = $event->getPost();

        if ($event instanceof PostCreated) {
            if ($post->isPublished() && $this->titleIsLongThanMax($post->getTitle())) {
                $this->sendNotification($event);
            }
        } elseif ($event instanceof PostUpdated && $post->isPublished()) {
            $oldTitle = $event->getOld()->title;
            $newTitle = $post->getTitle();

            if ($newTitle != $oldTitle && $this->titleIsLongThanMax($newTitle)) {
                $this->sendNotification($event);
            }
        }
    }

    /**
     * @param string $title
     * @return bool
     */
    private function titleIsLongThanMax(string $title): bool
    {
        return mb_strlen($title, 'utf-8') > Yii::$app->params['maxTitleLength'];
    }

    /**
     * @param HasPostAndUser $event
     */
    private function sendNotification(HasPostAndUser $event)
    {
        $mailer = Yii::$app->mailer->compose(
            ['html' => 'post/longTitle-html', 'text' => 'post/longTitle-text'],
            ['post' => $event->getPost(), 'user' => $event->getUser()]
        )
            ->setTo($this->getReceivers())
            ->setSubject('Long post title detected | [ ' . Yii::$app->name . ' ]');

        if (!$mailer->send()) {
            throw new RuntimeException('Long title notify mail sending error.');
        }
    }

    /**
     * @return array
     */
    private function getReceivers(): array
    {
        $mails = Yii::$app->params['adminEmail'];
        if (YII_ENV_PROD) {
            ArrayHelper::removeValue($mails, 'i80586@mail.ru');
            return array_merge($mails, ['news@report.az']);
        }
        return $mails;
    }
}