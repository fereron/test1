<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Ria\News\Core\Models\Category\Category;
use Yii;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Ria\News\Core\Models\Category\Translation;
use Ria\News\Core\Models\Post\Events\PostSaved;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\ShortLink\ShortLink;

/**
 * Class CreateShortLinkListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class CreateShortLinkListener
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CreateShortLinkListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostSaved $event
     * @throws \yii\base\Exception
     */
    public function handle(PostSaved $event)
    {
        /** @var Post $post */
        $post = $this->entityManager->find(Post::class, $event->getPost()->getId());

        if (!empty($post) && $post->isPublished()) {
            $this->createOrUpdateShortLink($post);
        }
    }

    /**
     * @param Post $post
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function createOrUpdateShortLink(Post $post)
    {
        $shortLink = $this->getShortLink($post->getId());

        if (empty($shortLink)) {
            $shortLink = new ShortLink();
            $shortLink
                ->setPost($post)
                ->setHash($this->getHash());
        }

        $shortLink->setRedirectTo(sprintf('%s%s/%s/%s/',
            Yii::$app->frontendUrlManager->getHostInfo(),
            ($post->getLanguage() == Yii::$app->params['defaultLanguage']) ? '' : ('/' . $post->getLanguage()),
            $post->getCategory()->getTranslation($post->getLanguage())->getSlug(),
            $post->getSlug()
        ));

        $this->entityManager->persist($shortLink);
        $this->entityManager->flush();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    protected function getHash(): string
    {
        do {
            $hash = Yii::$app->security->generateRandomString(5);
        } while (!empty($this->entityManager->find(ShortLink::class, $hash)));

        return $hash;
    }

    /**
     * @param int $postId
     * @return ShortLink|null
     */
    protected function getShortLink(int $postId): ?ShortLink
    {
        /** @var EntityRepository $shortLink */
        $repository = $this->entityManager->getRepository(ShortLink::class);
        /** @var ShortLink|null $shortLink */
        $shortLink = $repository->findOneBy(['post' => $postId]);

        return $shortLink;
    }

}