<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Ria\News\Core\Models\Post\Events\PostArchived;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Redirect\Redirect;
use Ria\News\Core\Query\Repositories\PostsRepository;
use RuntimeException;
use Yii;

/**
 * Class PostArchivedListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class PostArchivedListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PostArchivedListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostArchived $event
     */
    public function handle(PostArchived $event)
    {
        /** @var PostsRepository $postRepo */
        $postRepo  = $this->entityManager->getRepository(Post::class);
        $viewModel = $postRepo->getByIdForRoute($event->getPost()->getId(), $event->getPost()->getLanguage());
        $old       = Yii::$app->frontendUrlManager->createAbsoluteUrl(['post/view', 'post' => $viewModel]);

        $alreadyExisting = $this->entityManager->getRepository(Redirect::class)->findOneBy(['old' => $old]);
        $redirect        = $alreadyExisting ?: (new Redirect())->setOld($old);
        $redirect->setNew($event->getUrl());
        $this->entityManager->persist($redirect);
        $this->entityManager->flush();

        $mailer = Yii::$app->mailer->compose(
            ['html' => 'post/archived-html', 'text' => 'post/archived-text'],
            ['post' => $event->getPost(), 'url' => $event->getUrl()]
        )
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Post archived | [ ' . Yii::$app->name . ' ]');

        if (!$mailer->send()) {
            throw new RuntimeException(PostArchived::class . ' event notify mail sending error.');
        }
    }
}