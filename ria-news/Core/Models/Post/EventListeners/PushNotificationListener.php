<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use GuzzleHttp\Exception\GuzzleException;
use Ria\News\Core\Models\Post\Events\PostSaved;
use Ria\News\Core\Models\Post\Export;
use Ria\News\Core\Models\Post\Notification;
use Ria\Core\Services\PushNotificationService;
use Ria\News\Core\Models\Post\Post;

/**
 * Class PushNotificationListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class PushNotificationListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PushNotificationListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostSaved $event
     * @return void
     * @throws ORMException
     * @throws GuzzleException
     */
    public function handle(PostSaved $event)
    {
        if (
            $event->getPost()->isPublished()
            && $this->hasPushNotificationExport($event->getPost())
            && (new DateTime())->diff($event->getPost()->getPublishedAt())->format('G') <= 12
        ) {

            $notification = $this->entityManager
                ->getRepository(Notification::class)
                ->findOneBy(['post' => $event->getPost()->getId()]);

            if (!$notification) {
                $pushNotification = $this->sendPushNotification($event->getPost());

                $notification = new Notification();
                $notification->setStatus((int)$pushNotification);
                $notification->setPost($this->entityManager->getReference(Post::class, $event->getPost()->getId()));
                $notification->setCreatedAt(new DateTime());

                $this->entityManager->persist($notification);
                $this->entityManager->flush();
            }
        }
    }

    /**
     * @param Post $post
     * @return bool
     */
    public function hasPushNotificationExport(Post $post): bool
    {
        foreach ($post->getExports() as $export) {
            if ($export->value() == Export::PUSH_NOTIFICATIONS) return true;
        }
        return false;
    }

    /**
     * @param $post
     * @return bool
     * @throws GuzzleException
     */
    public function sendPushNotification(Post $post): bool
    {
        // Made due __PHP_Incomplete_Class exception
        /** @var Post $post */
        $post = $this->entityManager->find(Post::class, $post->getId());

        $postAsArray = [
            'id'            => $post->getId(),
            'language'      => $post->getLanguage(),
            'status'        => $post->getStatus(),
            'slug'          => $post->getSlug(),
            'category_slug' => $post->getCategory()->getTranslation($post->getLanguage())->getSlug(),
            'title'         => $post->getTitle(),
            'description'   => $post->getDescription(),
            'image'         => $post->getImage(),
        ];

        $pushNotification = new PushNotificationService($postAsArray);
        return $pushNotification->notify();
    }
}