<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Caxy\HtmlDiff\HtmlDiff;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use phpQuery;
use Ria\News\Core\Models\Post\Events\HasPostAndUser;
use Ria\News\Core\Models\Post\Events\PostCreated;
use Ria\News\Core\Models\Post\Events\PostDeleted;
use Ria\News\Core\Models\Post\Events\PostUpdated;
use Ria\News\Core\Models\Post\Log\Log;
use Ria\News\Core\Models\Post\Log\Type;
use Ria\News\Core\Models\Post\Plain;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\Status;
use Ria\Users\Core\Models\User;
use Yii;
use yii\web\View;

/**
 * Class LogPostListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class LogPostListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * LogPostListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param HasPostAndUser $event
     */
    public function handle(HasPostAndUser $event)
    {
        Yii::$app->getModule('news');

        /** @var Post $post */
        $post = $this->entityManager->find(Post::class, $event->getPost()->getId());

        if (empty($post)) {
            return;
        }

        /** @var User $user */
        $user = $this->entityManager->find(User::class, $event->getUser()->getId());

        if ($event instanceof PostCreated) {
            $this->createLog(Type::TYPE_CREATED, $post, $user, $this->takeSnapshot(Type::TYPE_CREATED, $post));
        } elseif ($event instanceof PostUpdated) {
            $old = $event->getOld();
            if ($event->isHasNotChanged()) {
                $this->createLog(Type::TYPE_VIEWED, $post, $user);
            } else {
                if ($user->hasRole('corrector')) {
                    $type = Type::TYPE_CORRECTED;
                } else if ($post->getStatus()->is(Status::ON_MODERATION) && $old->status != Status::ON_MODERATION) {
                    $type = Type::TYPE_SENT_TO_MODERATION;
                } else {
                    $type = Type::TYPE_UPDATED;
                }
                $this->createLog($type, $post, $user, $this->takeSnapshot($type, $post, $old));
            }
        } elseif ($event instanceof PostDeleted) {
            $this->createLog(Type::TYPE_DELETED, $post, $user);
        } else {
            throw new InvalidArgumentException('Event is not handled by ' . self::class);
        }
    }

    /**
     * @param string $type
     * @param Post $post
     * @param User $user
     * @param string|null $snapshot
     */
    private function createLog(string $type, Post $post, User $user, ?string $snapshot = null)
    {
        $log = (new Log())
            ->setType(new Type($type))
            ->setPost($post)
            ->setUser($user)
            ->setSnapshot($snapshot)
            ->setCreatedAt(new \DateTime());
        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }

    /**
     * @param $type
     * @param Post $post
     * @param Plain|null $old
     * @return string
     */
    private function takeSnapshot($type, Post $post, ?Plain $old = null): string
    {
        $newHtml = (new View())->render('@common/log/post/view', [
            'post' => Plain::fromEntity($post),
            'new' => $type == Type::TYPE_CREATED,
        ]);

        if (!$old) {
            return $newHtml;
        }

        $oldHtml = (new View())->render('@common/log/post/view', [
            'post' => $old,
            'new' => false,
        ]);

        $htmlDiff = new HtmlDiff($oldHtml, $newHtml);
        return $this->getSnapshotWithOnlyChangedData($htmlDiff->build());
    }

    /**
     * @param string $html
     * @return string
     */
    private function getSnapshotWithOnlyChangedData(string $html): string
    {
        $dom = phpQuery::newDocumentHTML($html);
        $tr = $dom->find('table tr');

        foreach ($tr as $trItem) {
            $trItem = pq($trItem);

            $hasChanges = $trItem->find('td del,td ins')->length;
            if (!$hasChanges) {
                $trItem->remove();
            }
        }

        return (string)$dom;
    }
}