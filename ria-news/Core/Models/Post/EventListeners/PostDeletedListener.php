<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Ria\News\Core\Models\Post\Events\PostDeleted;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\SearchIndexers\PostIndexer;
use RuntimeException;
use Yii;

/**
 * Class PostDeletedListener
 * @package Ria\News\Core\Models\Post\EventListeners
 */
class PostDeletedListener
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CreateShortLinkListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PostDeleted $event
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function handle(PostDeleted $event)
    {
        /** @var Post $post */
        $post = $this->entityManager->find(Post::class, $event->getPost()->getId());

        $this->removeFromIndex($post);
        $this->sendMail($event);
    }

    /**
     * @param Post $post
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function removeFromIndex(Post $post)
    {
        /** @var PostIndexer $postIndexer */
        $postIndexer = Yii::$container->get(PostIndexer::class);
        $postIndexer->remove($post);
    }

    /**
     * @param PostDeleted $event
     */
    private function sendMail(PostDeleted $event)
    {
        $mailer = Yii::$app->mailer->compose(
            ['html' => 'post/deleted-html', 'text' => 'post/deleted-text'],
            ['post' => $event->getPost(), 'user' => $event->getUser(), 'cause' => $event->getCause()]
        )
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Post deactivated | [ ' . Yii::$app->name . ' ]');

        if (!$mailer->send()) {
            throw new RuntimeException(PostDeleted::class . ' event notify mail sending error.');
        }
    }

}