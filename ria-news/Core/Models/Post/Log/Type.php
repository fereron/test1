<?php

namespace Ria\News\Core\Models\Post\Log;

use Ria\Core\Models\ObjectValue;

class Type extends ObjectValue
{
    const TYPE_CREATED            = 'created';
    const TYPE_VIEWED             = 'viewed';
    const TYPE_CORRECTED          = 'corrected';
    const TYPE_UPDATED            = 'updated';
    const TYPE_DELETED            = 'deleted';
    const TYPE_ARCHIVED           = 'archived';
    const TYPE_SENT_TO_MODERATION = 'sent_to_moderation';

    private $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public static function all()
    {
        return [
            self::TYPE_CREATED,
            self::TYPE_VIEWED,
            self::TYPE_CORRECTED,
            self::TYPE_UPDATED,
            self::TYPE_DELETED,
            self::TYPE_ARCHIVED,
            self::TYPE_SENT_TO_MODERATION,
        ];
    }

    public function is($type)
    {
        return $this->type == $type;
    }

    public function __toString()
    {
        return $this->type;
    }
}