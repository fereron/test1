<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Exceptions;

use DomainException;

/**
 * Class PostException
 * @package Ria\News\Core\Models\Post\Exceptions
 */
class PostException extends DomainException
{

}