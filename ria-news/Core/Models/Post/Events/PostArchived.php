<?php

namespace Ria\News\Core\Models\Post\Events;

use Ria\News\Core\Models\Post\Post;

class PostArchived
{
    private $post;

    private $url;

    public function __construct(Post $post, string $url)
    {
        $this->post = $post;
        $this->url = $url;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}