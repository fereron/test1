<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Events;

use Ria\News\Core\Models\Post\Plain;
use Ria\News\Core\Models\Post\Post;
use Ria\Users\Core\Models\User;

/**
 * Class PostUpdated
 * @package Ria\News\Core\Models\Post\Events
 */
class PostUpdated implements HasPostAndUser
{
    /**
     * @var Post
     */
    private $post;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Plain
     */
    private $old;
    /**
     * @var bool
     */
    private $hasNotChanged;

    public function __construct(Post $post, User $user, $old, bool $hasNotChanged)
    {
        $this->post          = $post;
        $this->user          = $user;
        $this->old           = $old;
        $this->hasNotChanged = $hasNotChanged;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Plain
     */
    public function getOld()
    {
        return $this->old;
    }

    /**
     * @return bool
     */
    public function isHasNotChanged(): bool
    {
        return $this->hasNotChanged;
    }
}