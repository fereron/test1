<?php

namespace Ria\News\Core\Models\Post\Events;

use Ria\News\Core\Models\Post\Post;
use Ria\Users\Core\Models\User;

/**
 * Class PostCreated
 * @package Ria\News\Core\Models\Post\Events
 */
class PostCreated implements HasPostAndUser
{
    /**
     * @var Post
     */
    private $post;
    /**
     * @var User
     */
    private $user;

    /**
     * PostCreated constructor.
     * @param Post $post
     * @param User $user
     */
    public function __construct(Post $post, User $user)
    {
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}