<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Post\Events;

use Ria\News\Core\Models\Post\Post;
use Ria\Users\Core\Models\User;

/**
 * Class PostDeleted
 * @package Ria\News\Core\Models\Post\Events
 */
class PostDeleted implements HasPostAndUser
{
    /**
     * @var Post
     */
    private $post;
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $cause;

    /**
     * PostCreated constructor.
     * @param Post $post
     * @param User $user
     * @param string $cause
     */
    public function __construct(Post $post, User $user, string $cause)
    {
        $this->post  = $post;
        $this->cause = $cause;
        $this->user  = $user;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @return string
     */
    public function getCause(): string
    {
        return $this->cause;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}