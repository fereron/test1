<?php

namespace Ria\News\Core\Models\Post\Events;

use Ria\News\Core\Models\Post\Post;
use Ria\Users\Core\Models\User;

/**
 * Interface HasPostAndUser
 * @package Ria\News\Core\Models\Post\Events
 */
interface HasPostAndUser
{
    /**
     * @return User
     */
    public function getUser(): User;

    /**
     * @return Post
     */
    public function getPost(): Post;
}