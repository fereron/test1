<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Region;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;
use Ria\Core\Models\Entity;

/**
 * @ORM\Table(name="regions")
 * @ORM\Entity(repositoryClass="Ria\News\Core\Query\Repositories\RegionsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Region extends Entity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="`order`", type="integer")
     */
    private $order;

    /**
     * @ORM\OneToMany(
     *     targetEntity="\Ria\News\Core\Models\Region\Translation",
     *     mappedBy="region",
     *     cascade={"persist", "remove"},
     *     indexBy="language",
     *     fetch="EAGER"
     *     )
     */
    private $translations;

    /**
     * Story constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @param string $order
     * @return $this
     */
    public function set($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @param string $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param string $language
     * @return Translation|false
     */
    public function getTranslation(string $language)
    {
        return $this->translations[$language];
    }

    /**
     * @param Translation $translation
     * @return $this
     */
    public function addTranslation(Translation $translation): self
    {
        if (!$this->getTranslations()->contains($translation)) {
            $this->translations[$translation->getLanguage()] = $translation;
            $translation->setRegion($this);
        }

        return $this;
    }

}