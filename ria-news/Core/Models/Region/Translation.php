<?php

namespace Ria\News\Core\Models\Region;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="regions_lang", uniqueConstraints={@UniqueConstraint(name="slug", columns={"slug", "language"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Translation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    public $language;

    /**
     * @ORM\ManyToOne(targetEntity="\Ria\News\Core\Models\Region\Region", inversedBy="translations")
     * @JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @param string $title
     * @param string $slug
     * @param string $language
     * @return $this
     */
    public function set(string $title, string $slug, string $language)
    {
        $this->title       = $title;
        $this->slug        = $slug;
        $this->language    = $language;

        return $this;
    }

    /**
     * @param Region $region
     * @return $this
     */
    public function setRegion(Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

}