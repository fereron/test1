<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Tag;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\PersistentCollection;
use Ria\News\Core\Models\Post\Post;

/**
 *
 * @ORM\Table(name="tags",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="name_unique",
 *            columns={"name", "language"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="Ria\News\Core\Query\Repositories\TagsRepository")
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ManyToMany(targetEntity="Ria\News\Core\Models\Post\Post", mappedBy="tags", cascade={"persist", "remove"})
     * @JoinTable(name="post_tag")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="\Ria\News\Core\Models\Tag\Translation", mappedBy="tag", cascade={"persist", "remove"}, indexBy="language")
     */
    private $translations;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function removePost(Post $post):self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param string $language
     * @return null|Translation
     */
    public function getTranslation(string $language): ?Translation
    {
        return $this->translations[$language];
    }

    /**
     * @param Translation $translation
     * @return $this
     */
    public function addTranslation(Translation $translation)
    {
        if (!$this->getTranslations()->contains($translation)) {
            $this->translations[$translation->getLanguage()] = $translation;
            $translation->setTag($this);
        }

        return $this;
    }
}