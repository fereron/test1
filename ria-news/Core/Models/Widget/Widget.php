<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\Widget;

use Doctrine\ORM\Mapping as ORM;
use Ria\News\Core\Models\Widget\Traits\LifecycleCallbacks;

/**
 * @ORM\Table(name="media_widgets")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Widget
{

    use LifecycleCallbacks;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Widget
     */
    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @param Type $type
     * @return Widget
     */
    public function setType(Type $type): self
    {
        $this->type = $type;
        return $this;
    }

}