<?php

namespace Ria\News\Core\Models\Widget\Traits;

use Doctrine\ORM\Mapping as ORM;
use Ria\News\Core\Models\Widget\Type;

/**
 * Trait Widget
 * @package Ria\News\Core\Models\Widget\Traits\LifecycleCallbacks
 * @property Type $type
 */
trait LifecycleCallbacks
{
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
        if (!is_object($this->type)) {
            return;
        }

        $this->type = $this->type->value();
    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {
        $this->type = new Type($this->type);
    }
}