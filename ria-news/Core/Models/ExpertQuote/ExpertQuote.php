<?php
declare(strict_types=1);

namespace Ria\News\Core\Models\ExpertQuote;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Ria\News\Core\Models\Post\Post;
use Ria\Persons\Core\Models\Person;

/**
 * @ORM\Table(name="expert_quotes")
 * @ORM\Entity(repositoryClass="Ria\News\Core\Query\Repositories\ExpertQuotesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ExpertQuote
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ria\Persons\Core\Models\Person")
     * @JoinColumn(name="expert_id", referencedColumnName="id")
     */
    private $expert;

    /**
     * @ORM\ManyToOne(targetEntity="Ria\News\Core\Models\Post\Post")
     * @JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Person
     */
    public function getExpert(): Person
    {
        return $this->expert;
    }

    /**
     * @param Person $expert
     * @return ExpertQuote
     */
    public function setExpert(Person $expert): self
    {
        $this->expert = $expert;
        return $this;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post|null $post
     * @return ExpertQuote
     */
    public function setPost(?Post $post): self
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return ExpertQuote
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

}