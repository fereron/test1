<?php
declare(strict_types=1);

namespace Ria\News\Core\Validators;

use yii\validators\RegularExpressionValidator;

/**
 * Class SlugValidator
 * @package core\validators
 *
 *
 *
 * NEED TO BE MOVED TO THE CORE
 */
class SlugValidator extends RegularExpressionValidator
{
    /**
     * @var string
     */
    public $pattern = '#^[a-z0-9-]*$#s';
    /**
     * @var string
     */
    public $message = 'Некорректная ссылка';
}