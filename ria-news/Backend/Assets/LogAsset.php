<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class LogAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/log.js',
    ];
    /**
     * @var string[]
     */
    public $css = [
        'css/log.css',
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];
}