<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CategoriesAsset
 * @package Ria\News\Backend\Assets
 */
class RegionsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/regions.js',
        'js/slug.js'
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];
}