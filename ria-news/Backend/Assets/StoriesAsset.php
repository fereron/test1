<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class StoriesAsset
 * @package Ria\News\Backend\Assets
 */
class StoriesAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/stories.js',
        'js/slug.js'
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];

}