<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CategoriesAsset
 * @package Ria\News\Backend\Assets
 */
class CitiesAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/cities.js',
        'js/slug.js'
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];
}