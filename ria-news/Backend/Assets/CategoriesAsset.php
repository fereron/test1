<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CategoriesAsset
 * @package Ria\News\Backend\Assets
 */
class CategoriesAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/categories.js',
        'js/slug.js'
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];

}