<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class JQueryFormAsset
 * @package Ria\News\Backend\Assets
 */
class JQueryFormAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/jquery.form.min.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];

}