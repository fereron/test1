<?php

namespace Ria\News\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class NewsAsset
 * @package Ria\News\Backend\Assets
 */
class NewsAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $sourcePath = '@module/public';
    /**
     * @var array
     */
    public $js = [
        'js/news.js',
        'js/log.js',
    ];

    public $css = [
        'css/posts.css',
        'css/log.css',
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];

    public $publishOptions = [
        'forceCopy' => YII_ENV_DEV,
    ];

}