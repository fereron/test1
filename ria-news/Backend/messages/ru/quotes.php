<?php

return [
    'Add expert quote' => 'Добавить мнение эксперта',
    'Expert'           => 'Эксперт',
    'Choose expert'    => 'Выберите эксперта',
    'Quote not found'  => 'Экспертное мнение не найдено'
];