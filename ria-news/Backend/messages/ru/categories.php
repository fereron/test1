<?php

return [
    'Categories'   => 'Категории',
    'Status'       => 'Статус',
    'Parent id'    => 'Родитель',
    'Template'     => 'Шаблон',
    'Title'        => 'Заголовок',
    'Sorting'      => 'Сортировка',
    'Description'  => 'Описание',
    'Slug'         => 'Slug',
    'Is active'    => 'Активность',
    'Cover'        => 'Изображение',
    'Save'         => 'Сохранить',
    'Create category' => 'Новая категория',
    'Update category' => 'Редактировать ',
    'Settings'     => 'Настройки'
];