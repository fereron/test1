<?php

return [
    'type_created' => 'Создана',
    'type_viewed' => 'Просмотрена',
    'type_corrected' => 'Откорректирована',
    'type_updated' => 'Обновлена',
    'type_deleted' => 'Удалена',
    'type_archived' => 'Архивирована',
    'type_sent_to_moderation' => 'Отправлена на модерацию',

    'SuperAdministrator' => 'Супер Администратор',
    'SeniorNewsman' => 'Старший новостник',
    'Administrator' => 'Администратор',
    'DepartmentHead' => 'Начальник департамента',
    'Designer' => 'Дизайнер',
    'LeadTranslator' => 'Старший переводчик',
    'Newsman' => 'Новостник',
    'Translator' => 'Переводчик',
];