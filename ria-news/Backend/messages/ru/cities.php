<?php

return [
    'Cities' => 'Города',
    'City' => 'Город',
    'Region' => 'Регион',
    'Create city' => 'Создать город',
    'Update city' => 'Редактировать',
    'Slug'        => 'Slug'
];