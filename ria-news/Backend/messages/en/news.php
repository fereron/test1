<?php

return [
    'created'                => 'Created',
    'on_moderation'          => 'On Moderation',
    'waiting_for_correction' => 'Waiting For Correction',
    'read'                   => 'Read',
    'archived'               => 'Archived',
    'private'                => 'Private',
    'deleted'                => 'Deleted',

    'lightning'    => 'Lightning',
    'camera'       => 'Camera',
    'video-camera' => 'Video-camera',

    'option_type_ads'    => 'Advertising news',
    'option_type_closed' => 'Closed news',

    'type_post'        => 'Post',
    'type_article'     => 'Article',
    'type_photo'       => 'Photo',
    'type_video'       => 'Video',
    'type_opinion'     => 'Opinion',
    'type_infographic' => 'Infographics',
    'type_partners'    => 'Partners',

    'Main image is too small {size}' => 'Main image is too small. Minimal side size - {size}px'
];
