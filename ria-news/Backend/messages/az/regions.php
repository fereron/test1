<?php

return [
    'Regions'       => 'Bölqələr',
    'Order'         => 'Sıralama',
    'Title'         => 'Başlıq',
    'Create region' => 'Bölqə yarat',
    'Update region' => 'Redaktə et',
    'Slug'         => 'Slug',
];