<?php

return [
    'Cities' => 'Şəhərlər',
    'City' => 'Şəhər',
    'Region' => 'Bölqə',
    'Create city' => 'Şəhər yarat',
    'Update city' => 'Redaktə et',
    'Slug'        => 'Slug'
];