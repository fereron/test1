<?php

return [
    'Stories'      => 'Sujetlər',
    'Title'        => 'Başlıq',
    'Description'  => 'Təsvir',
    'Slug'         => 'Slug',
    'Is active'    => 'Görünmə',
    'Cover'        => 'Şəkil',
    'Save'         => 'Yadda saxla',
    'Create story' => 'Yeni süjet',
    'Update story' => 'Redaktə et ',
    'Settings'     => 'Ayarlar',
    'Show on site' => 'Saytda göstər',
    'Status'       => 'Aktivliyi'
];