<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Doctrine\ORM\QueryBuilder;
use Ria\Core\Web\BackendController;
use Ria\Persons\Core\Models\Person;
use yii\web\Response;

/**
 * Class PersonsController
 * @package Ria\News\Backend\Controllers
 */
class PersonsController extends BackendController
{
    /**
     * @param string $language
     * @param null $q
     * @return Response
     */
    public function actionList(string $language, $q = null)
    {
        /** @var QueryBuilder $query */
        $query = $this->entityManager
            ->getRepository(Person::class)
            ->createQueryBuilder('p');

        $persons = $query
            ->select(['p.id', 'CONCAT(pt.first_name, \' \', pt.last_name) AS text'])
            ->join('p.translations', 'pt')
            ->where('p.type.type = :type')
            ->andWhere($query->expr()->like('pt.first_name', ':name'))
            ->andWhere('pt.language = :language')
            ->setParameters([
                'name'     => "%$q%",
                'language' => $language,
                'type'     => 'person'
            ])
            ->getQuery()
            ->execute();

        return $this->asJson(['results' => $persons]);
    }

}