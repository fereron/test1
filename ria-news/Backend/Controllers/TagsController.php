<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Ria\Core\Web\BackendController;
use Ria\News\Core\Models\Tag\Tag;

/**
 * Class TagsController
 * @package Ria\News\Backend\Controllers
 */
class TagsController extends BackendController
{

    /**
     * @param null $q
     * @param string $language
     * @return \yii\web\Response
     */
    public function actionList(string $language, $q = null)
    {
        /** @var QueryBuilder $query */
        $query = $this->entityManager
            ->getRepository(Tag::class)
            ->createQueryBuilder('t');

        $tags = $query
            ->select(['tr.name AS id', 'tr.name AS text'])
            ->join('t.translations', 'tr')
            ->where($query->expr()->like('tr.name', ':name'))
            ->andWhere('tr.language = :language')
            ->setParameters([
                'name'     => "%$q%",
                'language' => $language
            ])
            ->orderBy('tr.count', 'desc')
            ->getQuery()
            ->execute();

        return $this->asJson(['results' => $tags]);
    }

}