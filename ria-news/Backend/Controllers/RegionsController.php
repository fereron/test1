<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Doctrine\ORM\EntityRepository;
use Yii;
use Ria\Core\Web\BackendController;
use Ria\News\Core\Models\Region\Region;
use Ria\News\Core\Forms\Region\RegionSearch;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use Ria\News\Core\Commands\Region\CreateRegionCommand;
use Ria\News\Core\Commands\Region\UpdateRegionCommand;
use Ria\News\Core\Commands\Region\DeleteRegionCommand;
use Ria\News\Core\Forms\Region\RegionCreateForm;
use Ria\News\Core\Forms\Region\RegionUpdateForm;

use Ria\News\Core\Commands\Region\MoveUpRegionCommand;
use Ria\News\Core\Commands\Region\MoveDownRegionCommand;
/**
 * Class RegionsController
 * @package Ria\News\Backend\Controllers
 */
class RegionsController extends BackendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var EntityRepository $repo */
        $repo = $this->entityManager->getRepository(Region::class);
        $searchModel  = new RegionSearch($repo);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $form = new RegionCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new CreateRegionCommand($form));
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $form]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $region = $this->findModel(Region::class, $id);
        $form = RegionUpdateForm::createFromModel($region);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new UpdateRegionCommand($id, $form));
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $form,
            'region' => $region
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete(int $id)
    {
        $this->bus->handle(new DeleteRegionCommand($id));
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionMoveUp(int $id)
    {
        $this->bus->handle(new MoveUpRegionCommand($id));

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionMoveDown(int $id)
    {
        $this->bus->handle(new MoveDownRegionCommand($id));

        return $this->redirect(['index']);
    }



}