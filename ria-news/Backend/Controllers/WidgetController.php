<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use League\Tactician\CommandBus;
use Ria\Core\Web\BackendController;
use Ria\News\Core\Commands\Widget\CreateWidgetCommand;
use Ria\News\Module;
use yii\filters\AccessControl;

/**
 * Class WidgetController
 * @package Ria\News\Backend\Controllers
 */
class WidgetController extends BackendController
{

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'       => true,
                        'permissions' => ['postsList']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $content = \Yii::$app->request->post('content');

        if (empty($content)) {
            return $this->asJson([
                'success' => false,
                'message' => Module::t('widgets', 'Widget content cannot be blank.')
            ]);
        }

        $command = new CreateWidgetCommand($content);
        $this->bus->handle($command);

        return $this->asJson([
            'success' => true,
            'token'   => sprintf('{{widget-content-%d}}', $command->id)
        ]);
    }

}