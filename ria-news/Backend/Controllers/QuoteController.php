<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Ria\Core\Web\BackendController;
use Ria\News\Core\Commands\ExpertQuote\CreateQuoteCommand;
use Ria\News\Core\Commands\ExpertQuote\UpdateQuoteCommand;
use Ria\News\Core\Forms\ExpertQuote\QuoteForm;
use Ria\News\Core\Models\ExpertQuote\ExpertQuote;
use Ria\News\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class QuoteController
 * @package Ria\News\Backend\Controllers
 */
class QuoteController extends BackendController
{

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'       => true,
                        'permissions' => ['postsList']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $form = new QuoteForm();

        if ($form->load(\Yii::$app->request->post())) {
            if (!$form->validate()) {
                return $this->asJson([
                    'success' => false,
                    'message' => implode(',', $form->firstErrors)
                ]);
            }

            $this->bus->handle(new CreateQuoteCommand($form));

            $quote = $this->entityManager
                ->getRepository(ExpertQuote::class)
                ->latestOne();

            return $this->asJson([
                'success' => true,
                'quote'   => $this->getNormalizedQuote($quote, $form->language)
            ]);
        }

        throw new NotFoundHttpException();
    }

    /**
     * @param int $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate(int $id)
    {
        $quote = $this->findModel(ExpertQuote::class, $id);

        $form = new QuoteForm($quote);

        if ($form->load(\Yii::$app->request->post())) {
            if (!$form->validate()) {
                return $this->asJson([
                    'success' => false,
                    'message' => implode(',', $form->firstErrors)
                ]);
            }

            $this->bus->handle(new UpdateQuoteCommand($form));

            return $this->asJson([
                'success' => true
            ]);
        }

        throw new NotFoundHttpException();
    }

    /**
     * @param int $id
     * @param string $language
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGet(int $id, string $language)
    {
        /** @var ExpertQuote|null $quote */
        $quote = $this->findModel(ExpertQuote::class, $id);

        return $this->asJson([
            'success' => true,
            'quote'   => $this->getNormalizedQuote($quote, $language)
        ]);
    }

    /**
     * @param ExpertQuote $quote
     * @param string $language
     * @return array|null
     */
    protected function getNormalizedQuote(ExpertQuote $quote, string $language): ?array
    {
        $expert            = $quote->getExpert();
        $expertTranslation = $expert->getTranslation($language);

        return [
            'id'     => $quote->getId(),
            'expert' => [
                'id'       => $expert->getId(),
                'name'     => $expertTranslation->getFirstName() . ' ' . $expertTranslation->getLastName(),
                'position' => $expertTranslation->getPosition()
            ],
            'text'   => $quote->getText()
        ];
    }

}