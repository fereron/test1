<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Doctrine\ORM\QueryBuilder;
use Exception;
use Ria\Core\Web\BackendController;
use Ria\News\Core\Commands\Post\ArchivePostCommand;
use Ria\News\Core\Commands\Post\CreatePostCommand;
use Ria\News\Core\Commands\Post\DeletePostCommand;
use Ria\News\Core\Commands\Post\MoveDateCommand;
use Ria\News\Core\Commands\Post\PreviewPostCommand;
use Ria\News\Core\Commands\Post\UpdatePostCommand;
use Ria\News\Core\Forms\Post\MoveDateForm;
use Ria\News\Core\Forms\Post\PostDeleteForm;
use Ria\News\Core\Forms\Post\PostForm;
use Ria\News\Core\Forms\Post\PostSearch;
use Ria\News\Core\Forms\Redirect\RedirectForm;
use Ria\News\Core\Models\Category\Category;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Query\Repositories\CategoriesRepository;
use Ria\News\Core\Query\Repositories\PostsRepository;
use Ria\News\Core\Services\PhotoArchive;
use Ria\News\Core\Services\PhotoArchiveException;
use Ria\News\Core\Services\PhotosArchivingService;
use Ria\News\Module;
use Ria\Persons\Core\Models\Person;
use Ria\Users\Core\Helpers\Identity;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class NewsController
 * @package Ria\News\Modules\Backend\Controllers
 */
class NewsController extends BackendController
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions'     => ['index', 'list', 'experts-list'],
                        'allow'       => true,
                        'permissions' => ['postsList']
                    ],
                    [
                        'actions'     => ['preview', 'archive-photos', 'history', 'preview-existing', 'create', 'create-translation', 'cancel-form'],
                        'allow'       => true,
                        'permissions' => ['createPost']
                    ],
                    [
                        'actions'     => ['update', 'cancel-form'],
                        'allow'       => true,
                        'permissions' => ['updatePost']
                    ],
                    [
                        'actions'     => ['delete', 'archive', 'archive-inactive', 'move-date'],
                        'allow'       => true,
                        'permissions' => ['deletePost']
                    ],
                    [
                        'actions'     => ['unblock'],
                        'allow'       => true,
                        'permissions' => ['unblockPost']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var PostsRepository $postRepository */
        $postRepository = $this->entityManager->getRepository(Post::class);
        /** @var CategoriesRepository $categoryRepository */
        $categoryRepository = $this->entityManager->getRepository(Category::class);

        $searchModel  = new PostSearch($postRepository, $categoryRepository);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->params['newsListLanguage'] = $searchModel->language;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $lang
     * @return string
     * @throws Exception
     */
    public function actionCreate($lang)
    {
        $form = PostForm::create(null, ['language' => $lang]);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new CreatePostCommand($form));

            return $this->redirect(
                $form->chosenReturnToForm()
                    ? ['update', 'id' => $this->getLastCreatedPost()->getId()]
                    : ['index', 'PostSearch[language]' => $form->language]
            );
        }

        return $this->render('create', ['model' => $form]);
    }

    /**
     * @param int $group_id
     * @param string $lang
     * @param string $originalLang
     * @return string
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionCreateTranslation(int $group_id, string $lang, string $originalLang)
    {
        $originalPost = $this->findModel(Post::class, ['group_id' => $group_id, 'language' => $originalLang]);
        $form         = PostForm::createFromOriginal($originalPost, ['group_id' => $group_id, 'language' => $lang]);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new CreatePostCommand($form));

            return $this->redirect(
                $form->chosenReturnToForm()
                    ? ['update', 'id' => $this->getLastCreatedPost()->getId()]
                    : ['index', 'PostSearch[language]' => $form->language]
            );
        }

        return $this->render('create-translation', [
            'model' => $form,
            'post'  => $originalPost
        ]);
    }

    /**
     * @return Post|object
     */
    protected function getLastCreatedPost(): Post
    {
        return Yii::$app->doctrine->getEntityManager()
            ->getRepository(Post::class)
            ->findOneBy([], ['id' => 'desc']);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionPreviewExisting(int $id)
    {
        $this->findModel(Post::class, ['id' => $id]);
        $command = PreviewPostCommand::existing($id);
        $this->bus->handle($command);
        return $this->redirect(Yii::$app->frontendUrlManager->createAbsoluteUrl([
            'post/preview',
            'preview' => Yii::$app->cache->get(['preview', $command->key]),
            'key'     => $command->key
        ]));
    }

    /**
     * @param $lang
     * @return Response
     * @throws Exception
     */
    public function actionPreview($lang)
    {
        $form = PostForm::create(null, ['language' => $lang]);
        if ($form->load(Yii::$app->request->post()) && $form->validate(['categoryId'])) {
            $command = PreviewPostCommand::new($form);
            $this->bus->handle($command);
            return $this->asJson([
                'success' => true,
                'data'    => Yii::$app->frontendUrlManager->createAbsoluteUrl([
                    'post/preview',
                    'preview' => Yii::$app->cache->get(['preview', $command->key]),
                    'key'     => $command->key
                ])
            ]);
        }
        return $this->asJson([
            'success' => false,
            'error'   => empty($form->errors) ? 'Incorrect data' : $form->getFirstError('categoryId')
        ]);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionHistory(int $id)
    {
        /** @var Post $post */
        $post = $this->findModel(Post::class, ['id' => $id]);
        return $this->asJson(['success' => true, 'html' => $this->renderPartial('_log', [
            'logs' => $post->getLogs()->toArray()
        ])]);
    }

    /**
     * @param int $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionMoveDate(int $id)
    {
        $post = $this->findModel(Post::class, $id);
        $form = new MoveDateForm($post);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new MoveDateCommand($id, $form));
            return $this->redirect(['index']);
        }
        return $this->render('move-date', [
            'model' => $form,
            'post'  => $post,
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionArchive(int $id)
    {
        $form = new RedirectForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new ArchivePostCommand($id, $form->new));
            return $this->asJson(['success' => true]);
        }

        return $this->asJson(['success' => false, 'error' => $form->getFirstError('new')]);
    }

    /**
     * @param int $id
     * @return string|Response
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionUpdate(int $id)
    {
        /** @var Post $post */
        $post = $this->findModel(Post::class, ['id' => $id]);
        $this->tryToEnter($post->getId());

        $form = PostForm::create($post, ['group_id' => $post->getGroupId(), 'language' => $post->getLanguage()]);

        if ($form->load(Yii::$app->request->post())) {
            if ($form->chosenCancel()) {
                $this->release($post->getId());
                return $this->redirect(['index', 'PostSearch[language]' => $form->language]);
            }

            if ($form->validate()) {
                $this->bus->handle(new UpdatePostCommand($form));
                $this->release($post->getId());

                return $this->redirect(
                    $form->chosenReturnToForm()
                        ? ['update', 'id' => $id]
                        : ['index', 'PostSearch[language]' => $form->language]
                );
            }
        }

        return $this->render('update', [
            'model' => $form,
            'post'  => $post
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete(int $id)
    {
        $form = new PostDeleteForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new DeletePostCommand(
                $id,
                $form->cause,
                Yii::$app->user->identity->getUser(),
                $form->status
            ));
            return $this->asJson(['success' => true]);
        }

        return $this->asJson(['success' => false, 'error' => implode(',', $form->getFirstErrors())]);
    }

    /**
     * @param string $language
     * @param null $q
     * @param null $current_id
     * @return Response
     */
    public function actionList(string $language, $q = null, $current_id = null)
    {
        /** @var QueryBuilder $query */
        $query = $this->entityManager
            ->getRepository(Post::class)
            ->createQueryBuilder('p');

        $posts = $query
            ->select(['p.id AS id', 'p.title AS text'])
            ->where('p.language = :language')
            ->andWhere($query->expr()->like('p.title', ':title'))
            ->andWhere($query->expr()->gt('p.published_at', ':time'))
            ->andWhere($query->expr()->neq('p.id', ':current_id'))
            ->orderBy('p.published_at', 'DESC')
            ->setMaxResults(10)
            ->setParameters([
                'language'   => $language,
                'title'      => "%$q%",
                'time'       => time(),
                'current_id' => $current_id
            ])
            ->getQuery()
            ->execute();

        return $this->asJson(['results' => $posts]);
    }

    /**
     * @param string $language
     * @param string|null $q
     * @return Response
     */
    public function actionExpertsList(string $language, ?string $q = null)
    {
        /** @var QueryBuilder $query */
        $query = $this->entityManager
            ->getRepository(Person::class)
            ->createQueryBuilder('p');

        $persons = $query
            ->select(['p.id AS id'])
            ->addSelect($query->expr()->concat('tr.first_name', $query->expr()->literal(' '), 'tr.last_name') . ' AS text')
            ->join('p.translations', 'tr', 'with', 'tr.language = :language')
            ->where('p.type.type = :type')
            ->andWhere($query->expr()->like('tr.first_name', ':title'))
            ->orWhere($query->expr()->like('tr.last_name', ':title'))
            ->setParameters([
                'type'     => 'expert',
                'title'    => "%$q%",
                'language' => $language,
            ])
            ->getQuery()
            ->execute();

        return $this->asJson(['results' => $persons]);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionArchivePhotos(int $id)
    {
        /** @var Post $post */
        $post = $this->findModel(Post::class, ['id' => $id]);

        try {
            $photosArchiving = new PhotosArchivingService($post);
            $photosArchiving->createArchive();
        } catch (PhotoArchiveException | Exception $e) {
            return $this->redirect(['index']);
        }
    }

    /**
     * @param string $language
     * @param int|null $id
     * @return Response
     */
    public function actionCancelForm(string $language, ?int $id = null)
    {
        if ($id) {
            $this->release($id);
        }

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index', 'PostSearch[language]' => $language]);
        }
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionUnblock(int $id)
    {
        $this->release($id, true);
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    protected function tryToEnter(int $id)
    {
        $cache = Yii::$app->cache;
        /** @var Identity $user */
        $user = Yii::$app->user->identity;

        $postUpdateSession = $cache->get(['post-update', $id]);
        if ($postUpdateSession) {
            if ($postUpdateSession['id'] != $user->getId() && !$user->isSuperAdministrator()) {
                throw new NotFoundHttpException(
                    Module::t('news', 'Post is opened by: {user}', ['user' => $postUpdateSession['name']])
                );
            }
        } else {
            $cache->set(['post-update', $id], [
                'id'   => $user->getId(),
                'name' => $user->getFullName(),
            ], 3600);
        }
    }

    /**
     * @param int $id
     * @param bool $force
     */
    protected function release(int $id, $force = false)
    {
        $postUpdateSession = Yii::$app->cache->get(['post-update', $id]);
        if ($postUpdateSession && ($force ? true : $postUpdateSession['id'] == Yii::$app->user->getId())) {
            Yii::$app->cache->delete(['post-update', $id]);
        }
    }
}
