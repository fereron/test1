<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Ria\Core\Web\BackendController;
use Ria\News\Core\Commands\Speech\CreateSpeechCommand;
use Ria\News\Core\Commands\Speech\DeleteSpeechCommand;
use Ria\News\Core\Commands\Speech\UpdateSpeechCommand;
use Ria\News\Core\Forms\Speech\SpeechForm;
use Ria\News\Core\Models\Speech\Speech;
use Yii;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class SpeechController
 * @package Ria\News\Backend\Controllers
 */
class SpeechController extends BackendController
{

    /**
     * @param int|null $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpload(?int $id = null)
    {
        $model = empty($id)
            ? new SpeechForm()
            : SpeechForm::createFrom($this->findModel(Speech::class, $id));

        $model->file = UploadedFile::getInstance($model, 'file');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (empty($id)) {
                    $this->bus->handle(new CreateSpeechCommand($model));
                } else {
                    $this->bus->handle(new UpdateSpeechCommand($model));
                }

                /** @var Speech $speech */
                $speech = $this->getSpeechByPost((int)$model->postId);

                return $this->asJson([
                    'success' => true,
                    'speech' => [
                        'id' => $speech->getId(),
                    ],
                    'player'  => Html::tag('audio', '', [
                        'src'      => Yii::$app->speech->getFile($speech->getFilename() . '?v=' . $speech->getUpdatedAt()->getTimestamp()),
                        'controls' => true,
                        'preload'  => 'none'
                    ])
                ]);
            } else {
                return $this->asJson([
                    'success' => false,
                    'message' => implode(',', $model->getFirstErrors())
                ]);
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @param int $id
     */
    public function actionDelete(int $id)
    {
        $this->bus->handle(new DeleteSpeechCommand($id));
    }

    /**
     * @param int $postId
     * @return object|null
     */
    protected function getSpeechByPost(int $postId)
    {
        return $this->entityManager->getRepository(Speech::class)->findOneBy(['post' => $postId]);
    }

}