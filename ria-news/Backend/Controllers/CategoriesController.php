<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Ria\News\Core\Commands\Category\MoveDownCategoryCommand;
use Ria\News\Core\Commands\Category\MoveUpCategoryCommand;
use Ria\News\Core\Models\Category\Category;
use Yii;
use Ria\Core\Web\BackendController;
use Ria\News\Core\Commands\Category\CreateCategoryCommand;
use Ria\News\Core\Commands\Category\DeleteCategoryCommand;
use Ria\News\Core\Commands\Category\UpdateCategoryCommand;
use Ria\News\Core\Forms\Category\CategoryForm;
use Ria\News\Core\Forms\Category\CategorySearch;
use Ria\News\Core\Forms\Category\CategorySearchForm;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class CategoriesController
 * @package Ria\News\Backend\Controllers
 */
class CategoriesController extends BackendController
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'       => true,
                        'permissions' => ['manageCategories']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $repository = $this->entityManager->getRepository(Category::class);
        $searchModel  = new CategorySearch($repository);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $form = CategoryForm::create();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new CreateCategoryCommand($form));

            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $form]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $category = $this->findModel(Category::class, $id);
        $form     = CategoryForm::create($category);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new UpdateCategoryCommand($form));

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model'    => $form,
            'category' => $category
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->bus->handle(new DeleteCategoryCommand(intval($id)));

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionMoveUp(int $id)
    {
        $this->bus->handle(new MoveUpCategoryCommand($id));

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionMoveDown(int $id)
    {
        $this->bus->handle(new MoveDownCategoryCommand($id));

        return $this->redirect(['index']);
    }

}
