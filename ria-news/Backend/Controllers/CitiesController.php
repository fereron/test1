<?php
declare(strict_types=1);

namespace Ria\News\Backend\Controllers;

use Ria\News\Core\Commands\Category\MoveUpCategoryCommand;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Ria\Core\Web\BackendController;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Forms\City\CitySearch;
use Ria\News\Core\Forms\City\CityCreateForm;
use Ria\News\Core\Forms\City\CityUpdateForm;
use Ria\News\Core\Commands\City\CreateCityCommand;
use Ria\News\Core\Commands\City\UpdateCityCommand;
use Ria\News\Core\Commands\City\DeleteCityCommand;
use Ria\News\Core\Models\Region\Region;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class CitiesController
 * @package Ria\News\Backend\Controllers
 */
class CitiesController extends BackendController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var EntityRepository $repo */
        $repo = $this->entityManager->getRepository(City::class);
        $searchModel  = new CitySearch($repo);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',
            compact('searchModel', 'dataProvider')
        );
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $form = new CityCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new CreateCityCommand($form));
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model'   => $form,
            'regions' => $this->getRegions()
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $region = $this->findModel(City::class, $id);
        $form = CityUpdateForm::createFromModel($region);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->bus->handle(new UpdateCityCommand($id, $form));
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $form,
            'regions' => $this->getRegions()
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete(int $id)
    {
        $this->bus->handle(new DeleteCityCommand($id));
        return $this->redirect(['index']);
    }

    public function getRegions()
    {
        return $this->entityManager
            ->getRepository(Region::class)
            ->createQueryBuilder('r')
            ->select('r.id', 'r.order', 'rt.title')
            ->innerJoin('r.translations', 'rt', 'WITH', 'rt.language = :lang')
            ->setParameters(['lang' => Yii::$app->language])
            ->getQuery()
            ->execute();
    }

}