<?php

/**
 * @var yii\web\View $this
 * @var Ria\News\Core\Models\Post\Log\Log[] $logs
 */

use Ria\News\Module;

?>
<div class="nav-tabs-vertical" data-plugin="tabs">
    <ul class="nav nav-tabs mr-25" role="tablist">
        <?php foreach ($logs as $i => $log): ?>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?= !$i ? 'active' : '' ?> text-center tab-link"
                   data-toggle="tab"
                   href="#log-<?= $i ?>"
                   role="tab"
                >
                    <span class="badge badge-info"><?= Module::t('log', 'type_' . $log->getType()) ?></span>
                    <br>
                    <span class="badge badge-primary">
                    <?= Module::t('log', $log->getUser()->getRoles()[0]->getName()) ?>
                </span>
                    <br>
                    <?= $log->getUser()->getTranslation(Yii::$app->language)->getFullName() ?>
                    <br>
                    <?= $log->getCreatedAt()->format('Y-m-d H:i:s') ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content py-15 log-content">
        <?php foreach ($logs as $i => $log):
            $dom    = phpQuery::newDocumentHTML($log->getSnapshot());
            $labels = $dom->find('span.log-translation-label');

            foreach ($labels as $label) {
                $label = pq($label);
                $label->text(Module::t('news', $label->text()));
            }
            $snapshot = (string)$dom;
            ?>
            <div class="tab-pane <?= !$i ? 'active' : '' ?>" id="log-<?= $i ?>" role="tabpanel">
                <?= $snapshot ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
