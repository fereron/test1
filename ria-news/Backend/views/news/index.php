<?php

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var PostSearch $searchModel
 */

use kartik\date\DatePicker;
use kartik\helpers\Html;
use Ria\Core\Helpers\FlagIconHelper;
use Ria\Core\Helpers\StatusHelper;
use Ria\Core\Widgets\Gridview\GridViewRemark;
use Ria\News\Backend\Assets\NewsAsset;
use Ria\News\Backend\grid\PostActionColumn;
use Ria\News\Core\Forms\Post\PostSearch;
use Ria\News\Core\Helpers\SpeechHelper;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\View\Grid\PostTranslationsColumn;
use Ria\News\Module;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;

$this->title                   = Yii::t('common', 'News');
$this->params['breadcrumbs'][] = $this->title;

NewsAsset::register($this);

$hostInfo = str_replace('adm.', '', Yii::$app->request->hostInfo);
?>

<div class="panel">
    <div class="panel-body">
        <div class="panel-heading mb-20">
            <div class="row">
                <div class="col-lg-4 col-6">
                    <?= Html::activeDropDownList(
                        $searchModel,
                        'language',
                        Yii::$app->params['languages'],
                        ['class' => 'outer-select form-control']
                    ) ?>
                </div>
                <div class="col-lg-4 col-6">
                    <?= Html::activeDropDownList(
                        $searchModel,
                        'translator_id',
                        $searchModel->getTranslatorsList(),
                        [
                            'class' => 'outer-select form-control',
                            'prompt' => Module::t('news', 'Choose translator')
                        ]
                    ) ?>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="create-news-langs">
                        <?php foreach (Yii::$app->params['languages'] as $code => $label): ?>
                            <a href="<?= Url::to(['create', 'lang' => $code]) ?>" class="btn btn-info">
                                <i class="flag-icon flag-icon-<?= FlagIconHelper::getIcon($code) ?>"></i>
                                +
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <?= GridViewRemark::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => '.outer-select',
            'options'        => [
                'class' => 'gridview table-responsive posts-list'
            ],
            'columns'        => [
                [
                    'attribute'      => 'id',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions'  => ['class' => 'text-center', 'style' => 'width:120px'],
                ],
                [
                    'attribute' => 'title',
                    'label'     => Module::t('news', 'Title'),
                    'format'    => 'raw',
                    'value'     => function (Post $post) use ($hostInfo) {
                        $additional = [];
                        $linkPrefix = '';

                        $isFuturePost  = $post->getPublishedAt()->getTimestamp() > time();
                        $isPrivatePost = $post->getStatus()->isPrivate();

                        if (empty($post->getImage())) {
                            $additional[] = Module::t('news', 'without photo');
                        }
                        if ($isFuturePost) {
                            $additional[] = Module::t('news', 'for future');
                            $linkPrefix   = '/future';
                        } elseif ($isPrivatePost) {
                            $linkPrefix = '/private';
                        }

                        return Html::a($post->getTitle(), sprintf(
                                $hostInfo . '%s%s/%s/%s/',
                                ($post->getLanguage() == Yii::$app->params['defaultLanguage']) ? '' : ('/' . $post->getLanguage()),
                                $linkPrefix,
                                $post->getCategory()->getTranslation($post->getLanguage())->getSlug(),
                                $post->getSlug()
                            ), ['target' => '_blank', 'class' => 'post-link'])
                            . (empty($additional) ? '' : (' <sup>' . implode(' ' . Module::t('news', 'and') . ' ', $additional) . '</sup>'));
                    }
                ],
                [
                    'attribute' => 'category',
                    'filter'    => $searchModel->getCategoriesList(),
                    'label'     => Module::t('news', 'Category'),
                    'value'     => function (Post $post) {
                        return $post->getCategory()->getTranslation($post->getLanguage())->getTitle();
                    },
                ],
                [
                    'attribute' => 'author_id',
                    'filter'    => $searchModel->getAuthorsList(),
                    'label'     => Module::t('news', 'Author'),
                    'format'    => 'raw',
                    'value'     => function (Post $post) {
                        return Html::tag('span',
                            $post->getAuthor()->getTranslation($post->getLanguage())->getFullName(),
                            ['class' => 'badge badge-outline badge-info']
                        );
                    },
                ],
                [
                    'attribute' => 'status',
                    'filter'    => $searchModel->getStatusesList(),
                    'label'     => Module::t('news', 'Status'),
                    'format'    => 'raw',
                    'value'     => function (Post $post) {
                        return StatusHelper::postStatusLabel($post->getStatus());
                    },
                ],
                [
                    'header'         => Module::t('news', 'Note'),
                    'format'         => 'html',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions'  => ['class' => 'text-center'],
                    'value'          => function (Post $post) use ($searchModel) {
                        return Html::tag('span', $searchModel->getNoteForPost($post), ['class' => 'badge badge-md badge-default']);
                    }
                ],
                [
                    'header'         => Module::t('news', 'Busy'),
                    'format'         => 'raw',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions'  => ['class' => 'text-center'],
                    'value'          => function (Post $post) {
                        $data = Yii::$app->cache->get(['post-update', $post->getId()]);
                        $unblockButton = Yii::$app->user->can('unblockPost') ? Html::a('x', ['unblock', 'id' => $post->getId()], [
                            'class' => 'unblock-post',
                            'data-method' => 'post',
                            'data-confirm' => 'Confirm post unblocking',
                        ]) : '';
                        return ($data) ? Html::tag(
                                'span',
                                $data['name'] . $unblockButton,
                                ['class' => 'badge badge-lg badge-warning']) : '-';
                    }
                ],
                [
                    'attribute'      => 'published_at',
                    'label'          => Module::t('news', 'Datetime'),
                    'filter'         => DatePicker::widget([
                        'model'         => $searchModel,
                        'attribute'     => 'dateFrom',
                        'attribute2'    => 'dateTo',
                        'type'          => DatePicker::TYPE_RANGE,
                        'separator'     => false,
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose'      => true,
                            'format'         => 'yyyy-mm-dd'
                        ],
                    ]),
                    'format'         => 'raw',
                    'value'          => function (Post $post) {
                        return $post->getPublishedAt()->format('d.m.Y / H:i:s');
                    },
                    'contentOptions' => [
                        'style' => 'width:250px'
                    ],
                ],
                [
                    'header'         => Module::t('news', 'Audio'),
                    'format'         => 'html',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions'  => ['class' => 'text-center'],
                    'value'          => function (Post $post) {
                        return SpeechHelper::exists($post->getId())
                            ? Html::tag('span', '', ['class' => 'fa fa-microphone'])
                            : '';
                    }
                ],
                [
                    'label' => Module::t('news', 'Translations'),
                    'class' => PostTranslationsColumn::class
                ],
                [
                    'class'          => PostActionColumn::class,
                    'contentOptions' => ['class' => 'posts-actions'],
                    'visibleButtons' => [
                        'update' => function () {
                            return Yii::$app->user->can('updatePost');
                        },
                        'delete' => function () {
                            return Yii::$app->user->can('deletePost');
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>

<!-- Modal Log -->
<div class="modal fade" id="log" role="dialog">
    <div class="modal-dialog modal-simple modal-lg modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= Module::t('news', 'Post history') ?></h4>

                <button type="button" class="close" data-dismiss="modal">
                    <i class="icon md-close"></i>
                </button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('common', 'Close') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- End Log -->