<?php

/* @var $this yii\web\View */
/* @var $model PostForm */

/* @var $post Post|null */

use Core\Helpers\StatusHelper as ProjectStatusHelper;
use dosamigos\multiselect\MultiSelectListBox;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use Ria\News\Backend\Widgets\AddExpertQuote\AddExpertQuote;
use Ria\News\Backend\Widgets\PostSpeechUpload\PostSpeechUpload;
use Ria\News\Core\Components\CKEditor\CKEditor;
use Ria\News\Backend\Assets\NewsAsset;
use Ria\News\Backend\Widgets\AddPostMedia\AddPostMedia;
use Ria\News\Core\Forms\Post\PostForm;
use Ria\News\Core\Helpers\CityHelper;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\Type;
use Ria\News\Module;
use Ria\News\Backend\Widgets\AddTimeline\AddTimeline;
use Ria\Photos\Widgets\PhotoManager\PhotoManager;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$cityHelper = new CityHelper();
NewsAsset::register($this);
?>

<?= Html::hiddenInput('wasIsPublished', $model->isNewRecord() ? false : $post->isPublished(), ['id' => 'wasIsPublished']) ?>
<?= Html::hiddenInput('postId', $model->isNewRecord() ? false : $post->getId(), ['id' => 'postId']) ?>

<?= Html::errorSummary($model, ['class' => 'alert alert-danger', 'encode' => false]) ?>

<?php $form = ActiveForm::begin(['id' => 'post-form']); ?>
<div class="row">
    <div class="col-lg-8">
        <div class="panel">
            <div class="panel-body">

                <?= $form->field($model, 'title')->textInput([
                    'class'               => 'form-control has-counter check-length',
                    'data-counter-target' => '.title-counter',
                    'maxlength'           => true,
                    'data-max'            => 75
                ]) ?>

                <p class="char-counter">
                    <?= Yii::t('common', 'Characters count:') ?>
                    <span class="__counter title-counter <?= mb_strlen($model->title, 'utf-8') > Yii::$app->params['maxTitleLength'] ? 'has-error' : '' ?>">0</span>
                    /
                    75
                </p>

                <?= $form->field($model, 'marked_words')->widget(Select2::class, [
                    'options'       => [
                        'multiple'    => true,
                        'placeholder' => Module::t('news', 'Enter marked words'),
                        'id'          => 'set-marked-words'
                    ],
                    'pluginOptions' => [
                        'tags'               => true,
                        'allowClear'         => true,
                        'minimumInputLength' => 3,
                        'language'           => [
                            'errorLoading' => new JsExpression("function () { return 'Ищем...'; }"),
                        ],
                        'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                        'templateResult'     => new JsExpression('function(tag) { return tag.text; }'),
                        'templateSelection'  => new JsExpression('function (tag) { return tag.text; }'),
                    ],
                ]) ?>

                <?= $form->field($model, 'slug')->textInput(['disabled' => !$model->isNewRecord() ? ProjectStatusHelper::isActive($model->getPost()->getStatus()) : false]) ?>

                <?= $form->field($model, 'description')->textarea([
                    'class'               => 'form-control has-counter',
                    'data-counter-target' => '.description-counter'
                ]) ?>

                <p class="char-counter">
                    <?= Yii::t('common', 'Characters count:') ?> <span
                            class="__counter description-counter">0</span> / 200
                </p>

                <?php if (!$model->isNewRecord())
                    echo PostSpeechUpload::widget(['postId' => $model->getPostId()]) ?>

                <?= $form->field($model, 'tags')->widget(Select2::class, [
                    'options'       => [
                        'multiple'    => true,
                        'placeholder' => Module::t('news', 'Enter tags'),
                        'id'          => 'set-tags'
                    ],
                    'pluginOptions' => [
                        'tags'               => true,
                        'allowClear'         => true,
                        'minimumInputLength' => 3,
                        'language'           => [
                            'errorLoading' => new JsExpression("function () { return 'Ищем...'; }"),
                        ],
                        'ajax'               => [
                            'url'      => Url::to(['tags/list']),
                            'dataType' => 'json',
                            'data'     => new JsExpression("function(params) { return {language: '" . $model->language . "', q:params.term}; }")
                        ],
                        'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                        'templateResult'     => new JsExpression('function(tag) { return tag.text; }'),
                        'templateSelection'  => new JsExpression('function (tag) { return tag.text; }'),
                    ],
                ]) ?>

                <?= $form->field($model, 'persons')->widget(Select2::class, [
                    'data'          => $model->getPersonsList(false),
                    'options'       => [
                        'multiple'    => true,
                        'placeholder' => Module::t('news', 'Enter person name'),
                        'id'          => 'set-persons'
                    ],
                    'pluginOptions' => [
//                        'tags'               => true,
                        'allowClear'         => true,
                        'minimumInputLength' => 3,
                        'language'           => [
                            'errorLoading' => new JsExpression("function () { return 'Ищем...'; }"),
                        ],
                        'ajax'               => [
                            'url'      => Url::to(['persons/list']),
                            'dataType' => 'json',
                            'data'     => new JsExpression("function(params) { return {language: '" . $model->language . "', q:params.term}; }")
                        ],
                        'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                        'templateResult'     => new JsExpression('function(tag) { return tag.text; }'),
                        'templateSelection'  => new JsExpression('function (tag) { return tag.text; }'),
                    ],
                ]) ?>

                <?= $form->field($model, 'photos')->widget(PhotoManager::class, [
                    'main'   => [
                        'attribute' => 'image',
                        'value'     => $model->image
                    ],
                    'photos' => isset($post) ? $post->getPhotos()->toArray() : []
                ])->label(false) ?>

                <?= $form->field($model, 'content')->widget(CKEditor::class, [
                    'extraPlugins'  => [
//                            ['fixed', '@module/Widgets/FixedSize', 'plugin.js'], // for sticky toolbar
                        ['addMedia', '@module/Widgets/AddPostMedia/web/editorButton', 'plugin.js'],
                        ['addExpertQuote', '@module/Widgets/AddExpertQuote/web/editorButton', 'plugin.js'],
                        ['addTimeline', '@module/Widgets/AddTimeline/web/timelineButton', 'plugin.js'],
                        ['wordcount', '@module/Widgets/AddCharactersCounter/web/editorCounter', 'plugin.js']
                    ],
                    'options'       => ['id' => 'with-photo-manager'],
                    'editorOptions' => [
                        "title"                     => '',
                        'extraPlugins'              => 'addMedia,addExpertQuote,addTimeline,wordcount',
                        'height'                    => 500,
                        'language'                  => Yii::$app->language,
//                            'toolbarGroups' => [
//                                ['name' => 'clipboard', 'groups' => ['mode', 'undo', 'selection', 'clipboard', 'doctools']],
//                                ['name' => 'editing', 'groups' => ['find', 'spellchecker', 'tools', 'about']],
//                                '/',
//                                ['name' => 'paragraph', 'groups' => ['templates', 'list', 'indent', 'align']],
//                                '/',
//                                ['name' => 'styles'],
//                                ['name' => 'blocks'],
//                                '/',
//                                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors', 'cleanup']],
//                                ['name' => 'links', 'groups' => ['links', 'insert']],
//                                ['name' => 'others'],
//                            ],
                        'removeButtons'             => 'Smiley,Iframe,Flash,Font',
                        'removePlugins'             => 'contextmenu,liststyle,tabletools,tableselection',
                        'disableNativeSpellChecker' => false,
                        'entities'                  => false,
                        'extraAllowedContent'       => ['span(*)', 'div(*)'],
                    ]
                ]); ?>
                <p class="char-counter">
                    <?= Yii::t('common', 'Characters count:') ?> <span
                            class="__counter content-counter">0</span> / 2000
                </p>

                <?= $form->field($model, 'icon')->dropDownList($model->getIconsList(), ['prompt' => ' - ']) ?>

                <?= $form->field($model, 'source')->textInput() ?>

                <?= $form->field($model, 'note')->textInput() ?>

                <?= $form->field($model, 'related')->widget(Select2::class, [
                    'data'          => $model->getRelatedList(),
                    'options'       => [
                        'multiple'    => true,
                        'placeholder' => Module::t('news', 'Enter related post')
                    ],
                    'pluginOptions' => [
                        'allowClear'         => true,
                        'minimumInputLength' => 3,
                        'language'           => [
                            'errorLoading' => new JsExpression("function () { return 'Ищем...'; }"),
                        ],
                        'ajax'               => [
                            'url'      => Url::to(['list']),
                            'dataType' => 'json',
                            'data'     => new JsExpression("function(params) { return {language:'" . $model->language . "',q:params.term, current_id:" . (int)$model->getPostId() . "}; }")
                        ],
                        'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                        'templateResult'     => new JsExpression('function(post) { return post.text; }'),
                        'templateSelection'  => new JsExpression('function (post) { return post.text; }'),
                    ],
                ]) ?>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading with-border">
                <h4 class="panel-title"><?= Module::t('news', 'Export') ?></h4>
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'exports')->widget(MultiSelectListBox::class, [
                    'data'          => $model->getExports(),
                    'options'       => [
                        'multiple' => 'multiple',
                    ],
                    'clientOptions' => [
                        'selectionHeader'  => Module::t('news', 'Chosen'),
                        'selectableHeader' => Module::t('news', 'Available'),
                    ]
                ])->label(false) ?>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading with-border">
                <h4 class="panel-title"><?= Yii::t('meta', 'Meta tags') ?></h4>
            </div>
            <div class="panel-body">
                <?= $form->field($model->meta, 'title')->textInput() ?>
                <?= $form->field($model->meta, 'keywords')->textarea() ?>
                <?= $form->field($model->meta, 'description')->textarea() ?>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-body">
                <?= $form->field($model, 'categoryId')->dropDownList($model->getCategoriesList(), ['prompt' => ' - ']) ?>

                <?= $form->field($model, 'cityId')->dropDownList(
                    $cityHelper->sortedList($model->language),
                    ['prompt' => '-']
                ) ?>

                <?= $form->field($model, 'type')->dropDownList($model->getTypesList(), ['id' => 'type-select']) ?>

                <?= $form->field($model, 'authorId', ['enableClientValidation' => false])->widget(Select2::class, [
                    'data' =>  $model->getAuthorsList(),
                    'options' => [
                        'class' => 'form-control'
                    ],
                ]); ?>

                <?= $form->field($model, 'expertId', ['enableClientValidation' => false])->widget(Select2::class, [
                    'data' => $model->getExpertsList(),
                    'options' => [
                        'class' => 'form-control type-depended',
                        'data-accept' => Type::OPINION,
                    ],
                ]);
//                $form->field($model, 'expertId', ['enableClientValidation' => false])->dropDownList($model->getExpertsList(), [
//                    'class' => 'form-control type-depended',
//                    'data-accept' => Type::OPINION,
//                ]);
                ?>

                <?php if ($model->isTranslateCreation() || !empty($model->translatorId)) {
                    echo $form->field($model, 'translatorId')
                        ->dropDownList($model->getTranslatorsList(), ['prompt' => ' - ']);
                } ?>

                <?= $form->field($model, 'storyId')->dropDownList($model->getStoriesList(), ['prompt' => ' - ']) ?>

                <div class="form-group" id="youtube_id">
                    <?= $form->field($model, 'youtubeId', ['enableClientValidation' => false])->textInput() ?>
                    <div class="video-preview col-md-12">
                        <div class="row">
                            <iframe src="<?= $model->getYoutubeEmbedUrl() ?>" width="100%" height="252"
                                    frameborder="0"></iframe>
                        </div>
                    </div>
                </div>

                <?= $form->field($model, 'optionType')->dropDownList($model->getOptionTypesList(), ['prompt' => '-']) ?>

                <div class="row">
                    <div class="col-lg-7">
                        <?= $form->field($model, 'publishedAt', ['template' => '
                            <div class="form-group has-default bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                    </span>
                                    {input}
                                </div>
                            </div>'
                        ])->widget(DateTimePicker::class, [
                            'type'          => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'orientation'    => 'top right',
                                'todayHighlight' => true,
                                'todayBtn'       => true,
                                'format'         => 'yyyy-mm-dd hh:ii:ss',
                                'autoclose'      => true,
                            ]
                        ]) ?>
                    </div>

                    <div class="col-lg-5">
                        <div class="form-group">
                            <div class="checkbox-custom checkbox-primary">
                                <?= Html::activeCheckbox($model, 'setCurrentDate', ['checked' => 'checked', 'label' => false]) ?>
                                <?= Html::activeLabel($model, 'setCurrentDate') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?= $form->field($model, 'status')->dropDownList($model->getStatusesList()) ?>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="checkbox-custom checkbox-primary">
                            <?= Html::activeCheckbox($model, 'isMain', ['label' => false]) ?>
                            <?= Html::activeLabel($model, 'isMain') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="checkbox-custom checkbox-primary">
                            <?= Html::activeCheckbox($model, 'isExclusive', ['label' => false]) ?>
                            <?= Html::activeLabel($model, 'isExclusive') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="checkbox-custom checkbox-primary">
                            <?= Html::activeCheckbox($model, 'isActual', ['label' => false]) ?>
                            <?= Html::activeLabel($model, 'isActual') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="checkbox-custom checkbox-primary">
                            <?= Html::activeCheckbox($model, 'isBreaking', ['label' => false]) ?>
                            <?= Html::activeLabel($model, 'isBreaking') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="checkbox-custom checkbox-primary">
                            <?= Html::activeCheckbox($model, 'isImportant', ['label' => false]) ?>
                            <?= Html::activeLabel($model, 'isImportant') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="checkbox-custom checkbox-primary">
                            <?= Html::activeCheckbox($model, 'linksNoindex', ['label' => false]) ?>
                            <?= Html::activeLabel($model, 'linksNoindex') ?>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group float-right">
                    <?= Html::submitButton(Module::t('news', 'Save'), [
                        'class' => 'btnFormSubmit btn btn-success',
                        'name'  => 'PostForm[btnSave]',
                        'value' => 'stay'
                    ]) ?>
                    <?= Html::submitButton(Module::t('news', 'Save and Exit'), [
                        'class' => 'btnFormSubmit btn btn-success',
                        'name'  => 'PostForm[btnSave]',
                        'value' => 'exit'])
                    ?>
                    <button type="button" href="<?= Url::to(['preview', 'lang' => $model->language]) ?>"
                            id="show-preview"
                            class="btn btn-info">
                        <?= Module::t('news', 'Preview') ?>
                    </button>
                    <?= Html::a(
                        Module::t('news', 'Cancel'),
                        [
                            'cancel-form',
                            'language' => $model->language, 'id' => $model->getPostId()
                        ],
                        [
                            'data-return-url' => Url::toRoute(['index', 'PostSearch[language]' => $model->language]),
                            'onclick'         => new JsExpression(<<<JS
                                const btnCancel = $(this);
                                $.get(btnCancel.attr('href'), function() {
                                    document.location.href = btnCancel.data('return-url');
                                });
                                return false;
JS
                            ),
                            'class'           => 'btn btn-default',
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

?>

<?php ActiveForm::end(); ?>

<?= AddPostMedia::widget() ?>

<?= AddExpertQuote::widget(['language' => $model->language,
                            'postId'   => $model->getPostId()]) ?>

<?= AddTimeline::widget() ?>
