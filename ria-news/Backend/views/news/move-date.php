<?php

/**
 * @var View $this
 * @var MoveDateForm $model
 * @var Post $post
 */

use kartik\datetime\DateTimePicker;
use Ria\Core\Helpers\FlagIconHelper;
use Ria\News\Core\Forms\Post\MoveDateForm;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Module;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

$this->title = '<small>' . Html::tag('span', '', [
    'class' => 'flag-icon flag-icon-' . FlagIconHelper::getIcon($post->getLanguage())
]) . '</small> ';
$this->title .= Module::t('news', 'Move date for post: {title}', ['title' => $post->getTitle()]);
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index', 'PostSearchForm[language]' => $post->getLanguage()]];
$this->params['breadcrumbs'][] = strip_tags($this->title);

?>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel">
                <div class="panel-body">
                    <?= $form->field($model, 'cause')->textarea(['rows' => 7]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body">
                    <?= $form->field($model, 'date')->widget(DateTimePicker::class, [
                        'type'          => DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'orientation'    => 'top right',
                            'todayHighlight' => true,
                            'todayBtn'       => true,
                            'format'         => 'yyyy-mm-dd hh:ii:ss',
                            'autoclose'      => true,
                        ]
                    ]) ?>

                    <?= Html::submitButton(Module::t('news', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>