<?php

/* @var $this yii\web\View */

use Ria\Core\Helpers\FlagIconHelper;
use Ria\News\Module;
use yii\helpers\Html;

/* @var $model \Ria\News\Core\Forms\Post\PostForm */

$this->title = '<small>' . Html::tag('span', '', ['class' => 'flag-icon flag-icon-' . FlagIconHelper::getIcon($model->language)]) . '</small> '
. Module::t('news', 'Create new post');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'News'), 'url' => ['index', 'PostSearchForm[language]' => $model->language]];
$this->params['breadcrumbs'][] = strip_tags($this->title);

echo $this->render('_form', ['model' => $model]);