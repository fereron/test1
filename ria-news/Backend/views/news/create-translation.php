<?php

/**
 * @var $this yii\web\View
 * @var $model PostForm
 * @var $post Post
 */

use Ria\Core\Helpers\FlagIconHelper;
use Ria\News\Core\Forms\Post\PostForm;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Module;
use yii\helpers\Html;

$this->title = '<small>' . Html::tag('span', '', ['class' => 'flag-icon flag-icon-' . FlagIconHelper::getIcon($model->language)]) . '</small> ';
$this->title.= Module::t('news', 'New translation');
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index', 'PostSearchForm[language]' => $model->language]];
$this->params['breadcrumbs'][] = strip_tags($this->title);

echo $this->render('_form', ['model' => $model, 'post' => $post]);