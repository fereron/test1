<?php
/* @var $this View */
/* @var $model RegionForm */

use Ria\News\Core\Forms\Region\RegionForm;
use Ria\News\Module;
use yii\web\View;

$this->title                   = Module::t('regions', 'Create Region');
$this->params['breadcrumbs'][] = ['label' => Module::t('regions', 'Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', ['model' => $model]);

?>