<?php
/* @var $this View */
/* @var $model RegionForm */

use Ria\News\Backend\Assets\RegionsAsset;
use Ria\News\Core\Forms\Region\RegionForm;
use yii\web\View;
use Ria\News\Module;
use Ria\Core\Helpers\FlagIconHelper;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

RegionsAsset::register($this);
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel">
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <?php foreach (Yii::$app->params['languages'] as $languageCode => $label): ?>
                            <li class="nav-item">
                                <a class="nav-link<?= $languageCode == Yii::$app->params['defaultLanguage'] ? ' active' : '' ?>"
                                   href="#<?= $languageCode ?>"
                                   data-toggle="tab"
                                >
                                    <i class="flag-icon flag-icon-<?= FlagIconHelper::getIcon($languageCode) ?>"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?php foreach ($model->translations as $translation): ?>
                            <div class="tab-pane <?= $translation->language == Yii::$app->params['defaultLanguage'] ? 'active' : '' ?>" id="<?= $translation->language ?>">
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <?= $form->field($translation, 'title')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                    </div>

                                    <div class="col-md-6">
                                        <?= $form->field($translation, 'slug')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
            <div class="float-left">
                <?= Html::submitButton(Module::t('stories', 'Save'), ['class' => 'btn btn-lg btn-success']) ?>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body">
                    <div class="col-md-12">
                        <?= $form->field($model, 'order')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'form-control']) ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
<?php ActiveForm::end(); ?>
