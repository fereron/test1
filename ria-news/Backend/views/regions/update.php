<?php
/* @var $this View */
/* @var $model RegionForm */
/* @var $region Region */

use Ria\News\Core\Forms\Region\RegionForm;
use Ria\News\Core\Models\Region\Region;
use yii\web\View;
use Ria\News\Module;

$this->title                   = Module::t('regions', 'Update region');
$this->params['breadcrumbs'][] = ['label' => Module::t('regions', 'Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', ['model' => $model]);

?>
