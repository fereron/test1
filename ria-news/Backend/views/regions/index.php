<?php

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var RegionSearch $searchModel
 */

use Ria\Core\Widgets\Gridview\GridViewRemark;
use Ria\News\Core\Forms\Region\RegionSearch;
use yii\data\ActiveDataProvider;
use yii\web\View;
use Ria\News\Core\Models\Region\Region;
use Ria\Core\Widgets\ActionColumn\ActionColumn;
use yii\helpers\Html;
use Ria\News\Module;

$this->title                   = Module::t('regions', 'Regions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['show_create_btn'] = true;
?>

<div class="panel">
    <div class="panel-body">
        <?= GridViewRemark::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '100'],
                    'headerOptions'  => ['class' => 'text-center']
                ],
                [
                    'attribute' => 'order',
                    'label' => Module::t('regions', 'Order'),
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-center', 'width' => '100'],
                    'headerOptions'  => ['class' => 'text-center'],
                    'value' => function (Region $region) {
                        return
                            Html::a('<i class="icon wb-chevron-up"></i>', ['move-up', 'id' => $region->getId()], ['data-method' => 'post']) .
                            ' ' .
                            Html::a('<i class="icon wb-chevron-down"></i>', ['move-down', 'id' => $region->getId()], ['data-method' => 'post'])

                            . '<span class="mx-1">' . $region->getOrder() . '</span>';
                    },
                ],

                [
                    'attribute' => 'title',
                    'label'     => Module::t('regions', 'Title'),
                    'value'     => function (Region $region) {
                        return $region->getTranslation(Yii::$app->language)->getTitle();
                    },
                ],
                [
                    'class'          => ActionColumn::class,
                    'visibleButtons' => ['view' => false]
                ],
            ],
        ]); ?>

    </div>
</div>