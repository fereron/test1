<?php

use Ria\Core\Helpers\FlagIconHelper;
use sadovojav\ckeditor\CKEditor;
use Ria\News\Backend\Assets\StoriesAsset;
use Ria\News\Core\Forms\Story\StoryForm;
use Ria\Photos\Widgets\PhotoManager\Photo;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use kartik\switchinput\SwitchInput;
use yii\web\View;
use Ria\Photos\Widgets\PhotoManager\PhotoManager;
use Ria\News\Module;

/* @var $this View */
/* @var $model StoryForm */

StoriesAsset::register($this);
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-9">
            <div class="panel">
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <?php foreach (Yii::$app->params['languages'] as $languageCode => $label): ?>
                            <li class="nav-item">
                                <a class="nav-link<?= $languageCode == Yii::$app->params['defaultLanguage'] ? ' active' : '' ?>"
                                   href="#<?= $languageCode ?>"
                                   data-toggle="tab"
                                >
                                    <i class="flag-icon flag-icon-<?= FlagIconHelper::getIcon($languageCode) ?>"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content mt-4">
                        <?php foreach ($model->translations as $translation): ?>
                            <div class="tab-pane <?= $translation->language == Yii::$app->params['defaultLanguage'] ? 'active' : '' ?>"
                                 id="<?= $translation->language ?>">

                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $form->field($translation, 'title')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($translation, 'slug')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                    </div>
                                </div>

                                <?= $form->field($translation, 'description')->widget(CKEditor::class, ['editorOptions' => ['preset' => 'full', 'entities' => false]]); ?>

                                <br>
                                <fieldset>
                                    <legend><?= Yii::t('meta', 'Meta tags') ?></legend>
                                    <br>
                                    <?= $form->field($translation->meta, 'title')->textInput() ?>
                                    <?= $form->field($translation->meta, 'keywords')->textarea() ?>
                                    <?= $form->field($translation->meta, 'description')->textarea() ?>
                                </fieldset>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Module::t('stories', 'Settings') ?></h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'status')->widget(SwitchInput::class) ?>

                    <?= $form->field($model, 'showOnSite')->widget(SwitchInput::class) ?>

                    <?= $form->field($model, 'cover')->widget(Photo::class, ['filename' => $model->cover]) ?>
                </div>
            </div>

            <div class="float-right">
                <?= Html::submitButton(Module::t('stories', 'Save'), ['class' => 'btn btn-lg btn-success']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>