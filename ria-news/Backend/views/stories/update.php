<?php
/* @var $this yii\web\View */
/* @var $model StoryForm */

use Ria\News\Core\Forms\Story\StoryForm;
use Ria\News\Module;

$this->title = Module::t('stories', 'Update story') . $model->translations[Yii::$app->language]->title;
$this->params['breadcrumbs'][] = ['label' => Module::t('stories', 'Stories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
    'model' => $model
]);