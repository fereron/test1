<?php
/* @var $this View */
/* @var $model StoryForm */

use Ria\News\Core\Forms\Story\StoryForm;
use yii\web\View;
use Ria\News\Module;

$this->title                   = Module::t('stories', 'Create story');
$this->params['breadcrumbs'][] = ['label' => Module::t('stories', 'Stories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
    'model' => $model
]);