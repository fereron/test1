<?php
/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var \Ria\News\Core\Forms\Story\StorySearch $searchModel
 */

use Ria\Core\Widgets\Gridview\StatusColumn;
use Ria\Core\Widgets\ActionColumn\ActionColumn;
use Ria\Core\Widgets\Gridview\GridViewRemark;
use Ria\News\Core\Models\Story\Story;
use yii\data\ActiveDataProvider;
use yii\web\View;
use Ria\News\Module;

$this->title                     = Module::t('stories', 'Stories');
$this->params['breadcrumbs'][]   = $this->title;
$this->params['show_create_btn'] = true;
?>

<div class="panel">
    <div class="panel-body">
        <?= GridViewRemark::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'contentOptions' => ['class' => 'text-center', 'style' => 'width:120px'],
                    'headerOptions'  => ['class' => 'text-center']
                ],
                [
                    'attribute' => 'title',
                    'label'     => Module::t('stories', 'Title'),
                    'value'     => function (Story $story) {
                        return $story->getTranslation(Yii::$app->language)->getTitle();
                    },
                ],
                [
                    'header'    => Module::t('stories', 'Status'),
                    'class'     => StatusColumn::class,
                    'attribute' => 'status'
                ],
                [
                    'header'    => Module::t('stories', 'Show on site'),
                    'class'     => StatusColumn::class,
                    'attribute' => 'show_on_site'
                ],
                [
                    'class'          => ActionColumn::class,
                    'visibleButtons' => ['view' => false]
                ],
            ],
        ]); ?>

    </div>
</div>