<?php

/* @var $this View */
/* @var $model CityForm */
/* @var $regions array Region */

use Ria\News\Backend\Assets\CitiesAsset;
use yii\web\View;
use Ria\News\Module;
use Ria\Core\Helpers\FlagIconHelper;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Ria\News\Core\Models\Region\Region;
use Ria\News\Core\Forms\City\CityForm;

CitiesAsset::register($this);
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-lg-9">
        <div class="panel">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <?php foreach (Yii::$app->params['languages'] as $languageCode => $label): ?>
                        <li class="nav-item">
                            <a class="nav-link<?= $languageCode == Yii::$app->params['defaultLanguage'] ? ' active' : '' ?>"
                               href="#<?= $languageCode ?>"
                               data-toggle="tab"
                            >
                                <i class="flag-icon flag-icon-<?= FlagIconHelper::getIcon($languageCode) ?>"></i>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="row mt-4">
                    <div class="col-md-6">

                        <?= $form->field($model, 'region_id')->dropDownList(
                                ArrayHelper::map($regions, 'id', 'title')
                        ) ?>

                    </div>
                </div>

                <div class="tab-content">
                    <?php foreach ($model->translations as $translation): ?>
                        <div class="tab-pane <?= $translation->language == Yii::$app->params['defaultLanguage'] ? 'active' : '' ?>" id="<?= $translation->language ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($translation, 'title')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                    <?= $form->field($translation, 'slug')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
        <div class="float-left">
            <?= Html::submitButton(Module::t('stories', 'Save'), ['class' => 'btn btn-lg btn-success']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
