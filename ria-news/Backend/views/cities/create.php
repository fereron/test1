<?php
/* @var $this View */
/* @var $model CityForm */
/* @var $regions Region */

use Ria\News\Module;
use yii\web\View;
use Ria\News\Core\Models\Region\Region;
use Ria\News\Core\Forms\City\CityForm;

$this->title                   = Module::t('cities', 'Create city');
$this->params['breadcrumbs'][] = ['label' => Module::t('cities', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
    'model' => $model,
    'regions' => $regions
]);

?>



