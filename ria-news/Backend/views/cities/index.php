<?php
/**
 * @var View $this
 * @var DoctrineDataProvider $dataProvider
 * @var CitySearch $searchModel
 */

use Ria\Core\Data\DoctrineDataProvider;
use Ria\Core\Widgets\Gridview\GridViewRemark;
use Ria\News\Core\Helpers\RegionHelper;
use Ria\News\Core\Models\City\City;
use Ria\News\Core\Forms\City\CitySearch;
use yii\helpers\ArrayHelper;
use yii\web\View;
use Ria\Core\Widgets\ActionColumn\ActionColumn;
use Ria\News\Module;

$this->title                     = Module::t('cities', 'Cities');
$this->params['breadcrumbs'][]   = $this->title;
$this->params['show_create_btn'] = true;
?>

<div class="panel">
    <div class="panel-body">
        <?= GridViewRemark::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '100'],
                    'headerOptions'  => ['class' => 'text-center']
                ],
                [
                    'attribute' => 'title',
                    'label'     => Module::t('cities', 'City'),
                    'value'     => function (City $city) {
                        return $city->getTranslation(Yii::$app->language)->getTitle();
                    },
                ],
                [
                    'attribute' => 'region_id',
                    'filter'    => ArrayHelper::map(RegionHelper::getRegions(Yii::$app->language), 'id', 'title'),
                    'label'     => Module::t('cities', 'Region'),
                    'value'     => function (City $city) {
                        return $city->getRegion()->getTranslation(Yii::$app->language)->getTitle();
                    },
                ],
                [
                    'class'          => ActionColumn::class,
                    'visibleButtons' => ['view' => false]
                ],
            ],
        ]); ?>

    </div>
</div>