<?php
/* @var $this yii\web\View */
/* @var $model CategoryForm */

use Ria\Core\Helpers\FlagIconHelper;
use Ria\News\Backend\Assets\CategoriesAsset;
use Ria\News\Core\Forms\Category\CategoryForm;
use Ria\News\Module;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use kartik\switchinput\SwitchInput;

CategoriesAsset::register($this);
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-9">
            <div class="panel">

                <div class="panel-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php foreach (Yii::$app->params['languages'] as $languageCode => $label): ?>
                                <li class="nav-item">
                                    <a class="nav-link<?= $languageCode == Yii::$app->params['defaultLanguage'] ? ' active' : '' ?>"
                                       href="#<?= $languageCode ?>"
                                       data-toggle="tab"
                                    >
                                        <i class="flag-icon flag-icon-<?= FlagIconHelper::getIcon($languageCode) ?>"></i>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                        <div class="tab-content mt-4">
                            <?php foreach ($model->translations as $translation): ?>
                                <div class="tab-pane <?= $translation->language == Yii::$app->params['defaultLanguage'] ? 'active' : '' ?>"
                                     id="<?= $translation->language ?>"
                                >
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?= $form->field($translation, 'title')->textInput() ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <?= $form->field($translation, 'slug')->textInput() ?>
                                        </div>
                                    </div>
                                    <br>
                                    <fieldset>
                                        <legend><?= Yii::t('meta', 'Meta tags') ?></legend>
                                        <br>
                                        <?= $form->field($translation->meta, 'title')->textInput() ?>
                                        <?= $form->field($translation->meta, 'keywords')->textarea() ?>
                                        <?= $form->field($translation->meta, 'description')->textarea() ?>
                                    </fieldset>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Module::t('categories', 'Settings') ?></h3>
                </div>

                <div class="panel-body">
                    <?= $form->field($model, 'parent_id')->dropDownList($model->getParentsList(), ['prompt' => ' - ',]) ?>

                    <?= $form->field($model, 'template')->dropDownList($model->getTemplatesList()) ?>

                    <?= $form->field($model, 'status')->widget(SwitchInput::class) ?>

                    <div class="float-right">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-lg btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>