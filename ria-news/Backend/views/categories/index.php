<?php

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \Ria\News\Core\Forms\Category\CategorySearch $searchModel
 */

use Ria\Core\Widgets\Gridview\StatusColumn;
use Ria\Core\Widgets\ActionColumn\ActionColumn;
use Ria\Core\Widgets\Gridview\GridViewRemark;
use Ria\News\Core\Models\Category\Category;
use yii\helpers\Html;
use Ria\News\Module;
use yii\helpers\Url;

$this->title                     = Module::t('categories', 'Categories');
$this->params['breadcrumbs'][]   = $this->title;
$this->params['show_create_btn'] = true;
?>

<div class="panel">
    <div class="panel-body">
        <?= GridViewRemark::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'contentOptions' => ['class' => 'text-center', 'style' => 'width:120px'],
                    'headerOptions'  => ['class' => 'text-center']
                ],
                [
                    'label' => Module::t('categories', 'Sorting'),
                    'value' => function (Category $category) {
                        return
                            Html::a('<i class="icon wb-chevron-up"></i>', ['move-up', 'id' => $category->getId()], ['data-method' => 'post']) .
                            ' ' .
                            Html::a('<i class="icon wb-chevron-down"></i>', ['move-down', 'id' => $category->getId()], ['data-method' => 'post']);
                    },
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-center', 'width' => '100'],
                ],
                [
                    'label'     => Module::t('categories', 'Title'),
                    'attribute' => 'title',
                    'format'    => 'raw',
                    'value'     => function (Category $category) {
                        $indent = $category->isParent() ? '' : str_repeat('&nbsp;&nbsp;', 4);

                        return Html::tag(
                                'span',
                                $indent . $category->getTranslation(Yii::$app->language)->getTitle(),
                                ['style' => $category->isParent() ? 'font-weight:bold;' : '']
                        );
                    },
                ],
                [
                    'class' => StatusColumn::class
                ],
                [
                    'class'          => ActionColumn::class,
                    'visibleButtons' => ['view' => false],
                    'urlCreator'     => function (string $action, Category $category) {
                        return Url::toRoute([$action, 'id' => $category->getId()]);
                    }
                ],
            ],
        ]); ?>
    </div>
</div>