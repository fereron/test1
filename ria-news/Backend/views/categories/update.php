<?php
/* @var $this yii\web\View */
/* @var $model CategoryForm */

use Ria\News\Core\Forms\Category\CategoryForm;
use Ria\News\Module;

$this->title                   = Module::t('categories', 'Update category') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Module::t('categories', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
   'model' => $model
]);