<?php
declare(strict_types=1);

namespace Ria\News\Backend\grid;

use Ria\Core\Widgets\ActionColumn\ActionColumn;
use Ria\News\Core\Models\Post\Post;
use Ria\News\Core\Models\Post\Status;
use Yii;

/**
 * Class PostActionColumn
 * @package Ria\News\Backend\grid
 */
class PostActionColumn extends ActionColumn
{
    /**
     * @var string[]
     */
    public $headerOptions = ['style' => 'width: 100px'];

    /**
     * @var string
     */
    public $template = '{preview-existing} {history} {move-date} {update} {archive} {archive-photos} {delete}';

    /**
     *
     */
    public function init()
    {
        $user = Yii::$app->user;
        $this->visibleButtons = [
            'delete'    => $user->can('deletePost'),
            'archive'   => function (Post $post) use ($user) {
                return $user->can('deletePost') && !$post->getStatus()->is(Status::ARCHIVED);
            },
            'move-date' => function () use ($user) {
                return $user->can('Administrator') || $user->can('SuperAdministrator');
            },
            'archive-photos' => function (Post  $post) {
                return $post->getType()->isPhoto();
            }
        ];

        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    protected function initDefaultButtons()
    {
        $user = Yii::$app->user;
        $this->initDefaultButton('preview-existing', 'eye', [
            'target' => '_blank'
        ]);

        $this->initDefaultButton('history', 'hourglass-alt', [
            'class' => 'post-history',
            'data-target' => '#log',
        ]);

        $this->initDefaultButton('archive-photos', 'storage', [
            'target' => '_blank'
        ]);

        $this->initDefaultButton('update', 'edit');

        $this->initDefaultButton('move-date', 'calendar');

        if ($user->can("Administrator") || $user->can("SuperAdministrator")) {
            $this->initDefaultButton('delete', 'delete', [
                'class' => 'delete-post'
            ]);
        }
    }
}