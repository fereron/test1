<?php
declare(strict_types=1);

namespace Ria\News\Backend\Widgets\PostSpeechUpload;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Ria\News\Core\Forms\Speech\SpeechForm;
use Ria\News\Core\Models\Speech\Speech;
use yii\base\Widget;

/**
 * Class PostSpeechUpload
 * @package Ria\News\Backend\Widgets\PostSpeechUpload
 */
class PostSpeechUpload extends Widget
{

    /**
     * @var int
     */
    public $postId;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PostSpeechUpload constructor.
     * @param EntityManagerInterface $entityManager
     * @param array $config
     */
    public function __construct(EntityManagerInterface $entityManager, $config = [])
    {
        $this->entityManager = $entityManager;

        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function run()
    {
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Speech::class);
        /** @var Speech|null $entity */
        $entity = $repository->findOneBy(['post' => $this->postId]);

        $model = empty($entity) ? new SpeechForm() : SpeechForm::createFrom($entity);
        if (empty($model->postId)) {
            $model->postId = $this->postId;
        }

        return $this->render('speechForm', [
            'model'     => $model,
            'updatedAt' => empty($entity) ? null : $entity->getUpdatedAt()->getTimestamp()
        ]);
    }

}