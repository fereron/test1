<?php
/**
 * @var \yii\web\View $this
 * @var \Ria\News\Core\Forms\Speech\SpeechForm $model
 * @var string|null $updatedAt
 */

use Ria\News\Module;
use yii\helpers\Html;
use yii\helpers\Url;

$uploadUrl = Url::toRoute(['speech/upload']);
$deleteUrl = Url::toRoute(['speech/delete']);

$this->registerJs(<<<JS
    $('#btnUploadSpeech').click(function() {
        const thisElem = $(this);
        
        thisElem.text(thisElem.data('label-uploading'));
        
        let formData = new FormData();
        formData.append($('#speechform-postid').attr('name'), $('#speechform-postid').val());
        formData.append($('#speechform-file').attr('name'), $('#speechform-file').get(0).files[0]);

        $.ajax({
            url: thisElem.data('url'),
            type: 'POST',
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: (response) => {
                if (response.success) {
                    $('.audioSpeech').html(response.player);
                    
                    thisElem.data('url', '{$uploadUrl}?id=' + response.speech.id);
                    $('#btnDeleteSpeech').data('url', '{$deleteUrl}?id=' + response.speech.id);
                    $('#btnDeleteSpeech').parent().show();
                    
                    $('#speechform-file').val('');
                } else {
                    alert(response.message);
                }

                thisElem.text(thisElem.data('label-upload'));
            }
        });
    });

    $('#btnDeleteSpeech').click(function() {
        $.post($(this).data('url'), function() {
              $('.audioSpeech').empty();
              $('#btnDeleteSpeech').parent().hide();
              
              $('#btnUploadSpeech').data('url', '{$uploadUrl}');
        });
    });
JS
);
?>

<fieldset>
    <legend><?= Module::t('news', 'Post speech file') ?></legend>

    <div class="row">
        <?= Html::activeHiddenInput($model, 'postId') ?>

        <div class="col-lg-4">
            <?= Html::activeFileInput($model, 'file', ['accept' => '.mp3']) ?>
        </div>

        <div class="col-lg-2">
            <?= Html::button(
                Yii::t('common', 'Upload'),
                [
                    'class'                => 'btn btn-sm btn-round btn-info',
                    'id'                   => 'btnUploadSpeech',
                    'data-url'             => Url::toRoute(['speech/upload', 'id' => $model->getId()]),
                    'data-label-uploading' => Yii::t('common', 'Uploading...'),
                    'data-label-upload'    => Yii::t('common', 'Upload'),
                ]
            ) ?>
        </div>

        <div class="col-lg-1" <?= empty($model->getId()) ? 'style="display: none;"' : '' ?>>
            <?= Html::button(Yii::t('common', 'Delete'), [
                'class'    => 'btn btn-sm btn-round btn-danger',
                'id'       => 'btnDeleteSpeech',
                'data-url' => Url::toRoute(['speech/delete', 'id' => $model->getId()])
            ]) ?>
        </div>
    </div>

    <div class="audioSpeech">
        <?php if ($model->getId())
            echo Html::tag('audio', '', [
                'src'      => Yii::$app->speech->getFile($model->filename . '?v=' . $updatedAt),
                'controls' => true,
                'preload'  => 'none'
            ]) ?>
    </div>

    <br>
</fieldset>