$('#formAddPostTimeLine').submit(function (e) {
    e.preventDefault();

    let timeline = $('#timeline').val();

    if (!timeline.length) {
        alert($(this).data('empty-time'));
        return false;
    }

    $('#timeline').val('');

    CKEDITOR.instances['with-photo-manager'].insertHtml(
        `<div class="time-divider">
            <span>${timeline}</span>
        </div>`
    );

    $('#addTimelineModal').modal('hide');
});