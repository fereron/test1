<?php
declare(strict_types=1);

/**
 * @var \yii\web\View $this
 */

use Ria\News\Backend\Widgets\AddTimeline\Assets\AddTimelineAsset;
use Ria\News\Module;

AddTimelineAsset::register($this);
?>
<!-- Modal AddTimeline -->
<div class="modal fade" id="addTimelineModal" role="dialog">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <form id="formAddPostTimeLine" data-empty-time="<?= Module::t('news', 'Time must be filled') ?>">
                <div class="modal-header">
                    <h4 class="modal-title"><?= Module::t('news', 'Insert timeline') ?></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="icon md-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="timeline" placeholder="21:20">
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-info" id="btnInsertTimeline"><?= Yii::t('common', 'Insert') ?></button>
                    <button class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('common', 'Cancel') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End AddTimeline -->