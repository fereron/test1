<?php
declare(strict_types=1);

namespace Ria\News\Backend\Widgets\AddTimeline\Assets;

use yii\web\AssetBundle;

/**
 * Class AddTimelineAsset
 * @package Ria\News\Backend\Widgets\AddTimeline\Assets
 */
class AddTimelineAsset extends AssetBundle
{
    /** @var string */
    public $sourcePath = '@module/Widgets/AddTimeline/web';
    /**
     * @var string[]
     */
    public $js = [
        'js/addPostTimeline.js'
    ];
    /**
     * @var array
     */
    public $publishOptions = [
        'forceCopy' => YII_ENV_DEV
    ];
}