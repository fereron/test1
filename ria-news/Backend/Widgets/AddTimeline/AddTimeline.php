<?php
declare(strict_types=1);

namespace Ria\News\Backend\Widgets\AddTimeline;

use yii\base\Widget;

/**
 * Class AddTimeline
 * @package Ria\Persons\Backend\Widgets\AddTimeline
 */
class AddTimeline extends Widget
{
    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        return $this->render('addTimeline');
    }
}

