(function () {

    if (CKEDITOR.plugins.get('addExpertQuote')) {
        return false;
    }

    CKEDITOR.plugins.add('addExpertQuote', {

        lang: 'en',
        icons: 'addExpertQuote',
        hidpi: false,
        init: function (editor) {
            var pluginName = 'addExpertQuote';

            editor.addCommand(pluginName, {
                exec: function (e) {
                    let selectedText = getSelectedTextInPostEditor();
                    let $quoteForm = $('#quote-form');

                    if (isQuoteToken(selectedText)) {
                        let quoteId  = getQuoteId(selectedText);
                        let language = $('#quoteform-language').val();

                        $.get('/news/quote/get?id=' + quoteId + '&language=' + language, function (response) {
                            if (!response.success) {
                                alert(response.message);
                                return false;
                            }

                            $quoteForm.attr('action', '/news/quote/update?id=' + quoteId + '&language=' + language);
                            setQuoteFormAttributes(response.quote);
                        });
                    } else {
                        $quoteForm.attr('action', '/news/quote/create');
                        setQuoteFormAttributes({id: null, expert: {id: null}, text: ''});
                    }

                    $('#addExpertQuoteModal').modal('show');

                    return false;
                }
            });

            editor.ui.addButton && editor.ui.addButton('addExpertQuote', {
                label: editor.lang.addExpertQuote.toolbar,
                command: pluginName,
                toolbar: 'doctools,50'
            });

        }
    });

    function getSelectedTextInPostEditor() {
        return CKEDITOR.instances['with-photo-manager'].getSelection().getSelectedText();
    }

    function isQuoteToken(string) {
        return string.match(/{{expert-quote-\d+}}/);
    }

    function getQuoteId(token) {
        let regex = /{{expert-quote-(\d+)}}/;
        let match = regex.exec(token);
        return match[1];
    }

    function setQuoteFormAttributes(quote) {
        $('#quoteform-id').val(quote.id);

        if (quote.expert.id === null) {
            $('#quoteform-expertid').val(null).trigger('change');
        } else {
            let newOption = new Option(
                quote.expert.name,
                quote.expert.id,
                true,
                true
            );
            $('#quoteform-expertid').append(newOption).trigger('change');
        }

        CKEDITOR.instances['quoteform-text'].setData(quote.text);
    }

})();