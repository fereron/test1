<?php
declare(strict_types=1);

namespace Ria\News\Backend\Widgets\AddExpertQuote;

use Ria\News\Core\Forms\ExpertQuote\QuoteForm;
use yii\base\Widget;

/**
 * Class AddExpertQuote
 * @package Ria\News\Backend\Widgets\AddExpertQuote
 */
class AddExpertQuote extends Widget
{
    /**
     * @var string
     */
    public $language;
    /**
     * @var int|null
     */
    public $postId;

    /**
     * @return string
     */
    public function run()
    {
        $model           = new QuoteForm();
        $model->postId   = $this->postId;
        $model->language = $this->language;

        return $this->render('addExpertQuote', ['model' => $model]);
    }

}