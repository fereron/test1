<?php
/**
 * @var \yii\web\View $this
 * @var QuoteForm $model
 */

use kartik\select2\Select2;

use Ria\News\Core\Components\CKEditor\CKEditor;
use Ria\News\Core\Forms\ExpertQuote\QuoteForm;
use Ria\News\Module;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->registerJs(<<<JS
    $('#btnSaveQuote').click(function(e) {
        e.preventDefault();
        
        $('#quoteform-text').val(CKEDITOR.instances['quoteform-text'].getData());
        
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: $('#quote-form').attr('action'),
            data: $('#quote-form').serialize(),
            success: function(response) {
              if (!response.success) {
                  alert(response.message);
              } else {
                  if (typeof response.quote !== 'undefined') {
                    CKEDITOR.instances['with-photo-manager'].insertText('{{expert-quote-' + response.quote.id + '}}');
                  }
                  
                  $('#addExpertQuoteModal').modal('hide');
              }
            }
        });
        
        return false;
    });
JS
)
?>
<!-- Modal AddExpertQuote -->
<div class="modal fade" id="addExpertQuoteModal" role="dialog">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= Module::t('quotes', 'Add expert quote') ?></h4>

                <button type="button" class="close" data-dismiss="modal">
                    <i class="icon md-close"></i>
                </button>
            </div>

            <?php $form = ActiveForm::begin([
                'id'     => 'quote-form',
                'action' => ['/news/quote/create']
            ]) ?>

            <?= Html::activeHiddenInput($model, 'id') ?>
            <?= Html::activeHiddenInput($model, 'postId') ?>
            <?= Html::activeHiddenInput($model, 'language') ?>

            <div class="modal-body">
                <label for="quoteform-expertid"><?= Module::t('quotes', 'Expert') ?></label>
                <?= Select2::widget([
                    'model'         => $model,
                    'attribute'     => 'expertId',
                    'value'         => null,
                    'options'       => ['placeholder' => Module::t('quotes', 'Choose expert')],
                    'pluginOptions' => [
                        "title"              => '',
                        'dropdownParent'     => "#addExpertQuoteModal",
                        'allowClear'         => true,
                        'minimumInputLength' => 3,
                        'language'           => [
                            'errorLoading' => new JsExpression("function () { return 'Ищем...'; }"),
                        ],
                        'ajax'               => [
                            'url'      => Url::to(['/news/news/experts-list', 'language' => $this->context->language]),
                            'dataType' => 'json',
                            'data'     => new JsExpression("function(params) { return {q:params.term}; }")
                        ],
                        'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                        'templateResult'     => new JsExpression('function(response) { return response.text; }'),
                        'templateSelection'  => new JsExpression('function (response) { return response.text; }'),
                    ],
                ]) ?>
            </div>

            <div class="modal-body">
                <label for="quoteform-text">Текст</label>
                <?= CKEditor::widget([
                    'model'         => $model,
                    'attribute'     => 'text',
                    'editorOptions' => [
                        'title'                     => '',
                        'height'                    => 300,
                        'language'                  => $this->context->language,
                        'format_tags'               => 'p;h1;h2;h3;h4;h5;h6;pre;address;div;time',
                        'format_time'               => [
                            'name'       => 'Дата',
                            'element'    => 'span',
                            'attributes' => ['class' => 'timeline'],
                        ],
                        'toolbarGroups'             => [
                            ['name' => 'clipboard', 'groups' => ['mode', 'undo', 'selection']],
                            '/',
                            ['name' => 'paragraph', 'groups' => ['list', 'indent', 'align']],
                            '/',
                            ['name' => 'styles'],
                            '/',
                            ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors', 'cleanup']],
                            ['name' => 'links', 'groups' => ['links', 'insert']],
                        ],
                        'removeButtons'             => 'Smiley,Iframe,Flash,Font',
                        'removePlugins'             => 'contextmenu,liststyle,tabletools,tableselection',
                        'disableNativeSpellChecker' => false,
                    ]
                ]) ?>
            </div>

            <div class="modal-footer">
                <button class="btn btn-info" id="btnSaveQuote"><?= Yii::t('common', 'Save') ?></button>
                <button class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('common', 'Cancel') ?></button>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<!-- End AddMediaToPost -->