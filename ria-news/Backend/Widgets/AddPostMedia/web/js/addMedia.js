$('#btnInsertMedia').click(function (e) {
    e.preventDefault();

    let content = $('#mediaText').val();
    if (!content.length) {
        alert('Media code must be filled.');
        return false;
    }

    let params = {'content': content};

    $.post('/news/widget/create', params, function (widget) {
        if (!widget.success) {
            alert(widget.message);
        } else {
            CKEDITOR.instances['with-photo-manager'].insertText(widget.token);

            $('#addMediaModal').modal('hide');
        }
    });
});