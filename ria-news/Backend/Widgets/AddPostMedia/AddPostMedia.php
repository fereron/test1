<?php
declare(strict_types=1);

namespace Ria\News\Backend\Widgets\AddPostMedia;

use yii\base\Widget;

/**
 * Class AddPostMedia
 * @package Ria\News\Backend\Widgets\AddPostMedia
 */
class AddPostMedia extends Widget
{

    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        return $this->render('addPostMedia', [

        ]);
    }

}