<?php
/**
 * @var \yii\web\View $this
 */

use Ria\News\Backend\Widgets\AddPostMedia\Assets\AddPostMediaAsset;
use Ria\News\Module;

AddPostMediaAsset::register($this);
?>
<!-- Modal AddMediaToPost -->
<div class="modal fade" id="addMediaModal" role="dialog">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= Module::t('news', 'Insert media to post') ?></h4>

                <button type="button" class="close" data-dismiss="modal">
                    <i class="icon md-close"></i>
                </button>
            </div>

            <div class="modal-body">
                <textarea id="mediaText" class="form-control" rows="12" placeholder="<?= Module::t('news', 'Insert code here...') ?>"></textarea>
            </div>

            <div class="modal-footer">
                <button class="btn btn-info" id="btnInsertMedia"><?= Yii::t('common', 'Create') ?></button>
                <button class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('common', 'Cancel') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- End AddMediaToPost -->