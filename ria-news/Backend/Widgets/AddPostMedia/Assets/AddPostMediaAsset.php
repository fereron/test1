<?php
declare(strict_types=1);

namespace Ria\News\Backend\Widgets\AddPostMedia\Assets;

use yii\web\AssetBundle;

/**
 * Class AddPostMediaAsset
 * @package Ria\News\Backend\Widgets\AddPostMedia\Assets
 */
class AddPostMediaAsset extends AssetBundle
{
    /** @var array */
    public $publishOptions = [
        'forceCopy' => YII_ENV == 'dev',
    ];
    /** @var string */
    public $sourcePath = '@module/Widgets/AddPostMedia/web';
    /**
     * @var string[]
     */
    public $js = [
        'js/addMedia.js'
    ];

}