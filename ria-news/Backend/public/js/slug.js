var list = $('.language-list').find('a');

$.each(list, function (index, item) {
    var lang = $(item).data('lang');
    $(`#translationform_${lang}-title,#translationform_${lang}-slug`).bind('input', function () {
        $(`#translationform_${lang}-slug`).val(url_slug($(this).val()));
    });
});