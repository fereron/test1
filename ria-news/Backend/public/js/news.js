const form = $('#post-form');

$('#postform-slug').bind('input', function () {
    $(this).val(url_slug($(this).val()));
});

$('.delete-post').on('click', function (e) {
    e.preventDefault();
    const link = $(this);

    if (deactivatePost(link.attr('href'), 'deleted') === false) {
        return false;
    }
});

const getCause = msg => {
    let cause = prompt(msg);
    if (cause === null) {
        return false;
    } else if (cause === '') {
        cause = getCause(msg);
    }
    return cause;
}

function removeUnwantedCharachters(text) {
    text = text.replace('<p>&nbsp;</p>', '');
    var regex = /<(?!(br|\/br|p|\/p|a|\/a|strong|\/strong|sub|\/sub|sup|\/sup|table|\/table|tbody|\/tbody|tr|\/tr|th|\/th|td|\/td|h2|\/h2|section|\/section|div|\/div|span|\/span|ol|\/ol|li|\/li|ul|\/ul|em|\/em|img|\/img|blockquote|\/blockquote|hr|\/hr))([^>])+>/gi;

    return text.replace(regex, '');
}

var btnSubmitClicked = false;

form.on('beforeSubmit', function () {
    if (btnSubmitClicked) {
        alert('Пожалуйста, имейте терпение, новость сохраняется');
        return false;
    }

    btnSubmitClicked = true;

    let wasIsPublished = +$('#wasIsPublished').val();
    let postStatus = $('#postform-status').val();

    if (postStatus === 'archived' && wasIsPublished) {
        if (archivePost() === false) {
            btnSubmitClicked = false;

            return false;
        }
    } else if (postStatus !== 'read' && wasIsPublished) {
        const requestUrl = '/news/news/delete?id=' + $('#postId').val();
        if (deactivatePost(requestUrl) === false) {
            btnSubmitClicked = false;

            return false;
        }
    }

    CKEDITOR.instances['with-photo-manager'].setData(
        removeUnwantedCharachters(CKEDITOR.instances['with-photo-manager'].getData())
    );
});

function archivePost() {
    const newLink = getCause('Укажите действительную ссылку на другую новость');

    if (newLink === false) {
        return false;
    }

    let breakFormSubmit = false;

    $.ajax({
        type: 'POST',
        url: '/news/news/archive?id=' + $('#postId').val(),
        data: {new: newLink},
        async: false
    }).done(function (response) {
        if (response.success) {
            window.location.reload(false);
        } else {
            breakFormSubmit = true;

            alert(response.error);
        }
    }).fail(function (error) {
        console.log(error.message);
    });

    if (breakFormSubmit) {
        return false;
    }
}

function deactivatePost(requestUrl, status = null) {
    const cause = getCause('Укажите причину');

    if (cause === false) {
        return false;
    }

    let breakFormSubmit = false;

    $.ajax({
        type: 'POST',
        url: requestUrl,
        data: {'cause' : cause, 'status' : status},
        async: false,
    }).done(function (response) {
        if (response.success) {
            window.location.reload(false);
        } else {
            breakFormSubmit = true;

            alert(response.error);
        }
    }).fail(function (error) {
        console.log(error.message);
    });

    return ! breakFormSubmit;
}

let pending = false;
$('#show-preview').on('click', function (e) {
    e.preventDefault();

    if (pending) return false;

    var data = new FormData(form[0]);
    data.append('PostForm[slug]', $('#postform-slug').val());

    data.set('PostForm[content]', CKEDITOR.instances['with-photo-manager'].getData());
    $.ajax({
        type: 'POST',
        url: $(this).attr('href'),
        processData: false,
        contentType: false,
        data,
        beforeSend: function () {
            //pending = true;
        }
    }).done(function (response) {
        pending = false;
        if (response.success) {
            const win = window.open(response.data, '_blank');
            win.focus();
        } else {
            alert(response.error);
        }
    }).fail(function (error) {
        console.log(error.message);
    });
});

$('.post-history').on('click', function (e) {
    e.preventDefault();
    const link = $(this);
    const dataTarget = link.data('target');
    const modal = $(dataTarget);
    $.ajax({
        type: 'GET',
        url: link.attr('href'),
        beforeSend: function () {
            //pending = true;
        }
    }).done(function(response) {
        pending = false;
        if (response.success) {
            modal.find('.modal-body').html(response.html);
            $(document).find('del, ins').closest('tr').show();
            modal.modal('show');
        } else {
            alert(response.error);
        }
    }).fail(function (error) {
        alert(error.message);
    });
});

$(document).ready(function () {
    const typeSelect = $('#type-select');
    if (typeSelect.length) {
        disableAuthorOrExpertSelect(typeSelect);
    }

    typeSelect.on('change', function () {
        disableAuthorOrExpertSelect(typeSelect);
    });

    function disableAuthorOrExpertSelect(typeSelect) {
        const currentVal = typeSelect.val();
        $('.type-depended').each(function () {
            const select = $(this);
            if (
              (select.data('accept') && select.data('accept') !== currentVal)
              ||
              (select.data('except') && select.data('except') === currentVal)
            ) {
                select.val('').closest('.form-group').hide();
            } else {
                select.closest('.form-group').show();
            }
        });
    }

    $('body').on("click", ".nav-item", function (){
       $(".modal-body").scrollTop(0, 0);
    });
});