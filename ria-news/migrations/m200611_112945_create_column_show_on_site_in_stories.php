<?php

use yii\db\Migration;

/**
 * Class m200611_112945_create_column_show_on_site_in_stories
 */
class m200611_112945_create_column_show_on_site_in_stories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%stories}}',
            'show_on_site',
            $this->boolean()->notNull()->defaultValue(0)->after('status')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%stories}}', 'show_on_site');
    }
}
