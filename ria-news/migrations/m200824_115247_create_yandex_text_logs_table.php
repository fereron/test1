<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%yandex_text_logs}}`.
 */
class m200824_115247_create_yandex_text_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('yandex_text_logs', [
            'id'           => $this->primaryKey(11)->notNull(),
            'post_id'      => $this->integer(11)->unsigned()->notNull(),
            'text_id'      => $this->string(255)->null()->defaultValue(null),
            'status'       => $this->smallInteger(1),
            'fail_reason'  => $this->string(255)->null()->defaultValue(null),
            'created_at'   => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('yandex_text_logs_status', 'yandex_text_logs', 'status');

        $this->addForeignKey(
            'fk_yandex_text_logs_post_id',
            'yandex_text_logs',
            'post_id',
            'posts',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_yandex_text_logs_post_id', 'yandex_text_logs');
        $this->dropTable('yandex_text_logs');
    }
}