<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%redirects}}`.
 */
class m200720_112159_create_redirects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%redirects}}', [
            'id'  => $this->primaryKey()->unsigned(),
            'old' => $this->string()->notNull()->unique(),
            'new' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%redirects}}');
    }
}
