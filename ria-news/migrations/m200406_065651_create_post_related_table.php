<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_related}}`.
 */
class m200406_065651_create_post_related_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_related}}', [
            'post_id' => $this->integer()->unsigned()->notNull(),
            'related_id' => $this->integer()->unsigned()->notNull()
        ]);

        $this->addPrimaryKey('post_related_pk', 'post_related', ['post_id', 'related_id']);
        $this->addForeignKey('fk_relation_related_id', 'post_related', 'related_id', 'posts', 'id', 'CASCADE');
        $this->addForeignKey('fk_relation_post_id', 'post_related', 'post_id', 'posts', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_relation_post_id', 'post_related');
        $this->dropForeignKey('fk_relation_related_id', 'post_related');
        $this->dropTable('{{%post_related}}');
    }
}
