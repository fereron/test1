<?php

use yii\db\Migration;

/**
 * Class m200923_111000_drop_tables_yandex
 */
class m200923_111000_drop_tables_yandex extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%yandex_text_logs}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200923_111000_drop_tables_yandex cannot be reverted.\n";

        return false;
    }
}
