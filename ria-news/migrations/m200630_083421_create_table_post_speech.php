<?php

use yii\db\Migration;

/**
 * Class m200630_083421_create_table_post_speech
 */
class m200630_083421_create_table_post_speech extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_speech}}', [
            'id'                => $this->primaryKey(10)->unsigned()->notNull(),
            'post_id'           => $this->integer(10)->unsigned()->notNull()->unique(),
            'filename'          => $this->char(42)->notNull(),
            'original_filename' => $this->string(255)->null()->defaultValue(null),
            'created_at'        => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'        => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey(
            'fk_post_speech_post_id',
            '{{%post_speech}}',
            'post_id',

            'posts',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_speech_post_id', '{{%post_speech}}');
        $this->dropTable('{{%post_speech}}');
    }
}
