<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_persons}}`.
 */
class m200618_131632_create_post_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_person}}', [
            'post_id' => $this->integer()->unsigned()->notNull(),
            'person_id'  => $this->integer()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('pk_post_person', 'post_person', ['post_id', 'person_id']);
        $this->addForeignKey('fk_post_post_person', 'post_person', 'post_id', 'posts', 'id', 'CASCADE');
        $this->addForeignKey('fk_tag_post_person', 'post_person', 'person_id', 'persons', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post_person}}');
    }
}
