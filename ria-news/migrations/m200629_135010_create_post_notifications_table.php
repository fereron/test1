<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_notifications}}`.
 */
class m200629_135010_create_post_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_notifications}}', [
            'id'      => $this->primaryKey()->unsigned(),
            'post_id' => $this->integer()->unsigned()->notNull(),
            'status'  => $this->integer(1)->notNull()
        ]);

        $this->addForeignKey('fk_post_notifications', '{{%post_notifications}}', 'post_id', '{{%posts}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_notifications', '{{%post_notifications}}');
        $this->dropTable('{{%post_notifications}}');
    }
}
