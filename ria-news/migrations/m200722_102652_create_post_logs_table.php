<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_logs}}`.
 */
class m200722_102652_create_post_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_logs}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->notNull(),
            'post_id' => $this->integer(10)->unsigned()->notNull(),
            'user_id' => $this->integer(10)->notNull(),
            'snapshot' => $this->text(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey('fk_post_post_log', '{{%post_logs}}', 'post_id', '{{%posts}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_post_log', '{{%post_logs}}', 'user_id', '{{%users}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post_logs}}');
    }
}
