<?php

use yii\db\Migration;

/**
 * Class m200628_110913_create_table_post_views
 */
class m200628_110913_create_table_post_views extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_views}}', [
            'id'         => $this->primaryKey(11)->unsigned()->notNull(),
            'post_id'    => $this->integer(10)->unsigned()->notNull(),
            'views'      => $this->smallInteger(5)->unsigned()->notNull()->defaultValue(0),
            'ip'         => $this->string(40)->null()->defaultValue(null),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey(
            'fk_post_views_post_id',
            '{{%post_views}}',
            'post_id',

            'posts',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_views_post_id', '{{%post_views}}');
        $this->dropTable('{{%post_views}}');
    }
}
