<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categories}}`.
 */
class m191217_080312_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'id'         => $this->primaryKey(),
            'parent_id'  => $this->integer(),
            'template'   => $this->string()->notNull(),
            'sort'       => $this->smallInteger(5)->unsigned()->notNull()->defaultValue(1),
            'status'     => $this->boolean()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey('fk_parent_category', '{{%categories}}', 'parent_id', '{{%categories}}', 'id', 'CASCADE');

        $this->createTable('{{%categories_lang}}', [
            'id'          => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'language'    => $this->string(2)->notNull(),
            'title'       => $this->string()->notNull(),
            'meta'        => $this->text(),
            'slug'        => $this->string()->notNull(),
        ]);

        $this->createIndex('category_slug', '{{%categories_lang}}', ['language', 'slug'], true);
        $this->addForeignKey('fk_category_translation', '{{%categories_lang}}', 'category_id', '{{%categories}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%categories_lang}}');
        $this->dropTable('{{%categories}}');
    }
}
