<?php

use yii\db\Migration;

/**
 * Class m200908_082801_add_deactivated_to_posts_table
 */
class m200908_082801_add_deactivated_to_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'is_deactivated', $this->boolean()->defaultValue(0)->after('is_published'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'is_deactivated');
    }
}
