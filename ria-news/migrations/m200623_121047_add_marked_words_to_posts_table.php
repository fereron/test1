<?php

use yii\db\Migration;

/**
 * Class m200623_121047_add_marked_words_to_posts_table
 */
class m200623_121047_add_marked_words_to_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%posts}}',
            'marked_words',
            $this->string()->null()->after('slug')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'marked_words');

        return false;
    }

}
