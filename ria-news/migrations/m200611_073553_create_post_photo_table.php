<?php
declare(strict_types=1);

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_photo}}`.
 */
class m200611_073553_create_post_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_photo}}', [
            'post_id' => $this->integer()->unsigned()->notNull(),
            'photo_id' => $this->integer()->unsigned()->notNull(),
            'sort' => $this->integer()->notNull()->defaultValue(1),
            'PRIMARY KEY(post_id, photo_id)'
        ]);

        $this->addForeignKey('fk_post_photo_post', '{{%post_photo}}', 'post_id', '{{%posts}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_post_photo_photo', '{{%post_photo}}', 'photo_id', '{{%photos}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post_photo}}');
    }
}
