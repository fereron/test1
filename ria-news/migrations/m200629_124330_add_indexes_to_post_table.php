<?php

use yii\db\Migration;

/**
 * Class m200629_124330_add_indexes_to_post_table
 */
class m200629_124330_add_indexes_to_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `posts` 
                ADD KEY (is_main),
                ADD KEY (is_published);
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `posts` 
                DROP KEY is_main,
                DROP KEY is_published;
        ");
    }

}
