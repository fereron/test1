<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stories}}`.
 */
class m200214_071852_create_stories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stories}}', [
            'id'         => $this->primaryKey(),
            'cover'      => $this->string('42')->null(),
            'status'  => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->createTable('{{%stories_lang}}', [
            'id'          => $this->primaryKey(),
            'story_id'    => $this->integer()->notNull(),
            'slug'        => $this->string()->notNull(),
            'title'       => $this->string(),
            'description' => $this->text()->null(),
            'language'    => $this->string(2)->notNull(),
            'meta'        => $this->text()->comment('This field must contain data only in json format'),
        ]);

        $this->createIndex('stories_translate_index', '{{%stories_lang}}', ['story_id', 'language',]);
        $this->createIndex('slug', '{{%stories_lang}}', ['slug', 'language'], true);

        $this->addForeignKey(
            'fk_story_lang',
            '{{%stories_lang}}',
            'story_id',
            '{{%stories}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_story_lang', '{{%stories_lang}}');
        $this->dropIndex('stories_translate_index', '{{%stories_lang}}');
        $this->dropIndex('slug', '{{%stories_lang}}');
        $this->dropTable('{{%stories_lang}}');
        $this->dropTable('{{%stories}}');
    }
}