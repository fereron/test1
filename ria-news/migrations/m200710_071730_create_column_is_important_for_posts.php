<?php

use yii\db\Migration;

/**
 * Class m200710_071730_create_column_is_important_for_posts
 */
class m200710_071730_create_column_is_important_for_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%posts}}',
            'is_important',
            $this->boolean()->notNull()->defaultValue(false)->after('is_breaking')
        );

        $this->createIndex('posts_index_is_important', '{{%posts}}', 'is_important');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'is_important');
    }
}
