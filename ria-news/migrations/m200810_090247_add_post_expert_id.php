<?php

use yii\db\Migration;

/**
 * Class m200810_090247_add_post_expert_id
 */
class m200810_090247_add_post_expert_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'expert_id', $this->integer()->unsigned());
        $this->addForeignKey('fk_post_person', '{{%posts}}', 'expert_id', 'persons', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_person', '{{%posts}}');
        $this->dropColumn('{{%posts}}', 'expert_id');
    }
}
