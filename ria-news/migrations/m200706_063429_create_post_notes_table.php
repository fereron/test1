<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_notes}}`.
 */
class m200706_063429_create_post_notes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_notes}}', [
            'id'            => $this->primaryKey(10),
            'post_group_id' => $this->integer(10)->unsigned()->notNull()->unique(),
            'body'          => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post_notes}}');
    }
}
