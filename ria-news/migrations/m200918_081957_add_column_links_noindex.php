<?php

use yii\db\Migration;

/**
 * Class m200918_081957_add_column_links_noindex
 */
class m200918_081957_add_column_links_noindex extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'links_noindex',
            $this->boolean()->defaultValue(true)->after('meta'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'links_noindex');
    }
}
