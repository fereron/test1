<?php

use yii\db\Migration;

/**
 * Class m200611_122540_create_table_expert_quotes
 */
class m200611_122540_create_table_expert_quotes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%expert_quotes}}', [
            'id'         => $this->primaryKey(10)->unsigned(),
            'expert_id'  => $this->integer(11)->unsigned()->notNull(),
            'post_id'    => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'text'       => $this->text()->null()->defaultValue(null),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey(
            'fk_expert_quotes_expert',
            '{{%expert_quotes}}',
            'expert_id',
            '{{%persons}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_expert_quotes_post',
            '{{%expert_quotes}}',
            'post_id',
            '{{%posts}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_expert_quotes_post', '{{%expert_quotes}}');
        $this->dropForeignKey('fk_expert_quotes_expert', '{{%expert_quotes}}');

        $this->dropTable('{{%expert_quotes}');
    }
}
