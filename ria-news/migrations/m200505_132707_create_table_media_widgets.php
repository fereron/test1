<?php

use yii\db\Migration;

/**
 * Class m200505_132707_create_table_media_widgets
 */
class m200505_132707_create_table_media_widgets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('media_widgets', [
            'id'         => $this->primaryKey(10)->unsigned(),
            'content'    => $this->text()->notNull(),
            'type'       => "ENUM('youtube','facebook','twitter','instagram','vk','playbuzz','other') NOT NULL DEFAULT 'other'",
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('media_widgets');
    }
}
