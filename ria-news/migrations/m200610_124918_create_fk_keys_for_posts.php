<?php

use yii\db\Migration;

/**
 * Class m200610_124918_create_fk_keys_for_posts
 */
class m200610_124918_create_fk_keys_for_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_posts_story_id',
            '{{%posts}}',
            'story_id',
            '{{%stories}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_posts_city_id',
            '{{%posts}}',
            'city_id',
            '{{%cities}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_posts_story_id', '{{%posts}}');
        $this->dropForeignKey('fk_posts_city_id', '{{%posts}}');
    }
}
