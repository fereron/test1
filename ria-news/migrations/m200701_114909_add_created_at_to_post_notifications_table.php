<?php

use yii\db\Migration;

/**
 * Class m200701_114909_add_created_at_to_post_notifications_table
 */
class m200701_114909_add_created_at_to_post_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%post_notifications}}',
            'created_at',
            $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%post_notifications}}', 'created_at');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200701_114909_add_created_at_to_post_notifications_table cannot be reverted.\n";

        return false;
    }
    */
}
