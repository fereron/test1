<?php

use yii\db\Migration;

/**
 * Class m200629_135023_add_indexes_to_post_table
 */
class m200629_135023_add_indexes_to_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `posts` 
                ADD KEY (language),
                ADD KEY (is_actual),
                ADD KEY (is_breaking),
                ADD KEY (is_exclusive),
                ADD KEY (status),
                ADD KEY (type),
                ADD KEY (published_at),
                ADD KEY (group_id);
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `posts`  
                DROP KEY (language),
                DROP KEY (is_actual),
                DROP KEY (is_breaking),
                DROP KEY (is_exclusive),
                DROP KEY (status),
                DROP KEY (type),
                DROP KEY (published_at),
                DROP KEY (group_id);
        ");
    }
}
