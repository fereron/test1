<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post_exports}}`.
 */
class m200407_111117_create_post_exports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_exports}}', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->unsigned()->notNull(),
            'type' => $this->string(50)->notNull()
        ]);

        $this->createIndex('idx_unique_post_exports', '{{%post_exports}}', ['post_id', 'type'], true);
        $this->addForeignKey('fk_post_exports', '{{%post_exports}}', 'post_id', '{{%posts}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_exports', '{{%post_exports}}');
        $this->dropTable('{{%post_exports}}');
    }
}
