<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cities}}`.
 */
class m200506_075240_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /* Regions table */
        $this->createTable('{{%regions}}', [
            'id'    => $this->primaryKey(),
            'order' => $this->smallInteger(5)->unsigned()->notNull()->defaultValue(1),
        ]);

        $this->createTable('{{%regions_lang}}', [
            'id'        => $this->primaryKey(),
            'region_id' => $this->integer()->notNull(),
            'title'     => $this->string(),
            'slug'      => $this->string(),
            'language'  => $this->string(2)->notNull(),
        ]);

        $this->addForeignKey(
            'fk_region_lang',
            '{{%regions_lang}}',
            'region_id',
            '{{%regions}}',
            'id',
            'CASCADE'
        );

        /* Cities table */
        $this->createTable('{{%cities}}', [
            'id'        => $this->primaryKey()->unsigned(),
            'region_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk_region',
            '{{%cities}}',
            'region_id',
            '{{%regions}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_posts_city',
            '{{%posts}}',
            'city_id',
            '{{%cities}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->createTable('{{%cities_lang}}', [
            'id'       => $this->primaryKey(),
            'city_id'  => $this->integer()->unsigned()->notNull(),
            'title'    => $this->string(),
            'slug'     => $this->string(),
            'language' => $this->string(2)->notNull(),
        ]);

        $this->addForeignKey(
            'fk_city_lang',
            '{{%cities_lang}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cities_lang}}');
        $this->dropTable('{{%cities}}');
        $this->dropTable('{{%regions_lang}}');
        $this->dropTable('{{%regions}}');

        $this->dropForeignKey('fk_posts_city_id', '{{%posts}}');
    }
}
