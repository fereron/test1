<?php

use yii\db\Migration;

/**
 * Class m200708_073122_create_table_post_short_links
 */
class m200708_073122_create_table_post_short_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post_short_links}}', [
            'hash'        => $this->char(8)->notNull(),
            'post_id'     => $this->integer(10)->unsigned()->notNull()->unique(),
            'redirect_to' => $this->string(255)->notNull(),
            'transits'    => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'created_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'  => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addPrimaryKey('post_short_links_pk', '{{%post_short_links}}', 'hash');

        $this->addForeignKey(
            'fk_post_short_links_post_id',
            '{{%post_short_links}}',
            'post_id',

            'posts',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_short_links_post_id', '{{%post_short_links}}');
        $this->dropTable('{{%post_short_links}}');
    }
}
