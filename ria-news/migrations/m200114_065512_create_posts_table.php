<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%posts}}`.
 */
class m200114_065512_create_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%posts}}', [
            'id'            => $this->primaryKey()->unsigned(),
            'group_id'      => $this->integer(10)->unsigned()->comment('This field links translations. If this field is same with id, it means that this translation is original.'),
            'category_id'   => $this->integer()->notNull(),
            'translator_id' => $this->integer(10)->null(),
            'author_id'     => $this->integer(10)->notNull(),
            'story_id'      => $this->integer(10),
            'city_id'       => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'youtube_id'    => $this->string(11)->null(),
            'language'      => $this->char(2)->notNull(),
            'created_by'    => $this->integer(10)->notNull(),
            'slug'          => $this->string()->notNull(),
            'status'        => $this->string(50)->notNull(),
            'is_published'  => $this->boolean()->notNull()->defaultValue(false),
            'type'          => $this->string(20)->notNull(),
            'views'         => $this->integer()->unsigned()->null()->defaultValue(0),
            'title'         => $this->string()->notNull(),
            'image'         => $this->string(50)->defaultValue(null),
            'icon'          => $this->string(20)->null(),
            'description'   => $this->string(),
            'content'       => "LONGTEXT",
            'source'        => $this->string(),
            'is_main'       => $this->boolean()->notNull()->defaultValue(false),
            'is_actual'     => $this->boolean()->notNull()->defaultValue(false),
            'is_breaking'   => $this->boolean()->notNull()->defaultValue(false),
            'is_exclusive'  => $this->boolean()->notNull()->defaultValue(false),
            'option_type'   => "ENUM('ads', 'closed') NULL DEFAULT NULL",
            'meta'          => $this->text()->comment('This field must contain data only in json format'),
            'created_at'    => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'    => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            'published_at'  => $this->timestamp()->null(),
        ]);

        $this->addForeignKey('fk_category_post', 'posts', 'category_id', 'categories', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_posts_author_id', '{{%posts}}', 'author_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_posts_created_by', '{{%posts}}', 'created_by', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_posts_translator_id', '{{%posts}}', 'translator_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createIndex('idx_slug', 'posts', ['slug', 'language'], true);

        $this->createTable('tags', [
            'id'   => $this->primaryKey()->unsigned(),
            'slug' => $this->string(100)->unique()->notNull(),
        ]);

        $this->createTable('tags_lang', [
            'id'       => $this->primaryKey()->unsigned(),
            'tag_id'   => $this->integer(10)->notNull(),
            'name'     => $this->string(100)->notNull(),
            'language' => $this->char(2)->notNull(),
            'count'    => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createTable('post_tag', [
            'post_id' => $this->integer()->unsigned()->notNull(),
            'tag_id'  => $this->integer()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('pk_post_tag', 'post_tag', ['post_id', 'tag_id']);
        $this->addForeignKey('fk_post_post_tag', 'post_tag', 'post_id', 'posts', 'id', 'CASCADE');
        $this->addForeignKey('fk_tag_post_tag', 'post_tag', 'tag_id', 'tags', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_post_post_tag', 'posts_tags');
        $this->dropForeignKey('fk_tag_post_tag', 'posts_tags');

        $this->dropTable('{{%posts_tags}}');
        $this->dropTable('{{%tags}}');

        $this->dropForeignKey('fk_category_post', 'posts');
        $this->dropForeignKey('fk_posts_author_id', 'posts');
        $this->dropForeignKey('fk_posts_created_by', 'posts');
        $this->dropForeignKey('fk_posts_translator_id', 'posts');
        $this->dropForeignKey('fk_posts_city_id', 'posts');

        $this->dropTable('{{%posts}}');
    }
}
