<?php
declare(strict_types=1);

namespace Ria\News\Console\Controllers;

use Exception;
use Faker\Factory;
use Ria\News\Core\Models\Category\Template;
use Ria\News\Core\Models\Post\Icon;
use Ria\News\Core\Models\Post\Status;
use Ria\News\Core\Models\Post\Type;
use Yii;
use yii\console\Controller;
use Cocur\Slugify\Slugify;

/**
 * Class FakerController
 * @package console\controllers
 */
class FakerController extends Controller
{
    /**
     * @param int $count
     * @param null $parentId
     * @throws Exception
     */
    public function actionCategories(int $count = 5, $parentId = null)
    {
        $faker   = Factory::create();
        $command = Yii::$app->db->createCommand();
        for ($i = 1; $i <= $count; $i++) {
            $command->insert('categories', [
                'parent_id' => $parentId,
                'template'  => $faker->randomElement(Template::all()),
                'status'    => $faker->boolean,
            ])->execute();
            $categoryId = Yii::$app->db->getLastInsertID();
            foreach (Yii::$app->params['languages'] as $language => $label) {
                $faker->languageCode = $language;
                $command->insert('categories_lang', [
                    'category_id' => $categoryId,
                    'language'    => $language,
                    'title'       => ucfirst($faker->words(random_int(1, 3), true)),
                    'slug'        => $faker->unique()->slug(1),
                    'meta'        => '{"title": "test", "description": "test", "keywords": "test"}'
                ])->execute();
            }
            if (!$parentId) {
                $this->actionCategories(random_int(0, 5), $categoryId);
            }
        }
    }

    /**
     * @param int $count
     * @throws \yii\db\Exception
     */
    public function actionStories(int $count = 5)
    {
        $faker   = Factory::create();
        $command = Yii::$app->db->createCommand();
        for ($i = 1; $i <= $count; $i++) {
            $command->insert('stories', [
                'cover'  => null,
                'status' => $faker->boolean,
            ])->execute();
            $storyId = Yii::$app->db->getLastInsertID();
            foreach (Yii::$app->params['languages'] as $language => $label) {
                $faker->languageCode = $language;
                $command->insert('stories_lang', [
                    'story_id'    => $storyId,
                    'language'    => $language,
                    'title'       => ucfirst($faker->words(random_int(1, 3), true)),
                    'slug'        => $faker->unique()->slug(1),
                    'description' => $faker->text(),
                    'meta'        => '{"title": "test", "description": "test", "keywords": "test"}'
                ])->execute();
            }
        }
    }

    public function actionRegions()
    {
        $regions = [
            'az' => [
                'Abşeron'    => ['Abşeron', 'Xızı', 'Sumqayıt', 'Qobustan'],
                'Aran'       => ['Ağcabədi', 'Ağdaş', 'Beyləqan', 'Bərdə', 'Biləsuvar', 'Göyçay', 'Hacıqabul', 'İmişli', 'Kürdəmir',
                    'Neftçala', 'Saatlı', 'Sabirabad', 'Salyan', 'Ucar', 'Zərdab', 'Şirvan', 'Mingəçevir', 'Yevlax'
                ],
                'Qərb'       => ['Ağstafa', 'Daşkəsan', 'Gədəbəy', 'Goranboy', 'Xanlar', 'Qazax', 'Samux', 'Tovuz', 'Gəncə', 'Naftalan'],
                'Cənub'      => ['Lənkəran', 'Astara', 'Cəlilabad', 'Lerik', 'Masallı', 'Yardımlı'],
                'Şimal-Qərb' => ['Balakən', 'Qax', 'Qəbələ', 'Oğuz', 'Zaqatala', 'Şəki', 'Ağsu', 'İsmayıllı', 'Şamaxı'],
                'Şimal'      => ['Dəvəçi', 'Xaçmaz', 'Quba', 'Qusar', 'Siyəzən'],
                'Qarabağ'    => ['Ağdam', 'Tərtər', 'Füzuli'],
                'Naxçıvan'   => ['Naxçıvan']
            ],
            'ru' => [
                'Абшерон'      => ['Абшерон', 'Хызы', 'Сумгайыт', 'Гобустан'],
                'Аран'         => ['Агджабеди', 'Агдаш', 'Бейлаган', 'Барда', 'Билясувар', 'Гёйчай', 'Гаджигабул', 'Имишли',
                    'Кюрдамир', 'Нефтчала', 'Саатлы', 'Сабирабад', 'Сальян', 'Уджар', 'Зардаб', 'Ширван', 'Мингячевир', 'Евлах'
                ],
                'Запад'        => ['Агстафа', 'Дашкесан', 'Гедабей', 'Геранбой', 'Гёйгёль', 'Газах', 'Самух', 'Шамкир', 'Товуз', 'Гянджа', 'Нафталан'],
                'Юг'           => ['Лянкяран', 'Астара', 'Джалилабад', 'Лерик', 'Масаллы', 'Ярдымлы'],
                'Северо-Запад' => ['Балакен', 'Гах', 'Габала', 'Огуз', 'Загатала', 'Шеки', 'Агсу', 'Исмаиллы', 'Шамахы'],
                'Север'        => ['Девечи', 'Хачмаз', 'Губа', 'Гусар', 'Сиязань'],
                'Карабах'      => ['Агдам', 'Тертер', 'Физули'],
                'Нахчыван'     => ['Нахчыван']
            ],
            'en' => [
                'Absheron'   => ['Absheron', 'Khizi', 'Sumgayit', 'Gobustan'],
                'Aran'       => ['Agjabedi', 'Agdash', 'Beylagan', 'Barda', 'Bilasuvar', 'Goychay', 'Hajigabul', 'Imishli', 'Kurdamir',
                    'Neftchala', 'Saatli', 'Sabirabad', 'Salyan', 'Ujar', 'Zardab', 'Shirvan', 'Mingachevir', 'Yevlakh'],
                'West'       => ['Agstafa', 'Dashkasan', 'Gadabay', 'Goranboy', 'Goygol', 'Gazakh', 'Samukh', 'Shamkir', 'Tovuz', 'Ganja', 'Naftalan'],
                'South'      => ['Lankaran', 'Astara', 'Jalilabad', 'Lerik', 'Masalli', 'Yardimli'],
                'Northwest'  => ['Balakan', 'Gakh', 'Gabala', 'Oguz', 'Zagatala', 'Sheki', 'Agsu', 'Ismayilli', 'Shamakhi'],
                'North'      => ['Devechi', 'Khachmaz', 'Guba', 'Gusar', 'Siyazan'],
                'Karabakh'   => ['Aghdam', 'Tartar', 'Fuzuli'],
                'Nakhchivan' => ['Nakhchivan'],
            ]
        ];

        $command = Yii::$app->db->createCommand();
        $command->delete('regions')->execute();

        $slugify = new Slugify();

        foreach (array_values($regions['az']) as $regionIndex => $cities) {
            // regions
            $command->insert('regions', [
                'order' => random_int(1, 100)
            ])->execute();

            // region id
            $regionId = Yii::$app->db->getLastInsertID();

            foreach (Yii::$app->params['languages'] as $language => $label) {
                $regionNameByLanguage = array_keys($regions[$language])[$regionIndex];

                // regions lang
                $command->insert('regions_lang', [
                    'region_id' => $regionId,
                    'title'     => $regionNameByLanguage,
                    'slug'      => $slugify->slugify($regionNameByLanguage),
                    'language'  => $language
                ])->execute();
            }

            foreach (array_keys($cities) as $cityIndex) {
                // cities
                $command->insert('cities', [
                    'region_id' => $regionId
                ])->execute();

                // city id
                $cityId = Yii::$app->db->getLastInsertID();

                foreach (Yii::$app->params['languages'] as $language => $label) {
                    $cityNameByLanguage = array_values($regions[$language])[$regionIndex][$cityIndex];

                    // cities lang
                    $command->insert('cities_lang', [
                        'city_id'  => $cityId,
                        'title'    => $cityNameByLanguage,
                        'slug'     => $slugify->slugify($cityNameByLanguage),
                        'language' => $language
                    ])->execute();
                }
            }
        }
    }

    /**
     * @param int $count
     * @throws \yii\db\Exception
     */
    public function actionNews(int $count = 5)
    {
        $categories = Yii::$app->db->createCommand('SELECT id FROM categories')->queryAll();
        $stories    = Yii::$app->db->createCommand('SELECT id FROM stories')->queryAll();
        if (empty($categories)) {
            $this->stderr('Run categories faker first');
            return;
        } elseif (empty($stories)) {
            $this->stderr('Run stories faker first');
            return;
        }

        $randomUserId = Yii::$app->db->createCommand('SELECT id FROM users  ORDER BY RAND() LIMIT 1')->queryScalar();
        if (empty($randomUserId)) {
            $this->stderr('Run users faker first');
            return;
        }

        $faker   = Factory::create();
        $command = Yii::$app->db->createCommand();
        $group   = 1;

        for ($i = 1; $i <= $count; $i++) {
            foreach (Yii::$app->params['languages'] as $language => $label) {
                $title = $faker->realText(100);
                $command->insert('posts', [
                    'language'     => $language,
                    'created_by'   => $randomUserId,
                    'group_id'     => $group,
                    'story_id'     => $faker->randomElement($stories),
                    'status'       => Status::ON_MODERATION,
                    'type'         => $faker->randomElement(Type::all()),
                    'icon'         => $faker->randomElement(Icon::all()),
                    'image'        => null,
                    'is_exclusive' => $faker->boolean(20),
                    'category_id'  => $faker->randomElement($categories)['id'],
                    'author_id'    => $randomUserId,
                    'title'        => $title,
                    'description'  => $faker->realText(150),
                    'content'      => $faker->realText(700),
                    'source'       => $faker->realText(20),
                    'slug'         => $faker->unique()->slug(3),

                    'meta'         => json_encode([
                        'title'       => $title,
                        'description' => 'Test description',
                        'keywords'    => 'some,keys'
                    ]),
                    'published_at' => $faker->dateTime()->format('Y-m-d H:i:s'),
                    'is_main'      => $faker->boolean(20),
                    'is_actual'    => $faker->boolean(20),
                    'is_breaking'  => $faker->boolean(20),
                ])->execute();

                $postId = Yii::$app->db->getLastInsertID();

                for ($i = 0; $i < random_int(2, 5); $i++) {
                    $name = $faker->unique()->word();

                    $command->insert('tags', [
                        'slug' => $faker->unique()->slug(3),
                    ])->execute();

                    $tagId = Yii::$app->db->getLastInsertID();

                    $command->insert('tags_lang', [
                        'tag_id'   => $tagId,
                        'name'     => $name,
                        'language' => $language,
                    ])->execute();


                    $command->insert('post_tag', [
                        'post_id' => $postId,
                        'tag_id'  => $tagId,
                    ])->execute();
                }
            }

            $group += 3;
        }
    }
}
