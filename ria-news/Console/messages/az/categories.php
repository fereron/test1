<?php

return [
    'Categories'   => 'Bölmələr',
    'Status'       => 'Status',
    'Parent id'    => 'Valideyn',
    'Template'     => 'Şablon',
    'Title'        => 'Başlıq',
    'Sorting'      => 'Çeşidləmə',
    'Description'  => 'Təsvir',
    'Slug'         => 'Slug',
    'Is active'    => 'Görünmə',
    'Cover'        => 'Şəkil',
    'Save'         => 'Yadda saxla',
    'Create category' => 'Yeni bölmə',
    'Update category' => 'Redaktə et ',
    'Settings'     => 'Ayarlar'
];