<?php

return [
    'type_created' => 'Created',
    'type_viewed' => 'Viewed',
    'type_corrected' => 'Corrected',
    'type_updated' => 'Updated',
    'type_deleted' => 'Deleted',
    'type_archived' => 'Archived',
    'type_sent_to_moderation' => 'Sent to moderation',

    'SuperAdministrator' => 'Super Administrator',
    'SeniorNewsman' => 'Senior Newsman',
    'Administrator' => 'Administrator',
    'DepartmentHead' => 'Department Head',
    'Designer' => 'Designer',
    'LeadTranslator' => 'Lead translator',
    'Newsman' => 'Newsman',
    'Translator' => 'Translator',
];