<?php

return [
    'Add expert quote' => 'Ekspertin rəyi əlavə et',
    'Expert'           => 'Ekspert',
    'Choose expert'    => 'Eksperti seçin',
    'Quote not found'  => 'Ekspertin rəyi tapılmadı'
];