<?php

return [
    'Photo manager'     => 'Foto menecer',
    'Photos search'     => 'Şəkillərin axtarışı',
    'Search'            => 'Axtarış',
    'Author'            => 'Müəllif',
    'Description'       => 'Təsvir',
    'Source'            => 'Mənbə',
    'Upload photo'      => 'Yenisini yüklə',
    'Add'               => 'Əlavə et',
    'Delete physically' => 'Silmək',
    'Crop photo'        => 'Şəkli kəsmək',
    'Save'              => 'Yadda saxla',
    'Cancel'            => 'İmtina',
];