<?php

return [
    'Photo manager'     => 'Фото-менеджер',
    'Photos search'     => 'Поиск фотографий',
    'Search'            => 'Поиск',
    'Author'            => 'Автор',
    'Description'       => 'Описание',
    'Source'            => 'Источник',
    'Upload photo'      => 'Загрузить новую',
    'Add'               => 'Добавить',
    'Delete physically' => 'Удалить',
    'Crop photo'        => 'Кадрировать фото',
    'Save'              => 'Сохранить',
    'Cancel'            => 'Отмена',
];