<?php

return [
    'Stories'      => 'Сюжеты',
    'Title'        => 'Заголовок',
    'Description'  => 'Описание',
    'Slug'         => 'Slug',
    'Is active'    => 'Активность',
    'Cover'        => 'Изображение',
    'Save'         => 'Сохранить',
    'Create story' => 'Создать сюжет',
    'Update story' => 'Редактировать ',
    'Settings'     => 'Настройки',
    'Show on site' => 'Показывать на сайте',
    'Status' => 'Статус'
];