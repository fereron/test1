<?php

return [
    'Regions'       => 'Регионы',
    'Order'         => 'Сортировка',
    'Title'         => 'Заголовок',
    'Create region' => 'Создать регион',
    'Update region' => 'Редактировать',
    'Slug'         => 'Slug'
];
