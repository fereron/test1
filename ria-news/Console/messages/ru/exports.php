<?php

return [
    'common'             => 'Экспорт в RSS-ленты',
    'yandex_dzen'        => 'Яндекс-Дзен/Пульс',
    'push_notifications' => 'Push-уведомления',
    'yandex_text'        => 'Яндекс Тексты'
];