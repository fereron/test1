<?php
declare(strict_types=1);

namespace Ria\News\Common\Bootstrap;

use Ria\News\Core\Commands\ExpertQuote\CreateQuoteCommand;
use Ria\News\Core\Commands\ExpertQuote\UpdateQuoteCommand;
use Ria\News\Core\Commands\Post\ArchivePostCommand;
use Ria\News\Core\Commands\Post\MoveDateCommand;
use Ria\News\Core\Commands\Post\PreviewPostCommand;
use Ria\News\Core\Commands\Speech\CreateSpeechCommand;
use Ria\News\Core\Commands\Speech\DeleteSpeechCommand;
use Ria\News\Core\Commands\Speech\UpdateSpeechCommand;
use Ria\News\Core\Handlers\ExpertQuote\CreateQuoteHandler;
use Ria\News\Core\Handlers\ExpertQuote\UpdateQuoteHandler;
use Ria\News\Core\Handlers\Post\ArchivePostHandler;
use Ria\News\Core\Handlers\Post\MoveDateHandler;
use Ria\News\Core\Handlers\Post\PreviewPostHandler;
use Ria\News\Core\Handlers\Speech\SpeechHandler;
use Yii;
use yii\base\BootstrapInterface;
use yii\di\Container;
use League\Tactician\CommandBus;
use Ria\Core\Components\CommandBus\CommandBusFactory;

# <Commands>
#Posts
use Ria\News\Core\Commands\Post\CreatePostCommand;
use Ria\News\Core\Commands\Post\DeletePostCommand;
use Ria\News\Core\Commands\Post\UpdatePostCommand;

#Stories
use Ria\News\Core\Commands\Story\CreateStoryCommand;
use Ria\News\Core\Commands\Story\DeleteStoryCommand;
use Ria\News\Core\Commands\Story\UpdateStoryCommand;

#Categories
use Ria\News\Core\Commands\Category\CreateCategoryCommand;
use Ria\News\Core\Commands\Category\DeleteCategoryCommand;
use Ria\News\Core\Commands\Category\UpdateCategoryCommand;
use Ria\News\Core\Commands\Category\MoveUpCategoryCommand;
use Ria\News\Core\Commands\Category\MoveDownCategoryCommand;

#Regions
use Ria\News\Core\Commands\Region\CreateRegionCommand;
use Ria\News\Core\Commands\Region\UpdateRegionCommand;
use Ria\News\Core\Commands\Region\DeleteRegionCommand;
use Ria\News\Core\Commands\Region\MoveDownRegionCommand;
use Ria\News\Core\Commands\Region\MoveUpRegionCommand;

#Cities
use Ria\News\Core\Commands\City\CreateCityCommand;
use Ria\News\Core\Commands\City\DeleteCityCommand;
use Ria\News\Core\Commands\City\UpdateCityCommand;

#Widget
use Ria\News\Core\Commands\Widget\CreateWidgetCommand;

# </Commands>

# <Handlers>

#Categories
use Ria\News\Core\Handlers\Category\CreateCategoryHandler;
use Ria\News\Core\Handlers\Category\UpdateCategoryHandler;
use Ria\News\Core\Handlers\Category\DeleteCategoryHandler;
use Ria\News\Core\Handlers\Category\MoveUpCategoryHandler;
use Ria\News\Core\Handlers\Category\MoveDownCategoryHandler;

#Regions
use Ria\News\Core\Handlers\Region\CreateRegionHandler;
use Ria\News\Core\Handlers\Region\UpdateRegionHandler;
use Ria\News\Core\Handlers\Region\DeleteRegionHandler;
use Ria\News\Core\Handlers\Region\MoveUpRegionHandler;
use Ria\News\Core\Handlers\Region\MoveDownRegionHandler;

#Ciries
use Ria\News\Core\Handlers\City\CreateCityHandler;
use Ria\News\Core\Handlers\City\UpdateCityHandler;
use Ria\News\Core\Handlers\City\DeleteCityHandler;

# Posts
use Ria\News\Core\Handlers\Post\PostHandler;
use Ria\News\Core\Handlers\Post\PostDeleteHandler;

# Stories
use Ria\News\Core\Handlers\Story\StoryHandler;
use Ria\News\Core\Handlers\Story\DeleteStoryHandler;

# Widget
use Ria\News\Core\Handlers\Widget\CreateWidgetHandler;

# </Handlers>

/**
 * Class SetUp
 * @package Ria\News\Common\Bootstrap
 */
class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = Yii::$container;
        $handlers  = [
            # Posts
            CreatePostCommand::class       => PostHandler::class,
            UpdatePostCommand::class       => PostHandler::class,
            PreviewPostCommand::class      => PreviewPostHandler::class,
            DeletePostCommand::class       => PostDeleteHandler::class,
            ArchivePostCommand::class      => ArchivePostHandler::class,
            MoveDateCommand::class         => MoveDateHandler::class,

            # Stories
            CreateStoryCommand::class      => StoryHandler::class,
            UpdateStoryCommand::class      => StoryHandler::class,
            DeleteStoryCommand::class      => DeleteStoryHandler::class,

            # Categories
            CreateCategoryCommand::class   => CreateCategoryHandler::class,
            UpdateCategoryCommand::class   => UpdateCategoryHandler::class,
            DeleteCategoryCommand::class   => DeleteCategoryHandler::class,
            MoveDownCategoryCommand::class => MoveDownCategoryHandler::class,
            MoveUpCategoryCommand::class   => MoveUpCategoryHandler::class,

            #Regions
            CreateRegionCommand::class     => CreateRegionHandler::class,
            UpdateRegionCommand::class     => UpdateRegionHandler::class,
            DeleteRegionCommand::class     => DeleteRegionHandler::class,
            MoveUpRegionCommand::class     => MoveUpRegionHandler::class,
            MoveDownRegionCommand::class   => MoveDownRegionHandler::class,

            #Cities
            CreateCityCommand::class       => CreateCityHandler::class,
            UpdateCityCommand::class       => UpdateCityHandler::class,
            DeleteCityCommand::class       => DeleteCityHandler::class,
            // Widgets
            CreateWidgetCommand::class     => CreateWidgetHandler::class,

            CreateQuoteCommand::class => CreateQuoteHandler::class,
            UpdateQuoteCommand::class => UpdateQuoteHandler::class,

            CreateSpeechCommand::class => SpeechHandler::class,
            UpdateSpeechCommand::class => SpeechHandler::class,
            DeleteSpeechCommand::class => SpeechHandler::class,
        ];

        $container->setSingleton(CommandBus::class, function (Container $container) use ($handlers) {
            return CommandBusFactory::instantiate($container, $handlers);
        });
    }
}