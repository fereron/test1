<?php

namespace Ria\News\Tests\Post;

use PHPUnit\Framework\TestCase;
use Ria\News\Models\Post\Exceptions\PostException;
use Ria\News\Models\Post\Post;
use Ria\News\Models\Post\Type;

/**
 * Class TestPostEntity
 * @package PostRecord
 */
class PostEntityTest extends TestCase
{
    /**
     *
     */
    public function testOnlyVideoPostMightHasYouTubeVideo()
    {
        $this->expectException(PostException::class);
        $this->expectExceptionMessage('You can add a youtube video only for a video post');

        $post = new Post(new Type(Type::ARTICLE));
        $post->addYouTubeVideo('testVideoId');
    }
}
