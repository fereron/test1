<?php
/** @noinspection PhpIncludeInspection */
declare(strict_types=1);

namespace Ria\News;

use Ria\Core\Base\Module as BaseModule;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package Ria\News
 */
class Module extends BaseModule
{
    public function init()
    {
        parent::init();

        Yii::configure($this, ArrayHelper::merge(
            require(__DIR__ . '/Common/config/main.php'),
            require(__DIR__ . '/Common/config/main-local.php'),
            require Yii::getAlias('@module/config/main.php'),
            require Yii::getAlias('@module/config/main-local.php')
        ));

        $this->bootstrap();
    }
}