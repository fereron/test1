<?php
declare(strict_types=1);

namespace Ria\News\Frontend\Widgets\LastNews\Providers;

use Ria\News\Components\Queries\Post\PostRecord;
use Ria\News\Models\Post\Status;

/**
 * Class LastNewsProvider
 * @package Ria\News\Widgets\LastNews\Providers
 */
class LastNewsProvider implements LastNewsDataProviderInterface
{
    /**
     * @return array
     */
    public function getNews(): array
    {
        return PostRecord::find()
            ->where(['status' => [Status::ON_MODERATION, Status::PENDING]])
            ->orderBy('created_at DESC')
            ->limit(10)
            ->all();
    }
}