<?php
declare(strict_types=1);

namespace Ria\News\Frontend\Widgets\LastNews\Providers;

/**
 * Interface LastNewsDataProviderInterface
 * @package Ria\News\Widgets\LastNews\Providers
 */
interface LastNewsDataProviderInterface
{
    /**
     * @return array
     */
    public function getNews(): array;
}