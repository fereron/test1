<?php
/**
 * @var $news PostRecord[]
 */

use Ria\News\Components\ActiveRecord\Post\PostRecord;

?>
<h1>Last news</h1>

<ul>
    <?php foreach ($news as $record): ?>
        <li><?= $record->title?></li>
    <?php endforeach; ?>
</ul>