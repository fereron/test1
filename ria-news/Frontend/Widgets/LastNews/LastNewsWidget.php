<?php
declare(strict_types=1);

namespace Ria\News\Frontend\Widgets\LastNews;

use Ria\Core\View\BaseWidget;
use Ria\News\Widgets\LastNews\Providers\LastNewsDataProviderInterface;
use Ria\News\Widgets\LastNews\Providers\LastNewsProvider;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;

/**
 * Class LastNewsWidget
 * @package Ria\News\Widgets\LastNews
 */
class LastNewsWidget extends BaseWidget
{
    /**
     * @var LastNewsDataProviderInterface
     */
    public $provider = LastNewsProvider::class;

    /**
     * @return string
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function run()
    {
        /* @var $provider LastNewsDataProviderInterface */
        $provider = Yii::$container->get($this->provider);

        return $this->render('list', [
            'news' => $provider->getNews()
        ]);
    }
}