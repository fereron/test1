<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Forms;

use Ria\Votes\Core\Models\Vote;
use Yii;
use Ria\Core\Forms\CompositeForm;
use Ria\Core\Models\Status;
use Ria\Votes\Module;

/**
 * Class VoteForm
 * @package Ria\Votes\Core\Forms
 * @property VoteOptionForm[] $options
 * 
 */
class VoteForm extends CompositeForm
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $status;

    /**
     * @var int
     */
    public $showRecaptcha;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $language;

    /**
     * @var string
     */
    public $startDate;

    /**
     * @var string
     */
    public $endDate;

    /**
     * @param Vote|null $vote
     * @param array $config
     * @return static
     */
    public static function create(Vote $vote = null, array $config = []): self
    {
        $form = new self($config);
        $form->startDate =  date('Y-m-d');

        $formOptions = [];
        if ($vote) {

            $form->id            = $vote->getId();
            $form->title         = $vote->getTitle();
            $form->status        = $vote->getStatus();
            $form->showRecaptcha = $vote->showRecaptcha();
            $form->language      = $vote->getLanguage();
            $form->startDate     = $vote->getStartDate()->format('Y-m-d');
            $form->endDate       = $vote->getEndDate() ? $vote->getEndDate()->format('Y-m-d') : null;

            $voteOptions = $vote->getOptions()->toArray();
            ksort($voteOptions);
            foreach ($voteOptions as $option) {
                $formOptions[] = VoteOptionForm::create($option);
            }

            $form->options = $formOptions;
        }

        return $form;
    }

    /**
     * @return array
     */
    protected function internalForms(): array
    {
        return ['options'];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['status', 'showRecaptcha'], 'boolean'],
            [['title', 'startDate'], 'required'],
            ['title', 'string', 'max' => 255],
            [['startDate', 'endDate'], 'date', 'format' => 'php:Y-m-d'],
            ['endDate', 'checkEndDate'],
        ];
    }

    /**
     * @param string $attribute
     * @param array|null $params
     * @throws \yii\base\InvalidConfigException
     */
    public function checkEndDate(string $attribute, ?array $params)
    {
        $endDate = strtotime($this->endDate);
        if ($endDate < time() || strtotime($this->startDate) > $endDate) {
            $this->addError($attribute, Module::t('votes', 'You cannot set End Date to before.'));
        }
    }

    public function attributeLabels()
    {
        return [
            'title'     => Module::t('votes', 'Title'),
            'startDate' => Module::t('votes', 'Start Date'),
            'endDate'   => Module::t('votes', 'End Date'),
            'status'   => Module::t('votes', 'Status'),
        ];
    }

}