<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Forms;

use Doctrine\ORM\EntityRepository;
use Ria\Core\Data\DoctrineDataProvider;
use Yii;
use yii\base\Model;
use yii\data\DataProviderInterface;

/**
 * Class VoteSearch
 * @package Ria\Votes\Core\Forms
 */
class VoteSearch extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $language;

    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * VoteSearch constructor.
     * @param EntityRepository $repository
     * @param array $config
     */
    public function __construct(EntityRepository $repository, $config = [])
    {
        parent::__construct($config);
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'status'], 'integer'],
            [['title', 'language'], 'string'],
        ];
    }

    /**
     * @param $params
     * @return DataProviderInterface
     */
    public function search($params): DataProviderInterface
    {
        $this->load($params);

        $query = $this->repository->createQueryBuilder('v');
        $dataProvider = new DoctrineDataProvider([
            'query'      => $query,
            'modelClass' => $this->repository->getClassName(),
            'sort'       => [
                'attributes'   => ['id'],
                'defaultOrder' => ['id' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => 12,
                'pageSizeParam' => false
            ]
        ]);

        $query->andWhere('v.language = :locale')->setParameters([':locale' => $this->language ?: Yii::$app->params['defaultLanguage']]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->id) {
            $query->andWhere($query->expr()->eq('v.id', $this->id));
        }

        if ($this->status) {
            $query->andWhere($query->expr()->eq('v.status', $this->status));
        }

        if ($this->title) {
            $query->andWhere($query->expr()->like('v.title', $query->expr()->literal('%' . $this->title . '%')));
        }

        return $dataProvider;
    }
}