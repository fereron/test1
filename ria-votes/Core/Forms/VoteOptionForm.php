<?php

namespace Ria\Votes\Core\Forms;

use Ria\Core\Forms\CompositeForm;
use Ria\Votes\Core\Models\Option;
use Ria\Votes\Module;

class VoteOptionForm extends CompositeForm
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $sort = 0;

    public $isNewRecord = true;

    public static function create(Option $option)
    {
        $form = new self();
        $form->id = $option->getId();
        $form->title = $option->getTitle();
        $form->sort = $option->getSort();
        $form->isNewRecord = false;

        return $form;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['title', 'sort'], 'required'],
            ['id', 'integer'],
            ['title', 'string', 'max' => 255],
        ];
    }

    protected function internalForms()
    {
        // TODO: Implement internalForms() method.
    }

    public function attributeLabels()
    {
        return [
            'title' => Module::t('votes', 'Title'),
        ];
    }
}