<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Query\Repositories;

use Doctrine\ORM\EntityRepository;
use Mobile_Detect;
use Ria\Core\Query\EntitySpecificationRepositoryTrait;
use Ria\Votes\Core\Models\Log;
use Ria\Votes\Core\Models\Option;
use Ria\Votes\Core\Query\Hydrator\VoteHydrator;
use Tightenco\Collect\Support\Collection;

/**
 * Class VotesRepository
 * @package Ria\Votes\Core\Query\Repositories
 */
class VotesRepository extends EntityRepository
{
    use EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    function getSpecsNamespace(): string
    {
        return "Ria\\Persons\\Core\\Query\\Specifications";
    }

    /**
     * @param int $id
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getById(int $id)
    {
        $qb = $this->createQueryBuilder('v');

        $vote = $qb
            ->select(['v.id', 'v.title', 'v.end_date'])
            ->where('v.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(VoteHydrator::HYDRATION_MODE);

        if (!empty($vote)) {
            $vote->options = new Collection($this->getVoteOptions($vote->id));
        }

        return $vote;
    }

    /**
     * @param string $language
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getForMain(string $language)
    {
        $userAgent = (new Mobile_Detect)->getUserAgent();
        $qb = $this->createQueryBuilder('v');

        $sub = $this->getEntityManager()
            ->createQueryBuilder()
            ->select("l")
            ->from(Log::class,"l")
            ->andWhere('IDENTITY(l.vote) = v.id')
            ->andWhere('l.userAgent = :user_agent');

        $vote = $qb
            ->select(['v.id', 'v.showRecaptcha', 'v.title', 'v.end_date'])
            ->where('v.language = :language')
            ->andWhere(
                $qb->expr()->not($qb->expr()->exists($sub->getDQL()))
            )
            ->andWhere('v.status.status = :status')
            ->andWhere(
                $qb->expr()->orX($qb->expr()->isNull('v.end_date'), 'v.end_date >= :date')
            )
            ->setMaxResults(1)
            ->orderBy('v.created_at', 'DESC')
            ->setParameters([
                'language' => $language,
                'status' => true,
                'user_agent' => md5($userAgent),
                'date' => (new \DateTime())->format('Y-m-d') . ' 00:00:00',
            ])
            ->getQuery()
            ->getOneOrNullResult(VoteHydrator::HYDRATION_MODE);

        if (!empty($vote)) {
            $vote->options = new Collection($this->getVoteOptions($vote->id));
        }

        return $vote;
    }

    /**
     * @param int $vote_id
     * @return array
     */
    public function getVoteOptions(int $vote_id): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select(['o.id', 'o.title', 'o.score'])
            ->from(Option::class, 'o')
            ->innerJoin('o.vote', 'v', 'WITH', 'v.id = :vote_id')
            ->orderBy('o.sort')
            ->setParameter('vote_id', $vote_id)
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $language
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getResultsForMain(string $language)
    {
        $userAgent = (new Mobile_Detect)->getUserAgent();
        $qb = $this->createQueryBuilder('v');

        $sub = $this->getEntityManager()
            ->createQueryBuilder()
            ->select("l")
            ->from(Log::class,"l")
            ->andWhere('IDENTITY(l.vote) = v.id')
            ->andWhere('l.userAgent = :user_agent');

        $vote = $qb
            ->select(['v.id', 'v.title', 'v.end_date'])
            ->where('v.language = :language')
            ->andWhere(
                $qb->expr()->exists($sub->getDQL())
            )
            ->andWhere('v.status.status = :status')
            ->andWhere(
                $qb->expr()->orX($qb->expr()->isNull('v.end_date'), 'v.end_date >= :date')
            )
            ->setMaxResults(1)
            ->orderBy('v.created_at', 'DESC')
            ->setParameters([
                'language' => $language,
                'status' => true,
                'user_agent' => md5($userAgent),
                'date' => (new \DateTime())->format('Y-m-d') . ' 00:00:00',
            ])
            ->getQuery()
            ->getOneOrNullResult(VoteHydrator::HYDRATION_MODE);

        if (!empty($vote)) {
            $vote->options = new Collection($this->getVoteOptions($vote->id));
        }

        return $vote;
    }

    /**
     * @param int $voteId
     * @return int|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getVotedOption(int $voteId): ?int
    {
        $userAgent = (new Mobile_Detect)->getUserAgent();

        $sub = $this->getEntityManager()
            ->createQueryBuilder()
            ->select("vo.id")
            ->from(Log::class,"l")
            ->innerJoin('l.option', 'vo')
            ->where('IDENTITY(l.vote) = :vote_id')
            ->andWhere('l.userAgent = :user_agent')
            ->setMaxResults(1)
            ->setParameter('vote_id', $voteId)
            ->setParameter('user_agent', md5($userAgent))
            ->getQuery()
            ->getOneOrNullResult();

        return $sub && isset($sub['id']) ? $sub['id'] : null;
    }

}