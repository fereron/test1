<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Query\Hydrator;

use Ria\Core\Query\ViewModelHydrator;
use Ria\Votes\Core\Query\ViewModel\VoteViewModel;

/**
 * Class VoteHydrator
 * @package Ria\Votes\Core\Query\Hydrator
 */
class VoteHydrator extends ViewModelHydrator
{
    const HYDRATION_MODE = 'VoteHydrator';

    /**
     * @return string
     */
    protected function getViewModelClassName(): string
    {
        return VoteViewModel::class;
    }
}