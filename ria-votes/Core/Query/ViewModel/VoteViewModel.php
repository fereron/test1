<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Query\ViewModel;

use Ria\Core\Query\ViewModel;
use Tightenco\Collect\Support\Collection;

/**
 * Class VoteViewModel
 * @package Ria\Votes\Core\Query\ViewModel
 *
 * @property int $id
 * @property bool $showRecaptcha
 * @property string $title
 * @property \DateTime $end_date
 * @property Collection $options
 */
class VoteViewModel extends ViewModel
{

    /**
     * @param array $option
     * @return int
     */
    public function getOptionPercentage(array $option): int
    {
        return (int) round($option['score'] / $this->options->sum('score') * 100);
    }

}