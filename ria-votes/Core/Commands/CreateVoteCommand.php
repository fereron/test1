<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Commands;

/**
 * Class CreateVoteCommand
 * @package Ria\Votes\Core\Commands
 */
class CreateVoteCommand extends VoteCommand {}