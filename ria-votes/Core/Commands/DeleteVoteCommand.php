<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Commands;

use Ria\Core\Components\CommandBus\CommandInterface;

/**
 * Class DeleteVoteCommand
 * @package Ria\Votes\Core\Commands
 *
 * @property integer $id
 */
class DeleteVoteCommand implements CommandInterface
{
    /**
     * DeleteCategoryCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}