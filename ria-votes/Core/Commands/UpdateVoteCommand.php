<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Commands;

/**
 * Class UpdateVoteCommand
 * @package Ria\Votes\Core\Commands
 */
class UpdateVoteCommand extends VoteCommand {}