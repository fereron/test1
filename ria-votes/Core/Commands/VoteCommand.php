<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Commands;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\Core\Models\Status;
use Ria\Votes\Core\Forms\VoteForm;

/**
 * Class VoteCommand
 * @package Ria\Votes\Core\Commands
 *
 * @property int $id
 * @property Status $status
 * @property bool $showRecaptcha
 * @property string $title
 * @property string $language
 * @property string $startDate
 * @property string $endDate
 * @property array options
 */
class VoteCommand implements CommandInterface
{
    /**
     * VoteCommand constructor.
     * @param VoteForm $form
     * @param array $optionsForm
     * @throws \Exception
     */
    public function __construct(VoteForm $form, array $optionsForm)
    {
        $this->id            = $form->id;
        $this->status        = new Status((int)$form->status);
        $this->showRecaptcha = (bool) $form->showRecaptcha;
        $this->title         = $form->title;
        $this->language      = $form->language;
        $this->startDate     = new \DateTime($form->startDate);
        $this->endDate       = $form->endDate ? new \DateTime($form->endDate) : null;

        $this->options = array_map(function ($option) {
            return [
                'id'    => $option->id,
                'title' => $option->title,
                'sort'  => (int)$option->sort,
            ];
        }, $optionsForm);
    }

}