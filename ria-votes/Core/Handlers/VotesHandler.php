<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Handlers;

use Doctrine\Common\Collections\ArrayCollection;
use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Votes\Core\Commands\CreateVoteCommand;
use Ria\Votes\Core\Commands\DeleteVoteCommand;
use Ria\Votes\Core\Commands\UpdateVoteCommand;
use Ria\Votes\Core\Models\Option;
use Ria\Votes\Core\Models\Vote;

class VotesHandler extends EntityHandler
{
    /**
     * @param CreateVoteCommand $command
     */
    public function createVote(CreateVoteCommand $command)
    {
        $vote = (new Vote)
            ->setStatus($command->status)
            ->setShowRecaptcha($command->showRecaptcha)
            ->setLanguage($command->language)
            ->setTitle($command->title)
            ->setStartDate($command->startDate ?: new \DateTime())
            ->setEndDate($command->endDate)
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        foreach ($command->options as $commandOption) {
            $option = (new Option)
                ->setTitle($commandOption['title'])
                ->setSort($commandOption['sort'])
                ->setScore(0);

            $vote->addOption($option);
        }


        $this->persist($vote);
    }

    /**
     * @param UpdateVoteCommand $command
     */
    public function updateVote(UpdateVoteCommand $command)
    {
        /** @var Vote $vote */
        $vote  = $this->find(Vote::class, $command->id);

        $vote
            ->setStatus($command->status)
            ->setShowRecaptcha($command->showRecaptcha)
            ->setTitle($command->title)
            ->setStartDate($command->startDate)
            ->setEndDate($command->endDate)
            ->setUpdatedAt(new \DateTime());

        $options = new ArrayCollection();
        foreach ($command->options as $commandOption) {

            if (!$commandOption['id']) {
                $option = new Option;
                $option->setScore(0);
            } else {
                $option = $vote->getOptions()
                    ->filter(function (Option $option) use ($commandOption) {
                        return $option->getId() == $commandOption['id'];
                    })
                    ->first();
            }

            $option
                ->setTitle($commandOption['title'])
                ->setSort($commandOption['sort']);

            $vote->addOption($option);
            $options->add($option);
        }

        $vote->sync('options', $options);
        $this->persist($vote);
    }

    /**
     * @param DeleteVoteCommand $command
     */
    public function deleteVote(DeleteVoteCommand $command)
    {
        $this->remove($this->find(Vote::class, $command->id));
    }

}