<?php

namespace Ria\Votes\Core\Validators;

use Baha2Odeh\RecaptchaV3\RecaptchaV3;
use Ria\Votes\Core\Models\Vote;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\validators\Validator;

class RecaptchaV3Validator extends Validator
{
    /**
     * @var bool
     */
    public $skipOnEmpty = false;


    /**
     * Recaptcha component
     * @var string|array|RecaptchaV3
     */
    public $component = 'recaptchaV3';


    /**
     * the minimum score for this request (0.0 - 1.0)
     * @var null|int
     */
    public $acceptance_score = null;

    /**
     * @var RecaptchaV3
     */
    private $_component = null;
    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $component = Instance::ensure($this->component, RecaptchaV3::class);
        if ($component == null) {
            throw new InvalidConfigException(Yii::t('recaptchav3', 'component is required.'));
        }
        $this->_component = $component;

        if ($this->message === null) {
            $this->message = Yii::t('recaptchav3', 'The verification code is incorrect.');
        }
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @throws \yii\base\NotSupportedException
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var Vote $vote */
        $vote = \Yii::$app->doctrine->getEntityManager()
            ->getRepository(Vote::class)
            ->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :vote_id')
            ->setParameter('vote_id', $model->voteId)
            ->getQuery()
            ->getSingleResult();

        if ($vote->showRecaptcha()) {
            $result = $this->validateValue($model->$attribute);
            if (!empty($result)) {
                $this->addError($model, $attribute, $result[0], $result[1]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $result = $this->_component->validateValue($value);
        if($result === false){
            return [$this->message, []];
        }
        if($this->acceptance_score !== null && $result < $this->acceptance_score){
            return [$this->message, []];
        }
        return null;
    }

}