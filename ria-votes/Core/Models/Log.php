<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Ria\Core\Models\Entity;

/**
 * @ORM\Table(name="vote_logs")
 * @ORM\Entity
 */
class Log extends Entity
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="user_agent")
     */
    private $userAgent;

    /**
     * @ORM\ManyToOne(targetEntity="\Ria\Votes\Core\Models\Vote", inversedBy="logs")
     * @JoinColumn(name="vote_id", referencedColumnName="id")
     */
    private $vote;

    /**
     * @ORM\ManyToOne(targetEntity="\Ria\Votes\Core\Models\Option", inversedBy="logs")
     * @JoinColumn(name="vote_option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param mixed $user_agent
     * @return Log
     */
    public function setUserAgent($user_agent)
    {
        $this->userAgent = md5($user_agent);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param Option $option
     * @return Log
     */
    public function setOption(Option $option)
    {
        $this->option = $option;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * @param Vote $vote
     * @return Log
     */
    public function setVote(Vote $vote)
    {
        $this->vote = $vote;
        return $this;
    }

}