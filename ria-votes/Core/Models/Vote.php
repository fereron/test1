<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Ria\Core\Models\Entity;
use Ria\Core\Models\RelationsTrait;
use Ria\Core\Models\Status;

/**
 * @ORM\Table(name="votes")
 * @ORM\Entity(repositoryClass="Ria\Votes\Core\Query\Repositories\VotesRepository")
 */
class Vote extends Entity
{
    use RelationsTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Embedded(class="Ria\Core\Models\Status", columnPrefix=false)
     * @var Status
     */
    private $status;

    /**
     * @ORM\Column(type="boolean", name="show_recaptcha")
     */
    private $showRecaptcha;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    public $language;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end_date;

    /**
     * @ORM\OneToMany(
     *     targetEntity="\Ria\Votes\Core\Models\Option",
     *     mappedBy="vote",
     *     cascade={"persist", "remove"},
     *     indexBy="sort",
     *     orphanRemoval=true
     *     )
     */
    private $options;

    /**
     * @ORM\OneToMany(
     *     targetEntity="\Ria\Votes\Core\Models\Log",
     *     mappedBy="vote",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     *     )
     */
    private $logs;

    /**
     * Vote constructor.
     */
    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->logs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @param Status $status
     * @return self
     */
    public function setStatus(Status $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param Option $option
     * @return $this
     */
    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options->add($option);
            $option->setVote($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Vote
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     * @return Vote
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @param mixed $created_at
     * @return Vote
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @param mixed $updated_at
     * @return Vote
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     * @return Vote
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     * @return Vote
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param Log $log
     * @return $this
     */
    public function addLog(Log $log): self
    {
        if (!$this->logs->contains($log)) {
            $this->logs->add($log);
            $log->setVote($this);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function showRecaptcha(): bool
    {
        return $this->showRecaptcha;
    }

    /**
     * @param mixed $showRecaptcha
     * @return Vote
     */
    public function setShowRecaptcha(bool $showRecaptcha)
    {
        $this->showRecaptcha = $showRecaptcha;
        return $this;
    }

}