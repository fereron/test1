<?php
declare(strict_types=1);

namespace Ria\Votes\Core\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Ria\Core\Models\Entity;

/**
 * @ORM\Table(name="vote_options")
 * @ORM\Entity
 */
class Option extends Entity
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="sort", type="integer")
     */
    private $sort;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity="\Ria\Votes\Core\Models\Vote", inversedBy="options")
     * @JoinColumn(name="vote_id", referencedColumnName="id")
     */
    private $vote;

    /**
     * @ORM\OneToMany(
     *     targetEntity="\Ria\Votes\Core\Models\Log",
     *     mappedBy="option",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     *     )
     */
    private $logs;

    /**
     * Option constructor.
     */
    public function __construct()
    {
        $this->logs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @return mixed
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * @param Vote $vote
     * @return $this
     */
    public function setVote(Vote $vote): self
    {
        $this->vote = $vote;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Option
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     * @return Option
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return $this
     */
    public function incrementScore()
    {
        $this->score++;
        return $this;
    }

    /**
     * @return $this
     */
    public function decrementScore()
    {
        $this->score--;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param mixed $sort
     * @return Option
     */
    public function setSort(int $sort)
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param Log $log
     * @return $this
     */
    public function addLog(Log $log): self
    {
        if (!$this->logs->contains($log)) {
            $this->logs->add($log);
            $log->setOption($this);
        }

        return $this;
    }

}