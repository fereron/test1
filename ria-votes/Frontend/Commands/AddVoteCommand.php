<?php
declare(strict_types=1);

namespace Ria\Votes\Frontend\Commands;

use Ria\Core\Components\CommandBus\CommandInterface;
use Ria\Votes\Frontend\Forms\VoteForm;

/**
 * Class AddVoteCommand
 * @package Ria\Votes\Core\Commands
 *
 * @property int $voteId
 * @property int $optionId
 * @property string $userAgent
 */
class AddVoteCommand implements CommandInterface
{

    /**
     * VoteAddedCommand constructor.
     * @param VoteForm $form
     */
    public function __construct(VoteForm $form)
    {
        $this->voteId    = (int)$form->voteId;
        $this->optionId  = (int)$form->optionId;
        $this->userAgent = $form->userAgent;
    }

}