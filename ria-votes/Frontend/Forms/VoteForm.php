<?php
declare(strict_types=1);

namespace Ria\Votes\Frontend\Forms;

use Mobile_Detect;
use Ria\Core\Validators\ExistValidator;
use Ria\Votes\Core\Models\Log;
use Ria\Votes\Core\Models\Option;
use Ria\Votes\Core\Validators\RecaptchaV3Validator;
use yii\base\Model;

class VoteForm extends Model
{
    /**
     * @var string
     */
    public $voteId;

    /**
     * @var string
     */
    public $optionId;

    /**
     * @var string
     */
    public $userAgent;

    /**
     * @var string
     */
    public $recaptcha;

    /**
     * VoteForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->userAgent = (new Mobile_Detect)->getUserAgent();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['voteId', 'optionId'], 'required'],
            ['optionId', ExistValidator::class, 'targetClass' => Option::class, 'targetAttribute' => 'id'],
            ['voteId', ExistValidator::class, 'targetClass' => Option::class, 'targetAttribute' => 'vote'],
            ['userAgent', 'checkUserDoesntVoted'],
            ['recaptcha', RecaptchaV3Validator::class, 'acceptance_score' => null]
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkUserDoesntVoted($attribute, $params)
    {
        $entityManager = \Yii::$app->doctrine->getEntityManager();

        $options = $entityManager
            ->getRepository(Option::class)
            ->createQueryBuilder('o')
            ->select('o.id')
            ->where('IDENTITY(o.vote) = :vote_id')
            ->setParameter('vote_id', $this->voteId)
            ->getQuery()
            ->execute();

        $query = $entityManager
            ->getRepository(Log::class)
            ->createQueryBuilder('l');

        $query
            ->select("count(l.userAgent)")
            ->where("l.userAgent = :userAgent")
            ->andWhere(
                $query->expr()->in("IDENTITY(l.option)", ':option_ids')
            )
            ->setParameter('userAgent', md5($this->userAgent))
            ->setParameter('option_ids', array_column($options, 'id'));

        if ($query->getQuery()->getSingleScalarResult() > 0) {
            $this->addError($attribute, \Yii::t('yii', '{attribute} "{value}" has already been taken.'));
        }
    }

}