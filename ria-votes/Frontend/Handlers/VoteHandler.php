<?php
declare(strict_types=1);

namespace Ria\Votes\Frontend\Handlers;

use Ria\Core\Components\CommandBus\Handlers\EntityHandler;
use Ria\Votes\Core\Models\Log;
use Ria\Votes\Core\Models\Option;
use Ria\Votes\Core\Models\Vote;
use Ria\Votes\Frontend\Commands\AddVoteCommand;

/**
 * Class VoteHandler
 * @package Ria\Votes\Frontend\Handlers
 */
class VoteHandler extends EntityHandler
{

    /**
     * @param AddVoteCommand $command
     */
    public function add(AddVoteCommand $command)
    {
        /** @var Vote $vote */
        $vote = $this->find(Vote::class, $command->voteId);

        /** @var Option $option */
        $option = $this->find(Option::class, $command->optionId);
        $option->incrementScore();

        $log = (new Log)
            ->setUserAgent($command->userAgent);

        $option->addLog($log);
        $vote->addLog($log);

        $this->entityManager->persist($vote);
        $this->entityManager->persist($option);
        $this->entityManager->flush();
    }

}