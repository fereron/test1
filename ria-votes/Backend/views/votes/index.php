<?php

/**
 * @var View $this
 * @var VoteSearch $searchModel
 * @var DoctrineDataProvider $dataProvider
 */

use Ria\Core\Data\DoctrineDataProvider;
use Ria\Core\Helpers\FlagIconHelper;
use Ria\Core\Helpers\StatusHelper;
use Ria\Core\Models\Status;
use Ria\Votes\Core\Forms\VoteSearch;
use Ria\Votes\Core\Models\Vote;
use yii\helpers\Html;
use yii\helpers\Url;
use Ria\Core\Widgets\Gridview\GridViewRemark;
use Ria\Core\Widgets\ActionColumn\ActionColumn;
use Ria\Votes\Module;
use yii\web\View;

$this->title = Module::t('votes', 'Votes');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-body">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-2">
                    <?= Html::activeDropDownList(
                        $searchModel,
                        'language',
                        Yii::$app->params['languages'],
                        ['class' => 'outer-select form-control']
                    ) ?>
                </div>
                <div class="col-lg-10">
                    <div class="text-right mb-2">
                        <?php foreach (Yii::$app->params['languages'] as $code => $label): ?>
                            <a href="<?= Url::to(['create', 'lang' => $code]) ?>" class="btn btn-info">
                                <i class="flag-icon flag-icon-<?= FlagIconHelper::getIcon($code) ?>"></i>
                                +
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <?= GridviewRemark::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.outer-select',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['class' => 'text-center', 'style' => 'width:100px'],
                    'headerOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'title',
                    'label' => Module::t('votes', 'Title')
                ],
                [
                    'attribute' => 'status',
                    'filter' => Status::list(),
                    'format' => 'raw',
                    'value' => function (Vote $vote) {
                        return StatusHelper::statusLabel($vote->getStatus());
                    }
                ],
                [
                    'class' => ActionColumn::class,
                    'visibleButtons' => ['view' => false]
                ],
            ],
        ]); ?>
    </div>
</div>
