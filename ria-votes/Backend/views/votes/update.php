<?php

/* @var $this View */
/* @var $model VoteForm */
/* @var $voteOptionForms VoteOptionForm[] */

use Ria\Core\Helpers\FlagIconHelper;
use Ria\Votes\Core\Forms\VoteForm;
use Ria\Votes\Core\Forms\VoteOptionForm;
use Ria\Votes\Module;
use yii\helpers\Html;
use yii\web\View;

$this->title =
    Html::tag('span', '', ['class' => 'font-size-20 mr-5 flag-icon flag-icon-' . FlagIconHelper::getIcon($model->language)])
    . Module::t('votes', 'Update vote');

$this->params['breadcrumbs'][] = ['label' => Module::t('votes', 'Votes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = strip_tags($this->title);

echo $this->render('_form', [
    'model' => $model,
    'voteOptionForms' => $voteOptionForms,
]);