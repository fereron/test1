<?php
declare(strict_types=1);

use kartik\date\DatePicker;
use kartik\switchinput\SwitchInput;
use Ria\Core\Widgets\DynamicForm\DynamicFormWidget;
use Ria\Votes\Backend\Assets\SortableAsset;
use Ria\Votes\Backend\Assets\VotesAsset;
use Ria\Votes\Core\Forms\VoteForm;
use Ria\Votes\Core\Forms\VoteOptionForm;
use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use Ria\Votes\Module;

/* @var $this View */
/* @var $model VoteForm */
/* @var $voteOptionForms VoteOptionForm[] */

SortableAsset::register($this);
VotesAsset::register($this);
?>

<?= Html::errorSummary($model, ['class' => 'alert alert-danger', 'encode' => false]) ?>

<?php $form = ActiveForm::begin(['id' => 'dynamic-form', 'options' => ['enctype' => 'multipart/form-data', 'autocomplete' => 'off']]) ?>
<div class="row">
    <div class="col-lg-8">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'title')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 100, // the maximum times, an element can be cloned (default 999)
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item-option', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $voteOptionForms[0],
            'formId' => 'dynamic-form',
            'formFields' => ['title', 'sort'],
        ]); ?>

        <div class="panel">
            <div class="panel-body">
                <div class="panel-heading">
                    <h2 class="panel-title p-0"><?= Module::t('votes', 'Options') ?></h2>
                </div>

                <div class="panel-heading bg-white">
                    <div class="actions text-right">
                        <button type="button" class="add-item-option btn btn-success mt-10"><i class="fa fa-plus"></i> <?= Module::t('votes', 'Add option') ?></button>
                    </div>
                </div>

                <div class="container-items sortable-with-handle">
                    <?php foreach ($voteOptionForms as $index => $voteOptionForm): ?>
                        <div class="item panel"><!-- widgetBody -->
                            <div class="panel-body p-10">
                                <?php if (!$voteOptionForm->isNewRecord): ?>
                                    <?= Html::activeHiddenInput($voteOptionForm, "[{$index}]id", ['class' => 'form-control id']); ?>
                                <?php endif; ?>

                                <?= Html::activeHiddenInput($voteOptionForm, "[{$index}]sort", ['class' => 'form-control sort']); ?>

                                <div class="row">
                                    <div class="col-md-1 d-flex align-items-center justify-content-center">
                                        <i class="icon wb-list sortable-handle font-size-16" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-9">
                                        <?= $form->field($voteOptionForm, "[{$index}]title")->textInput(['maxlength' => true, 'class' => 'form-control title']) ?>
                                    </div>
                                    <div class="col-md-2 d-flex align-items-center">
                                        <button type="button" class="pull-right remove-item btn btn-danger"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>

    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= Html::activeLabel($model, 'startDate') ?>
                            <?= $form->field($model, 'startDate', ['template' => '
                            <div class="form-group has-default bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                    </span>
                                    {input}
                                </div>
                            </div>'
                            ])->widget(DatePicker::class, [
                                'type'          => DatePicker::TYPE_INPUT,
                                'pluginOptions' => [
                                    'orientation'    => 'bottom right',
                                    'todayHighlight' => true,
                                    'todayBtn'       => true,
                                    'format'         => 'yyyy-mm-dd',
                                    'autoclose'      => true,
                                ],
                            ]) ?>
                        </div>

                        <div class="col-lg-6">
                            <?= Html::activeLabel($model, 'endDate') ?>
                            <?= $form->field($model, 'endDate', ['template' => '
                            <div class="form-group has-default bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                    </span>
                                    {input}
                                </div>
                            </div>'
                            ])->widget(DatePicker::class, [
                                'type'          => DatePicker::TYPE_INPUT,
                                'pluginOptions' => [
                                    'orientation'    => 'bottom right',
                                    'todayHighlight' => true,
                                    'todayBtn'       => true,
                                    'format'         => 'yyyy-mm-dd',
                                    'autoclose'      => true,
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->widget(SwitchInput::class) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'showRecaptcha')->widget(SwitchInput::class) ?>
                        </div>
                    </div>

                    <?= Html::submitButton(Module::t('votes', 'Save'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php ActiveForm::end() ?>
