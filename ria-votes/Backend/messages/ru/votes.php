<?php

return [
    'Votes'       => 'Голосования',
    'Update vote' => 'Редактировать',
    'Title'       => 'Заголовок',
    'Start Date'  => 'Дата начала',
    'End Date'    => 'Дата конца',
    'Options'     => 'Ответы',
    'Add option'  => 'Новый ответ',
    'Status'      => 'Статус',
    'Save'        => 'Сохранить',
    'Create new'  => 'Создать новый',

];
