<?php

return [
    'Votes'       => 'Sorğular',
    'Update vote' => 'Redaktə et',
    'Title'       => 'Başlıq',
    'Create new'  => 'Yenisini yarat',
    'Start Date'  => 'Başlama tarixi',
    'End Date'    => 'Bitmə tarixi',
    'Options'     => 'Cavablar',
    'Add option'  => 'Yeni cavab',
    'Status'      => 'Status',
    'Save'        => 'Yadda Saxla',

];

