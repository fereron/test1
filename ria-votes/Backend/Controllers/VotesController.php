<?php
declare(strict_types=1);

namespace Ria\Votes\Backend\Controllers;

use Doctrine\ORM\EntityRepository;
use Ria\Core\Forms\CompositeForm;
use Ria\Votes\Core\Commands\CreateVoteCommand;
use Ria\Votes\Core\Commands\DeleteVoteCommand;
use Ria\Votes\Core\Commands\UpdateVoteCommand;
use Ria\Votes\Core\Forms\VoteForm;
use Ria\Votes\Core\Forms\VoteOptionForm;
use Ria\Votes\Core\Forms\VoteSearch;
use Ria\Votes\Core\Models\Vote;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use Ria\Core\Web\BackendController;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class VotesController
 * @package Ria\Votes\Backend\Controllers
 */
class VotesController extends BackendController
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'       => true,
                        'permissions' => ['manageVotes']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var EntityRepository $repo */
        $repo         = $this->entityManager->getRepository(Vote::class);
        $searchModel  = new VoteSearch($repo);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * @param string $lang
     * @return string|Response
     */
    public function actionCreate(string $lang)
    {
        $form = VoteForm::create(null, ['language' => $lang]);
        $voteOptionForms = [new VoteOptionForm];

        if ($form->load(Yii::$app->request->post())) {
            $voteOptions = CompositeForm::createMultiple(VoteOptionForm::class);
            CompositeForm::loadMultiple($voteOptions, Yii::$app->request->post());

            if ($form->validate() && Model::validateMultiple($voteOptions)) {
                $this->bus->handle(new CreateVoteCommand($form, $voteOptions));

                return $this->redirect(['index', 'VoteSearch[language]' => $form->language]);
            }
        }

        return $this->render('create', [
            'model' => $form,
            'voteOptionForms' => $voteOptionForms
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $vote = $this->findModel(Vote::class, $id);
        $form = VoteForm::create($vote);
        $voteOptionForms = $form->options;

        if ($form->load(Yii::$app->request->post())) {
            $voteOptions = CompositeForm::createMultiple(VoteOptionForm::class);
            CompositeForm::loadMultiple($voteOptions, Yii::$app->request->post());

            if ($form->validate() && Model::validateMultiple($voteOptions)) {
                $this->bus->handle(new UpdateVoteCommand($form, $voteOptions));

                return $this->redirect(['index', 'VoteSearch[language]' => $form->language]);
            }
        }

        return $this->render('update', [
            'model' => $form,
            'voteOptionForms' => $voteOptionForms,
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete(int $id)
    {
        $this->bus->handle(new DeleteVoteCommand($id));

        return $this->redirect(['index']);
    }

}