let el = $('.sortable-with-handle').get(0);

if (typeof el !== "undefined") {
    window.Sortable.create(el, {
        onSort: function (/**Event*/ evt) {
            $('.sortable-with-handle .row').each(function (index, item) {
                $(item).parent().find('.sort').val(index);
            });
        }
    });
}

$(".dynamicform_wrapper").on("afterInsert", function() {
    $('.sortable-with-handle .row').each(function (index, item) {
        $(item).parent().find('.sort').val(index);
    });
});