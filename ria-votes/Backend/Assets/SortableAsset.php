<?php

namespace Ria\Votes\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class SortableAsset
 * @package Ria\Votes\Backend\Assets
 */
class SortableAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot/remark';
    /**
     * @var string
     */
    public $baseUrl = '@web/remark';
    /**
     * @var array
     */
    public $css = [
        'global/vendor/sortable/sortable.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'global/vendor/sortable/Sortable.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];
}