<?php

namespace Ria\Votes\Backend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class VotesAsset
 * @package Ria\News\Backend\Assets
 */
class VotesAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@module/public';

    /**
     * @var array
     */
    public $js = [
        'js/votes.js',
    ];

    /**
     * @var array
     */
    public $css = [];

    /**
     * @var array
     */
    public $depends = [
        JqueryAsset::class
    ];

}