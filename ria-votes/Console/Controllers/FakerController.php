<?php
declare(strict_types=1);

namespace Ria\Votes\Console\Controllers;

use Yii;
use Faker\Factory;
use yii\base\Exception;
use yii\console\Controller;

/**
 * Class FakerController
 * @package app\commands
 */
class FakerController extends Controller
{
    /**
     * @param int $count
     * @param string $type
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function create($count, $type) {
        $faker   = Factory::create();
        $command = Yii::$app->db->createCommand();

        for ($i = 1; $i <= $count; $i++) {
            $command->insert('persons', [
                'type'             => $type,
                'status'           => 1,
            ])->execute();

            $personId = Yii::$app->db->getLastInsertID();

            foreach (['en', 'ru'] as $language) {
                $faker->languageCode = $language;
                $command->insert('persons_lang', [
                    'person_id'     => $personId,
                    'language'    => $language,
                    'first_name'  => $firstName = $faker->firstName,
                    'last_name'   => $lastName = $faker->lastName,
                    'position'    => $faker->jobTitle,
                    'text' => $faker->realText(100),
                    'meta' => '{"title": "", "description": "", "keywords": ""}',
                ])->execute();
            }
        }
    }

    /**
     * @param int $count
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function actionPerson(int $count = 5)
    {
        $this->create($count, 'person');
    }

    /**
     * @param int $count
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function actionExpert(int $count = 5)
    {
        $this->create($count, 'expert');
    }
}