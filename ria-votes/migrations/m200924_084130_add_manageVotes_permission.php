<?php

use Doctrine\Common\Collections\ArrayCollection;
use Ria\Users\Core\Models\Permission;
use Ria\Users\Core\Models\Role;
use yii\db\Migration;

/**
 * Class m200924_084130_add_manageVotes_permission
 */
class m200924_084130_add_manageVotes_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $permission = new \Ria\Users\Core\Models\Permission();

        $permission->setName('manageVotes');

        $em = Yii::$app->doctrine->getEntityManager();

        $em->persist($permission);


        $qb = $em->getRepository(Role::class)
            ->createQueryBuilder('r');

        /** @var Role[] $roles */
        $roles = $qb->select('r')
            ->where($qb->expr()->in('r.name', ':roles'))
            ->setParameter('roles', ['DepartmentHead', 'Administrator','SuperAdministrator' ])
            ->getQuery()
            ->execute();

        foreach ($roles as $role) {
            $role->addPermission($permission);
            $em->persist($role);
        }

        $em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200924_084130_add_manageVotes_permission cannot be reverted.\n";

        return false;
    }

}
