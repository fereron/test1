<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%votes}}`.
 */
class m200910_113410_create_votes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // votes
        $this->createTable('{{%votes}}', [
            'id'             => $this->primaryKey(),
            'status'         => $this->tinyInteger(3)->notNull()->defaultValue(1),
            'show_recaptcha' => $this->boolean()->unsigned()->notNull()->defaultValue(0),
            'title'          => $this->char(255)->notNull(),
            'language'       => $this->char(2)->notNull(),
            'start_date'     => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'end_date'       => $this->timestamp()->null(),
            'created_at'     => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'     => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // vote_options
        $this->createTable('{{%vote_options}}', [
            'id'      => $this->primaryKey(),
            'vote_id' => $this->integer(10)->notNull(),
            'title'   => $this->char(255)->notNull(),
            'sort'    => $this->integer()->unsigned()->notNull(),
            'score'   => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'vote_options_fk_vote_id',
            'vote_options',
            'vote_id',
            'votes',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // vote_logs
        $this->createTable('{{%vote_logs}}', [
            'id'             => $this->primaryKey(),
            'vote_id'        => $this->integer(10)->notNull(),
            'vote_option_id' => $this->integer(10)->notNull(),
            'user_agent'     => $this->char(255)->notNull(),
            'created_at'     => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey(
            'vote_logs_fk_vote_option_id',
            'vote_logs',
            'vote_option_id',
            'vote_options',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('vote_logs_fk_vote_option_id', 'vote_logs');
        $this->dropTable('{{%vote_logs}}');

        $this->dropForeignKey('vote_options_fk_vote_id', 'vote_options');
        $this->dropTable('{{%vote_options}}');

        $this->dropTable('{{%votes}}');
    }
}
