<?php
declare(strict_types=1);

namespace Ria\Votes\Common\Bootstrap;

use League\Tactician\CommandBus;
use Ria\Core\Components\CommandBus\CommandBusFactory;
use Ria\Votes\Core\Commands\CreateVoteCommand;
use Ria\Votes\Core\Commands\DeleteVoteCommand;
use Ria\Votes\Core\Commands\UpdateVoteCommand;
use Ria\Votes\Core\Handlers\VotesHandler;
use Yii;
use yii\base\BootstrapInterface;
use yii\di\Container;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = Yii::$container;

        $handlers = [
            CreateVoteCommand::class => VotesHandler::class,
            UpdateVoteCommand::class => VotesHandler::class,
            DeleteVoteCommand::class => VotesHandler::class,
        ];

        $container->setSingleton(CommandBus::class, function (Container $container) use ($handlers) {
            return CommandBusFactory::instantiate($container, $handlers);
        });
    }
}