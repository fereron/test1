<?php

namespace Ria\Core\Query;

use ReflectionClass;

/**
 * Class ViewModel
 * @package Ria\Core\Query
 */
abstract class ViewModel
{

    /**
     * ViewModel constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function toArray(): array
    {
        $data = [];
        $reflection = new ReflectionClass($this);

        foreach($reflection->getProperties() as $property) {
            $data[$property->getName()] = $property->getValue($this);
        }

        return $data;
    }

}