<?php

namespace Ria\Core\Query;

use Doctrine\ORM\Internal\Hydration\ArrayHydrator;
use Doctrine\ORM\Internal\Hydration\HydrationException;
use PDO;
use Ria\Core\Models\Meta;

/**
 * Class ViewModelHydrator
 * @package Ria\Core\Query
 */
abstract class ViewModelHydrator extends ArrayHydrator
{
    /**
     * @return string
     */
    abstract protected function getViewModelClassName(): string;

    /**
     * @return array
     * @throws HydrationException
     */
    protected function hydrateAllData()
    {
        $result = [];

        while ($row = $this->_stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrateRowData($row, $result);

            $className = $this->getViewModelClassName();
            $lastResult = end($result);

            if (isset($lastResult['content.content'])) {
                $lastResult['content'] = $lastResult['content.content'];
                unset($lastResult['content.content']);
            }

            if (array_key_exists('meta', $lastResult)) {
                $metaAsArray = json_decode($lastResult['meta'], true);
                $lastResult['meta'] = new Meta(
                    $metaAsArray['title'],
                    $metaAsArray['description'],
                    $metaAsArray['keywords']
                );
            }

            $result[array_key_last($result)] = new $className($lastResult);
        }

        return $result;
    }
}