<?php

namespace Ria\Core\Query;

use Doctrine\ORM\EntityManagerInterface;
use Yii;
use yii\base\BaseObject;
use yii\caching\TagDependency;

/**
 * Class BaseProvider
 * @package Ria\Core\Components
 *
 * @property array $options
 */
abstract class BaseProvider extends BaseObject
{
    /**
     * @var array
     */
    protected $method = [];

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @return mixed
     */
    abstract public function getData();

    /**
     * BaseProvider constructor.
     * @param EntityManagerInterface $entityManager
     * @param array $config
     */
    public function __construct(EntityManagerInterface $entityManager, $config = [])
    {
        parent::__construct($config);
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getMethod(): array
    {
        return $this->method;
    }

    /**
     * @param array $method
     */
    public function setMethod(array $method): void
    {
        $this->method = $method;
    }


    /**
     * @param array $filters
     */
    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    protected function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @param $key
     * @param array $tags
     * @param callable $function
     * @return mixed
     */
    protected function getOrSetCache($key, array $tags, callable $function)
    {
        return Yii::$app->cache->getOrSet($key, $function, null, new TagDependency(['tags' => $tags]));
    }
}