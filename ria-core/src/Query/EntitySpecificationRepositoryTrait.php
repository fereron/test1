<?php

namespace Ria\Core\Query;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Happyr\DoctrineSpecification\Filter\Filter;
use Happyr\DoctrineSpecification\Logic\AndX;
use Happyr\DoctrineSpecification\Query\QueryModifier;
use Happyr\DoctrineSpecification\Result\ResultModifier;
use ReflectionClass;
use ReflectionException;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\di\NotInstantiableException;

trait EntitySpecificationRepositoryTrait
{
    use \Happyr\DoctrineSpecification\EntitySpecificationRepositoryTrait;

    /**
     * @return string
     */
    abstract function getSpecsNamespace(): string;

    /**
     * @return string|null
     */
    public function getHydrationMode()
    {
        return null;
    }

    /**
     * Get results when you match with a Specification.
     *
     * @param Filter|QueryModifier|array $specification
     * @param string|int|null $hydrationMode Processing mode to be used during the hydration process.
     * @param ResultModifier|null $modifier
     *
     * @return mixed[]
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ReflectionException
     */
    public function match($specification, $hydrationMode = null, ResultModifier $modifier = null)
    {
        if (is_array($specification)) {
            $specification = $this->getSpecifications($specification);
        }

        $query = $this->getQuery($specification, $modifier);

        return $query->getResult($hydrationMode ?? $this->getHydrationMode());
    }

    /**
     * Get single result when you match with a Specification.
     *
     * @param Filter|QueryModifier $specification
     * @param string|int|null $hydrationMode Processing mode to be used during the hydration process.
     * @param ResultModifier|null $modifier
     *
     * @return mixed
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ReflectionException
     * @throw Exception\NonUniqueException  If more than one result is found
     * @throw Exception\NoResultException   If no results found
     *
     */
    public function matchSingleResult($specification, $hydrationMode = null, ResultModifier $modifier = null)
    {
        if (is_array($specification)) {
            $specification = $this->getSpecifications($specification);
        }

        $query = $this->getQuery($specification, $modifier);

        try {
            return $query->getSingleResult($hydrationMode ?? $this->getHydrationMode());
        } catch (NonUniqueResultException $e) {
            throw new \Happyr\DoctrineSpecification\Exception\NonUniqueResultException($e->getMessage(), $e->getCode(), $e);
        } catch (NoResultException $e) {
            throw new \Happyr\DoctrineSpecification\Exception\NoResultException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Get single result or null when you match with a Specification.
     *
     * @param Filter|QueryModifier $specification
     * @param string|int|null $hydrationMode Processing mode to be used during the hydration process.
     * @param ResultModifier|null $modifier
     *
     * @return mixed|null
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throw Exception\NonUniqueException  If more than one result is found
     *
     */
    public function matchOneOrNullResult($specification, $hydrationMode = null, ResultModifier $modifier = null)
    {
        try {
            if (is_array($specification)) {
                $specification = $this->getSpecifications($specification);
            }

            return $this->matchSingleResult($specification, $hydrationMode ?? $this->getHydrationMode(), $modifier);
        } catch (\Happyr\DoctrineSpecification\Exception\NoResultException $e) {
            return null;
        }
    }

    /**
     * @param array $specifications
     * @return object
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function getSpecifications(array $specifications)
    {
        $specs = [];

        foreach ($specifications as $index => $value) {
            $specs[] = $this->getFilterClass($index, $value);
        }

        return (new ReflectionClass(AndX::class))->newInstanceArgs($specs);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $filters
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function applySpecifications(QueryBuilder $queryBuilder, array $filters): void
    {
        $specs = [];

        $this->setAlias($queryBuilder->getRootAliases()[0]);

        foreach ($filters as $index => $value) {
            $specs[] = $this->getFilterClass($index, $value);
        }

        $this->applySpecification($queryBuilder, (new ReflectionClass(AndX::class))->newInstanceArgs($specs));
    }

    /**
     * @param $index
     * @param $value
     * @return mixed|object
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    private function getFilterClass($index, $value)
    {
        if (is_string($index)) {
            $class_name = $this->getSpecsNamespace() . '\\' . ucfirst($index);

            return new $class_name($value, $this->getAlias());
        }

        $class_name = $this->getSpecsNamespace() . '\\' . ucfirst($value);

        $dependencies = [];
        $constructor = (new ReflectionClass($class_name))->getConstructor();

        if ($constructor !== null) {
            foreach ($constructor->getParameters() as $param) {
                if (version_compare(PHP_VERSION, '5.6.0', '>=') && $param->isVariadic()) {
                    break;
                } elseif ($param->isDefaultValueAvailable()) {
                    $dependencies[] = $param->getDefaultValue();
                } else {
                    $c = $param->getClass();
                    $dependencies[] = Instance::of($c === null ? null : $c->getName());
                }
            }
        }

        $dependencies[array_key_last($dependencies)] = $this->getAlias();

        return Yii::$container->get($class_name, $dependencies);
    }
}