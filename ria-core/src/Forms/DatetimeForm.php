<?php
declare(strict_types=1);

namespace Ria\Core\Forms;

use yii\base\Model;

/**
 * Class DatetimeForm
 * @package Ria\Core\Forms
 */
class DatetimeForm extends Model
{
    /**
     * @var string
     */
    public $date;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['date', 'required'],
            ['date', 'datetime', 'format' => 'php: Y-m-d H:i:s', 'max' => time()]
        ];
    }
}