<?php
declare(strict_types=1);

namespace Ria\Core\Forms;

use Ria\Core\Models\Meta;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * Class MetaForm
 * @package Ria\Core\Forms
 */
class MetaForm extends Model
{
    /**
     * @var string
     */
    public $language;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $description;
    /**
     * @var string
     */
    public $keywords;

    /**
     * @param string $language
     * @param Meta|null $meta
     * @return MetaForm
     */
    public static function create(string $language, ?Meta $meta = null): MetaForm
    {
        $props = ['language' => $language];

        if ($meta) {
            $props['title'] = $meta->getTitle();
            $props['description'] = $meta->getDescription();
            $props['keywords'] = $meta->getKeywords();
        }
        return new MetaForm($props);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . '_' . $this->language;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 200],
            [['description', 'keywords'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title'       => Yii::t('meta', 'Meta title'),
            'keywords'    => Yii::t('meta', 'Meta keywords'),
            'description' => Yii::t('meta', 'Meta description'),
        ];
    }
}