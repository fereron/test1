<?php

namespace Ria\Core\Data;

use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\QueryBuilder;
use yii\data\BaseDataProvider;

class DoctrineDataProvider extends BaseDataProvider
{
    /**
     * @var string|callable the column that is used as the key of the data models.
     * This can be either a column name, or a callable that returns the key value of a given data model.
     *
     * If this is not set, the following rules will be used to determine the keys of the data models:
     *
     * - If [[query]] is an [[\yii\db\ActiveQuery]] instance, the primary keys of [[\yii\db\ActiveQuery::modelClass]] will be used.
     * - Otherwise, the keys of the [[models]] array will be used.
     *
     * @see getKeys()
     */
    public $key;

    /**
     * @var QueryBuilder the query that is used to fetch data models and [[totalCount]]
     * if it is not explicitly set.
     */
    public $query;

    public $modelClass;

    /**
     * @return array|int|mixed|string
     * @throws MappingException
     * @throws QueryException
     */
    protected function prepareModels()
    {
        $query = clone $this->query;

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            if ($pagination->totalCount === 0) {
                return [];
            }

            $query->setMaxResults($pagination->getLimit())->setFirstResult($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy(array_key_first($sort->getOrders()), $sort->getOrders()[array_key_first($sort->getOrders())]);
        }

        $query->indexBy(
            $query->getRootAliases()[0],
            $query->getRootAliases()[0] . '.' . $this->query->getEntityManager()
                ->getClassMetadata($this->modelClass)
                ->getSingleIdentifierFieldName()
        );

        return $query->getQuery()->execute();
    }

    /**
     * @param array $models
     * @return array
     */
    protected function prepareKeys($models)
    {
        return array_keys($models);
    }

    /**
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    protected function prepareTotalCount()
    {
        $query = clone $this->query;

        return (int) $query
            ->select(sprintf("count(%s.id)", $query->getRootAliases()[0]))
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     */
    public function setSort($value)
    {
        parent::setSort($value);
        if (($sort = $this->getSort()) !== false) {
            if (empty($sort->attributes)) {
                $metadata = $this->query->getEntityManager()->getClassMetadata($this->modelClass);
                $attrs = $metadata->fieldNames;
            } else {
                $attrs = array_keys($sort->attributes);
            }

            foreach ($attrs as $fieldName) {
                $mainModelField = $this->query->getEntityManager()
                    ->getClassMetadata($this->modelClass)->hasField($fieldName);
                $field = $mainModelField ? $this->query->getRootAliases()[0] . '.' . $fieldName : $fieldName;

                $sort->attributes[$fieldName] = [
                    'asc'   => [$field => 'ASC'],
                    'desc'  => [$field => 'DESC'],
                    'label' => $fieldName,
                ];
            }
        }
    }

}