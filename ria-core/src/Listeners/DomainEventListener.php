<?php

namespace Ria\Core\Listeners;

use Ria\Core\Components\Dispatcher\EventDispatcher;
use Ria\Core\Models\Entity;
use Ria\Core\Models\TagCache;
use Yii;
use yii\base\InvalidConfigException;
use yii\caching\TagDependency;
use yii\di\NotInstantiableException;

/**
 * Class DomainEventListener
 * @package Ria\Core\Listeners
 */
class DomainEventListener
{
    private $entities = array();

    /**
     * @param $event
     */
    public function postPersist($event)
    {
        $this->keepAggregateRoots($event);
    }

    /**
     * @param $event
     */
    public function postUpdate($event)
    {
        $this->keepAggregateRoots($event);
    }

    /**
     * @param $event
     */
    public function postRemove($event)
    {
        $this->keepAggregateRoots($event);
    }

    /**
     * @param $event
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function postFlush($event)
    {
        /** @var EventDispatcher $dispatcher */
        $dispatcher = Yii::$container->get(EventDispatcher::class);

        foreach ($this->entities as $entity) {
            foreach ($entity->popEvents() as $event) {
                $dispatcher->dispatch($event);
            }
        }
        $this->entities = [];
    }

    /**
     * @param $event
     */
    private function keepAggregateRoots($event)
    {
        $entity = $event->getEntity();

        if (!($entity instanceof Entity)) {
            return;
        }

        if ($entity instanceof TagCache) {
            TagDependency::invalidate(Yii::$app->cache, $entity->getCacheTags());
        }

        $this->entities[] = $entity;
    }
}