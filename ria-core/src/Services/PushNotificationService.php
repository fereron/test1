<?php
declare(strict_types=1);

namespace Ria\Core\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Yii;
use yii\helpers\Json;

/**
 * Class PushNotificationService
 * @package Ria\Core\Services
 */
class PushNotificationService
{
    /**
     * @var string
     */
    private $restApiUrl = 'https://onesignal.com/api/v1/notifications';

    /**
     * @var array
     */
    private $post;

    /**
     * @var Client
     */
    protected $client;

    /**
     * PushNotificationService constructor.
     * @param $post
     */
    public function __construct($post)
    {
        $this->post   = $post;
        $this->client = new Client();
    }

    /**
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function notify(): bool
    {
        $response = Json::decode($this->sendMessage());

        return !empty($response['id']);
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendMessage()
    {
        Yii::$app->language = $this->post['language'];

        // create absolute url to post
        $postUrl = Yii::$app->frontendUrlManager->createAbsoluteUrl('/') . $this->post['category_slug'] . "/" . $this->post['slug'] . '/';

        // modify post content, remove html tags and set charset
//        $postContent = strip_tags(trim(html_entity_decode((string)$this->post['description'], ENT_QUOTES, 'UTF-8'), "\xc2\xa0"));
//        if (!empty($postContent)) {
//            $postContent = mb_substr($postContent, 0, 110) . '...';
//        }

        // modify post title, remove html tags and set charset
        $postTitle = strip_tags(trim(html_entity_decode((string)$this->post['title'], ENT_QUOTES, 'UTF-8'), "\xc2\xa0"));
        $postTitle = mb_substr($postTitle, 0, 100) . '...';

        // data for create notification
        $fields = [
            'app_id'            => Yii::$app->params['one_signal']['appId'],
            'included_segments' => ['All'],
            'chrome_web_image'  => Yii::$app->photo->getThumb(610, $this->post['image']),
            'contents'          => ["en" => $postTitle],
            'headings'          => ["en" => $postTitle],
            'url'               => $postUrl,
        ];

        try {
            $response = $this->client->request('POST', $this->restApiUrl, [
                'json'    => $fields,
                'headers' => [
                    'Content-Type'  => 'application/json; charset=utf-8',
                    'Authorization' => 'Basic ' . Yii::$app->params['one_signal']['restApiKey']
                ],
            ]);
        } catch (RequestException $exception) {
            return Json::encode([]);
        }

        return $response->getBody()->getContents();
    }
}