<?php

namespace Ria\Core\Base;

use Closure;
use ReflectionClass;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;

class Module extends \yii\base\Module
{
    /**
     * @var array
     */
    public $bootstrap = [];

    /**
     * @var \yii\base\Module
     */
    public static $app;

    public function init()
    {
        $class = (new ReflectionClass(static::class));

        $this->controllerNamespace = $class->getNamespaceName() . '\\' . ucfirst(YII_MODE) . '\Controllers';

        $this->setAliases(['@module' => dirname($class->getFileName()) . DIRECTORY_SEPARATOR . ucfirst(YII_MODE)]);
        $this->setBasePath('@module');

        self::$app = $this;
    }

    /**
     * @throws InvalidConfigException
     */
    protected function bootstrap()
    {
        foreach ($this->bootstrap as $mixed) {
            $component = null;
            if ($mixed instanceof Closure) {
                Yii::debug('Bootstrap with Closure', __METHOD__);
                if (!$component = call_user_func($mixed, $this)) {
                    continue;
                }
            } elseif (is_string($mixed)) {
                if ($this->has($mixed)) {
                    $component = $this->get($mixed);
                } elseif ($this->hasModule($mixed)) {
                    $component = $this->getModule($mixed);
                } elseif (strpos($mixed, '\\') === false) {
                    throw new InvalidConfigException("Unknown bootstrapping component ID: $mixed");
                }
            }

            if (!isset($component)) {
                $component = Yii::createObject($mixed);
            }

            if ($component instanceof BootstrapInterface) {
                Yii::debug('Bootstrap with ' . get_class($component) . '::bootstrap()', __METHOD__);
                $component->bootstrap(Yii::$app);
            } else {
                Yii::debug('Bootstrap with ' . get_class($component), __METHOD__);
            }
        }
    }

    /**
     * @param $category
     * @param $message
     * @param array $params
     * @param null $language
     * @return string
     * @throws InvalidConfigException
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        if (static::$app !== null) {
            return static::$app->get('i18n')->translate($category, $message, $params, $language ?: Yii::$app->language);
        }

        $placeholders = [];
        foreach ((array) $params as $name => $value) {
            $placeholders['{' . $name . '}'] = $value;
        }

        return ($placeholders === []) ? $message : strtr($message, $placeholders);
    }
}