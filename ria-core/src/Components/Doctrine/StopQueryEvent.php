<?php

namespace Ria\Core\Components\Doctrine;

use yii\base\Event;

class StopQueryEvent extends Event
{
    /**
     * @var DebugStack
     */
    private $logger;

    public function __construct(DebugStack $logger, $config = [])
    {
        parent::__construct($config);
        $this->logger = $logger;
    }

    /**
     * @return DebugStack
     */
    public function getLogger(): DebugStack
    {
        return $this->logger;
    }
}