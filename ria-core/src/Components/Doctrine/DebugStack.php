<?php

namespace Ria\Core\Components\Doctrine;

use Doctrine\DBAL\Logging\SQLLogger;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;
use yii\base\Event;

class DebugStack implements SQLLogger
{
    /**
     * Executed SQL queries.
     *
     * @var mixed[][]
     */
    public $queries = [];

    /**
     * If Debug Stack is enabled (log queries) or not.
     *
     * @var bool
     */
    public $enabled = true;

    /** @var float|null */
    public $start = null;

    /** @var int */
    public $currentQuery = 0;

    /**
     * @var AbstractPlatform
     */
    private $platform;

    /**
     * Initialize log listener with database
     * platform, which is needed for parameter
     * conversion
     *
     * @param AbstractPlatform $platform
     */
    public function __construct(AbstractPlatform $platform)
    {
        $this->platform = $platform;
    }


    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null)
    {
        if (! $this->enabled) {
            return;
        }

        $this->start                          = microtime(true);
        $this->queries[++$this->currentQuery] = ['sql' => $this->generateSql($sql, $params, $types), 'executionMS' => 0];

//        if ($this->enabled) {
//            $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
//            $this->queries[$this->currentQuery]['stack'] = array_splice($stack, 2);
//        }
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
        if (! $this->enabled) {
            return;
        }

        $this->queries[$this->currentQuery]['executionMS'] = round(microtime(true) - $this->start, 4) * 1000;
        Event::trigger(Doctrine::class, Doctrine::EVENT_STOP_QUERY, new StopQueryEvent($this));
    }

    /**
     * Create the SQL with mapped parameters
     *
     * @param string      $sql
     * @param null|array  $params
     * @param null|array  $types
     *
     * @return string
     */
    private function generateSql($sql, $params, $types)
    {
        if (null === $params || !count($params)) {
            return $sql;
        }

        $converted = $this->getConvertedParams($params, $types);

        if (is_int(key($params))) {
            $index = key($converted);

            $sql = preg_replace_callback('@\?@sm', function ($match) use (&$index, $converted) {
                return $converted[$index++] ?? '';
            }, $sql);
        } else {
            foreach ($converted as $key => $value) {
                $sql = str_replace(':'.$key, $value, $sql);
            }
        }

        return $sql;
    }

    /**
     * Get the converted parameter list
     *
     * @param array $params
     * @param array $types
     *
     * @return array
     */
    private function getConvertedParams($params, $types)
    {
        $result = array();
        foreach ($params as $position => $value) {
            if (isset($types[$position])) {;

                if (is_array($value)) {
                    $type = Types::ARRAY;
                } else {
                    $type = $types[$position];
                }

                if (is_string($type)) {
                    $type = Type::getType($type);
                }
                if ($type instanceof Type) {
                    $value = $type->convertToDatabaseValue($value, $this->platform);
                }
            } else {
                // Remove `$value instanceof \DateTime` check when PHP version is bumped to >=5.5
                if (is_object($value) && ($value instanceof \DateTime || $value instanceof \DateTimeInterface)) {
                    $value = $value->format($this->platform->getDateTimeFormatString());
                } elseif (!is_null($value)) {
                    $type = Type::getType(gettype($value));
                    $value = $type->convertToDatabaseValue($value, $this->platform);
                }
            }
            if (is_string($value)) {
                $value = "'{$value}'";
            } elseif (is_null($value)) {
                $value = 'NULL';
            }
            $result[$position] = $value;
        }

        return $result;
    }

}