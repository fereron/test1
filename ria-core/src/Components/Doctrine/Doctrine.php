<?php

namespace Ria\Core\Components\Doctrine;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Logging\SQLLogger;
use Doctrine\DBAL\Platforms\MySQL57Platform;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
//use Gedmo\Sortable\SortableListener;
//use Gedmo\Tool\Logging\DBAL\QueryAnalyzer;
use Ria\Core\Listeners\DomainEventListener;
use Ria\News\Core\Models\Category\EventListeners\CategoryEventListener;
use Ria\News\Core\Models\Post\EventListeners\PostCreatedListener;
use Ria\News\Core\Models\Post\EventListeners\PostUpdatedListener;
use yii\base\Component;

class Doctrine extends Component
{
    const EVENT_STOP_QUERY = 'event_stop_query';

    public $host;
    public $database;
    public $username;
    public $password;
    public $charset = 'utf8';

    /**
     * @var string
     */
    public $driver;

    /**
     * @var bool
     */
    private $isDev = YII_ENV == 'dev';

    /**
     * @var bool
     */
    public $proxiesDir = false;

    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     * @var SQLLogger
     */
    private $logger;

    /**
     * @throws ORMException
     */
    public function init()
    {
        $eventManager = new EventManager();
        $eventManager->addEventListener([Events::postPersist, Events::postUpdate, Events::postRemove, Events::postFlush], new DomainEventListener());
        $eventManager->addEventListener([Events::preUpdate], new CategoryEventListener());
        $eventManager->addEventListener([Events::postPersist], new PostCreatedListener());
        $eventManager->addEventListener([Events::postPersist, Events::preUpdate], new PostUpdatedListener());

        //$eventManager->addEventSubscriber(new SortableListener());

        // TODO fix bug with cache in Doctrine (isDevMode)
        $config = Setup::createAnnotationMetadataConfiguration(
            [],
            true,
            $this->proxiesDir,
            null,
            false
        );

        $this->logger = new DebugStack(new MySQL57Platform());

        $config->setSQLLogger($this->logger);

        $entityManager = EntityManager::create($this->getConnection(), $config, $eventManager);

        $this->setEntityManager($entityManager);
    }

    /**
     * @return array
     */
    private function getConnection(): array
    {
        return [
            'driver'   => $this->driver,
            'user'     => $this->username,
            'password' => $this->password,
            'host'     => $this->host,
            'dbname'   => $this->database,
            'charset'  => $this->charset
        ];
    }

    /**
     * @param $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @return EntityManager|null
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * @return bool
     */
    public function getIsDev(): bool
    {
        return $this->isDev;
    }


    public function getLogger()
    {
        return $this->logger;
    }
}