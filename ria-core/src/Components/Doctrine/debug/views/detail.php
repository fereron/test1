<?php

use Ria\Core\Components\Doctrine\debug\DebugSearch;
use Ria\Core\Components\Doctrine\debug\panels\DoctrinePanel;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\VarDumper;

/** @var ArrayDataProvider $dataProvider */
/** @var DoctrinePanel $panel */
/** @var DebugSearch $searchModel */

echo Html::tag('h1', $panel->getName() . ' Queries');

if (Yii::$app->log->traceLevel < 1) {
    echo "<div class=\"callout callout-warning\">Check application configuration section [log] for <b>traceLevel</b></div>";
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'db-panel-detailed-grid',
    'options' => ['class' => 'detail-grid-view table-responsive'],
    'filterModel' => $searchModel,
    'filterUrl' => $panel->getUrl(),
    'pager' => [
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' => [
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'tag' => 'a',
            'href' => 'javascript:;',
            'tabindex' => '-1',
            'class' => 'page-link'
        ]
    ],
    'columns' => [
        [
            'attribute' => 'duration',
            'value' => function ($data) {
                return sprintf('%.1f ms', $data['executionMS']);
            },
            'options' => [
                'width' => '10%',
            ],
            'headerOptions' => [
                'class' => 'sort-numerical'
            ]
        ],
        [
            'attribute' => 'sql',
            'value' => function ($data) use ($panel) {
                $query = Html::tag('div', Html::encode($data['sql']));

                if (!empty($data['params'])) {
                    $query .= Html::tag('div', VarDumper::dumpAsString($data['params']));
                }

                return $query;
            },
            'format' => 'raw',
            'options' => [
                'width' => '90%',
            ],
        ]
    ],
]);
