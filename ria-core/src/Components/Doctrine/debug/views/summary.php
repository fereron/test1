<?php

use Ria\Core\Components\Doctrine\debug\panels\DoctrinePanel;

/* @var DoctrinePanel $panel  */
/* @var int $count */
/* @var int $duration */

?>
<?php if ($count): ?>
    <div class="yii-debug-toolbar__block">
        <a href="<?= $panel->getUrl() ?>"
           title="Executed <?= $count ?> database queries which took <?= $duration ?>."
        >
            <?= $panel->getSummaryName() ?>
            <span class="yii-debug-toolbar__label yii-debug-toolbar__label_info"><?= $count ?></span>
            <span class="yii-debug-toolbar__label"><?= $duration ?> ms</span>
        </a>
    </div>
<?php endif; ?>
