<?php

namespace Ria\Core\Components\Doctrine\debug\panels;

use Ria\Core\Components\Doctrine\debug\DebugSearch;
use Ria\Core\Components\Doctrine\Doctrine;
use Ria\Core\Components\Doctrine\StopQueryEvent;
use Yii;
use yii\base\Event;
use yii\debug\Panel;

class DoctrinePanel extends Panel
{
    private $queries;

    public function init()
    {
        parent::init();
        Event::on(Doctrine::class, Doctrine::EVENT_STOP_QUERY, function (StopQueryEvent $event) {
            $this->queries = $event->getLogger()->queries;
        });
    }

    public function getName()
    {
        return 'Doctrine';
    }

    public function getSummaryName()
    {
        return 'Doctrine';
    }

    /**
     * {@inheritdoc}
     */
    public function getSummary()
    {
        if (empty($this->data)) return '';

        return $this->render('summary', [
            'panel' => $this,
            'count' => count($this->data),
            'duration' => $this->calculateDuration()
        ]);
    }

    public function getDetail()
    {
        if (empty($this->data)) return '';
        
        $search = new DebugSearch();
        $search->load(Yii::$app->request->queryParams);

        return $this->render('detail', [
            'panel' => $this,
            'dataProvider' => $search->search($this->data),
            'searchModel' => $search,
        ]);
    }

    public function save()
    {
        return $this->queries;
    }

    private function calculateDuration()
    {
        $microseconds = 0;
        foreach ($this->data as $query) {
            $microseconds += $query['executionMS'];
        }

        return $microseconds;
    }

    private function render($view, $data = [])
    {
        return Yii::$app->view->render('@dev/ria-core/src/Components/Doctrine/debug/views/' . $view, $data);
    }
}