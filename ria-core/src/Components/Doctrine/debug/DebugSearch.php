<?php

namespace Ria\Core\Components\Doctrine\debug;

use yii\data\ArrayDataProvider;
use yii\debug\components\search\Filter;
use yii\debug\models\search\Base;

class DebugSearch extends Base
{
    public $sql;

    public $duration;

    public function rules()
    {
        return [
            [['sql', 'duration'], 'safe']
        ];
    }

    public function search($models)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => false,
            'sort' => [
                'attributes' => ['duration'],
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $filter = new Filter();
        $this->addCondition($filter, 'sql', true);
        $this->addCondition($filter, 'duration', true);
        $dataProvider->allModels = $filter->filter($models);

        return $dataProvider;
    }
}