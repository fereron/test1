<?php

namespace Ria\Core\Components\Doctrine;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\ORMException;
use InvalidArgumentException;

class EntityManager extends \Doctrine\ORM\EntityManager
{
    /**
     * Factory method to create EntityManager instances.
     *
     * @param array|Connection $connection   An array with the connection parameters or an existing Connection instance.
     * @param Configuration    $config       The Configuration instance to use.
     * @param EventManager     $eventManager The EventManager instance to use.
     *
     * @return \Doctrine\ORM\EntityManager The created EntityManager.
     *
     * @throws InvalidArgumentException
     * @throws ORMException
     */
    public static function create($connection, Configuration $config, EventManager $eventManager = null)
    {
        if ( ! $config->getMetadataDriverImpl()) {
            throw ORMException::missingMappingDriverImpl();
        }

        $connection = static::createConnection($connection, $config, $eventManager);

        return new self($connection, $config, $connection->getEventManager());
    }

    public function createQuery($dql = '')
    {
        $query = new Query($this);

        if ( ! empty($dql)) {
            $query->setDQL($dql);
        }

        return $query;
    }
}