<?php

namespace Ria\Core\Components\Job;

/**
 * Class AsyncEventJob
 * @package Ria\Core\Components\Job
 */
class AsyncEventJob extends Job
{
    /**
     * @var
     */
    public $event;

    /**
     * AsyncEventJob constructor.
     * @param $event
     */
    public function __construct($event)
    {
        $this->event = $event;
    }
}