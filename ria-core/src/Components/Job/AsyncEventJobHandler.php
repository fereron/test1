<?php

namespace Ria\Core\Components\Job;

use Ria\Core\Components\Dispatcher\EventDispatcher;

/**
 * Class AsyncEventJobHandler
 * @package Ria\Core\Components\Job
 */
class AsyncEventJobHandler
{

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * AsyncEventJobHandler constructor.
     * @param EventDispatcher $dispatcher
     */
    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param AsyncEventJob $job
     */
    public function handle(AsyncEventJob $job)
    {
        $this->dispatcher->dispatch($job->event);
    }
}