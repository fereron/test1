<?php

namespace Ria\Core\Components\Job;

use yii\queue\JobInterface;
use yii\queue\Queue;

/**
 * Class Job
 * @package Ria\Core\Components\Job
 */
abstract class Job implements JobInterface
{

    /**
     * @param Queue $queue
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function execute($queue)
    {
        $listener = $this->resolveHandler();
        $listener($this, $queue);
    }


    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function resolveHandler()
    {
        return [\Yii::createObject(static::class . 'Handler'), 'handle'];
    }
}