<?php
declare(strict_types=1);

namespace Ria\Core\Components\FileSystem;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Exception;
use LogicException;

/**
 * Class LocalAdapter
 * @package Ria\Core\Components\FileSystem
 */
class LocalAdapter extends Local
{

    /**
     * Constructor.
     *
     * @param string $root
     * @param int $writeFlags
     * @param int $linkHandling
     * @param array $permissions
     *
     * @throws LogicException
     */
    public function __construct($root, $writeFlags = LOCK_EX, $linkHandling = self::DISALLOW_LINKS, array $permissions = [])
    {
        $root                = is_link($root) ? realpath($root) : $root;
        $this->permissionMap = array_replace_recursive(static::$permissions, $permissions);
//        $this->ensureDirectory($root);

//        if (!is_dir($root) || !is_readable($root)) {
//            throw new LogicException('The root path ' . $root . ' is not readable.');
//        }

        $this->setPathPrefix($root);
        $this->writeFlags = $writeFlags;
    }

    /**
     * Ensure the root directory exists.
     *
     * @param string $root root directory path
     *
     * @return void
     *
     * @throws Exception in case the root directory can not be created
     */
    protected function ensureDirectory($root)
    {
        if (!is_dir($root)) {
//            $umask = umask(0);

//            if (!@mkdir($root, $this->permissionMap['dir']['public'], true)) {
//                $mkdirError = error_get_last();
//            }

//            umask($umask);
            clearstatcache(false, $root);

//            if (!is_dir($root)) {
//                $errorMessage = isset($mkdirError['message']) ? $mkdirError['message'] : '';
//                throw new Exception(sprintf('Impossible to create the root directory "%s". %s', $root, $errorMessage));
//            }
        }
    }

}