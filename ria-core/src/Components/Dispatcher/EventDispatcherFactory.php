<?php

namespace Ria\Core\Components\Dispatcher;

use Ria\News\Core\Models\Post\EventListeners\AnalyzerQuotesListener;

use Ria\News\Core\Models\Post\EventListeners\CreateShortLinkListener;
use Ria\News\Core\Models\Post\EventListeners\LogPostListener;
use Ria\News\Core\Models\Post\EventListeners\LongTitleListener;
use Ria\News\Core\Models\Post\EventListeners\PostArchivedListener;
use Ria\News\Core\Models\Post\EventListeners\PostYandexTextListener;
use Ria\News\Core\Models\Post\EventListeners\PushNotificationListener;
use Ria\News\Core\Models\Post\EventListeners\PostDeletedListener;
use Ria\News\Core\Models\Post\EventListeners\ReindexPostListener;
use Ria\News\Core\Models\Post\Events\PostArchived;
use Ria\News\Core\Models\Post\Events\PostCreated;
use Ria\News\Core\Models\Post\Events\PostDeleted;
use Ria\News\Core\Models\Post\Events\PostSaved;
use Ria\News\Core\Models\Post\Events\PostUpdated;

/**
 * Class EventDispatcherFactory
 * @package Ria\Core\Components\Dispatcher
 */
class EventDispatcherFactory
{

    /**
     * @param $container
     * @return SimpleEventDispatcher
     */
    public static function instantiate($container)
    {
        return new SimpleEventDispatcher($container, [
            PostCreated::class => [
                AnalyzerQuotesListener::class,
                LogPostListener::class,
                LongTitleListener::class,
            ],
            PostUpdated::class => [
                LogPostListener::class,
                LongTitleListener::class,
            ],
            PostSaved::class   => [
                CreateShortLinkListener::class,
                PushNotificationListener::class,
                ReindexPostListener::class,
            ],
            PostDeleted::class => [
                PostDeletedListener::class,
                LogPostListener::class,
            ],
            PostArchived::class => [
                PostArchivedListener::class
            ]
        ]);
    }

}