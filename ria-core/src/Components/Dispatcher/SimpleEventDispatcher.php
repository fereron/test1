<?php

namespace Ria\Core\Components\Dispatcher;

use yii\di\Container;

/**
 * Class SimpleEventDispatcher
 * @package Ria\Core\Components\Dispatcher
 */
class SimpleEventDispatcher implements EventDispatcher
{
    /**
     * @var array
     */
    private $listeners;
    /**
     * @var Container
     */
    private $container;

    /**
     * SimpleEventDispatcher constructor.
     * @param Container $container
     * @param array $listeners
     */
    public function __construct(Container $container, array $listeners)
    {
        $this->container = $container;
        $this->listeners = $listeners;
    }

    /**
     * @param array $events
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function dispatchAll(array $events)
    {
        foreach ($events as $event) {
            $this->dispatch($event);
        }
    }

    /**
     * @param $event
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function dispatch($event)
    {
        $eventName = get_class($event);
        if (array_key_exists($eventName, $this->listeners)) {
            foreach ($this->listeners[$eventName] as $listenerClass) {
                $listener = $this->resolveListener($listenerClass);
                $listener($event);
            }
        }
    }

    /**
     * @param $listenerClass
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function resolveListener($listenerClass)
    {
        return [$this->container->get($listenerClass), 'handle'];
    }
}