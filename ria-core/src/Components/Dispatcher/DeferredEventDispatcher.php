<?php

namespace Ria\Core\Components\Dispatcher;

/**
 * Class DeferredEventDispatcher
 * @package Ria\Core\Components\Dispatcher
 */
class DeferredEventDispatcher implements EventDispatcher
{
    /**
     * @var bool
     */
    private $deffer = false;
    /**
     * @var array
     */
    private $queue = [];
    /**
     * @var EventDispatcher
     */
    private $next;

    /**
     * DeferredEventDispatcher constructor.
     * @param EventDispatcher $next
     */
    public function __construct(EventDispatcher $next)
    {
        $this->next = $next;
    }

    /**
     * @param array $events
     */
    public function dispatchAll(array $events)
    {
        foreach ($events as $event) {
            $this->dispatch($event);
        }
    }

    /**
     * @param $event
     */
    public function dispatch($event)
    {
        if ($this->deffer) {
            $this->queue[] = $event;
        } else {
            $this->next->dispatch($event);
        }
    }

    /**
     * @inheritdoc
     */
    public function deffer()
    {
        $this->deffer = true;
    }

    /**
     * @inheritdoc
     */
    public function clean()
    {
        $this->deffer = false;
        $this->queue = [];
    }

    /**
     * @inheritdoc
     */
    public function release()
    {
        foreach ($this->queue as $event) {
            $this->next->dispatch($event);
        }
        $this->clean();
    }
}