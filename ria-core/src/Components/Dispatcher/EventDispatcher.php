<?php

namespace Ria\Core\Components\Dispatcher;

/**
 * Interface EventDispatcher
 * @package Ria\Core\Components\Dispatcher
 */
interface EventDispatcher
{
    /**
     * @param $event
     * @return mixed
     */
    public function dispatch($event);

    /**
     * @param array $events
     * @return mixed
     */
    public function dispatchAll(array $events);
}