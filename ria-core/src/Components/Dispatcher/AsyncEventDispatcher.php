<?php

namespace Ria\Core\Components\Dispatcher;

use Ria\Core\Components\Job\AsyncEventJob;
use yii\queue\Queue;

/**
 * Class AsyncEventDispatcher
 * @package Ria\Core\Components\Dispatcher
 * @property Queue $queue
 */
class AsyncEventDispatcher implements EventDispatcher
{
    /**
     * @var
     */
    private $queue;

    /**
     * AsyncEventDispatcher constructor.
     * @param $queue
     */
    public function __construct($queue)
    {
        $this->queue = $queue;
    }

    /**
     * @param array $events
     */
    public function dispatchAll(array $events)
    {
        foreach ($events as $event) {
            $this->dispatch($event);
        }
    }

    /**
     * @param $event
     */
    public function dispatch($event)
    {
        $this->queue->push(new AsyncEventJob($event));
    }
}