<?php

namespace Ria\Core\Components\CommandBus\Handlers;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

abstract class EntityHandler implements HandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function prepareRelationCollection($classname, array $data)
    {
        return new ArrayCollection(array_map(function ($id) use ($classname) {
            return $this->entityManager->find($classname, $id);
        }, $data));
    }

    protected function find($className, $id) {
        try {
            return $this->entityManager->find($className, $id);
        } catch (DBALException $e) {
            throw new HandlerException($e->getMessage());
        }
    }

    protected function persist($object)
    {
        try {
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            if (!strpos($e->getMessage(), '_photo')) {
                throw new HandlerException($e->getMessage());
            }
        } catch (DBALException $e) {
            throw new HandlerException($e->getMessage());
        }
    }

    protected function remove($object)
    {
        try {
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        } catch (DBALException $e) {
            throw new HandlerException($e->getMessage());
        }
    }
}