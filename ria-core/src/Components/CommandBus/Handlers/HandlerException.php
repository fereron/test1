<?php
declare(strict_types=1);

namespace Ria\Core\Components\CommandBus\Handlers;

use RuntimeException;

/**
 * Class CommandException
 * @package Ria\Core\Command
 */
class HandlerException extends RuntimeException {}