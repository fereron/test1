<?php

namespace Ria\Core\Components\CommandBus;

use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Plugins\LockingMiddleware;
use Ria\Core\Components\CommandBus\Middleware\ExceptionMiddleware;
use Ria\Core\Helpers\ExceptionManager;
use yii\di\Container;

class CommandBusFactory
{

    public static function instantiate(Container $container, array $commandHandlers)
    {
        $handlerMiddleware = new CommandHandlerMiddleware(
            new ClassNameExtractor(),
            new InMemoryLocator($container->get(CommandHandlersAdapter::class)->instantiate($commandHandlers)),
            new ClassNameInflector()
        );

        /** @var ExceptionManager $exceptionManager */
        $exceptionManager = $container->get(ExceptionManager::class);

        return new CommandBus([
            new LockingMiddleware(),
            new ExceptionMiddleware($exceptionManager),
            $handlerMiddleware
        ]);
    }

}