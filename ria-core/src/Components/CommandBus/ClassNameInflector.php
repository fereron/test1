<?php

namespace Ria\Core\Components\CommandBus;

use League\Tactician\Handler\MethodNameInflector\MethodNameInflector;

/**
 * Assumes the method is only the last portion of the class name.
 *
 * Examples:
 *  - \CreateUserCommand => $handler->create()
 */
class ClassNameInflector implements MethodNameInflector
{

    /**
     * {@inheritdoc}
     */
    public function inflect($command, $commandHandler)
    {
        if (method_exists($commandHandler, 'handle')) {
            return 'handle';
        }

        $commandName = $this->getClassName(get_class($command));
        $handlerName = $this->getClassName(get_class($commandHandler));

        $handlerName = $this->removeFromString($handlerName, 'Handler');
        $commandName = $this->removeFromString($commandName, 'Command');
        $commandName = $this->removeFromString($commandName, $handlerName);

        return strtolower($commandName[0]) . substr($commandName, 1);
    }

    /**
     * @param string $className
     * @return string
     */
    private function getClassName(string $className): string
    {
        // If class name has a namespace separator, only take last portion
        if (strpos($className, '\\') !== false) {
            $className = substr($className, strrpos($className, '\\') + 1);
        }

        return $className;
    }

    /**
     * @param string $string
     * @param string $needle
     * @return string
     */
    private function removeFromString(string $string, string $needle): string
    {
        if (strpos($string, $needle) !== false) {
            $string = str_replace($needle, '', $string);
        }

        return $string;
    }
}