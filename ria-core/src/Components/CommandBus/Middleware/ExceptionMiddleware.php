<?php

namespace Ria\Core\Components\CommandBus\Middleware;

use League\Tactician\Middleware;
use Ria\Core\Helpers\ExceptionManager;

class ExceptionMiddleware implements Middleware
{
    /**
     * @var ExceptionManager
     */
    private $exceptionManager;

    public function __construct(ExceptionManager $exceptionManager)
    {
        $this->exceptionManager = $exceptionManager;
    }

    public function execute($command, callable $next)
    {
        $this->exceptionManager->wrap(function () use ($next, $command) {
            $next($command);
        });
    }
}