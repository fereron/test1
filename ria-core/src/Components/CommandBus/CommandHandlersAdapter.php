<?php

namespace Ria\Core\Components\CommandBus;

use yii\di\Container;

class CommandHandlersAdapter
{
    /**
     * @var Container
     */
    private $container;

    /**
     * CommandHandlersAdapter constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function instantiate(array &$commandHandlers)
    {
        foreach ($commandHandlers as $command => $handler) {
            if (!is_object($handler)) {
                $commandHandlers[$command] = $this->container->get($handler);
            }
        }

        return $commandHandlers;
    }

}