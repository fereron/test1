<?php
declare(strict_types=1);

namespace Ria\Core\Components;

use Yii;
use yii\helpers\Url;

/**
 * Class UrlTranslator
 * @package Ria\Core\Components
 */
class UrlTranslator
{

    /**
     * @param array $params
     * @param bool $emptyUrlIfNotExist
     * @return array
     */
    public function getUrls(array $params, bool $emptyUrlIfNotExist): array
    {

        $route        = Yii::$app->controller->route;
        $actionParams = Yii::$app->controller->actionParams;

        $urls = [];

//        dump($params);
//        dump($emptyUrlIfNotExist);

        foreach (Yii::$app->urlManager->languages as $key => $language) {


            $this->changeRoutes($language);
            if (!isset($params[$language]) && $emptyUrlIfNotExist) {
                $urls[$language] = null;
            } else {
                $urls[$language] = $this->makeUrl(
                    $route,
                    array_replace($actionParams, $params[$language] ?? []),
                    $language
                );
            }
        }

        $this->changeRoutes(Yii::$app->language);

        return $urls;
    }

    /**
     * @param string $language
     */
    protected function changeRoutes(string $language)
    {
        Yii::$app->urlManager->rules = [];
        Yii::$app->urlManager->addRules(
            include sprintf("%s/config/routes/routes-%s.php", Yii::getAlias('@app'),
                $language
            ));
    }

    /**
     * @param string $route
     * @param array $params
     * @param string $language
     * @return string
     */
    protected function makeUrl(string $route, array $params, string $language): string
    {
        return Url::toRoute(
            [$route] + $params + ['language' => $language],
            true
        );
    }

}