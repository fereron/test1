<?php

namespace Ria\Core\Widgets\Breadcrumbs;

/**
 * Class Breadcrumbs
 * @package Ria\Core\Widgets\Breadcrumbs
 */
class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    /**
     * @var string
     */
    public $tag = 'ol';
    /**
     * @var string
     */
    public $itemTemplate = "<li class='breadcrumb-item'>{link}</li>\n";
    /**
     * @var string
     */
    public $activeItemTemplate = "<li class='breadcrumb-item active'>{link}</li>\n";

}