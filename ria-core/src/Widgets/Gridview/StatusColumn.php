<?php
declare(strict_types=1);

namespace Ria\Core\Widgets\Gridview;

use yii\grid\Column;
use yii\helpers\Html;

/**
 * Class StatusColumn
 * @package Ria\Core\View\Grid
 */
class StatusColumn extends Column
{
    /**
     * @var string
     */
    public $attribute = 'status';

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index): string
    {
        $value = is_array($model) ? $model[$this->attribute] : $model->{$this->attribute};

        switch ($value) {
            case true:
                $class = 'success';
                $label = 'Active';
                break;
            case false:
                $class = 'danger';
                $label = 'Inactive';
                break;
            default:
                $class = 'default';
                $label = 'Undefined';
                break;
        }

        return Html::tag('label', $label, ['class' => 'badge badge-lg badge-' . $class]);
    }

    /**
     * @return string
     */
    protected function renderFilterCellContent(): string
    {
        return Html::activeDropDownList($this->grid->filterModel, $this->attribute, ['Inactive', 'Active'],
            ['class' => 'form-control', 'prompt' => ' - ']);
    }
}