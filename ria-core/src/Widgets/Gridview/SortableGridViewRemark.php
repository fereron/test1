<?php

namespace Ria\Core\Widgets\Gridview;

use sjaakp\sortable\SortableGridView;

/**
 * Class SortableGridViewRemark
 * @package Ria\Core\Widgets\Gridview
 */
class SortableGridViewRemark extends SortableGridView
{

    /**
     * @var array
     */
    public $options = ['class' => 'gridview table-responsive'];
    /**
     * @var array
     */
    public $tableOptions = ['class' => 'table table-striped'];

}