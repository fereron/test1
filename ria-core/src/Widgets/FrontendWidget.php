<?php
declare(strict_types=1);

namespace Ria\Core\Widgets;

use Ria\Core\Query\BaseProvider;
use InvalidArgumentException;
use ReflectionClass;
use RuntimeException;
use yii\base\Widget;

/**
 * Class FrontendWidget
 * @package Ria\Core\Widgets
 */
class FrontendWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'index';

    /**
     * @var array|null
     */
    public $provider = null;
    /**
     * @var array
     */
    public $data = [];

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        return $this->render($this->template, $this->provider
            ? $this->getProvider()->getData()
            : $this->data
        );
    }

    /**
     * @return BaseProvider|object
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function getProvider()
    {
        if (empty($this->provider['class'])) {
            throw new InvalidArgumentException('Provider class must be specified.');
        }

        $reflection = new ReflectionClass($this->provider['class']);
        if (!$reflection->isSubclassOf(BaseProvider::class)) {
            throw new RuntimeException('Provider class must be instance of ProviderInterface.');
        }

        $options = isset($this->provider['options']) ? $this->provider['options'] : [];

        return \Yii::$container->get($this->provider['class'], [], $options);
    }

    /**
     * Returns the directory containing the view files for this widget.
     * The default implementation returns the 'views' subdirectory under the directory containing the widget class file.
     * @return string the directory containing the view files for this widget.
     */
    public function getViewPath()
    {
        $class = new ReflectionClass($this);

        return dirname($class->getFileName()) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . \Yii::$app->params['theme'];
    }
}