<?php

namespace Ria\Core\Helpers;

use yii\helpers\StringHelper as BaseStringHelper;

class StringHelper extends BaseStringHelper
{
    /**
     * Convert a string to snake case.
     *
     * @param string $value
     * @param string $delimiter
     * @return string
     */
    public static function snake($value, $delimiter = '-')
    {
        if (!ctype_lower($value)) {
            $value = preg_replace('/\s+/u', '', ucwords($value));

            $value = strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1' . $delimiter, $value));
        }

        return $value;
    }

    /**
     * Convert a value to studly caps case.
     *
     * Example: group_id => GroupId
     *
     * @param string $value
     * @return string
     */
    public static function studly($value)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));

        return str_replace(' ', '', $value);
    }


    /**
     * @param string $string
     * @return string|null
     */
    public static function getFirstParagraph(string $string): ?string
    {
        if (mb_stripos($string, '</p>', 0, 'utf-8') === false) {
            return null;
        }

        $string = mb_substr($string, 0, mb_stripos($string, '</p>', 0, 'utf-8') + 4, 'utf-8');
        return str_replace(['&nbsp;', 'nbsp;', '&amp;'], [" ", " ", ""], strip_tags($string));
    }

    /**
     * @param string $string
     * @return string
     */
    public static function clearEmptyParagraphs(string $string): string
    {
        return preg_replace("/<p[^>]*>(\s|&nbsp;)*<\/p>/u", '', $string);
    }

    /**
     * @param string $string
     * @param string $replaceTo
     * @return string
     */
    public static function clearEmptyTags(string $string, string $replaceTo = ''): string
    {
        //** Return if string not given or empty.
        if (!is_string($string)
            || trim($string) == '')
            return $string;

        //** Recursive empty HTML tags.
        return preg_replace(

        //** Pattern written by Junaid Atari.
            '/<([^<\/>]*)>([\s]*?|(?R))<\/\1>/imsU',

            //** Replace with nothing if string empty.
            $replaceTo,

            //** Source string
            $string
        );
    }

    /**
     * @param string $string
     * @return string
     */
    public static function clearDoubleQuotes(string $string): string
    {
        return str_replace('"', '', strip_tags($string));
    }

    /**
     * @param string $youtubeLink
     * @return string|null
     */
    public static function getYouTubeHash(string $youtubeLink): ?string
    {
        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $youtubeLink, $matches);
        return $matches[1] ?? null;
    }

}