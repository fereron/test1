<?php

namespace Ria\Core\Helpers;

use Ria\Core\Models\Status;
use Ria\News\Module;
use yii\helpers\Html;

/**
 * Class StatusHelper
 * @package Ria\Core\Helpers
 */
class StatusHelper
{
    /**
     * @param Status $status
     * @return string
     */
    public static function statusLabel(Status $status): string
    {
        switch ($status->getStatus()) {
            case Status::ACTIVE:
                $badgeClass = 'success';
                break;
            case Status::INACTIVE:
                $badgeClass = 'danger';
                break;
            default:
                $badgeClass = 'default';
        }

        return Html::tag('span', $status->getLabel(), ['class' => 'badge badge-' . $badgeClass]);
    }

    /**
     * @param string $status
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function postStatusLabel(string $status): string
    {
        switch ($status) {
            case 'read':
                $badgeClass = 'success';
                break;
            case 'created':
            case 'on_moderation':
            case 'waiting_for_correction':
                $badgeClass = 'info';
                break;
            case 'private':
                $badgeClass = 'warning';
                break;
            case 'deleted':
            case 'archived':
                $badgeClass = 'danger';
                break;
            default:
                $badgeClass = 'default';
        }

        return Html::tag('span',
            Module::t('news', $status),
            ['class' => 'badge badge-outline badge-' . $badgeClass]
        );
    }
}