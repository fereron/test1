<?php
declare(strict_types=1);

namespace Ria\Core\Helpers\Converter;

use ReflectionClass;
use ReflectionException;

/**
 * Trait JsonConvertibleTrait
 * @package Ria\Core\Helpers\Converter
 */
trait JsonConvertibleTrait
{
    /**
     * @param string $struct
     * @return ConvertibleInterface
     */
    public static function create(string $struct): ConvertibleInterface
    {
        $arguments  = json_decode($struct, true);
        $class      = new ReflectionClass(__CLASS__);
        $object     = $class->newInstanceWithoutConstructor();
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            if (array_key_exists($property->getName(), $arguments)) {
                $property->setAccessible(true);
                $property->setValue($object, $arguments[$property->getName()]);
            }
        }
        /** @var ConvertibleInterface $object */
        return $object;
    }

    /**
     * @return string
     * @throws ReflectionException
     */
    public function encode(): string
    {
        $struct = [];

        $class      = new ReflectionClass($this);
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $struct[$property->getName()] = $property->getValue($this);
        }

        return json_encode($struct);
    }
}