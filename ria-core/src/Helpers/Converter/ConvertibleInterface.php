<?php
declare(strict_types=1);

namespace Ria\Core\Helpers\Converter;

/**
 * Interface ConvertibleInterface
 * @package Ria\Core\Helpers\Converter
 */
interface ConvertibleInterface
{
    /**
     * @param string $struct
     * @return static
     */
    public static function create(string $struct): ConvertibleInterface;

    /**
     * @return string
     */
    public function encode(): string;
}