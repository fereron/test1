<?php

namespace Ria\Core\Helpers;

use Exception;
use Yii;

class ExceptionManager
{
    /**
     * @param callable $function
     * @return mixed
     * @throws Exception
     */
    public function wrap(callable $function)
    {
        try {
            return $function();
        } catch (Exception $e) {
            if (YII_ENV == 'prod') {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', Yii::t('error', 'internal_server_error'));
            }

            throw $e;
        }
    }
}