<?php
declare(strict_types=1);

namespace Ria\Core\Helpers;

/**
 * Class FlagIconHelper
 * @package Ria\Core\Helpers
 */
class FlagIconHelper
{
    private const LOCALE_ICON = [
        'en' => 'us'
    ];

    /**
     * @param string $locale
     * @return string
     */
    public static function getIcon(string $locale): string
    {
        return array_key_exists($locale, self::LOCALE_ICON)
            ? self::LOCALE_ICON[$locale]
            : $locale;
    }

}