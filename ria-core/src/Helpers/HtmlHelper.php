<?php
declare(strict_types=1);

namespace Ria\Core\Helpers;

use phpQuery;
use Yii;
use yii\helpers\Html;

/**
 * Class HtmlHelper
 * @package Ria\Core\Helpers
 */
final class HtmlHelper
{

    /**
     * @param string $string
     * @return string
     */
    public static function clearEmptyParagraphs(string $string): string
    {
        return preg_replace("/<p[^>]*>(\s|&nbsp;)*<\/p>/u", '', $string);
    }

    /**
     * @param string $html
     * @param array $options
     * @return string
     */
    public static function replaceImgToAmpImg(string $html, array $options = []): string
    {
        $html = preg_replace(
            '/(<img[^>]+>(?:<\/img>)?)/i',
            '$1</amp-img>',
            $html
        );
        return str_replace('<img', '<amp-img ' . Html::renderTagAttributes($options), $html);
    }

    /**
     * @param string $html
     * @param array $options
     * @return string
     */
    public static function replaceIframeToAmpIframe(string $html, array $options = []): string
    {
        return preg_replace(
            '/<iframe.*?src=["|\']([^"\']*)["|\'].*>.*[\n\r]?[<\/iframe>]?/',
            '<figure>
                        <amp-iframe src="$1" ' . Html::renderTagAttributes($options) . '>
                        </amp-iframe>
                    </figure>',
            $html
        );
    }

    /**
     * @param string $html
     * @return string
     */
    public static function removeStyleAttributes(string $html): string
    {
        $patterns = [
            '/(<[^>]+) style=".*?"/i',
            "/(<[^>]+) style='.*?'/i"
        ];
        foreach ($patterns as $pattern) {
            $html = preg_replace($pattern, '$1', $html);
        }
        return $html;
    }

    /**
     * @param string $content
     * @param string $hostInfo
     * @return string
     */
    public static function addNoindexToExternalLinks(string $content, string $hostInfo): string
    {
        $dom = phpQuery::newDocumentHTML($content);

        $links = $dom->find("a:not([href^='{$hostInfo}'])");
        foreach ($links as $link) {
            $link = pq($link);

            $link->attr('target', '_blank')->wrap('<noindex>');
        }

        return (string)$dom;
    }

    /**
     * @param string $html
     * @return string
     */
    public static function removeIframesWithHttpSource(string $html): string
    {
        return preg_replace('/(<iframe.*?src="http:\/\/.*".*>.*[\n\r]?<\/iframe>)/', '', $html);
    }

    /**
     * @param int $postId
     * @param string $language
     * @return string
     */
    public static function getCounterImage(int $postId, string $language): string
    {
        if (YII_ENV_DEV) {
            return '';
        }
        return Html::img(
            sprintf('%s/images/views.png?', Yii::$app->request->getHostInfo()) .
            http_build_query([
                'id'   => $postId,
                'lang' => $language,
                'x'    => microtime(true)
            ]),
            ['width' => 1, 'height' => 1, 'hidden' => 'hidden']
        );
    }

    /**
     * @param int $postId
     * @param string $language
     * @return string
     */
    public static function getCounterAmpImage(int $postId, string $language): string
    {
        if (YII_ENV_DEV) {
            return '';
        }
        return Html::tag('amp-img', '', [
            'src'    => sprintf('%s/images/views.png?', Yii::$app->request->getHostInfo()) .
                http_build_query([
                    'id'   => $postId,
                    'lang' => $language,
                    'x'    => microtime(true)
                ]),
            'width'  => 1,
            'height' => 1
        ]);
    }

}