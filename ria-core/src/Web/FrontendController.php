<?php
declare(strict_types=1);

namespace Ria\Core\Web;

use Doctrine\ORM\EntityManagerInterface;
use Ria\Core\Components\UrlTranslator;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class FrontendController
 * @package Ria\Core\Web
 *
 * @property string $language
 */
class FrontendController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;
    /**
     * @var array
     */
    protected $translatedUrls = [];
    /**
     * @var string
     */
    private $_language;

    /**
     * FrontendController constructor.
     * @param $id
     * @param $module
     * @param EntityManagerInterface $entityManager
     * @param array $config
     */
    public function __construct($id, $module, EntityManagerInterface $entityManager, $config = [])
    {
        $this->entityManager = $entityManager;

        $this->_language = Yii::$app->language;

        $this->view->params['renderAnalyticCodes'] = true;

        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @throws NotFoundHttpException
     */
    public function throwPageNotFound()
    {
        throw new NotFoundHttpException(Yii::t('common', 'page_not_found'));
    }

    /**
     * Translate urls
     * @param array $params
     * @param bool $emptyUrlsIfNotExist
     */
    public function translateUrls(array $params = [], bool $emptyUrlsIfNotExist = false)
    {
        $this->translatedUrls = (new UrlTranslator())->getUrls($params, $emptyUrlsIfNotExist);
    }

    /**
     * @param array $tags
     */
    public function registerOgTags(array $tags)
    {
        foreach ($tags as $param => $value) {
            if ($value !== false) {
                $this->view->registerMetaTag(['property' => 'og:' . $param, 'content' => $value]);
            }
        }
    }

    /**
     * @param int|null $page
     * @param array|null $route
     * @param string $pageParam
     */
    public function registerPaginationTags(?int $page = null, ?array $route = null, string $pageParam = 'page')
    {
        if (empty($route)) {
            $route       = [Yii::$app->controller->getRoute()];
            $routeParams = Yii::$app->controller->actionParams;
        } else {
            $route       = $route[0];
            $routeParams = $route[1];
        }

        if (empty($page)) {
            $page = Yii::$app->request->get($pageParam);
        }

        unset($routeParams[$pageParam]);
        if (!empty($page) && $page >= 1) {
            $this->view->registerLinkTag([
                'rel'  => 'canonical',
                'href' => Yii::$app->urlManager->createAbsoluteUrl(ArrayHelper::merge($route, $routeParams))
            ]);
        }

        $page = (int)$page;
        if (!$page) {
            $page = 1;
        }

        $prevPage = $page - 1;
        $nextPage = $page + 1;

        if ($prevPage > 0) {
            if ($prevPage > 1) {
                $routeParams = ArrayHelper::merge($routeParams, [$pageParam => $prevPage]);
            }

            $this->view->registerLinkTag([
                'rel'  => 'prev',
                'href' => Yii::$app->urlManager->createAbsoluteUrl(ArrayHelper::merge($route, $routeParams))
            ]);
        }

        $this->view->registerLinkTag([
            'rel'  => 'next',
            'href' => Yii::$app->urlManager->createAbsoluteUrl(ArrayHelper::merge($route, ArrayHelper::merge($routeParams, [$pageParam => $nextPage])))
        ]);
    }

    /**
     * @return array
     */
    public function getTranslatedUrls(): array
    {
        return $this->translatedUrls;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->_language;
    }

}