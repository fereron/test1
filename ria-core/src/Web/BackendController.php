<?php

declare(strict_types=1);

namespace Ria\Core\Web;

use Doctrine\ORM\EntityManagerInterface;
use League\Tactician\CommandBus;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class BackendController
 * @package Ria\Core\Web
 */
class BackendController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * @return array|array[]
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * BackendController constructor.
     * @param $id
     * @param $module
     * @param EntityManagerInterface $entityManager
     * @param CommandBus $bus
     * @param array $config
     */
    public function __construct($id, $module, EntityManagerInterface $entityManager, CommandBus $bus, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->entityManager = $entityManager;
        $this->bus = $bus;
    }

    /**
     * @param string $className
     * @param mixed $condition
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel(string $className, $condition)
    {
        if (!is_array($condition)) {
            $condition = ['id' => $condition];
        }
        if (($model = $this->entityManager->getRepository($className)->findOneBy($condition)) == null) {
            throw new NotFoundHttpException('Page not found');
        }

        return $model;
    }
}