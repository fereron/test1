<?php
declare(strict_types=1);

namespace Ria\Core\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Status
 * @ORM\Embeddable()
 */
class Status extends ObjectValue
{
    const ACTIVE   = 1;
    const INACTIVE = 0;
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $status;

    /**
     * Status constructor.
     * @param int $status
     */
    public function __construct(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public static function list(): array
    {
        return [
            self::ACTIVE   => 'Active',
            self::INACTIVE => 'Inactive'
        ];
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return self::list()[$this->status];
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == self::ACTIVE;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    public function __toString()
    {
        return (string) $this->status;
    }
}