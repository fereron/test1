<?php

namespace Ria\Core\Models;

use Ria\Core\Helpers\StringHelper;
use yii\base\UnknownPropertyException;

class Entity
{
    private $events = array();

    public function popEvents()
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }

    protected function raise($event)
    {
        $this->events[] = $event;
    }

    /**
     * @param $property
     * @return mixed
     * @throws UnknownPropertyException
     */
    public function __get($property)
    {
        $getter = 'get' . StringHelper::studly($property);

        if (!method_exists($this, $getter)) {
            throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $property);
        }
        return $this->$getter();
    }

}