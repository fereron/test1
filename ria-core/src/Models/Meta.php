<?php
declare(strict_types=1);

namespace Ria\Core\Models;

use Ria\Core\Helpers\Converter\ConvertibleInterface;
use Ria\Core\Helpers\Converter\JsonConvertibleTrait;

/**
 * Class Meta
 * @package Ria\News\Models
 */
class Meta implements ConvertibleInterface
{
    use JsonConvertibleTrait;

    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $keywords;

    /**
     * Meta constructor.
     * @param $title
     * @param $description
     * @param $keywords
     */
    public function __construct(string $title, string $description, string $keywords)
    {
        $this->title       = $title;
        $this->description = $description;
        $this->keywords    = $keywords;
    }

    public static function fromArray(array $meta)
    {
        return new self($meta['title'], $meta['description'], $meta['keywords']);
    }

    public static function fromJson(string $json)
    {
        return self::fromArray(json_decode($json, true));
    }

    public static function fromObject(object $object)
    {
        return new self($object->title, $object->description, $object->keywords);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }

    public function __toString()
    {
        return $this->encode();
    }
}