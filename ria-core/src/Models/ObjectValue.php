<?php
declare(strict_types=1);

namespace Ria\Core\Models;

/**
 * Class ObjectValue
 * @package Ria\Core\Models
 */
abstract class ObjectValue
{
    /**
     * @return mixed
     */
    abstract public function __toString();
}