<?php

namespace Ria\Core\Models;

interface TagCache
{
    public function getCacheTags(): array;
}