<?php
declare(strict_types=1);

namespace Ria\Core\Models;

use Doctrine\Common\Collections\ArrayCollection;

trait RelationsTrait
{
    /**
     * @param ArrayCollection $items
     * @param string $relation
     * @return $this
     */
    public function sync(string $relation, ArrayCollection $items)
    {
        // Remove unused relation
        foreach ($this->getForRemoval($relation, $items) as $item) {
            $this->{$relation}->removeElement($item);
        }

        // Add new relations
        foreach ($items as $item) {
            if (!$this->{$relation}->contains($item)) {
                $this->{$relation}->add($item);
            }
        }

        return $this;
    }

    /**
     * @param string $relation
     * @param ArrayCollection $items
     * @return array
     */
    public function getForRemoval(string $relation, ArrayCollection $items)
    {
        /** @var ArrayCollection $selfIds */
        $selfIds = $this->{$relation}->map(function ($relation) {
            return $relation->getId();
        });
        if ($selfIds->isEmpty()) {
            return [];
        }
        $alienIds  = $items->map(function ($relation) {
            return $relation->getId();
        });
        $unusedIds = array_diff($selfIds->toArray(), $alienIds->toArray());

        return $this->{$relation}->filter(function ($relation) use ($unusedIds) {
            return in_array($relation->getId(), $unusedIds);
        });
    }
}
