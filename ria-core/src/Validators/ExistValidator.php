<?php
declare(strict_types=1);

namespace Ria\Core\Validators;

use Closure;
use Doctrine\ORM\EntityManagerInterface;
use Yii;
use yii\base\Model;
use yii\validators\Validator;

/**
 * Class ExistValidator
 * @package Ria\Core\Validators
 */
class ExistValidator extends Validator
{
    /**
     * @var string
     */
    public $targetClass;
    /**
     * @var string
     */
    public $targetAttribute;
    /**
     * @var string|array|Closure additional filter to be applied to the DB query used to check the existence of the attribute value.
     * This can be a string or an array representing the additional query condition (refer to [[\yii\db\Query::where()]]
     * on the format of query condition), or an anonymous function with the signature `function ($query)`, where `$query`
     * is the [[\yii\db\Query|Query]] object that you can modify in the function.
     */
    public $filter;
    /**
     * @var string
     */
    public $message;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, $config = [])
    {
        parent::__construct($config);
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if ($this->message !== null) {
            return;
        }

        $this->message = Yii::t('yii', '{attribute} does not exist.');
    }

    /**
     * @param Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $alias = substr($this->targetAttribute, 0, 1);
        $targetAttribute = $this->targetAttribute ? $alias . '.' . $this->targetAttribute :  $alias . '.' . $attribute;

        $query = $this->em->getRepository($this->targetClass)
            ->createQueryBuilder($alias)
            ->where("{$targetAttribute} = ?1")
            ->setParameter('1', $model->$attribute)
            ->select("count({$targetAttribute})");

        if ($this->filter instanceof Closure) {
            call_user_func($this->filter, $query);
        } elseif ($this->filter !== null) {
            $query->andWhere($this->filter);
        }

        if ($query->getQuery()->getSingleScalarResult() == 0) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}