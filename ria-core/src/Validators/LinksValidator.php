<?php
declare(strict_types=1);

namespace Ria\Core\Validators;

use Ria\News\Module;
use yii\validators\Validator;

/**
 * Class LinksValidator
 * @package Ria\Core\Validators
 */
class LinksValidator extends Validator
{

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @throws \yii\base\InvalidConfigException
     */
    public function validateAttribute($model, $attribute): void
    {
        preg_match_all('/<a href=\\"([^\\"]*)\\">(.*)<\\/a>/iU', $model->{$attribute}, $matches);

        $notValidUrls = [];
        foreach ($matches[1] as $i => $link) {
            if (!$this->isValidDomain($link)) {
                $notValidUrls[] = $matches[2][$i];
            }
        }

        if (!empty($notValidUrls)) {
            $message = sprintf("%s <br> <b>%s</b>",
                Module::t('news', 'Check the correctness of the following links:'),
                implode("<br>", $notValidUrls)
            );
            $this->addError($model, $attribute, $message);
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public function isValidDomain(string $url): bool
    {
        if (preg_match('/mailto:[^\?]*/', $url)) {
            return true;
        }

        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            return false;
        }

        $domain = parse_url($url)['host'] ?? null;

        if (is_null($domain)) {
            return true;
        }

        return (preg_match("/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/i", $domain) //valid chars check
            && preg_match("/^.{1,253}$/", $domain) //overall length check
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain)); //length of each label
    }

}