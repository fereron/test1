<?php

namespace Helpers;

use PHPUnit\Framework\TestCase;
use ReflectionException;
use Ria\Core\Helpers\Converter\ConvertibleInterface;
use Ria\Core\Helpers\Converter\JsonConvertibleTrait;

/**
 * Class TestJsonConverter
 * @package Helpers
 */
class TestJsonConverter extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function testSetupingObjectFromJson()
    {
        $jsonStruct = json_encode([
            'prop1' => 'Value1',
            'prop2' => 'Value2'
        ]);

        $mock = MockConverter::create($jsonStruct);
        self::assertEquals('Value1', $mock->getProp1());
        self::assertEquals('Value2', $mock->getProp2());

        return $mock;
    }

    /**
     * @depends testSetupingObjectFromJson
     * @param MockConverter $mock
     * @throws ReflectionException
     */
    public function testEncodingObjectToStruct(MockConverter $mock)
    {
        $jsonStruct = json_encode([
            'prop1' => 'Value1',
            'prop2' => 'Value2'
        ]);

        self::assertEquals($jsonStruct, $mock->encode());
    }
}

/**
 * Class MockConverter
 * @package Helpers
 */
class MockConverter implements ConvertibleInterface
{
    use JsonConvertibleTrait;

    private $prop1;
    private $prop2;

    /**
     * @return mixed
     */
    public function getProp2()
    {
        return $this->prop2;
    }

    /**
     * @return mixed
     */
    public function getProp1()
    {
        return $this->prop1;
    }
}